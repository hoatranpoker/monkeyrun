﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class UGUIMenu : MonoBehaviour
{
    [MenuItem("Editor/Anchor Around Object %t")]
    static void uGUIAnchorAroundObject()
    {
        var o = Selection.activeGameObject;
        if (o != null && o.GetComponent<RectTransform>() != null)
        {
            var r = o.GetComponent<RectTransform>();
            var p = o.transform.parent.GetComponent<RectTransform>();

            var offsetMin = r.offsetMin;
            var offsetMax = r.offsetMax;
            var _anchorMin = r.anchorMin;
            var _anchorMax = r.anchorMax;

            var parent_width = p.rect.width;
            var parent_height = p.rect.height;

            var anchorMin = new Vector2(_anchorMin.x + (offsetMin.x / parent_width),
                                        _anchorMin.y + (offsetMin.y / parent_height));
            var anchorMax = new Vector2(_anchorMax.x + (offsetMax.x / parent_width),
                                        _anchorMax.y + (offsetMax.y / parent_height));

            r.anchorMin = anchorMin;
            r.anchorMax = anchorMax;

            r.offsetMin = new Vector2(0, 0);
            r.offsetMax = new Vector2(1, 1);
            r.pivot = new Vector2(0.5f, 0.5f);

        }
    }

    [MenuItem("Editor/Copy Collider %i")]
    static void CopyCollider()
    {
        var o = Selection.activeGameObject;
        if (o != null)
        {
            var b = o.GetComponent<BoxCollider2D>();
            var c = o.GetComponent<CapsuleCollider2D>();
            c.size = b.size;
        }
    }


    [MenuItem("Editor/Ground Resize %g")]
    static void ResizeSprite()
    {
        var o = Selection.activeGameObject;
        if (o != null)
        {
            var rs = o.GetComponentsInChildren<SpriteRenderer>();
            float totallenght = 0;
            float lenght = 0;
            SpriteRenderer center = null;
            foreach (var i in rs)
            {
                if (i.name == "center" || i.name == "left" || i.name == "right")
                {
                    if (i.name == "center")
                    {
                        lenght = i.sprite.texture.width / i.sprite.pixelsPerUnit;
                        center = i;
                    }
                    totallenght += i.sprite.texture.width / i.sprite.pixelsPerUnit;
                }
            }
            float s = o.transform.localScale.x * totallenght;
            o.transform.localScale = new Vector3(o.transform.localScale.y, o.transform.localScale.y, o.transform.localScale.z);

            float clenght = 0;
            foreach (var i in rs)
            {
                switch (i.name)
                {
                    case "center":
                        i.size = new Vector2(s - (totallenght - lenght), i.size.y);
                        clenght += i.size.x;
                        break;

                    case "left":
                        i.transform.localPosition = new Vector3((s - (totallenght - lenght) + i.sprite.texture.width / i.sprite.pixelsPerUnit) / -2, i.transform.localPosition.y, i.transform.localPosition.z);
                        clenght += i.size.x;
                        break;

                    case "right":
                        i.transform.localPosition = new Vector3((s - (totallenght - lenght) + i.sprite.texture.width / i.sprite.pixelsPerUnit) / 2, i.transform.localPosition.y, i.transform.localPosition.z);
                        clenght += i.size.x;
                        break;

                    case "lcenter":
                        i.size = new Vector2(s - (totallenght - lenght), i.size.y);
                        break;

                    case "lleft":
                        i.transform.localPosition = new Vector3((s - (totallenght - lenght) + i.sprite.texture.width / i.sprite.pixelsPerUnit) / -2, i.transform.localPosition.y, i.transform.localPosition.z);
                        break;

                    case "lright":
                        i.transform.localPosition = new Vector3((s - (totallenght - lenght) + i.sprite.texture.width / i.sprite.pixelsPerUnit) / 2, i.transform.localPosition.y, i.transform.localPosition.z);
                        break;
                }
            }
            var c = o.GetComponent<CapsuleCollider2D>();
            if (c != null)
                c.size = new Vector2(clenght, c.size.y);
        }
    }

    [MenuItem("Editor/Clear Data")]
    static void ClearData()
    {
        PlayerPrefs.DeleteAll();
    }
}