#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("n4XW8wb2Sy0un0Ed5OrriVVvE7b/kZFuEki0wbIDJesZ5bkm4ubgDPZR0IQzYhX4LYHR1uKteIwRBIUVzed2DVj8Gouf8xCtUsx0c/Dhqzw/jQ4tPwIJBiWJR4n4Ag4ODgoPDHi759QGffVCilvv8G+d/FGJd83vYf4O7TUhVaAMa9Nlrc0aEPXIkNn9Wd9N50WmkZv2nZ+0JB7GQEKgO0YVZ6xgxdqujPf0IqpsE2AtzGWbb4g8RKAOiT49+owHTGCXpfO3c0j79NUfPjcb6EZYy5mThyCZ/pCA5bKF7wUQgMxpJpcy4IjOco3kYbidjQ4ADz+NDgUNjQ4OD4wJ8H9isA1pAFNl6z4OxBsNZ0k3KMpZbGkMzg8RP8nap7ImSg0MDg8O");
        private static int[] order = new int[] { 11,3,13,6,11,10,11,10,13,10,12,13,13,13,14 };
        private static int key = 15;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
