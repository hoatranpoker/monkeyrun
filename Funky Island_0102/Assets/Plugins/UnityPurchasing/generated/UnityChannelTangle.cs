#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class UnityChannelTangle
    {
        private static byte[] data = System.Convert.FromBase64String("+jllVoT/d8AI2W1y7R9+0wv1T20wB22HkgJO66QVsGIKTPAPZuM6Hx0HVHGEdMmvrB3Dn2ZoaQvX7ZE0f9tdz2XHJBMZdB8dNqacRMLAIrnEl+Uu4kdYLA51dqAo7pHir07nGeuC0edpvIxGmY/ly7WqSNvu645MT2X0j9p+mAkdcZIv0E728XJjKb69D4yvvYCLhKcLxQt6gIyMjIiNjn0TE+yQyjZDMIGnaZtnO6RgZGKO7Qq+xiKMC7y/eA6FzuIVJ3E18cp5dledvLWZasTaSRsRBaIbfBICZ3TTUgax4Jd6rwNTVGAv+g6ThgeXD4yCjb0PjIePD4yMjQ6Lcv3gMo/jfIxvt6PXIo7pUecvT5iSd0oSW42TvUtYJTCkyI+OjI2M");
        private static int[] order = new int[] { 10,8,11,5,11,9,6,10,9,10,12,13,12,13,14 };
        private static int key = 141;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
