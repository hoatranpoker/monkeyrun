#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("qSdtqPHmXbNb6M8ym12AFOH641oBrQsl9JKknHG5qwD7KujVfv02ocLf0N/V18LTltTPltfYz5bG18TCCELFLVhk0rl9z/mCbhSIT85J3X7E19XC39XTlsXC18LT29PYwsWYhv9uwCmFotMXwSJ/m7S1t7a3FTS32tOW/9jVmIeQhpKwteOyvaWr98bG2tOW9dPEwt/Q39XXwt/Z2Jb3w8ba05bk2dnClvX3hqihu4aAhoKEhqewteOyvKW898bG2tOW/9jVmIcjKMy6EvE97WKggYV9crn7eKLfZzS3trC/nDD+MEHV0rO3hjdEhpywvp2wt7OzsbS3oKjewsLGxYyZmcGpMzUzrS+L8YFEHy32OJpiByakbnbVhcFBjLGa4F1suZe4bAzFr/kDs7a1NLe5toY0t7y0NLe3tlInH7+wteOruLKgsqKdZt/xIsC/SELdOwOMG0K5uLYkvQeXoJjCY4q7bdSgmPYQQfH7yb7ohqmwteOrlbKuhqDSg5Wj/aPvqwUiQUAqKHnmDHfu5jnFN9Zwre2/mSQETvL+RtaOKKNDg4SHgoaFgOyhu4WDhoSGj4SHgoaZhjd1sL6dsLezs7G0tIY3AKw3BbCGubC146ult7dJsrOGtbe3SYarmpbV08TC39Df1dfC05bG2drf1c89rz9oT/3aQ7EdlIa0Xq6ITua/Ze8Rs7/Kofbgp6jCZQE9lY3xFWPZklRdZwHGabnzV5F8R9vOW1EDoaGW19jSltXTxMLf0N/V18Lf2diWxgeG7lrssoQ63gU5q2jTxUnR6NMKsrCltOPlh6WGp7C147K8pbz3xsaghqKwteOytaW798bG2tOW5NnZwsHBmNfGxtrTmNXZ25nXxsba09XXltnQlsLe05bC3tPYltfGxtrf1dfU2tOWxcLX2NLXxNKWwtPE28WW18n3Hi5PZ3zQKpLdp2YVDVKtnHWpgC/6m84BWzotakXBLUTAZMGG+Xe5K4tFnf+erH5IeAMPuG/oqmB9i4uQ0ZY8hdxBuzR5aF0VmU/l3O3SzIY0t8CGuLC146u5t7dJsrK1tLfC3tnE38LPh6CGorC147K1pbv3xm+AyXcx428RLw+E9E1uY8coyBfkHRXHJPHl43cZmfcFTk1VxntQFfq7sL+cMP4wQbu3t7OztrU0t7e26rFay481PeWWZY5yBwks+bzdSZ1K88ip+t3mIPc/csLUvaY19zGFPDc2op1m3/EiwL9IQt07mPYQQfH7yZCGkrC147K9pav3xsba05b108TCvuiGNLensLXjq5ayNLe+hjS3sobROb4ClkF9GpqW2cYAibeGOgH1ed/Q39XXwt/Z2Jb3w8Le2cTfws+Hz5bXxcXD29PFltfV1dPGwtfY1dOW9feGNLeUhruwv5ww/jBBu7e3t4WA7IbUh72Gv7C147KwpbTj5Yel5NPa39fY1dOW2diWwt7fxZbV08R/r8RD67hjyektRJO1DOM5++u7R5ww/jBBu7e3s7O2htSHvYa/sLXj2NKW1dnY0t/C39nYxZbZ0JbDxdOGNLINhjS1FRa1tLe0tLe0hruwvx5qyJSDfJNjb7lg3WIUkpWnQRca5hw8Y2xSSma/sYEGw8OX");
        private static int[] order = new int[] { 15,59,39,50,46,27,50,46,50,9,18,34,35,49,57,57,30,23,31,20,55,46,37,29,44,30,52,53,45,52,39,35,36,41,42,52,48,37,56,45,53,58,44,47,51,45,48,52,58,58,51,53,58,53,56,55,59,58,58,59,60 };
        private static int key = 182;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
