﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class DailyrewardController : MonoBehaviour {

    [Header("Button daily")]
    public DailyButton[] buttonDaily;

    public UIButton btnExit;

	private void Start () {
        btnExit.SetUpEvent(ExitAction);
        CheckDaily();
	}

    private int NumberReward()
    {
        return PlayerPrefs.GetInt(SaveID.NumberReward);
    }

    private void CheckDaily()
    {
        if (PlayerPrefs.GetString(SaveID.ClearDaily) == SaveID.True || PlayerPrefs.GetString(SaveID.Day_7) == SaveID.True)
        {
            DataManager.Instance.ClearDaily();
            PlayerPrefs.SetString(SaveID.Reward, SaveID.False);
        }
        if (PlayerPrefs.GetString(SaveID.Reward)==SaveID.True)
        { 
            buttonDaily[NumberReward()].Canreward = true;
            buttonDaily[NumberReward()].imgNotif.SetActive(true);
        }

        for (int i = 0; i < buttonDaily.Length; i++)
        {
            buttonDaily[i].CallStart();
        }
    }

    private void ExitAction()
    {
        SoundController.Instance.PlayFX();
        PageMenu.Instance.CheckDailyReward();
        gameObject.SetActive(false);
    }

    
}

public enum DAILY_REWARD { None,Day_1,Day_2,Day_3, Day_4, Day_5, Day_6, Day_7 }