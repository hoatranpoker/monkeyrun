﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class DailyButton : MonoBehaviour {

    public DAILY_REWARD type;
    public GameObject bgRewarded;
    public GameObject imgNotif;
    public bool Canreward = false;

    public void CallStart()
    {
        CheckReward();
    }
    private void CheckReward()
    {
        string str = "fasle";
        switch (type)
        {
            case DAILY_REWARD.Day_1:
                if(PlayerPrefs.GetString(SaveID.Day_1)!=null)
                    str = PlayerPrefs.GetString(SaveID.Day_1);
                break;
            case DAILY_REWARD.Day_2:
                if (PlayerPrefs.GetString(SaveID.Day_2) != null)
                    str = PlayerPrefs.GetString(SaveID.Day_2);
                break;
            case DAILY_REWARD.Day_3:
                if (PlayerPrefs.GetString(SaveID.Day_3) != null)
                    str = PlayerPrefs.GetString(SaveID.Day_3);
                break;
            case DAILY_REWARD.Day_4:
                if (PlayerPrefs.GetString(SaveID.Day_4) != null)
                    str = PlayerPrefs.GetString(SaveID.Day_4);
                break;
            case DAILY_REWARD.Day_5:
                if (PlayerPrefs.GetString(SaveID.Day_5) != null)
                    str = PlayerPrefs.GetString(SaveID.Day_5);
                break;
            case DAILY_REWARD.Day_6:
                if (PlayerPrefs.GetString(SaveID.Day_6) != null)
                    str = PlayerPrefs.GetString(SaveID.Day_6);
                break;
            case DAILY_REWARD.Day_7:
                if (PlayerPrefs.GetString(SaveID.Day_7) != null)
                    str = PlayerPrefs.GetString(SaveID.Day_7);
                break;
        }
        if (str == "true")
        {
            bgRewarded.SetActive(true);
            imgNotif.SetActive(false);
        }
        else
        {
            bgRewarded.SetActive(false);
           
            if (Canreward)
                this.GetComponent<Button>().onClick.AddListener(ActionReward);
        }
    }


    private void ActionReward()
    {
        this.GetComponent<Button>().interactable = false;
        SoundController.Instance.PlayFX();
        switch (type)
        {
            case DAILY_REWARD.Day_1:
                Modules.totalCoin += 200;
                SaveLoad.SaveTotalCoin();
                FirebaseManager.Instance.LogEventEarnBanana("Daily_Reward_Day1");
                PlayerPrefs.SetString(SaveID.Day_1, "true");
                PlayerPrefs.SetString(SaveID.ClearDaily,SaveID.False);
                break;
            case DAILY_REWARD.Day_2:
                Modules.totalMagnet += 2;
                SaveLoad.SaveTotalMagnet();
                PlayerPrefs.SetString(SaveID.Day_2, "true");
                break;
            case DAILY_REWARD.Day_3:
                Modules.totalProtect += 2;
                SaveLoad.SaveTotalProtect();
                PlayerPrefs.SetString(SaveID.Day_3, "true");
                break;
            case DAILY_REWARD.Day_4:
                Modules.total2XBanana += 2;
                SaveLoad.SaveTotal2XBanana();
                PlayerPrefs.SetString(SaveID.Day_4, "true");
                break;
            case DAILY_REWARD.Day_5:
                Modules.totalCoin += 600;
                SaveLoad.SaveTotalCoin();
                FirebaseManager.Instance.LogEventEarnBanana("Daily_Reward_Day5");
                PlayerPrefs.SetString(SaveID.Day_5, "true");
                break;
            case DAILY_REWARD.Day_6:
                Modules.totalReborn += 2;
                SaveLoad.SaveTotalReborn();
                PlayerPrefs.SetString(SaveID.Day_6, "true");
                break;
            case DAILY_REWARD.Day_7:
                Modules.totalCoin += 2000;
                SaveLoad.SaveTotalCoin();
                FirebaseManager.Instance.LogEventEarnBanana("Daily_Reward_Day7");
                PlayerPrefs.SetString(SaveID.Day_7, "true");
                PlayerPrefs.SetString(SaveID.ClearDaily,SaveID.True);
                
                break;
        }
        CheckReward();
        PageMenu.Instance.CheckDailyReward();
        PlayerPrefs.SetString(SaveID.Reward, "false");
        int tmp = PlayerPrefs.GetInt(SaveID.NumberReward);
        PlayerPrefs.SetInt(SaveID.NumberReward, tmp + 1);
    }
}
