﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Facebook.Unity;
using DG.Tweening;
using System.Collections;

public class PageMenu : MonoBehaviour {

    #region singleton
    public static PageMenu Instance;
    private void Awake()
    {
        if (Instance == null) Instance = this;
    }
    #endregion

    #region Config
    //xu ly ngon ngu
    public Text textButPlay, textButEndless;
    [Header("Popup ")]
    public GameObject Popup_Setting;
    public GameObject Popup_Achivement;
    public GameObject Popup_LuckyCharm;
    public GameObject Popup_Dailyreward;
    public GameObject Popup_LuckySpin;
    public FirstRechages Popup_FirstRecharge;
    public GameObject Popup_buyCoins;
    public GameObject Popup_tutorial;
    public GameObject PanelMask;

    [Header("Button")]
    public Button btnSetting;
    public Button btnShop;
    public Button btnAchivement;
    public Button btnPlaySellectChapter;
    public Button btnPlayRunEnless;
    public UIButton btnTutorial;
    public UIButton btnSpecial;
    public UIButton btnDailyreward;
    public UIButton btnLuckySpin;
    public UIButton btnWatchAds;
    public UIButton btnShowPopupFirstRechrage;
    public UIButton btnPurchase;
    public UIButton btnInvite;

    #endregion

    #region Main
    private void Start()
    {
        Popup_tutorial.SetActive(Modules.tutorial());
        SetEventButton();
        SetAnimationButton();
        CheckDailyReward();
        CheckAchivement();
        CheckFirstRecharge();       
        PanelMask.SetActive(false);
        StartCoroutine(RunFakeUpdate());
        Modules.airBounce = true;
        //AdMobManager.Instance.showBannerAd();

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ShowPopupQuitGame();
        }
    }

    #endregion

    #region Inits
    private void SetEventButton()
    {
        btnSetting.onClick.AddListener(ButSettingClick);
        btnShop.onClick.AddListener(ButShopClick);
        btnAchivement.onClick.AddListener(ButAchievementClick);        
        btnPlaySellectChapter.onClick.AddListener(ButPlayClick);
        btnPlayRunEnless.onClick.AddListener(ButEndlessClick);
        btnGoogleSv.SetUpEvent(ShowPopupButtonGoogleSv);
        btnSpecial.SetUpEvent(ButSpeial);
        btnShowPopupFirstRechrage.SetUpEvent(ButShowFirstRechage);
        btnDailyreward.SetUpEvent(ButDailyreward);
        btnLuckySpin.SetUpEvent(ButLuckyAction);
        btnWatchAds.SetUpEvent(ShowAds);
        btnPurchase.SetUpEvent(() => { LoadingSceneManager.LoadingShopFromAddCoin(); });
        btnInvite.SetUpEvent(ButInviteClick);
        btnTutorial.SetUpEvent(ButTutorialClick);
    }

    private void SetAnimationButton()
    {
        //btnPlaySellectChapter.gameObject.transform.DOPunchScale(new Vector3(0.05f, 0.05f, 1f), 2,3).SetLoops(-1);
        if (Modules.luckyCharm||(PlayerPrefs.GetString(SaveID.SpecialSaled) == SaveID.False)) btnSpecial.gameObject.SetActive(false);
        if (Modules.firstRecharge) btnShowPopupFirstRechrage.gameObject.SetActive(false);
        if (!GoogleSeviceManager.Instance.Authenticated && Modules.HaveInternet()) warningLoginGG.SetActive(true);

        btnSpecial.transform.DOPunchScale(Vector3.one * 0.05f, 3).SetLoops(-1);
        btnLuckySpin.transform.GetChild(0).DORotate(new Vector3(0, 0, 360), 2, RotateMode.LocalAxisAdd).SetLoops(-1).SetDelay(3);
    }

    [Header("Warning spin")]
    public GameObject warningspin;
    public void CheckDailyReward()
    {
        btnDailyreward.transform.DOPunchRotation(new Vector3(0, 0, 6), 1f, 5).SetLoops(-1);
        if (PlayerPrefs.GetString(SaveID.Reward)==SaveID.True)
        {
            btnDailyreward.transform.GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            btnDailyreward.transform.GetChild(0).gameObject.SetActive(false);
            btnDailyreward.transform.DOPause();
        }

        if (PlayerPrefs.GetString(SaveID.FreeSpin) == SaveID.True)
        {
            warningspin.SetActive(true);
        }
    }

    private void UpdateLanguage()
    {
        if (Modules.listTextFont.Count <= 0) return;
        textButPlay.font = Modules.listFont[Modules.listTextFont[0].indexFont];
        textButPlay.text = Modules.listTextFont[0].textValue;
        textButEndless.font = Modules.listFont[Modules.listTextFont[1].indexFont];
        textButEndless.text = Modules.listTextFont[1].textValue;
    }

    #endregion

    #region Action Button

    private void ButInviteClick()
    {
        SoundController.Instance.PlayFX();
        FirebaseManager.Instance.SendInviteAsync();
    }

    private void ButSpeial()
    {
       SoundController.Instance.PlayFX();
       Popup_LuckyCharm.SetActive(true);
    }

    private void ButShowFirstRechage()
    {
        SoundController.Instance.PlayFX();
        Popup_FirstRecharge.gameObject.SetActive(true);
        Popup_FirstRecharge.CallStart();
    }

    private void ButLuckyAction()
    {
        SoundController.Instance.PlayFX();
        Popup_LuckySpin.SetActive(true);
    }

    private void ButDailyreward()
    {
        SoundController.Instance.PlayFX();
        Popup_Dailyreward.SetActive(true);
    }

    private void ButPlayClick()
    {
        LoadingSceneManager.LoadingScene(SCENE_TYPE.Chapter);
        SoundController.Instance.PlayFX();
    }

    private void ButTutorialClick()
    {
         DataManager.Instance.loadSeceneTutorial = true;
         LoadingSceneManager.LoadingScene(SCENE_TYPE.Loading); 
    }

    public void ButEndlessClick()
    {
        Modules.runEndless = true;
        SoundController.Instance.PlayFX();
        ShowMask();
    }

    public void ButShopClick()
    {
        Modules.oldSceneShop = SceneManager.GetActiveScene().name;
        LoadingSceneManager.LoadingScene(SCENE_TYPE.Shop);
        SoundController.Instance.PlayFX();
    }

    public void ButGoogleLeader()
    {
        GoogleSeviceManager.Instance.ShowAchivementUI();
        SoundController.Instance.PlayFX();
    }
	
    public void ButAchievementClick()
    {
        Popup_Achivement.gameObject.SetActive(true);
        bgButtonAchive.enabled = false;
        notifAchivement.enabled = false;
        SoundController.Instance.PlayFX();
    }

    public void ButSettingClick()
    {
        Popup_Setting.SetActive(true);
        SoundController.Instance.PlayFX();
    }

    public void ShowPopup_buyCoin()
    {
        Popup_buyCoins.SetActive(true);
    }

    private void ShowMask()
    {
        PanelMask.SetActive(true);
        PanelMask.GetComponent<Animator>().SetTrigger("Close");
        LoadingSceneManager.LoadingScene(SCENE_TYPE.Loading);
    }

    private void ShowAds()
    {
        //AdMobManager.Instance.showBannerAd();
        SoundController.Instance.PlayFX();
        Modules.typeRewardVideo = type_RewardAds.Goldx200;
        AdMobManager.Instance.showRewardVideo();
    }

    #endregion

    #region Google Sevice
    [Header("Google Sevice")]
    public UIButton btnGoogleSv;
    public GameObject bgButtonGoogleSv;
    public UIButton btnAchivementGoogleSevice;
    public UIButton btnLeaderbroad;
    public GameObject warningLoginGG;

    private void ShowPopupButtonGoogleSv()
    {
        SoundController.Instance.PlayFX();
        if (!GoogleSeviceManager.Instance.Authenticated&&Modules.HaveInternet()) GoogleSeviceManager.Instance.SignIn();
        warningLoginGG.SetActive(false);
        bgButtonGoogleSv.SetActive(true);
        btnGoogleSv.SetUpEvent(ClosePopupButtonGgSV);
        btnGoogleSv.GetComponent<Image>().color = new Color32(178, 178, 178, 255);
        btnAchivementGoogleSevice.SetUpEvent(GoogleSeviceManager.Instance.ShowAchivementUI);
        btnLeaderbroad.SetUpEvent(GoogleSeviceManager.Instance.ShowLeaderbroadUI);
    }

    private void ClosePopupButtonGgSV()
    {
        SoundController.Instance.PlayFX();
        btnGoogleSv.SetUpEvent(ShowPopupButtonGoogleSv);
        bgButtonGoogleSv.SetActive(false);
        btnGoogleSv.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
    }

    #endregion

    #region Achiment Reward
    [Header("Reward Achivement")]
    public Image bgButtonAchive;
    public Image notifAchivement;
    public void CheckAchivement()
    {
        bgButtonAchive.enabled = false;
        notifAchivement.enabled = false;
        for (int i = 0; i < Achievements.listTask.Count;i++)
        {
            if (Achievements.listTask[i].stateTask == 2)
            {
                bgButtonAchive.enabled = true;
                notifAchivement.enabled = true;
                break;
            }
        }
    }

    #endregion

    #region First Recharge
    public void CheckFirstRecharge() 
    {
        if (Modules.getFirstRecharge)
        {
            btnShowPopupFirstRechrage.gameObject.SetActive(false);
        }
        else 
        {
            btnShowPopupFirstRechrage.gameObject.SetActive(true);
        }
    }
    #endregion

    #region Spin Popup
    [Header("Text Timecountdouwn Spin")]
    public Text txtTimeCountdownSpin;
    public Text txtTimeCDSaleSpecial;
  

    #endregion

    #region Ienumber
   
    private Coroutine fakeUpdate;
    private IEnumerator RunFakeUpdate()
    {
        while (true)
        {
            float timeStep = 0.03f;
            if (btnWatchAds != null)
            {
                if (AdMobManager.Instance.VideoAdLoaded())
                {
                    btnWatchAds.transform.DOPunchRotation(new Vector3(0, 0, 6), 1f, 5).SetLoops(-1);
                    StopCoroutine(RunFakeUpdate());
                    break;
                }
                else
                {
                    btnWatchAds.transform.DOPause();
                    AdMobManager.Instance.loadRewardVideo();
                }
                    
            }
            yield return new WaitForSecondsRealtime(timeStep);
        }
    }
    #endregion

    #region Quit Game
    [Header("Quit Game")]
    public GameObject popupQuitGame;
    public UIButton btnYesQuit;
    public UIButton btnNoQuit;

    public void ShowPopupQuitGame()
    {
        popupQuitGame.SetActive(true);
        btnYesQuit.SetUpEvent(() => {
            DataManager.Instance.EventSesiontimeOnday();
            Application.Quit(); });
        btnNoQuit.SetUpEvent(() => { popupQuitGame.SetActive(false); });
        
    }

    #endregion
}
