﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstRechages : MonoBehaviour {

    [SerializeField] private UIButton btnFirstRechage;
    [SerializeField] private UIButton btnCloseFirstRechage;

    public void CallStart()
    {
        btnCloseFirstRechage.SetUpEvent(() =>
        {
            SoundController.Instance.PlayFX();
            gameObject.SetActive(false);
        });

        if (Modules.firstRecharge)
        {
            btnFirstRechage.SetUpEvent(GetFirstrechage);
        }
        else
        {
            btnFirstRechage.SetUpEvent(LinkToShop);
        }
    }

    private void GetFirstrechage()
    {
        SoundController.Instance.PlayFX();
        Modules.totalMagnet += 10;
        SaveLoad.SaveTotalMagnet();
        Modules.total2XBanana += 10;
        SaveLoad.SaveTotal2XBanana();
        Modules.totalProtect += 10;
        SaveLoad.SaveTotalProtect();
        Modules.getFirstRecharge = true;
        SaveLoad.SaveGetFirstRecharge();
        gameObject.SetActive(false);
        PageMenu.Instance.CheckFirstRecharge();
    }

    private void LinkToShop()
    {
        Debug.LogError(1);
        SoundController.Instance.PlayFX();
        LoadingSceneManager.LoadingShopFromAddCoin();
    }
}
