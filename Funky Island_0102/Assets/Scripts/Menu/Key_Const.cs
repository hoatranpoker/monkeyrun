﻿
public class Key_Const
{
    //Sences name
    public const string Boot = "Boot";
    public const string Menu = "Menu";
    public const string Loading = "Loading";
    public const string Chapter = "Chapter";
    public const string Tutorial = "Tutorial";
    public const string Play = "Play";
    public const string Shop = "Shop";
    public const string Achivement = "Achievement";

}

public class IAP_PRODUCT
{
    //PACK SHOP
    public const string BUY_1_MiniPack = "buy_1_minipack";
    public const string BUY_2_SmallPack = "buy_2_smallpack";
    public const string BUY_5_MediumPack= "buy_5_mediumpack";
    public const string BUY_10_LargePack = "buy_10_largepack";
    public const string BUY_20_ExtraLargPack = "buy_20_extralargpack";
    public const string BUY_50_HugePack = "buy_50_hugepack";
    public const string BUY_100_ExtraHugePack = "buy_100_extrahugepack";

    //SPECIAL SHOP 
    public const string BUY_1_RemoveAds = "buy_1_removeads";
    public const string BUY_5_LuckyCharm = "buy_5_luckycharm";
    public const string BUY_10_GoldenBanana = "buy_10_goldenbanana";
    public const string BUY_10_MegaSale = "buy_10_megasale";
    public const string BUY_5_WaterBounce = "buy_5_waterbounce";
    public const string BUY_10_AirBounce = "buy_10_airbounce";
}

public class GoogleID
{
    //leader
    public const string Leaderbroad_distance = GPGSIds.leaderboard_distance;

    //unlock
    public const string Achivement_Upgrade = GPGSIds.achievement_upgrade;
    public const string Achivement_Newbie_CompleteLv1 = GPGSIds.achievement_newbie;
    public const string Achivement_Adventure_PassLv20 = GPGSIds.achievement_adventure;
    public const string Achivement_Marathon_Run2000m = GPGSIds.achievement_marathon;
    public const string Achivement_MonkeyRich_Buy1PackInIAP = GPGSIds.achievement_monkey_rich;
    public const string Achivement_Gamer_PlayGame30m = GPGSIds.achievement_gamer;

    //incre
    public const string Achivement_iCostBanana_Use5000coins = GPGSIds.achievement_cost_banana;
    public const string Achivement_iCollectBanana_1000Ingame = GPGSIds.achievement_collect_banana;
    public const string Achivement_iCollecStarInGame_30 = GPGSIds.achievement_collect_star;
    public const string Achivement_iShoping_Buy5item = GPGSIds.achievement_shopping;
}

public class Log_Name_Event
{
    //Ads
    public const string Event_Ads_LeaveTheApplication = "Ads_LeaveTheApplication";
    public const string Event_Ads_ClickBaner= "Ads_Clicked_Baner";

    //Level
    public const string Endless_Start = "Endless_Start";
    public const string Endless_Complete = "Endless_Complete";
    public const string Level_User = "Level_User_";
    public const string Level_Start = "Level_Start_";
    public const string Level_Victory = "Level_Victory_";
    public const string Level_Failed = "Level_Failed_";
    public const string Monkey_Die_ = "Monkey_Die_Map_";

    public const string Km_Die = "User_EventDie_Meter";
    public const string Continue = "Continue_Level_";
    public const string EarnBanana = "Banana_Earn_";
    public const string SpentBanana = "Banana_Spent_";
    public const string SessionTimeOnDay = "Session_Time_Day_";
    public const string SessionTimeOneGame = "Session_Time_Game_";
    public const string SessionTimeOpenGame = "Session_Time_OpenGame_Day_";
}

public class SaveID
{
    //Base
    public const string True = "true";
    public const string False = "false";
    public const string Tutorial = "Tutorialfirstime";

    public const string FirstOpenAppOnDay = "FirstOpenAppOnDay";
    //Daily Reward
    public const string Day_1 = "Day_1";
    public const string Day_2 = "Day_2";
    public const string Day_3 = "Day_3";
    public const string Day_4 = "Day_4";
    public const string Day_5 = "Day_5";
    public const string Day_6 = "Day_6";
    public const string Day_7 = "Day_7";
    public const string ClearDaily = "ClearDaily";
    public const string NumberReward = "NumberReward";
    public const string Reward = "Rewarded";
    public const string LastDayReward = "LastdayReward";

    //spin time
    public const string TimeOpenGame= "TimeOpenGame";
    public const string TimeQuitGame="TimeQuitGame";
    public const string TimeSpinCD= "TimeSpinCD";
    public const string TimeQuitSpin= "TimeQuitSpin";
    public const string FreeSpin = "FreeSpin";
    public const string NumberSpin = "NumberSpin";
    //special time
    public const string SpecialSaled = "SpecialSale";
    public const string canCDSpecial = "canCDSpecial";
    public const string TimeSpcialCD = "TimeSpcialCD";
    public const string TimeSpecialCDQuitGame = "TimeSpecialCDQuitGame";
    public const string TotalStarAchiment = "TotalStarAchiment";

    //SessionTime
    public const string TimeOnDay = "TimeOnDay";
    public const string NumberTimeUserPlayOnDay = "NumberTimeUserPlayOnDay";

    //Map-Background
    public const string BackgroundNumber = "BackgroundNumber";

    //Clear Update
    public const string ClearCoins_V18 = "ClearCoins_V18";
    
}