﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaleSpecial : MonoBehaviour {

    #region Init
    [SerializeField] private UIButton btnX;
    [SerializeField] private UIButton btnBuy;

    public bool canCD()
    {
        if (PlayerPrefs.GetString(SaveID.canCDSpecial) == SaveID.True)
            return true;
        else
            return false;
    }

    private void Start()
    {
        btnX.SetUpEvent(ActionBtnX);
        btnBuy.SetUpEvent(ActionBuy);
        if (canCD())
        {
            CountDownTimeSpecialtStart(7200);
            PlayerPrefs.SetString(SaveID.canCDSpecial, SaveID.False);
        }
    }

    private void OnEnable()
    {
        txtPrice.text = "???";
        CheckActive();
        ReloadCost();
        //Cd time
        if(!canCD())
            CountDownTimeSpecialtStart(timesCD());
    }

    private void OnDisable()
    {
        PlayerPrefs.SetString(SaveID.TimeSpecialCDQuitGame, DataManager.Instance.timeNow().ToString());
        PlayerPrefs.SetInt(SaveID.TimeSpcialCD, (int)timeScpecialCountdown);
    }

    #endregion

    #region Action

    private void ActionBtnX()
    {
        gameObject.SetActive(false);
    }

    private void ActionBuy()
    {
        if (Purchaser.Instance != null)
        {
            if (productID != "")
            {
                Purchaser.Instance.BuyProductID(productID);
                DataManager.Instance.Special = true;
            }
        }
        gameObject.SetActive(false);
        SoundController.Instance.PlayFX();
    }

    #endregion

    #region IAP

    public Text txtPrice;
    public TypeNormalPack type;
    private void CheckActive()
    {
        switch (type)
        {
            case TypeNormalPack.Mini:
                productID = IAP_PRODUCT.BUY_1_MiniPack;
                break;
            case TypeNormalPack.Small:
                productID = IAP_PRODUCT.BUY_2_SmallPack;
                break;
            case TypeNormalPack.Medium:
                productID = IAP_PRODUCT.BUY_5_MediumPack;
                break;
            case TypeNormalPack.Large:
                productID = IAP_PRODUCT.BUY_10_LargePack;
                break;
            case TypeNormalPack.ExtraLarge:
                productID = IAP_PRODUCT.BUY_20_ExtraLargPack;
                break;
            case TypeNormalPack.Huge:
                productID = IAP_PRODUCT.BUY_50_HugePack;
                break;
            case TypeNormalPack.ExtraHuge:
                productID = IAP_PRODUCT.BUY_100_ExtraHugePack;
                break;
        }
    }

    public string Name { get { return type.ToString(); } set { } } //Ten trung voi ten co trong Name list product IAP

    public string productID;

    public bool UpdateData(string productName)
    {
        bool sucsses = false;
        if (Purchaser.Instance == null) return false;
        foreach (var item in Purchaser.Instance.productIAP)
        {
            if (item.Name == productName)
            {
                Name = productName;
                if (Purchaser.Instance.IsInitialized())
                {
                    if (txtPrice != null)
                    {
                        txtPrice.text = "???";
                        string priceStr = Purchaser.m_StoreController.products.WithID(item.ID).metadata.localizedPriceString.ToString();
                        if (priceStr != "")
                        {
                            txtPrice.text = "" + priceStr;
                            sucsses = true;
                        }
                    }
                }
                productID = item.ID;
                break;
            }
        }
        return sucsses;
    }

    private void ReloadCost()
    {
        if (!UpdateData(Name))
            Invoke("ReloadCost", 1);
    }

    #endregion

    #region Countdown Special On MainMenu
  
    private int timesCD()
    {
        double second = Convert.ToDateTime(DataManager.Instance.timeNow()).Subtract(Convert.ToDateTime(PlayerPrefs.GetString(SaveID.TimeSpecialCDQuitGame))).TotalSeconds;
        int tmp = PlayerPrefs.GetInt(SaveID.TimeSpcialCD) - (int)second;
        if (tmp <= 0) tmp = 0;
        return tmp;
    }

    public float timeScpecialCountdown;
    public Text txtTimeCDSpecial;
    private Coroutine fakeCd;

    public void CountDownTimeSpecialtStart(float _time)
    {
        StopAllCoroutines();
        timeScpecialCountdown = _time;
        fakeCd = StartCoroutine(fakeCountDownSpecial());
    }

    private IEnumerator fakeCountDownSpecial()
    {
        while (timeScpecialCountdown > 0)
        {
            timeScpecialCountdown--;
            UpdateLevelTimer_Hms(timeScpecialCountdown);
            yield return new WaitForSeconds(1f);
            if (timeScpecialCountdown <= 0)
                PlayerPrefs.SetString(SaveID.SpecialSaled, SaveID.False);
        }
    }

    public void UpdateLevelTimer_Hms(float totalSeconds)
    {
        int seconds = Mathf.RoundToInt(totalSeconds % 60f);
        int minutes = (int)(totalSeconds / 60);
        int hour = (int)(minutes / 60);
        minutes -= hour * 60;

        txtTimeCDSpecial.text = hour.ToString("00") + ":" + minutes.ToString("00") + ":" + seconds.ToString("00");
    }
    #endregion
}
