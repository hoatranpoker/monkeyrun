﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxShopPack : MonoBehaviour {

    public TypeNormalPack type;
    public LabelLanguages txtTitleName;
    public Text txtBananaBonus;
    public Text txtPrice;

    public UIButton btnBuy;

    public List<GameObject> ItemBonus;

    private Shop_Pack shop_Pack {
        get { return DataManager.Instance.shopModel.shop_Packs[(int)type]; }
        set { DataManager.Instance.shopModel.shop_Packs[(int)type] = value; }
    }

    private void Awake()
    {
        txtPrice.text = "???";
        CheckActive();
        ReloadCost();
    }

    private void Start()
    {
        btnBuy.SetUpEvent(ActionBuy);
        CallStart();
    }

    private void CallStart()
    {
        txtTitleName.KEY = shop_Pack.name;
        txtTitleName.SetText();
        txtBananaBonus.text = shop_Pack.ValueClaim.ToString();
        
    }

    private void SetItemBonus()
    {
        for (int i = 0; i < shop_Pack.List_bonusItem.Count; i++)
        {
            //ItemBonus[i].transform.parent.gameObject.SetActive(true);
            ItemBonus[i].transform.GetChild(1).GetComponent<Text>().text = "x" + shop_Pack.List_bonusItem[i].number;
            ItemBonus[i].transform.GetChild(0).gameObject.SetActive(true);
            ItemBonus[i].transform.GetChild(1).gameObject.SetActive(true);
            ItemBonus[i].gameObject.SetActive(true);
            
       }
    }

    private void ActionBuy()
    {
        if (Purchaser.Instance != null)
        {
            if (productID != "")
            {
                Purchaser.Instance.BuyProductID(productID);
                //FacebookController.Instance.LogPurchasedEvent(1, shop_Pack.name, shop_Pack.type.ToString(), "USD", shop_Pack.ValueClaim);
            }
        }
        PageShop.Instance.CheckShopSpecial();
        SoundController.Instance.PlayFX();
    }

    #region Product
    public string Name { get { return type.ToString(); } set { } } //Ten trung voi ten co trong Name list product IAP
    public string productID;

    private void CheckActive()
    {
        switch (type)
        {
            case TypeNormalPack.Mini:
                productID = IAP_PRODUCT.BUY_1_MiniPack;
                break;
            case TypeNormalPack.Small:
                productID = IAP_PRODUCT.BUY_2_SmallPack;
                break;
            case TypeNormalPack.Medium:
                productID = IAP_PRODUCT.BUY_5_MediumPack;
                break;
            case TypeNormalPack.Large:
                productID = IAP_PRODUCT.BUY_10_LargePack;
                break;
            case TypeNormalPack.ExtraLarge:
                productID = IAP_PRODUCT.BUY_20_ExtraLargPack;
                break;
            case TypeNormalPack.Huge:
                productID = IAP_PRODUCT.BUY_50_HugePack;
                break;
            case TypeNormalPack.ExtraHuge:
                productID = IAP_PRODUCT.BUY_100_ExtraHugePack;
                break;
        }
    }

    private void OnEnable()
    {
        CheckActive();
        ReloadCost();
    }

    private bool UpdateData(string productName)
    {
        bool sucsses = false;
        if (Purchaser.Instance == null) return false;
        foreach (var item in Purchaser.Instance.productIAP)
        {
            if (item.Name == productName)
            {
                Name = productName;
                if (Purchaser.Instance.IsInitialized())
                {
                    if (txtPrice != null)
                    {
                        txtPrice.text = "???";
                        string priceStr = Purchaser.m_StoreController.products.WithID(item.ID).metadata.localizedPriceString.ToString();
                        if (priceStr != "")
                        {
                            txtPrice.text = ""+priceStr;
                            sucsses = true;
                        }
                    }
                }
                productID = item.ID;
                break;
            }
        }
        return sucsses;
    }

    private void ReloadCost()
    {
        if (!UpdateData(Name))
            Invoke("ReloadCost", 0.1f);
    }

    #endregion

}
