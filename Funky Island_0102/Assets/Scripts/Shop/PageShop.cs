﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PageShop : MonoBehaviour {

    #region Singleton
    public static PageShop Instance;
    private void Awake()
    {
        if (Instance == null) Instance = this;
    }
    #endregion

    public GameObject popup_Items, popup_Upgrade,popup_Pack,popup_Special, popup_BuyCoins;
    [Header("Text on Shop")]
    public LabelLanguages txttitleShop;

    [Header("Button Shop")]
    public Button btnItems;
    public Button btnUpgrades;
    public Button btnPack;
    public Button btnSpecial;

    public UIButton btnExit;
    public UIButton btnPurChase;

    [Header("Sprite")]
    public Sprite sprOnbutton;
    public Sprite sprOffbutton;

    public GameObject contentShopSpecial; //Giam chieu rong khi da mua hang
    public List<BoxShopSpecial> list_BoxShopScpecial;
    [SerializeField]private List<Button> list_button;

    public Text txtCoins;

    private void Start()
    {
        SetEventButton();
        ShowPageShop(DataManager.Instance.pageShopStart);
        txtCoins.text = Modules.totalCoin.ToString();
    }

    private void OnDisable()
    {
        DataManager.Instance.pageShop = false;
    }

    private void OnEnable()
    {
        DataManager.Instance.pageShop = true;
    }

    #region Action Intance

    public void UpdateTxtTitleShop(string key)
    {
        txttitleShop.KEY = key;
        txttitleShop.SetText();
    }

    public void ShowPopup_BuyCoins()
    {
        popup_BuyCoins.SetActive(true);
        
    }

    public void CheckShopSpecial()
    {
        for (int i = 0; i < list_BoxShopScpecial.Count; i++)
        {
            list_BoxShopScpecial[i].CheckActive();
        }
    }

    public void ShowPagePack()
    {
        ShowPageShop(PAGE_SHOP.Pack);
    }
    #endregion

    #region Action 

    public string key(PAGE_SHOP page) //key title
    {
        string tmp = "";
        switch (page)
        {
            case PAGE_SHOP.Item:
                tmp = KeyLanguages.SHOP_TITLE_ITEM;
                break;
            case PAGE_SHOP.Upgrade:
                tmp = KeyLanguages.SHOP_TITLE_UPGRADE;
                break;
            case PAGE_SHOP.Pack:
                tmp = KeyLanguages.SHOP_TITLE_SHOP;
                break;
            case PAGE_SHOP.Special:
                tmp = KeyLanguages.SHOP_TITLE_SPECIAL;
                break;
        }
        return tmp;
    }
    

    private void SetEventButton()
    {
        list_button.Add(btnItems);
        list_button.Add(btnUpgrades);
        list_button.Add(btnPack);
        list_button.Add(btnSpecial);

        btnExit.SetUpEvent(ButHomeClick);
        btnPurChase.SetUpEvent(() => { ShowPageShop(PAGE_SHOP.Pack); });
        btnItems.onClick.AddListener(()=> { ShowPageShop(PAGE_SHOP.Item); });
        btnUpgrades.onClick.AddListener(()=> { ShowPageShop(PAGE_SHOP.Upgrade); });
        btnPack.onClick.AddListener(()=> { ShowPageShop(PAGE_SHOP.Pack); });
        btnSpecial.onClick.AddListener(()=> { ShowPageShop(PAGE_SHOP.Special); });
    }

    private void SetTextButton(Button btn)
    {
        btn.GetComponent<Image>().sprite = sprOnbutton;
        btn.GetComponent<Image>().SetNativeSize();
        btn.transform.GetChild(0).gameObject.SetActive(true);
        btn.transform.GetChild(1).gameObject.SetActive(false);
        for (int i = 0; i < list_button.Count; i++)
        {
            if (list_button[i] != btn)
            {
                list_button[i].GetComponent<Image>().sprite = sprOffbutton;
                list_button[i].GetComponent<Image>().SetNativeSize();
                list_button[i].transform.GetChild(1).gameObject.SetActive(true);
                list_button[i].transform.GetChild(0).gameObject.SetActive(false);
            }
            
        }
    }

    public void ShowPageShop(PAGE_SHOP page)
    {
        popup_Items.SetActive(false);
        popup_Upgrade.SetActive(false);
        popup_Pack.SetActive(false);
        popup_Special.SetActive(false);

        UpdateTxtTitleShop(key(page));
       
        switch (page)
        {
            case PAGE_SHOP.Item:
                popup_Items.SetActive(true);
                SetTextButton(btnItems);
                break;
            case PAGE_SHOP.Upgrade:
                popup_Upgrade.SetActive(true);
                SetTextButton(btnUpgrades);
                break;
            case PAGE_SHOP.Pack:
                DataManager.Instance.pageShopStart = PAGE_SHOP.Item;
                SetTextButton(btnPack);
                popup_Pack.SetActive(true);
                break;
            case PAGE_SHOP.Special:
                popup_Special.SetActive(true);
                SetTextButton(btnSpecial);
                break;
        }
        SoundController.Instance.PlayFX();
    }

    private void ButHomeClick()
    {
        if (Modules.additiveIAP)
        {
            Modules.additiveIAP = false;
            Modules.pauseTimeSave = false;
            //SceneManager.UnloadSceneAsync(Key_Const.Shop);
            SceneManager.UnloadScene(Key_Const.Shop);
            PagePlay.Instance.UpdateItem();
        }
        else
        {
            SceneManager.LoadScene(Modules.oldScene);
            Time.timeScale = 1;

        }
       
        SoundController.Instance.PlayFX();
    }

    #endregion

}

public enum PAGE_SHOP { Item,Upgrade,Pack,Special}