﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxItems : MonoBehaviour {

    public TypeItemBuy type;
    public LabelLanguages txtTitle;
    public LabelLanguages txtDesc;
    [Tooltip("so luong item dang co")]
    public Text txtHaving;
    private int megaSale = 1; //pack megaSEll
    public Image imgItem;

    public UIButton btnBuy;
    private Shop_Item shop_Item {
        get { return DataManager.Instance.shopModel.shop_Items[(int)type]; }
        set { DataManager.Instance.shopModel.shop_Items[(int)type]=value; }
    }
	
	private void Start() {
        if (Modules.megaSale) megaSale = 2;
        CallStart();
        btnBuy.SetUpEvent(ActionBuy);
	}

    public void UpdateText()
    {
        txtHaving.text = shop_Item.having.ToString();
    }

    private void CallStart()
    {
        txtTitle.KEY = shop_Item.name;
        txtTitle.SetText();
        txtDesc.KEY = shop_Item.Desc;
        txtDesc.SetText();
        imgItem.sprite = shop_Item.image;
        //imgItem.SetNativeSize();
        btnBuy.transform.GetChild(1).GetComponent<Text>().text = (shop_Item.price / megaSale).ToString();
        UpdateText();
    }

    private void ActionBuy()
    {
        SoundController.Instance.PlayFX();
        if (Modules.totalCoin >= (shop_Item.price/megaSale))
        {
            Modules.totalCoin -= (shop_Item.price / megaSale);
            SaveLoad.SaveTotalCoin();
            shop_Item.having += 1;
            UpdateText();
            FirebaseManager.Instance.LogEventSpentBanana("Shop_Item_"+ type);
            GoogleSeviceManager.Instance.IncrementAchivemnet(GoogleID.Achivement_iShoping_Buy5item, 1);
            GoogleSeviceManager.Instance.IncrementAchivemnet(GoogleID.Achivement_iCostBanana_Use5000coins, shop_Item.price / megaSale);
        }
        else
        {
            PageShop.Instance.ShowPopup_BuyCoins();
        }
    }

}
