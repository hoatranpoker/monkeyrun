﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxShopSpecial : MonoBehaviour {

    public TypeSpecialPack type;
    public LabelLanguages txtTitle;
    public LabelLanguages txtDesc;
    public Text txtPrice;

    public UIButton btnBuy;

    private Shop_Special shop_Special
    {
        get { return DataManager.Instance.shopModel.shop_Specials[(int)type]; }
        set { DataManager.Instance.shopModel.shop_Specials[(int)type] = value; }
    }

    private void Awake()
    {
        CheckActive();
        ReloadCost();
    }

    private void Start()
    {
        if(!PageShop.Instance.list_BoxShopScpecial.Contains(this)) PageShop.Instance.list_BoxShopScpecial.Add(this);
        CallStart();
        btnBuy.SetUpEvent(ActionBuy);
    }

	public void CallStart()
    {
        txtTitle.KEY = shop_Special.name;
        txtTitle.SetText();
        txtDesc.KEY = shop_Special.Desc;
        txtDesc.SetText();
      
    }

    private void ActionBuy()
    {
        if (Purchaser.Instance != null)
        {
            if (productID != "")
            {
                Purchaser.Instance.BuyProductID(productID);
                
            }
        }
        SoundController.Instance.PlayFX();
    }

    public void CheckActive()
    {
        switch (type)
        {
            case TypeSpecialPack.AirBounce:
                productID = IAP_PRODUCT.BUY_10_AirBounce;
                if (Modules.airBounce) {
                    gameObject.SetActive(false); }
                break;
            case TypeSpecialPack.GoldenBanana:
                productID = IAP_PRODUCT.BUY_10_GoldenBanana;
                if (Modules.goldenBanana) gameObject.SetActive(false);
                break;
            case TypeSpecialPack.LuckyCharm:
                productID = IAP_PRODUCT.BUY_5_LuckyCharm;
                if (Modules.luckyCharm) gameObject.SetActive(false);
                break;
            case TypeSpecialPack.MegaSale:
                productID = IAP_PRODUCT.BUY_10_MegaSale;
                if(Modules.megaSale) gameObject.SetActive(false);
                break;
            case TypeSpecialPack.RemoveAds:
                productID = IAP_PRODUCT.BUY_1_RemoveAds;
                if (Modules.removeAds) gameObject.SetActive(false);
                break;
            case TypeSpecialPack.WaterBounce:
                productID = IAP_PRODUCT.BUY_5_WaterBounce;
                if (Modules.waterBounce) gameObject.SetActive(false);
                break;

        }
    }

    #region Product
    public string Name { get { return type.ToString(); } set { } } //Ten trung voi ten co trong Name list product IAP
    
    public string productID;

    void OnEnable()
    {
        txtPrice.text = "???";
        CheckActive();
        ReloadCost();
    }

    public bool UpdateData(string productName)
    {
        bool sucsses = false;
        if (Purchaser.Instance == null) return false;
        foreach (var item in Purchaser.Instance.productIAP)
        {
            if (item.Name == productName)
            {
                Name = productName;
                if (Purchaser.Instance.IsInitialized())
                {
                    if (txtPrice != null)
                    {
                        txtPrice.text = "???";
                        string priceStr = Purchaser.m_StoreController.products.WithID(item.ID).metadata.localizedPriceString.ToString();
                        if (priceStr != "")
                        {
                            txtPrice.text = "" + priceStr;
                            sucsses = true;
                        }
                    }
                }
                productID = item.ID;
                break;
            }
        }
        return sucsses;
    }

    void ReloadCost()
    {
        if (!UpdateData(Name))
            Invoke("ReloadCost", 1);
    }

    #endregion
}
