﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BoxBuyCoin : MonoBehaviour {

    public bool additiveIAP = false;
    public Text txtTitle;
    public Text txtDesc;
    public void ButYesClick()
    {
        Modules.tabIAPBuyCoin = true;

        if (DataManager.Instance.pageShop)
        {
            PageShop.Instance.ShowPagePack();
        }
        else
        {
            Modules.oldScene = SceneManager.GetActiveScene().name;
            DataManager.Instance.pageShopStart = PAGE_SHOP.Pack;
            //if (SceneManager.GetActiveScene().name == Key_Const.Play)
            //{
            //    PagePlay.Instance.boxSaveMe.SetActive(false);
            //}

            if (additiveIAP)
            {
                SceneManager.LoadScene(Key_Const.Shop, LoadSceneMode.Additive);
            }
            else LoadingSceneManager.LoadingShopFromAddCoin();
        }
        Modules.additiveIAP = additiveIAP;
        gameObject.SetActive(false);
        SoundController.Instance.PlayFX();
    }

    public void ButNoClick()
    {
        Modules.pauseTimeSave = false;
        //transform.GetComponent<Animator>().Play("MessageBoxClose", 0, 0f);
        gameObject.SetActive(false);
        SoundController.Instance.PlayFX();
    }

    public void SetTextBoxBuyCoins(string _txtTitle,string _txtDesc)
    {
        txtTitle.text = _txtTitle;
        txtDesc.text = _txtDesc;
    }
}
