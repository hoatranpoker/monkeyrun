﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxUpgrade : MonoBehaviour {

    public TypeItemUpgrade type;

    public Image imgItemUpgrade;
    public GameObject Process;
    [SerializeField]private Text txtDesc;

    public UIButton btnUpgrade;
    public Text txtPrice;
    private Shop_Upgrade shop_Upgrade
    {
        get { return DataManager.Instance.shopModel.shop_Upgrades[(int)type]; }
        set { DataManager.Instance.shopModel.shop_Upgrades[(int)type] = value; }
    }
   
    void Start () {
        CallStart();
        btnUpgrade.SetUpEvent(UpgradeAction);

    }
	
    public void CallStart()
    {
        SetText();
        if (shop_Upgrade.sprItem != null)
            imgItemUpgrade.sprite = shop_Upgrade.sprItem;
        imgItemUpgrade.SetNativeSize();
    }

    public void SetText()
    {
        if (shop_Upgrade.levelCurent <5)
            txtPrice.text = shop_Upgrade.level_upgrade[shop_Upgrade.levelCurent].costItem.ToString();
        else {
            btnUpgrade.ClearEvent();
            txtPrice.text = "Max"; }

        string tmp = Language.GetKey(shop_Upgrade.Desc);
        txtDesc.text = tmp.Replace("[?]", shop_Upgrade.level_upgrade[shop_Upgrade.levelCurent].effectItem.ToString());

        for (int i = 0; i < shop_Upgrade.levelCurent; i++)
        {
            Process.transform.GetChild(i).gameObject.SetActive(true);
        }

    }

    private void UpgradeAction()
    {
        if (Modules.totalCoin>= shop_Upgrade.level_upgrade[shop_Upgrade.levelCurent].costItem)
        {
            if(shop_Upgrade.levelCurent <5)
            {
                Modules.totalCoin -= shop_Upgrade.level_upgrade[shop_Upgrade.levelCurent].costItem;
                SaveLoad.SaveTotalCoin();
                shop_Upgrade.levelCurent++;
                switch (type)
                {
                    case TypeItemUpgrade.Carpet:
                        SaveLoad.SaveLevelCarpet();
                        break;
                    case TypeItemUpgrade.Magnet:
                        SaveLoad.SaveLevelMagnet();
                        break;
                    case TypeItemUpgrade.Protect:
                        SaveLoad.SaveLevelProtect();
                        break;
                    case TypeItemUpgrade.Rhino:
                        SaveLoad.SaveLevelRhino();
                        break;
                    case TypeItemUpgrade.Skateboard:
                        SaveLoad.SaveLevelSkateboard();
                        break;
                    case TypeItemUpgrade.X2Banana:
                        SaveLoad.SaveLevel2XBanana();
                        break;
                }
                FirebaseManager.Instance.LogEventSpentBanana("Shop_Upgrade_"+ type);
                GoogleSeviceManager.Instance.UnlockAchivement(GoogleID.Achivement_Upgrade);
            }
            else
            {
                Debug.LogError("Max Level");
                btnUpgrade.ClearEvent();
            }
           
        }
        else
        {
            PageShop.Instance.ShowPopup_BuyCoins();
        }
        SetText();
        SoundController.Instance.PlayFX();
    }
}
