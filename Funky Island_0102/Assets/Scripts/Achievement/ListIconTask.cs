﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListIconTask : MonoBehaviour {

    public List<Sprite> listIcon = new List<Sprite>();

    void Awake()
    {
        Achievements.listIcon = listIcon;
    }
}
