﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Achievements : MonoBehaviour {

    #region Inits
    public static int totalTask = 88;
    public static List<TaskDetail> listTask = new List<TaskDetail>();
    public static List<Sprite> listIcon = new List<Sprite>();
    public static List<LevelMonkey> listLevel = new List<LevelMonkey>();

    #endregion

    #region Setup base

    public static void CreateListTask()
    {
        listTask = new List<TaskDetail>();

        //1 Collect Star
        listTask.Add(new TaskDetail(TypeTask.CollectStar, 1, 10, 50, 50, -1, 0)); //0
        listTask.Add(new TaskDetail(TypeTask.CollectStar, 2, 20, 60, 60, 0, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectStar, 3, 30, 70, 70, 1, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectStar, 4, 40, 80, 80, 2, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectStar, 5, 50, 90, 90, 3, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectStar, 6, 60, 100, 100, 4, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectStar, 7, 70, 110, 110, 5, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectStar, 8, 80, 120, 120, 6, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectStar, 9, 90, 130, 130, 7, 0)); 

        //2 Pass Level
        listTask.Add(new TaskDetail(TypeTask.PassLevel, 1, 3, 50, 50, -1, 0)); //9
        listTask.Add(new TaskDetail(TypeTask.PassLevel, 2, 6, 60, 60, 9, 0));
        listTask.Add(new TaskDetail(TypeTask.PassLevel, 3, 9, 70, 70, 10, 0));
        listTask.Add(new TaskDetail(TypeTask.PassLevel, 4, 12, 80, 80, 11, 0));
        listTask.Add(new TaskDetail(TypeTask.PassLevel, 5, 15, 90, 90, 12, 0));
        listTask.Add(new TaskDetail(TypeTask.PassLevel, 6, 18, 100, 100, 13, 0));
        listTask.Add(new TaskDetail(TypeTask.PassLevel, 7, 21, 110, 110, 14, 0));
        listTask.Add(new TaskDetail(TypeTask.PassLevel, 8, 24, 120, 120, 15, 0)); 
        
        //3 Collect Magnet
        listTask.Add(new TaskDetail(TypeTask.CollectMagnet, 1, 1, 50, 50, -1, 0)); //17
        listTask.Add(new TaskDetail(TypeTask.CollectMagnet, 2, 5, 100, 100, 17, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectMagnet, 3, 10, 200, 200, 18, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectMagnet, 4, 15, 400, 400, 19, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectMagnet, 5, 20, 600, 600, 20, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectMagnet, 6, 30, 800, 800, 21, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectMagnet, 7, 40, 1000, 1000, 22, 0)); 

        //4 Collect Protect
        listTask.Add(new TaskDetail(TypeTask.CollectProtect, 1, 1, 50, 50, -1, 0)); //24
        listTask.Add(new TaskDetail(TypeTask.CollectProtect, 2, 5, 100, 100, 24, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectProtect, 3, 10, 200, 200, 25, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectProtect, 4, 15, 400, 400, 26, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectProtect, 5, 20, 600, 600, 27, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectProtect, 6, 30, 800, 800, 28, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectProtect, 7, 40, 1000, 1000, 29, 0)); 

        //5 Collect X2Banana
        listTask.Add(new TaskDetail(TypeTask.Collect2XBanana, 1, 1, 50, 50, -1, 0));  //31
        listTask.Add(new TaskDetail(TypeTask.Collect2XBanana, 2, 5, 100, 100, 31, 0));
        listTask.Add(new TaskDetail(TypeTask.Collect2XBanana, 3, 10, 200, 200, 32, 0));
        listTask.Add(new TaskDetail(TypeTask.Collect2XBanana, 4, 15, 400, 400, 33, 0));
        listTask.Add(new TaskDetail(TypeTask.Collect2XBanana, 5, 20, 600, 600, 34, 0));
        listTask.Add(new TaskDetail(TypeTask.Collect2XBanana, 6, 30, 800, 800, 35, 0));
        listTask.Add(new TaskDetail(TypeTask.Collect2XBanana, 7, 40, 1000, 1000, 36, 0)); 
        
        //6 RunMarathon
        listTask.Add(new TaskDetail(TypeTask.RunMarathon, 1, 500, 50, 50, -1, 0)); //38
        listTask.Add(new TaskDetail(TypeTask.RunMarathon, 2, 1000, 70, 70, 38, 0));
        listTask.Add(new TaskDetail(TypeTask.RunMarathon, 3, 1500, 90, 90, 39, 0));
        listTask.Add(new TaskDetail(TypeTask.RunMarathon, 4, 2000, 110, 110, 40, 0));
        listTask.Add(new TaskDetail(TypeTask.RunMarathon, 5, 2500, 130, 130, 41, 0));
        listTask.Add(new TaskDetail(TypeTask.RunMarathon, 6, 3000, 150, 150, 42, 0));
        listTask.Add(new TaskDetail(TypeTask.RunMarathon, 7, 3500, 170, 170, 43, 0));
        listTask.Add(new TaskDetail(TypeTask.RunMarathon, 8, 4000, 190, 190, 44, 0));
        listTask.Add(new TaskDetail(TypeTask.RunMarathon, 9, 4500, 210, 210, 45, 0));
        listTask.Add(new TaskDetail(TypeTask.RunMarathon, 10, 5000, 230, 230, 46, 0)); 

        //7 CollectFlying
        listTask.Add(new TaskDetail(TypeTask.CollectFlying, 1, 1, 50, 50, -1, 0)); //48
        listTask.Add(new TaskDetail(TypeTask.CollectFlying, 2, 10, 60, 60, 48, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectFlying, 3, 20, 70, 70, 49, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectFlying, 4, 30, 80, 80, 50, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectFlying, 5, 50, 90, 90, 51, 0));

        //8 Run FLying
        listTask.Add(new TaskDetail(TypeTask.RunFlying, 1, 500, 50, 50, -1, 0)); //53
        listTask.Add(new TaskDetail(TypeTask.RunFlying, 2, 1000, 60, 60, 53, 0));
        listTask.Add(new TaskDetail(TypeTask.RunFlying, 3, 1500, 70, 70, 54, 0));
        listTask.Add(new TaskDetail(TypeTask.RunFlying, 4, 2000, 80, 80, 55, 0));
        listTask.Add(new TaskDetail(TypeTask.RunFlying, 5, 2500, 90, 90, 56, 0)); 

        //9 Collect Skateboard
        listTask.Add(new TaskDetail(TypeTask.CollectSkateboard, 1, 1, 50, 50, -1, 0)); //58
        listTask.Add(new TaskDetail(TypeTask.CollectSkateboard, 2, 10, 60, 60, 58, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectSkateboard, 3, 20, 70, 70, 59, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectSkateboard, 4, 30, 80, 80, 60, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectSkateboard, 5, 50, 90, 90, 61, 0)); 

        //10 Run Skateboard
        listTask.Add(new TaskDetail(TypeTask.RunSkateboard, 1, 500, 50, 50, -1, 0));  //63
        listTask.Add(new TaskDetail(TypeTask.RunSkateboard, 2, 1000, 60, 60, 63, 0));
        listTask.Add(new TaskDetail(TypeTask.RunSkateboard, 3, 1500, 70, 70, 64, 0));
        listTask.Add(new TaskDetail(TypeTask.RunSkateboard, 4, 2000, 80, 80, 65, 0));
        listTask.Add(new TaskDetail(TypeTask.RunSkateboard, 5, 2500, 90, 90, 66, 0)); 

        //11 Collect Rhino
        listTask.Add(new TaskDetail(TypeTask.CollectRhino, 1, 1, 50, 50, -1, 0));  //68
        listTask.Add(new TaskDetail(TypeTask.CollectRhino, 2, 10, 60, 60, 68, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectRhino, 3, 20, 70, 70, 69, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectRhino, 4, 30, 80, 80, 70, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectRhino, 5, 50, 90, 90, 71, 0)); 

        //12 Run Rhino
        listTask.Add(new TaskDetail(TypeTask.RunRhino, 1, 500, 50, 50, -1, 0)); //73
        listTask.Add(new TaskDetail(TypeTask.RunRhino, 2, 1000, 60, 60, 73, 0));
        listTask.Add(new TaskDetail(TypeTask.RunRhino, 3, 1500, 70, 70, 74, 0));
        listTask.Add(new TaskDetail(TypeTask.RunRhino, 4, 2000, 80, 80, 75, 0));
        listTask.Add(new TaskDetail(TypeTask.RunRhino, 5, 2500, 90, 90, 76, 0)); 

        //13 Collect StupidBird
        listTask.Add(new TaskDetail(TypeTask.CollectStupidBird, 1, 1, 50, 50, -1, 0)); //78
        listTask.Add(new TaskDetail(TypeTask.CollectStupidBird, 2, 10, 60, 60, 78, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectStupidBird, 3, 20, 70, 70, 79, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectStupidBird, 4, 30, 80, 80, 80, 0));
        listTask.Add(new TaskDetail(TypeTask.CollectStupidBird, 5, 50, 90, 90, 81, 0)); 

        //14 Run StupidBird
        listTask.Add(new TaskDetail(TypeTask.RunStupidBird, 1, 500, 50, 50, -1, 0)); //83
        listTask.Add(new TaskDetail(TypeTask.RunStupidBird, 2, 1000, 60, 60, 83, 0));
        listTask.Add(new TaskDetail(TypeTask.RunStupidBird, 3, 1500, 70, 70, 84, 0));
        listTask.Add(new TaskDetail(TypeTask.RunStupidBird, 4, 2000, 80, 80, 85, 0));
        listTask.Add(new TaskDetail(TypeTask.RunStupidBird, 5, 2500, 90, 90, 86, 0));

        ////15 Buy 1 pack
        //listTask.Add(new TaskDetail(TypeTask.Buy1Pack, 1, 1, 2000, 1000, -1, 0)); //88

        ////16 Collect Banana
        //listTask.Add(new TaskDetail(TypeTask.CollectBanana, 1, 100, 50, 50, -1, 0)); //89
        //listTask.Add(new TaskDetail(TypeTask.CollectBanana, 2, 500, 60, 60, 89, 0));
        //listTask.Add(new TaskDetail(TypeTask.CollectBanana, 3, 1000, 80, 80, 90, 0));
        //listTask.Add(new TaskDetail(TypeTask.CollectBanana, 4, 5000, 100, 100, 91, 0));
        //listTask.Add(new TaskDetail(TypeTask.CollectBanana, 5, 10000, 120, 120, 92, 0));

        ////17 Cost Banana
        //listTask.Add(new TaskDetail(TypeTask.CostBanana, 1, 500, 50, 50, -1, 0)); //94
        //listTask.Add(new TaskDetail(TypeTask.CostBanana, 2, 1000, 60, 60, 94, 0));
        //listTask.Add(new TaskDetail(TypeTask.CostBanana, 3, 5000, 80, 80, 95, 0));
        //listTask.Add(new TaskDetail(TypeTask.CostBanana, 4, 10000, 100, 100, 96, 0));
        //listTask.Add(new TaskDetail(TypeTask.CostBanana, 5, 20000, 120, 120, 97, 0));

        //DataManager.Instance.achivementModel.taskDetails = listTask;
    }

    public static void CreateListLevel()
    {
        listLevel = new List<LevelMonkey>();
        listLevel.Add(new LevelMonkey(1, 100, TypeRewardLevel.Magnet, 5));
        listLevel.Add(new LevelMonkey(2, 300, TypeRewardLevel.Protect, 5));
        listLevel.Add(new LevelMonkey(3, 700, TypeRewardLevel.X2Banana, 5));
        listLevel.Add(new LevelMonkey(4, 1100, TypeRewardLevel.Reborn, 5));
        listLevel.Add(new LevelMonkey(5, 1600, TypeRewardLevel.Banana, 500));
        listLevel.Add(new LevelMonkey(6, 2200, TypeRewardLevel.Magnet, 10));
        listLevel.Add(new LevelMonkey(7, 2900, TypeRewardLevel.Protect, 10));
        listLevel.Add(new LevelMonkey(8, 3700, TypeRewardLevel.X2Banana, 10));
        listLevel.Add(new LevelMonkey(9, 4600, TypeRewardLevel.Reborn, 10));
        listLevel.Add(new LevelMonkey(10, 5600, TypeRewardLevel.Banana, 1000));
        listLevel.Add(new LevelMonkey(11, 6700, TypeRewardLevel.Magnet, 15));
        listLevel.Add(new LevelMonkey(12, 7900, TypeRewardLevel.Protect, 15));
        listLevel.Add(new LevelMonkey(13, 9200, TypeRewardLevel.X2Banana, 15));
        listLevel.Add(new LevelMonkey(14, 10600, TypeRewardLevel.Reborn, 15));
        listLevel.Add(new LevelMonkey(15, 12100, TypeRewardLevel.Banana, 1500));
        listLevel.Add(new LevelMonkey(16, 13700, TypeRewardLevel.Magnet, 20));
        listLevel.Add(new LevelMonkey(17, 15400, TypeRewardLevel.Protect, 20));
        listLevel.Add(new LevelMonkey(18, 17200, TypeRewardLevel.X2Banana, 20));
        listLevel.Add(new LevelMonkey(19, 19100, TypeRewardLevel.Reborn, 20));
        listLevel.Add(new LevelMonkey(20, 21100, TypeRewardLevel.Banana, 2000));

        //DataManager.Instance.achivementModel.listLevel = listLevel;
    }

    #endregion

    #region Action

    public static int GetLevelNow()
    {
        int result = 0;
        for (int i = listLevel.Count - 1; i >= 0; i--)
        {
            if (Modules.expPlayer >= listLevel[i].requireExp)
            {
                result = listLevel[i].numberLevel;
                break;
            }
        }
        return result;
    }

    public static Vector2 GetRangeExpLevelNow()
    {
        Vector2 result = new Vector2(0, 0);
        if (listLevel.Count > 0) result = new Vector2(0, listLevel[0].requireExp);
        for (int i = listLevel.Count - 1; i >= 0; i--)
        {
            if (Modules.expPlayer >= listLevel[i].requireExp)
            {
                if (i < listLevel.Count - 1)
                    result = new Vector2(listLevel[i].requireExp, listLevel[i + 1].requireExp);
                else result = new Vector2(listLevel[i].requireExp, listLevel[i].requireExp);
                break;
            }
        }
        return result;
    }

    private static void UpdateShowNewTask(int oldTask)
    {
        for (int i = 0; i < listTask.Count; i++)
        {
            if (listTask[i].followTask == oldTask)
            {
                listTask[i].stateTask = 1;
                switch (listTask[i].typeTask)
                {
                    case TypeTask.CollectStar:
                        maxCollectStar = listTask[i].numberNeed;
                        numCollectStar = 0;
                        allowCollectStar = true;
                        break;
                    case TypeTask.PassLevel:
                        UpdateTaskPassLevel();
                        break;
                    case TypeTask.CollectMagnet:
                        maxCollectMagnet = listTask[i].numberNeed;
                        numCollectMagnet = 0;
                        allowCollectMagnet = true;
                        break;
                    case TypeTask.CollectProtect:
                        maxCollectProtect = listTask[i].numberNeed;
                        numCollectProtect = 0;
                        allowCollectProtect = true;
                        break;
                    case TypeTask.Collect2XBanana:
                        maxCollect2XBanana = listTask[i].numberNeed;
                        numCollect2XBanana = 0;
                        allowCollect2XBanana = true;
                        break;
                    case TypeTask.RunMarathon:
                        maxRunMarathon = listTask[i].numberNeed;
                        numRunMarathon = 0;
                        allowRunMarathon = true;
                        break;
                    case TypeTask.CollectFlying:
                        maxCollectFlying = listTask[i].numberNeed;
                        numCollectFlying = 0;
                        allowCollectFlying = true;
                        break;
                    case TypeTask.RunFlying:
                        maxRunFlying = listTask[i].numberNeed;
                        numRunFlying = 0;
                        allowRunFlying = true;
                        break;
                    case TypeTask.CollectSkateboard:
                        maxCollectSkateboard = listTask[i].numberNeed;
                        numCollectSkateboard = 0;
                        allowCollectSkateboard = true;
                        break;
                    case TypeTask.RunSkateboard:
                        maxRunSkateboard = listTask[i].numberNeed;
                        numRunSkateboard = 0;
                        allowRunSkateboard = true;
                        break;
                    case TypeTask.CollectRhino:
                        maxCollectRhino = listTask[i].numberNeed;
                        numCollectRhino = 0;
                        allowCollectRhino = true;
                        break;
                    case TypeTask.RunRhino:
                        maxRunRhino = listTask[i].numberNeed;
                        numRunRhino = 0;
                        allowRunRhino = true;
                        break;
                    case TypeTask.CollectStupidBird:
                        maxCollectStupidBird = listTask[i].numberNeed;
                        numCollectStupidBird = 0;
                        allowCollectStupidBird = true;
                        break;
                    case TypeTask.RunStupidBird:
                        maxRunStupidBird = listTask[i].numberNeed;
                        numRunStupidBird = 0;
                        allowRunStupidBird = true;
                        break;
                }
            }
        }
        SaveLoad.SaveTaskState();
    }

    public static void GetRewardTask(int index)
    {
        //lay du lieu phan thuong
        listTask[index].stateTask = 3;
        UpdateShowNewTask(index);
    }

    #endregion

    #region Action Mission

    public static int numCollectStar = 0;
    public static int maxCollectStar = 0;
    public static bool allowCollectStar = true;
    public static void CheckTaskCollectStar(int addNumber)
    {
        if (!allowCollectStar) return;
        numCollectStar += addNumber;
        if (numCollectStar >= maxCollectStar)
        {
            numCollectStar = 0;
            allowCollectStar = false;
            for (int i = 0; i < listTask.Count; i++)
            {
                if (listTask[i].typeTask == TypeTask.CollectStar && listTask[i].stateTask == 1)
                {
                    listTask[i].stateTask = 2;
                    break;
                }
            }
            SaveLoad.SaveTaskState();
        }
        SaveLoad.SaveCollectStar();
    }

    public static int numPassLevel = 0;
    public static void CheckTaskPassLevel(int addNumber)
    {
        numPassLevel += addNumber;
        SaveLoad.SavePassLevel();
        UpdateTaskPassLevel();
        SaveLoad.SaveTaskState();
    }

    public static void UpdateTaskPassLevel()
    {
        for (int i = 0; i < listTask.Count; i++)
        {
            if (listTask[i].typeTask == TypeTask.PassLevel && listTask[i].stateTask == 1 && listTask[i].numberNeed <= numPassLevel)
            {
                listTask[i].stateTask = 2;
                break;
            }
        }
    }

    public static int numCollectMagnet = 0;
    public static int maxCollectMagnet = 0;
    public static bool allowCollectMagnet = true;
    public static void CheckTaskCollectMagnet(int addNumber)
    {
        if (!allowCollectMagnet) return;
        numCollectMagnet += addNumber;
        if (numCollectMagnet >= maxCollectMagnet)
        {
            numCollectMagnet = 0;
            allowCollectMagnet = false;
            for (int i = 0; i < listTask.Count; i++)
            {
                if (listTask[i].typeTask == TypeTask.CollectMagnet && listTask[i].stateTask == 1)
                {
                    listTask[i].stateTask = 2;
                    break;
                }
            }
            SaveLoad.SaveTaskState();
        }
        SaveLoad.SaveCollectMagnet();
    }

    public static int numCollectProtect = 0;
    public static int maxCollectProtect = 0;
    public static bool allowCollectProtect = true;
    public static void CheckTaskCollectProtect(int addNumber)
    {
        if (!allowCollectProtect) return;
        numCollectProtect += addNumber;
        if (numCollectProtect >= maxCollectProtect)
        {
            numCollectProtect = 0;
            allowCollectProtect = false;
            for (int i = 0; i < listTask.Count; i++)
            {
                if (listTask[i].typeTask == TypeTask.CollectProtect && listTask[i].stateTask == 1)
                {
                    listTask[i].stateTask = 2;
                    break;
                }
            }
            SaveLoad.SaveTaskState();
        }
        SaveLoad.SaveCollectProtect();
    }

    public static int numCollect2XBanana = 0;
    public static int maxCollect2XBanana = 0;
    public static bool allowCollect2XBanana = true;
    public static void CheckTaskCollect2XBanana(int addNumber)
    {
        if (!allowCollect2XBanana) return;
        numCollect2XBanana += addNumber;
        if (numCollect2XBanana >= maxCollect2XBanana)
        {
            numCollect2XBanana = 0;
            allowCollect2XBanana = false;
            for (int i = 0; i < listTask.Count; i++)
            {
                if (listTask[i].typeTask == TypeTask.Collect2XBanana && listTask[i].stateTask == 1)
                {
                    listTask[i].stateTask = 2;
                    break;
                }
            }
            SaveLoad.SaveTaskState();
        }
        SaveLoad.SaveCollect2XBanana();
    }

    public static int numRunMarathon = 0;
    public static int maxRunMarathon = 0;
    public static bool allowRunMarathon = true;
    public static void CheckTaskRunMarathon(int addNumber)
    {
        if (!allowRunMarathon) return;
        numRunMarathon += addNumber;
        if (numRunMarathon >= maxRunMarathon)
        {
            numRunMarathon = 0;
            allowRunMarathon = false;
            for (int i = 0; i < listTask.Count; i++)
            {
                if (listTask[i].typeTask == TypeTask.RunMarathon && listTask[i].stateTask == 1)
                {
                    listTask[i].stateTask = 2;
                    break;
                }
            }
            SaveLoad.SaveTaskState();
        }
        SaveLoad.SaveRunMarathon();
    }

    public static int numCollectFlying = 0;
    public static int maxCollectFlying = 0;
    public static bool allowCollectFlying = true;
    public static void CheckTaskCollectFlying(int addNumber)
    {
        if (!allowCollectFlying) return;
        numCollectFlying += addNumber;
        if (numCollectFlying >= maxCollectFlying)
        {
            numCollectFlying = 0;
            allowCollectFlying = false;
            for (int i = 0; i < listTask.Count; i++)
            {
                if (listTask[i].typeTask == TypeTask.CollectFlying && listTask[i].stateTask == 1)
                {
                    listTask[i].stateTask = 2;
                    break;
                }
            }
            SaveLoad.SaveTaskState();
        }
        SaveLoad.SaveCollectFlying();
    }

    public static int numRunFlying = 0;
    public static int maxRunFlying = 0;
    public static bool allowRunFlying = true;
    public static void CheckTaskRunFlying(int addNumber)
    {
        if (!allowRunFlying) return;
        numRunFlying += addNumber;
        if (numRunFlying >= maxRunFlying)
        {
            numRunFlying = 0;
            allowRunFlying = false;
            for (int i = 0; i < listTask.Count; i++)
            {
                if (listTask[i].typeTask == TypeTask.RunFlying && listTask[i].stateTask == 1)
                {
                    listTask[i].stateTask = 2;
                    break;
                }
            }
            SaveLoad.SaveTaskState();
        }
        SaveLoad.SaveRunFlying();
    }

    public static int numCollectSkateboard = 0;
    public static int maxCollectSkateboard = 0;
    public static bool allowCollectSkateboard = true;
    public static void CheckTaskCollectSkateboard(int addNumber)
    {
        if (!allowCollectSkateboard) return;
        numCollectSkateboard += addNumber;
        if (numCollectSkateboard >= maxCollectSkateboard)
        {
            numCollectSkateboard = 0;
            allowCollectSkateboard = false;
            for (int i = 0; i < listTask.Count; i++)
            {
                if (listTask[i].typeTask == TypeTask.CollectSkateboard && listTask[i].stateTask == 1)
                {
                    listTask[i].stateTask = 2;
                    break;
                }
            }
            SaveLoad.SaveTaskState();
        }
        SaveLoad.SaveCollectSkateboard();
    }

    public static int numRunSkateboard = 0;
    public static int maxRunSkateboard = 0;
    public static bool allowRunSkateboard = true;
    public static void CheckTaskRunSkateboard(int addNumber)
    {
        if (!allowRunSkateboard) return;
        numRunSkateboard += addNumber;
        if (numRunSkateboard >= maxRunSkateboard)
        {
            numRunSkateboard = 0;
            allowRunSkateboard = false;
            for (int i = 0; i < listTask.Count; i++)
            {
                if (listTask[i].typeTask == TypeTask.RunSkateboard && listTask[i].stateTask == 1)
                {
                    listTask[i].stateTask = 2;
                    break;
                }
            }
            SaveLoad.SaveTaskState();
        }
        SaveLoad.SaveRunSkateboard();
    }

    public static int numCollectRhino = 0;
    public static int maxCollectRhino = 0;
    public static bool allowCollectRhino = true;
    public static void CheckTaskCollectRhino(int addNumber)
    {
        if (!allowCollectRhino) return;
        numCollectRhino += addNumber;
        if (numCollectRhino >= maxCollectRhino)
        {
            numCollectRhino = 0;
            allowCollectRhino = false;
            for (int i = 0; i < listTask.Count; i++)
            {
                if (listTask[i].typeTask == TypeTask.CollectRhino && listTask[i].stateTask == 1)
                {
                    listTask[i].stateTask = 2;
                    break;
                }
            }
            SaveLoad.SaveTaskState();
        }
        SaveLoad.SaveCollectRhino();
    }

    public static int numRunRhino = 0;
    public static int maxRunRhino = 0;
    public static bool allowRunRhino = true;
    public static void CheckTaskRunRhino(int addNumber)
    {
        if (!allowRunRhino) return;
        numRunRhino += addNumber;
        if (numRunRhino >= maxRunRhino)
        {
            numRunRhino = 0;
            allowRunRhino = false;
            for (int i = 0; i < listTask.Count; i++)
            {
                if (listTask[i].typeTask == TypeTask.RunRhino && listTask[i].stateTask == 1)
                {
                    listTask[i].stateTask = 2;
                    break;
                }
            }
            SaveLoad.SaveTaskState();
        }
        SaveLoad.SaveRunRhino();
    }

    public static int numCollectStupidBird = 0;
    public static int maxCollectStupidBird = 0;
    public static bool allowCollectStupidBird = true;
    public static void CheckTaskCollectStupidBird(int addNumber)
    {
        if (!allowCollectStupidBird) return;
        numCollectStupidBird += addNumber;
        if (numCollectStupidBird >= maxCollectStupidBird)
        {
            numCollectStupidBird = 0;
            allowCollectStupidBird = false;
            for (int i = 0; i < listTask.Count; i++)
            {
                if (listTask[i].typeTask == TypeTask.CollectStupidBird && listTask[i].stateTask == 1)
                {
                    listTask[i].stateTask = 2;
                    break;
                }
            }
            SaveLoad.SaveTaskState();
        }
        SaveLoad.SaveCollectStupidBird();
    }

    public static int numRunStupidBird = 0;
    public static int maxRunStupidBird = 0;
    public static bool allowRunStupidBird = true;
    public static void CheckTaskRunStupidBird(int addNumber)
    {
        if (!allowRunStupidBird) return;
        numRunStupidBird += addNumber;
        if (numRunStupidBird >= maxRunStupidBird)
        {
            numRunStupidBird = 0;
            allowRunStupidBird = false;
            for (int i = 0; i < listTask.Count; i++)
            {
                if (listTask[i].typeTask == TypeTask.RunStupidBird && listTask[i].stateTask == 1)
                {
                    listTask[i].stateTask = 2;
                    break;
                }
            }
            SaveLoad.SaveTaskState();
        }
        SaveLoad.SaveRunStupidBird();
    }

    #endregion

}

#region Enum

[System.Serializable]
public class TaskDetail
{
    public TypeTask typeTask;
    public int numberLevel;
    public int numberNeed;
    public int bananasReward;
    public int expReward;
    public int followTask; //ma id task rang buoc
    public int stateTask; //0 chua lam, 1 dang lam, 2 da lam xong, 3 da nhan thuong

    public TaskDetail(TypeTask typeInput, int levelInput, int numberInput, int bananaInput, int expInput, int followInput, int stateInput)
    {
        this.typeTask = typeInput;
        this.numberLevel = levelInput;
        this.numberNeed = numberInput;
        this.bananasReward = bananaInput;
        this.expReward = expInput;
        this.followTask = followInput;
        this.stateTask = stateInput;
    }
}

public enum TypeTask
{
    CollectStar,
    PassLevel,
    CollectMagnet,
    CollectProtect,
    Collect2XBanana,
    RunMarathon,
    CollectFlying,
    RunFlying,
    CollectSkateboard,
    RunSkateboard,
    CollectRhino,
    RunRhino,
    CollectStupidBird,
    RunStupidBird,
    CollectBanana,
    CostBanana,
    FirstRechage,
    Buy1Pack,
    PlayGameTime,
    ViewAds,
    UpgradeMaget,
    UpgradeX2Banana,
    UpgradeProtect,

}

[System.Serializable]
public class LevelMonkey
{
    public int numberLevel;
    public int requireExp;
    public TypeRewardLevel typeReward;
    public int numberReward;

    public LevelMonkey(int levelInput, int expInput, TypeRewardLevel typeInput, int numberInput)
    {
        this.numberLevel = levelInput;
        this.requireExp = expInput;
        this.typeReward = typeInput;
        this.numberReward = numberInput;
    }
}

public enum TypeRewardLevel
{
    Magnet,
    Protect,
    X2Banana,
    Reborn,
    Banana
}

#endregion