﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskControll : MonoBehaviour {

    public int idTask = 0;
    public Text textTitle, textNote;
    public Text coinReward, expReward;
    public Image iconTask;
    public GameObject maskDisable;// giai doan 0
    public GameObject beforeFinish; //giai doan 1
    public UIButton btnReward;//giao doan 2
    public GameObject btnRewarded; //giai doan 3

    private void Start()
    {
        if (Achievements.listTask.Count <= 0) return;
        iconTask.sprite = Achievements.listIcon[idTask];
        iconTask.SetNativeSize();
        SetInit();
        CallStart();

    }

    private void CallStart()
    {
        coinReward.text = Achievements.listTask[idTask].bananasReward.ToString();
        expReward.text = Achievements.listTask[idTask].expReward.ToString();
        switch (Achievements.listTask[idTask].typeTask)
        {
            case TypeTask.Collect2XBanana:
                textTitle.font = Modules.listFont[Modules.listTextFont[53].indexFont];
                textTitle.text = Modules.listTextFont[53].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[54].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

            case TypeTask.CollectFlying:
                if (Achievements.listTask[idTask].stateTask == 1)
                    SetProcess(Achievements.numCollectFlying, Achievements.maxCollectFlying);
                textTitle.font = Modules.listFont[Modules.listTextFont[65].indexFont];
                textTitle.text = Modules.listTextFont[65].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[66].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

            case TypeTask.CollectMagnet:
                if (Achievements.listTask[idTask].stateTask == 1)
                    SetProcess(Achievements.numCollectMagnet, Achievements.maxCollectStar);
                textTitle.font = Modules.listFont[Modules.listTextFont[57].indexFont];
                textTitle.text = Modules.listTextFont[57].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[58].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

            case TypeTask.CollectProtect:
                if (Achievements.listTask[idTask].stateTask == 1)
                    SetProcess(Achievements.numCollectProtect, Achievements.maxCollectProtect);
                textTitle.font = Modules.listFont[Modules.listTextFont[59].indexFont];
                textTitle.text = Modules.listTextFont[59].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[60].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

            case TypeTask.CollectRhino:
                if (Achievements.listTask[idTask].stateTask == 1)
                    SetProcess(Achievements.numCollectRhino, Achievements.maxCollectRhino);
                textTitle.font = Modules.listFont[Modules.listTextFont[73].indexFont];
                textTitle.text = Modules.listTextFont[73].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[74].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

            case TypeTask.CollectSkateboard:
                if (Achievements.listTask[idTask].stateTask == 1)
                    SetProcess(Achievements.numCollectSkateboard, Achievements.maxCollectSkateboard);
                textTitle.font = Modules.listFont[Modules.listTextFont[69].indexFont];
                textTitle.text = Modules.listTextFont[69].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[70].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

            case TypeTask.CollectStar:
                if (Achievements.listTask[idTask].stateTask == 1)
                    SetProcess(Achievements.numCollectStar, Achievements.maxCollectStar);
                textTitle.font = Modules.listFont[Modules.listTextFont[53].indexFont];
                textTitle.text = Modules.listTextFont[53].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[54].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

            case TypeTask.CollectStupidBird:
                if (Achievements.listTask[idTask].stateTask == 1)
                    SetProcess(Achievements.numCollectStupidBird, Achievements.maxCollectStupidBird);
                textTitle.font = Modules.listFont[Modules.listTextFont[77].indexFont];
                textTitle.text = Modules.listTextFont[77].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[78].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

            case TypeTask.PassLevel:
                if (Achievements.listTask[idTask].stateTask == 1)
                    SetProcess();
                textTitle.font = Modules.listFont[Modules.listTextFont[55].indexFont];
                textTitle.text = Modules.listTextFont[55].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[56].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

            case TypeTask.RunFlying:
                if (Achievements.listTask[idTask].stateTask == 1)
                    SetProcess(Achievements.numRunFlying, Achievements.maxRunFlying);
                textTitle.font = Modules.listFont[Modules.listTextFont[67].indexFont];
                textTitle.text = Modules.listTextFont[67].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[68].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

            case TypeTask.RunMarathon:
                if (Achievements.listTask[idTask].stateTask == 1)
                    SetProcess(Achievements.numRunMarathon, Achievements.maxRunMarathon);
                textTitle.font = Modules.listFont[Modules.listTextFont[63].indexFont];
                textTitle.text = Modules.listTextFont[63].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[64].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

            case TypeTask.RunRhino:
                if (Achievements.listTask[idTask].stateTask == 1)
                    SetProcess(Achievements.numRunRhino, Achievements.maxRunRhino);
                textTitle.font = Modules.listFont[Modules.listTextFont[75].indexFont];
                textTitle.text = Modules.listTextFont[75].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[76].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

            case TypeTask.RunSkateboard:
                if (Achievements.listTask[idTask].stateTask == 1)
                    SetProcess(Achievements.numRunSkateboard, Achievements.maxRunSkateboard);
                textTitle.font = Modules.listFont[Modules.listTextFont[71].indexFont];
                textTitle.text = Modules.listTextFont[71].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[72].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

            case TypeTask.RunStupidBird:
                if (Achievements.listTask[idTask].stateTask == 1)
                    SetProcess(Achievements.numRunStupidBird, Achievements.maxRunStupidBird);
                textTitle.font = Modules.listFont[Modules.listTextFont[79].indexFont];
                textTitle.text = Modules.listTextFont[79].textValue.Replace("[?]", Achievements.listTask[idTask].numberLevel.ToString());
                textNote.text = Modules.listTextFont[80].textValue.Replace("[?]", Achievements.listTask[idTask].numberNeed.ToString());
                break;

        }

    }

    private void SetInit()
    {
        maskDisable.SetActive(false);
        beforeFinish.SetActive(true);
        btnReward.gameObject.SetActive(false);
        btnRewarded.SetActive(false);
        switch (Achievements.listTask[idTask].stateTask)
        {
            case 0:
                maskDisable.SetActive(true);
                break;
            case 1:
                canSetProcess = true;
                break;
            case 2:
                btnReward.gameObject.SetActive(true);
                btnReward.SetUpEvent(ButRewardClick);
                break;
            case 3:
                btnRewarded.SetActive(false);
                break;
        }
    }

    public GameObject process;
    private bool canSetProcess=false;
    private void SetProcess(int num=0,int max=1)
    {
        if (!canSetProcess) return;
        if (max <= 0) max = 1;
        int tmp = (int)((num * 5) / max);
        for (int i = 0; i < tmp; i++)
            process.transform.GetChild(i).gameObject.SetActive(true);
    }

    public void ButRewardClick()
    {
        if (Achievements.listTask.Count <= 0) return;
        if (Achievements.listTask[idTask].stateTask != 2) return;
        PageAchievement.Instance.GetRewardValue(Achievements.listTask[idTask].bananasReward, Achievements.listTask[idTask].expReward);
        Achievements.GetRewardTask(idTask);
        transform.parent.GetComponent<TaskListControll>().UpdateList();
        PageMenu.Instance.CheckAchivement();
        SetInit();
        SoundController.Instance.PlayFX();
    }
}
