﻿using Spine.Unity;
using UnityEngine;
using UnityEngine.UI;

public class PageGift : MonoBehaviour {

    public GameObject ItemReward;
    public SkeletonGraphic GiftAnimation;
    public AudioClip audioShowItem;

	private void Start () {
        GiftAnimation.AnimationState.Complete += AnimationState_Complete;

    }

    private void OnEnable()
    {
        if (audioShowItem)
            AudioSource.PlayClipAtPoint(audioShowItem, Camera.main.transform.position, Modules.soundSetting);
        GiftAnimation.AnimationState.SetAnimation(0, "unlock", false);
        PageAchievement.Instance.btnGift.SetActive(false);
    }

    private void AnimationState_Complete(Spine.TrackEntry trackEntry)
    {
        if (trackEntry.animation.name == "unlock")
        {
            ClosePageGift();
        }
    }

    public void SetItemReward(Sprite sprite,int number)
    {
        ItemReward.GetComponent<Image>().sprite = sprite;
        ItemReward.GetComponentInChildren<Text>().text = "x" + number;
    }
	
    public void ClosePageGift()
    {
        DataManager.Instance.BonusAchivement = false;
        //PageMenu.Instance.CheckAchivement();
        gameObject.SetActive(false);
        PageAchievement.Instance.RewardComplete();
    }
}
