﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class PageAchievement : MonoBehaviour {

    #region singleton
    public static PageAchievement Instance;
    private void Awake()
    {
        if (Instance == null) Instance = this;
    }

    #endregion

    #region Inits
    public Text textTitlePage, textLevel;
    public List<Text> textTitle, textNote, textButReward;
    public UIButton btnExit;

    private void Start()
    {
        oldLevel = Achievements.GetLevelNow();
        if (DataManager.Instance.BonusAchivement) btnGift.SetActive(true);

        btnGift.GetComponent<UIButton>().SetUpEvent(() => { Popup_Gift.SetActive(true); });
        btnExit.SetUpEvent(ButHomeClick);
        UpdateLevelState();
    }

    #endregion

    #region ActionButton
    public void ButHomeClick()
    {
        //SceneManager.LoadScene("Menu");
        gameObject.SetActive(false);
        SoundController.Instance.PlayFX();
    }

    public void ButPurchaseClick()
    {
        Modules.tabIAPBuyCoin = false;
        Modules.oldScene = SceneManager.GetActiveScene().name;
        LoadingSceneManager.LoadingShopFromAddCoin();
        SoundController.Instance.PlayFX();
    }

    public void GetRewardValue(int coin, int exp)
    {
        Modules.totalCoin += coin;
        SaveLoad.SaveTotalCoin();
        Modules.expPlayer += exp;
        SaveLoad.SaveExpPlayer();
        UpdateLevelState();
        FirebaseManager.Instance.LogEventEarnBanana("Achivement");
    }

    #endregion

    #region Gift

    [Header("Bottom Gift")]
    public GameObject Popup_Gift;
    public GameObject btnGift;
    public Image lineLevel;
    public Text textExp;
    private int oldLevel;
    public void UpdateLevelState()
    {
        int levelNow = Achievements.GetLevelNow();

        Vector2 rangeLevel = Achievements.GetRangeExpLevelNow();
        int expFollowLevel = Modules.expPlayer - Mathf.RoundToInt(rangeLevel.x);
        textExp.text = expFollowLevel.ToString() + "/" + Mathf.RoundToInt(rangeLevel.y);
        float percent = (float)expFollowLevel / rangeLevel.y;
       
        if (percent>1)
        {
            lineLevel.DOFillAmount(1,1).OnComplete(()=>{
                lineLevel.fillAmount = 0;
                float tmp = percent - 1;
                lineLevel.DOFillAmount(tmp, 1.5f * percent);
            });
           
        }
        else
        {
            lineLevel.DOFillAmount(percent, 1.5f * percent);
        }

        CheckGift(levelNow);
        if (Modules.listTextFont.Count > 0)
        {
            textLevel.font = Modules.listFont[Modules.listTextFont[15].indexFont];
            textLevel.text = Modules.listTextFont[15].textValue.Replace("[?]", levelNow.ToString());
        }
    }

    public void CheckGift(int level)
    {
        if (level > oldLevel)
        {
            oldLevel = level;
            btnGift.gameObject.SetActive(true);
            DataManager.Instance.BonusAchivement = true;
            for (int i = 0; i < Achievements.listLevel.Count; i++)
            {
                if (Achievements.listLevel[i].numberLevel == level)
                {
                    if (Achievements.listLevel[i].typeReward == TypeRewardLevel.Magnet)
                    {
                        SetItemGift(sprMagnet, Achievements.listLevel[i].numberReward);
                    }
                    else if (Achievements.listLevel[i].typeReward == TypeRewardLevel.Protect)
                    {
                        SetItemGift(sprProtect, Achievements.listLevel[i].numberReward);  
                    }
                    else if (Achievements.listLevel[i].typeReward == TypeRewardLevel.X2Banana)
                    {
                        SetItemGift(sprX2Banana, Achievements.listLevel[i].numberReward);
                    }
                    else if (Achievements.listLevel[i].typeReward == TypeRewardLevel.Reborn)
                    {
                        SetItemGift(sprReborn, Achievements.listLevel[i].numberReward);
                    }
                    else if (Achievements.listLevel[i].typeReward == TypeRewardLevel.Banana)
                    {
                        SetItemGift(sprBanana, Achievements.listLevel[i].numberReward);
                    }
                    break;
                }
            }
        }
    }

    [Header("Icon Items")]
    public Sprite sprMagnet;
    public Sprite sprProtect;
    public Sprite sprX2Banana;
    public Sprite sprReborn;
    public Sprite sprBanana;

    private void SetItemGift(Sprite sprite, int number)
    {
        Popup_Gift.GetComponent<PageGift>().SetItemReward(sprite, number);
    }

    public void RewardComplete()
    {
        for (int i = 0; i < Achievements.listLevel.Count; i++)
        {
            if (Achievements.listLevel[i].numberLevel == Achievements.GetLevelNow())
            {
                if (Achievements.listLevel[i].typeReward == TypeRewardLevel.Magnet)
                {
                    print("Magnet reward");
                    Modules.totalMagnet += Achievements.listLevel[i].numberReward;
                    SaveLoad.SaveTotalMagnet();
                }
                else if (Achievements.listLevel[i].typeReward == TypeRewardLevel.Protect)
                {
                    print("Protect reward");
                    Modules.totalProtect += Achievements.listLevel[i].numberReward;
                    SaveLoad.SaveTotalProtect();
                }
                else if (Achievements.listLevel[i].typeReward == TypeRewardLevel.X2Banana)
                {
                    print("X2Banana reward");
                    Modules.total2XBanana += Achievements.listLevel[i].numberReward;
                    SaveLoad.SaveTotal2XBanana();
                }
                else if (Achievements.listLevel[i].typeReward == TypeRewardLevel.Reborn)
                {
                    print("Reborn reward");
                    Modules.totalReborn += Achievements.listLevel[i].numberReward;
                    SaveLoad.SaveTotalReborn();
                }
                else if (Achievements.listLevel[i].typeReward == TypeRewardLevel.Banana)
                {
                    print("Banana reward");
                    Modules.totalCoin += Achievements.listLevel[i].numberReward;
                    SaveLoad.SaveTotalCoin();
                    FirebaseManager.Instance.LogEventEarnBanana("Achivement");
                }
                break;
            }
        }

    }

    #endregion

}