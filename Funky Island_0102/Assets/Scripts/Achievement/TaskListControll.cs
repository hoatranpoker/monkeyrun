﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TaskListControll : MonoBehaviour {

	private void Awake () {
        UpdateList();
    }

    public void UpdateList()
    {
        if (Achievements.listTask.Count <= 0) return;
        transform.GetChild(0).gameObject.SetActive(true);
        GameObject temp = transform.GetChild(0).gameObject;
        for (int i = 1; i < transform.childCount; i++) Destroy(transform.GetChild(i).gameObject);
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].stateTask == 1)
            {
                GameObject newTask = Instantiate(temp, transform);
                newTask.GetComponent<TaskControll>().idTask = i;
            }
            else if (Achievements.listTask[i].stateTask == 2)
            {
                GameObject newTask = Instantiate(temp, transform);
                newTask.GetComponent<TaskControll>().idTask = i;
                newTask.transform.SetAsFirstSibling();
            }
        }
        transform.GetChild(0).gameObject.SetActive(false);
    }

}
