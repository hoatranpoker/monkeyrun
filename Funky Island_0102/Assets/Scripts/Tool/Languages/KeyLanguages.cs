﻿
public class KeyLanguages
{
    public const string SHOP_TITLE_SHOP = "SHOP_TITLE_SHOP";
    public const string SHOP_TITLE_ITEM = "SHOP_TITLE_ITEM";
    public const string SHOP_TITLE_UPGRADE = "SHOP_TITLE_UPGRADE";
    public const string SHOP_TITLE_SPECIAL = "SHOP_TITLE_SPECIAL";

    public const string SHOP_UPGRADE_DESC = "Each level increases the duration ";
}
