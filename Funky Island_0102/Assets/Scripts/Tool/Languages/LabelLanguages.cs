﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LabelLanguages : MonoBehaviour
{

    public string KEY;
    public string Value { get { return Language.GetKey(KEY); } } //sau khi get key 
    private Text current_label;

    void Awake()
    {
        current_label = this.gameObject.GetComponent<Text>();
        Language.LoadLanguageFile(Languages.en);
        Language.AddLableLanguage(this);
    }

    void Start()
    {
        if(KEY!=null) SetText();
    }

    //Hàm gán lại text cho label
    public virtual void SetText()
    {
        if (current_label == null)
        {
            Debug.LogError("Null " + this.gameObject.name);
        }
        else
            current_label.text = Language.GetKey(KEY);
    }

    //Lúc chuyển scene sẽ remove hết list
    void OnDestroy()
    {
        Language.RemoveLaleLanguage(this);
    }

}
