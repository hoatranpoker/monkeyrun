﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingSceneManager : MonoBehaviour {

    public static LoadingSceneManager Instance;
	
    public static void LoadingScene(SCENE_TYPE loadScene)
    {
        Modules.oldScene = SceneManager.GetActiveScene().name;
        Time.timeScale = 1;
        switch (loadScene)
        {
            case SCENE_TYPE.Boot:
                SceneManager.LoadScene(Key_Const.Boot);
                break;
            case SCENE_TYPE.Menu:
                SceneManager.LoadScene(Key_Const.Menu);
                break;
            case SCENE_TYPE.Shop:
                SceneManager.LoadScene(Key_Const.Shop);
                break;
            case SCENE_TYPE.Play:
                SceneManager.LoadScene(Key_Const.Play);
                break;
            case SCENE_TYPE.Chapter:
                SceneManager.LoadScene(Key_Const.Chapter);
                break;
            case SCENE_TYPE.Loading:
                SceneManager.LoadScene(Key_Const.Loading);
                break;
            case SCENE_TYPE.Tutorial:
                SceneManager.LoadScene(Key_Const.Tutorial);
                break;
        }
     
    }

    public static void LoadingShopFromAddCoin(PAGE_SHOP type= PAGE_SHOP.Pack)
    {
        if(SceneManager.GetActiveScene().name != Key_Const.Shop)
        {
            Modules.oldScene = SceneManager.GetActiveScene().name;
        }
        else
        {
            Modules.oldScene = Key_Const.Menu;
        }

        Time.timeScale = 1;
        DataManager.Instance.pageShopStart = type;
        LoadingScene(SCENE_TYPE.Shop);
    }
}

public enum SCENE_TYPE { Boot,Menu,Play,Shop,Achivement,Chapter,Loading,Tutorial}