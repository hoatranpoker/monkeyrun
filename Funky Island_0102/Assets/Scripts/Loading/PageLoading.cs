﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class PageLoading : MonoBehaviour {

    public Slider sliderProcess;
    public string NameSecene="Play";
    public Text txtloading;
    private AsyncOperation asyncLoad;

    private void Start () {
        if (DataManager.Instance.loadSeceneTutorial) NameSecene = Key_Const.Tutorial;
        txtloading.DOText("...", 0.2f).SetLoops(-1);
        StartCoroutine(LoadYourAsyncScene());
        //SceneManager.LoadScene("Play");
    }

    IEnumerator LoadYourAsyncScene()
    {
        asyncLoad = SceneManager.LoadSceneAsync(NameSecene,LoadSceneMode.Single);
        asyncLoad.allowSceneActivation = false;
        sliderProcess.DOValue(1.2f, 5f).OnUpdate(()=> {
            if (sliderProcess.value == 1)
            {
                asyncLoad.allowSceneActivation = true;
            }
        }).OnComplete(() => { asyncLoad.allowSceneActivation = true; });
       
        yield return null;
       
    }

}
