﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingSetMap : MonoBehaviour {

    [SerializeField] private Sprite[] ListBG;
    [SerializeField] private Image imgBG;


	private void Awake()
    {
        if (Modules.NumberPageLoading < ListBG.Length-1)
        {
            Modules.NumberPageLoading++;
        }
        else
        {
            Modules.NumberPageLoading = 0;
        }

        imgBG.sprite = ListBG[Modules.NumberPageLoading];

    }
}
