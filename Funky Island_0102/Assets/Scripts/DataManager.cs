﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;

public class DataManager : MonoBehaviour {

    #region Singleton and Start
    public static DataManager Instance;
    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
       
    }

    #endregion

    #region Config
    [Header("Model")]
    public ShopModel shopModel;
    public AchivementModel achivementModel;

    [Header("Fonts")]
    public Font fontMain; //danh cho title
    public Font fontSub; // dung de viet 

    [HideInInspector]
    public bool BonusAchivement = false;  //check xem da nhan bonus achivement
    [HideInInspector]
    public bool pageShop=false; //check xem co o page shop ko

    [HideInInspector]
    public bool Rate=false; //vote star on CH PLAY
    public string LinkAppAndroid="";
    public string LinkAppIOS="";

    [HideInInspector]
    public bool loadSeceneTutorial=false;
    [HideInInspector]
    public bool chapterTutorial = false;
    [HideInInspector]
    public int numberTutorial = 0;
    [HideInInspector]
    public bool OnShowTutorial;
    [HideInInspector]
    public bool Special = false;
    [HideInInspector]
    public bool IAPBuying = false;

    [HideInInspector]
    public GAME_FINISH game_Finish;

    [Header("Ads")]
    public AdMobManager adMobManager;
    public Sprite sprAdsOn;
    public Sprite sprAdsOff;

    #endregion

    #region Main

    private void Start()
    {
        Modules.GetSaveData();
        Modules.myFBController = this.gameObject;
        Modules.myAdController = this.gameObject;
        if (Modules.totalCoin != 0)
            PlayerPrefs.SetString(SaveID.Tutorial, SaveID.True);
        SetBonusNewDay();
        SetDataWhenGameStart();

        if (!adMobManager)adMobManager=this.GetComponent<AdMobManager>();
        canShowAds = true;
    }

    //Quit Game
    private void OnApplicationQuit()
    {
        //Save 
        PlayerPrefs.SetString(SaveID.TimeQuitGame, DateTime.Now.ToString());

        //Event Log
        EventSesiontimeOnday();
    }

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.Alpha1))
    //    {
    //        Debug.LogError(PlayerPrefs.GetString("TaskData"));
    //    }
    //}

    #endregion

    #region Data ShopModel

    public PAGE_SHOP pageShopStart=PAGE_SHOP.Item; // page shop when secene shop start

    //Shop Item
    public int LoadData_TotalShopItem(TypeItemBuy type)
    {
        return shopModel.shop_Items[(int)type].having;
    }

    public void Setdata_TotalShopItem(TypeItemBuy type, int number)
    {
        shopModel.shop_Items[(int)type].having = number;
    }

    //Shop Upgrade
    public int LoadData_LevelItemUpgrade(TypeItemUpgrade type)
    {
        return shopModel.shop_Upgrades[(int)type].levelCurent;
    }

    public void Setdata_LevelItemUpgrade(TypeItemUpgrade type,int number)
    {
        shopModel.shop_Upgrades[(int)type].levelCurent = number;
    }

    #endregion

    #region ChageText
    public void ChageText(string TextChar, string old, string newText)
    {
        TextChar.Replace(old, newText);
    }

    #endregion

    #region Game Start

    public void SetBonusNewDay()
    {
        //ClearData
        if (timeNow().Day != LastDay())
        {
            PlayerPrefs.SetString(SaveID.FirstOpenAppOnDay,SaveID.True);
            PlayerPrefs.SetString(SaveID.FreeSpin,SaveID.True);
            PlayerPrefs.SetString(SaveID.Reward, SaveID.True);
            PlayerPrefs.SetString(SaveID.SpecialSaled,SaveID.True);
            PlayerPrefs.SetString(SaveID.TimeQuitSpin,timeNow().ToString());
            PlayerPrefs.SetString(SaveID.TimeSpecialCDQuitGame,timeNow().ToString());
            PlayerPrefs.SetString(SaveID.canCDSpecial, SaveID.True);
            PlayerPrefs.SetInt(SaveID.LastDayReward, timeNow().Day);
            PlayerPrefs.SetInt(SaveID.TimeOnDay, 0);
            PlayerPrefs.SetInt(SaveID.NumberTimeUserPlayOnDay, 1);

            if (PlayerPrefs.GetString(SaveID.ClearDaily) == SaveID.True|| PlayerPrefs.GetString(SaveID.Day_7) == SaveID.True) ClearDaily();
        }
    }

    public void SetDataWhenGameStart()
    {
        PlayerPrefs.SetString(SaveID.TimeOpenGame, timeNow().ToString());

        if (PlayerPrefs.GetString(SaveID.ClearCoins_V18)!=SaveID.True)
        {
            if(Modules.totalCoin >=15000) Modules.totalCoin = 15000;
            PlayerPrefs.SetString(SaveID.ClearCoins_V18, SaveID.True);
        }
    }

    public void ClearDaily()
    {
        PlayerPrefs.SetString(SaveID.Day_1, SaveID.False);
        PlayerPrefs.SetString(SaveID.Day_2, SaveID.False);
        PlayerPrefs.SetString(SaveID.Day_3, SaveID.False);
        PlayerPrefs.SetString(SaveID.Day_4, SaveID.False);
        PlayerPrefs.SetString(SaveID.Day_5, SaveID.False);
        PlayerPrefs.SetString(SaveID.Day_6, SaveID.False);
        PlayerPrefs.SetString(SaveID.Day_7, SaveID.False);
        PlayerPrefs.SetString(SaveID.ClearDaily, SaveID.False);
        PlayerPrefs.SetInt(SaveID.NumberReward, 0);

    }

    #endregion

    #region Time Manager

    public DateTime timeNow()
    {
        return DateTime.Now;
    }

    public int LastDay()
    {
        return PlayerPrefs.GetInt(SaveID.LastDayReward);
    }

    private int TimeOnDay()
    {
        int tmp = PlayerPrefs.GetInt(SaveID.TimeOnDay) + (int)timePlayOneGame();
        return tmp;
    }

    private double timePlayOneGame()
    {
        double second = Convert.ToDateTime(timeNow()).Subtract(Convert.ToDateTime(PlayerPrefs.GetString(SaveID.TimeOpenGame))).TotalSeconds;
        return second;
    }

    #endregion

    #region Show ads distance 1m

    [HideInInspector]
    private bool canShowAds = true;

    public void ShowAdsInter() {
        if (Modules.HaveInternet() && canShowAds)
        {
            AdMobManager.Instance.showInterstitial();
            canShowAds = false;
            DG.Tweening.DOVirtual.DelayedCall(60f, () => {canShowAds = true; });
        }
    }

    #endregion

    #region LogEventSesiontimeOnday
    public void EventSesiontimeOnday()
    {
        FirebaseManager.Instance.LogUserLevel();
        ReturnTimeOnDay();
        ReturnTimeOneGame();
        ReturnNumberOpenGameOnday();
    }
    
    private void ReturnTimeOnDay()
    {
        string strName = "";
        if (TimeOnDay() < 300)
        {
            strName = "Under_5m";
        }
        else if (300 <= TimeOnDay() && TimeOnDay() < 600)
        {
            strName = "Over_5m";
        }
        else if (600 <= TimeOnDay() && TimeOnDay() < 900)
        {
            strName = "Over_10m";
        }
        else if (900 <= TimeOnDay() && TimeOnDay() < 1800)
        {
            strName = "Over_15m";
        }
        else if (1800 <= TimeOnDay() && TimeOnDay() < 3600)
        {
            strName = "Over_30m";
        }
        else if (3600 <= TimeOnDay())
        {
            strName = "Over_1h";
        }
        PlayerPrefs.SetInt(SaveID.TimeOnDay, TimeOnDay());
        FirebaseManager.Instance.LogEventSessionTime_On_day(strName);

    }

    private void ReturnTimeOneGame()
    {
        string strName = "";

        double second = Convert.ToDateTime(timeNow()).Subtract(Convert.ToDateTime(PlayerPrefs.GetString(SaveID.TimeOpenGame))).TotalSeconds;
        if (second < 300)
        {
            strName = "Under_5m";
        }
        else if (300 <= second && second < 600)
        {
            strName = "Over_5m";
        }
        else if (600 <= second && second < 900)
        {
            strName = "Over_10m";
        }
        else if (900 <= second && second < 1800)
        {
            strName = "Over_15m";
        }
        else if (1800 <= second && second < 3600)
        {
            strName = "Over_30m";
        }
        else if (3600 <= second)
        {
            strName = "Over_1h";
        }

        FirebaseManager.Instance.LogEventSessionTime_One_Game(strName);
    }

    private void ReturnNumberOpenGameOnday()
    {
        int number = PlayerPrefs.GetInt(SaveID.NumberTimeUserPlayOnDay);
        PlayerPrefs.SetInt(SaveID.NumberTimeUserPlayOnDay, number+1);
        FirebaseManager.Instance.LogEventSession_Open_Game(number.ToString());
    }

    #endregion

}

public enum GAME_FINISH
{
    None,
    Lose,
    Victory,
    RunEnless
}