﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTutorial : MonoBehaviour {

    public static MapTutorial Instance;

    private void Awake()
    {
        if (Instance == null) Instance = this;
    }

    public TUTORIAL_STATE State;
    public TUTORIAL_TYPE tutType;

    public InputManager inputManager;

    private string txtTutInfo="";

    private void Start () {
        PlayerPrefs.SetString(SaveID.Tutorial, SaveID.True);
        DataManager.Instance.loadSeceneTutorial = false;
        inputManager = PlayerController.Instance.GetComponent<InputManager>();
        FirebaseManager.Instance.LogEventStartTutorial();
        //reset
        tutDown = false;
        tutJump = false;
        tutSuft = false;
        tutJumpAndHold = false;
        tutJumpAnd=false;
        canHold = false;
        PlayerController.Instance.rhinoTime = 5f;
    }

	private void Update () {

        if (State == TUTORIAL_STATE.Running)
        {
            if(tutType == TUTORIAL_TYPE.UseProtect)
            {
                CheckTut_Protect();
            }
            else if (tutType == TUTORIAL_TYPE.UseMagnet)
            {
                CheckTut_Magnet();
            }
            else
            {
                if (inputManager.isPointerClick)
                    PagePlay.Instance.CLoseTutorial();
                switch (tutType)
                {
                    case TUTORIAL_TYPE.Jump:
                        CheckTut_Jump();

                        break;
                    case TUTORIAL_TYPE.JumpAndHold:
                        CheckTut_JumpAndHold();

                        break;
                    case TUTORIAL_TYPE.Down:
                        CheckTut_Down();

                        break;
                    case TUTORIAL_TYPE.Suft:
                        CheckTut_Suft();

                        break;
                   
                }
            }
        }
    }

    public void CheckTutorial(TUTORIAL_TYPE _type,TUTORIAL_STATE _state)
    {
        this.tutType = _type;
        this.State = _state;

        if (_type!=TUTORIAL_TYPE.Suft
            &&_type != TUTORIAL_TYPE.Jump 
            && _type != TUTORIAL_TYPE.JumpAndHold) PagePlay.Instance.ShowTutorial(_type);

    }

    #region Jump
    [HideInInspector]
    public bool tutJump = false;
    public void CheckTut_Jump()
    {
        if (!tutJump && PlayerController.Instance.playerStates == PlayerStates.Run)
        {
            txtTutInfo = "Press on screen to jump!";
            PagePlay.Instance.SetTutInfo(txtTutInfo);
            PagePlay.Instance.ShowTutorial(TUTORIAL_TYPE.Jump);
            tutJump = true;
        }

        if (PlayerController.Instance.playerStates == PlayerStates.Jump && PlayerController.Instance.transform.position.y > -1)
        {
            State = TUTORIAL_STATE.Complete;
            PagePlay.Instance.CLoseTutorial();
            
        }
    }

    #endregion

    #region Down
    [HideInInspector]
    public bool tutDown = false;
    public void CheckTut_Down()
    {
        if (tutDown)
        {
            State = TUTORIAL_STATE.Complete;
            PagePlay.Instance.CLoseTutorial();
        }
        else
        {
            txtTutInfo = "Slide downward to jump down!";
            PagePlay.Instance.SetTutInfo(txtTutInfo);
            PagePlay.Instance.ShowTutorial(TUTORIAL_TYPE.Down);
        }
    }

    #endregion

    #region JumpAndHold
    [HideInInspector]
    public bool tutJumpAndHold = false;
    public bool tutJumpAnd = false;
    private bool canHold=false;
    public void CheckTut_JumpAndHold()
    {
        if (tutJumpAndHold)
        {
            State = TUTORIAL_STATE.Complete;
            PagePlay.Instance.CLoseTutorial();
            return;
        }

        if (inputManager.isPointerClick &&canHold)
        {
            PagePlay.Instance.CLoseTutorial();
            return;
        }

        if (tutJumpAnd&&!canHold)
        {
            txtTutInfo = "Press on screen to jump!";
            PagePlay.Instance.SetTutInfo(txtTutInfo);
            PagePlay.Instance.ShowTutorial(TUTORIAL_TYPE.Jump);
            canHold = true;
        }

        if (PlayerController.Instance.playerStates == PlayerStates.Jump && PlayerController.Instance.transform.position.y > -1.3f&& canHold)
        {
            txtTutInfo = "Hold to glide !";
            PagePlay.Instance.SetTutInfo(txtTutInfo);
            PagePlay.Instance.ShowTutorial(TUTORIAL_TYPE.JumpAndHold);
        }

    }

    #endregion

    #region Suft
    [HideInInspector]
    public bool tutSuft = false;
    public void CheckTut_Suft()
    {
        if (tutSuft)
        {
            State = TUTORIAL_STATE.Complete;
            PagePlay.Instance.CLoseTutorial();
        }
        else if(PlayerController.Instance.canbeSuft)
        {
            txtTutInfo = "Press and prolonged to glide !";
            PagePlay.Instance.SetTutInfo(txtTutInfo);
            PagePlay.Instance.ShowTutorial(TUTORIAL_TYPE.Suft);
        }

    }

    #endregion

    #region Protect
    [HideInInspector]
    public bool tutProtect = false;
    public void CheckTut_Protect()
    {
        if (tutProtect)
        {
            State = TUTORIAL_STATE.Complete;
            PagePlay.Instance.CLoseTutorial();
        }
        else
        {
            txtTutInfo = "Use Protect for life";
            PagePlay.Instance.SetTutInfo(txtTutInfo);
            Time.timeScale = 0;
        }
    }


    #endregion

    #region
    [HideInInspector]
    public bool tutMagnet = false;
    public void CheckTut_Magnet()
    {
        if (tutMagnet)
        {
            State = TUTORIAL_STATE.Complete;
            PagePlay.Instance.CLoseTutorial();
        }
        else
        {
            txtTutInfo = "Use Magnet for attracts bananas to you";
            PagePlay.Instance.SetTutInfo(txtTutInfo);
            Time.timeScale = 0;
        }
    }
    #endregion

}

public enum TUTORIAL_TYPE
{
    None,
    Jump,
    JumpAndHold,
    Down,
    Suft,
    UseProtect,
    UseMagnet,
    Complete
}

public enum TUTORIAL_STATE
{
    None,
    Running,
    Complete,
    FinishTutorial
}