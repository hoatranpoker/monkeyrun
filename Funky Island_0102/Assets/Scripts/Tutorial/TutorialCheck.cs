﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialCheck : MonoBehaviour {

    public GROUND_TUT_TYPE type_Ground;
    public TUTORIAL_TYPE type_Tut;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            switch (type_Ground)
            {
                case GROUND_TUT_TYPE.Start:
                    MapTutorial.Instance.CheckTutorial(type_Tut, TUTORIAL_STATE.Running);
                   
                    break;
                    
                case GROUND_TUT_TYPE.End:
                    switch (type_Tut)
                    {
                        case TUTORIAL_TYPE.JumpAndHold:
                            MapTutorial.Instance.tutJumpAndHold = true;
                            break;
                    }  
                    break;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if(type_Ground == GROUND_TUT_TYPE.Start)
            {
                switch (type_Tut)
                {
                    case TUTORIAL_TYPE.Jump:
                        MapTutorial.Instance.CheckTutorial(type_Tut, TUTORIAL_STATE.Running);
                        break;
                    case TUTORIAL_TYPE.JumpAndHold:
                        if (PlayerController.Instance.isGrounded)
                            MapTutorial.Instance.tutJumpAnd = true;
                        break;
                }
            }
            
        }
    }

}

public enum GROUND_TUT_TYPE { None,Start,End,Complete}