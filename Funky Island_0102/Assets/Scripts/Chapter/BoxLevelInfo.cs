﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BoxLevelInfo : MonoBehaviour {

    public Text textTitle;
    public GameObject containText;

    public void ButCloseClick()
    {
        transform.GetComponent<Animator>().Play("MessageBoxClose", 0, 0f);
        SoundController.Instance.PlayFX();
    }

    public void ButPlayClick()
    {
        Modules.runEndless = false;
        SceneManager.LoadScene("Loading");
        SoundController.Instance.PlayFX();
    }
}
