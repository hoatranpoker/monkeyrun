﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PageChapter : MonoBehaviour {

    public static PageChapter Instance;

    private void Awake()
    {
        if (Instance == null) Instance = this;
    }

    public GameObject boxLevelWar;
    public GameObject containLevel;
    public GameObject PanelMask;
    public GameObject Tutorial;
    private int levelSelect = 1;
    private int pageSelect = 1;
    private int maxPageLevel = 1;
    //xu ly pageList
    public GameObject containList;

    private void Start()
    {
        if (DataManager.Instance.chapterTutorial)
        {
            Tutorial.SetActive(true);
            DataManager.Instance.chapterTutorial = false;
        }
        CheckShowPopupRate();
        adMobNow = DataManager.Instance.gameObject.GetComponent<AdMobManager>();
        btnWatch.SetUpEvent(ShowAds);
        boxLevelWar.SetActive(false);
        //xu ly phan trang
        levelSelect = 1;//luon tim o vi tri level cuoi cunng da hoan thanh
        for (int i = 0; i < Modules.dataLevel.Count; i++)
        {
            if (Modules.dataLevel[i] == -1)
            {
                levelSelect = i;
                break;
            }
        }
        pageSelect = Mathf.CeilToInt((float)levelSelect / (float)Modules.numLevelPerPage);//luon bat dau tu 1
        maxPageLevel = Mathf.CeilToInt((float)Modules.totalLevel / (float)Modules.numLevelPerPage);
        CreateListPageIcon();
        UpdateListLevel();
        SetListItem();
    }

    #region Popup Rate
    [Header("Popup Rate")]
    public GameObject PopupRate;
    public UIButton btnExit_Rate;
    public UIButton btnOk_Rate;
    private void CheckShowPopupRate()
    {
        if (DataManager.Instance.Rate&&!Modules.rateApp)
        {
            PopupRate.SetActive(true);
            DataManager.Instance.Rate = false;
        }
        btnExit_Rate.SetUpEvent(CloseRatePopup);
        btnOk_Rate.SetUpEvent(ActionGoRate);
    }

    private void CloseRatePopup()
    {
        PopupRate.SetActive(false);
        SoundController.Instance.PlayFX();
    }

    private void ActionGoRate()
    {
        SoundController.Instance.PlayFX();
#if UNITY_IOS
       Application.OpenURL("itms-apps:itunes.apple.com/app/crossy-road-endless-arcade/"+DataManager.Instance.LinkAppIOS);
 
#elif UNITY_ANDROID
        Application.OpenURL("http://play.google.com/store/apps/details?id="+DataManager.Instance.LinkAppAndroid);
#else
        Application.OpenURL("http://play.google.com/store/apps/details?id="+DataManager.Instance.LinkAppAndroid);

#endif
        Modules.rateApp = true;
        SaveLoad.SaveRateApp();
        Modules.totalCoin += 1000;
        SaveLoad.SaveTotalCoin();
        FirebaseManager.Instance.LogEventEarnBanana("RateApp");
        PopupRate.SetActive(false);
    }

    #endregion

    #region Action

    private void CreateListPageIcon()
    {
        for (int i = 1; i < maxPageLevel; i++)
        {
            GameObject newIcon = Instantiate(containList.transform.GetChild(0).gameObject, containList.transform);
        }
        UpdatePageSelectIcon();
    }

    private void UpdatePageSelectIcon()
    {
        for (int i = 0; i < maxPageLevel; i++)
        {
            if (i == (pageSelect - 1)) containList.transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
            else containList.transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
        }
    }

    private void UpdateListLevel()
    {
        int totalLevelInPage = Modules.numLevelPerPage;
        if (pageSelect == maxPageLevel)//neu la page cuoi cung
            totalLevelInPage = Modules.totalLevel - ((pageSelect - 1) * Modules.numLevelPerPage);
        //install danh sach level o trang hien tai
        GameObject temp = containLevel.transform.GetChild(0).GetChild(0).gameObject;
        for (int i = 1; i < containLevel.transform.childCount; i++)
            if (containLevel.transform.GetChild(i).childCount > 0)
                Destroy(containLevel.transform.GetChild(i).GetChild(0).gameObject);
        for (int i = 0; i < totalLevelInPage; i++)
        {
            GameObject objLevel = temp;
            if (i > 0)
                objLevel = Instantiate(temp, containLevel.transform.GetChild(i).transform);
            objLevel.GetComponent<ButtonChapter>().codeLevel = (pageSelect - 1) * Modules.numLevelPerPage + (i + 1);
            objLevel.GetComponent<ButtonChapter>().levelControll = transform.gameObject;
            objLevel.GetComponent<ButtonChapter>().CallStart();
        }
        containLevel.transform.parent.GetComponent<Animator>().SetTrigger("TriPlay");
    }

    public void ButHomeClick()
    {
        LoadingSceneManager.LoadingScene(SCENE_TYPE.Menu);
        SoundController.Instance.PlayFX();
    }

    public void ButPurchaseClick()
    {
        LoadingSceneManager.LoadingShopFromAddCoin();
    }

    public void ButShopClick()
    {
        Modules.oldSceneShop = SceneManager.GetActiveScene().name;
        LoadingSceneManager.LoadingScene(SCENE_TYPE.Shop); 
        SoundController.Instance.PlayFX();
    }

    public void ButNextClick()
    {
        pageSelect++;
        if (pageSelect > maxPageLevel)
            pageSelect = 1;
        UpdateListLevel();
        UpdatePageSelectIcon();
        SoundController.Instance.PlayFX();
    }

    public void ButBackClick()
    {
        pageSelect--;
        if (pageSelect < 1)
            pageSelect = maxPageLevel;
        UpdateListLevel();
        UpdatePageSelectIcon();
        SoundController.Instance.PlayFX();
    }

    public void ButLevelClick(int level)
    {
        Modules.levelRun = level;
        if (Modules.dataLevel.Count <= 0) return;

        //Test
        //Modules.dataLevel[level - 1] = 1;

        if (Modules.dataLevel[level - 1] == -1)
        {
            boxLevelWar.SetActive(true);
            if (Modules.listTextFont.Count > 0)
            {
                boxLevelWar.GetComponent<BoxLevelWar>().textTitle.font = Modules.listFont[Modules.listTextFont[15].indexFont];
                boxLevelWar.GetComponent<BoxLevelWar>().textTitle.text = Modules.listTextFont[15].textValue.Replace("[?]", level.ToString());
                boxLevelWar.GetComponent<BoxLevelWar>().textContent.font = Modules.listFont[Modules.listTextFont[16].indexFont];
                boxLevelWar.GetComponent<BoxLevelWar>().textContent.text = Modules.listTextFont[16].textValue.Replace("[?]", (level - 1).ToString());
            }
            boxLevelWar.GetComponent<Animator>().Play("MessageBoxOpen", 0, 0f);
        }
       
        SoundController.Instance.PlayFX();
    }

    public void Level_Select(int lv)
    {
        Modules.levelRun = lv;
        if (Modules.dataLevel.Count <= 0) return;
        if (Modules.dataLevel[lv - 1] == -1)
        {
            boxLevelWar.SetActive(true);
            if (Modules.listTextFont.Count > 0)
            {
                boxLevelWar.GetComponent<BoxLevelWar>().textTitle.font = Modules.listFont[Modules.listTextFont[15].indexFont];
                boxLevelWar.GetComponent<BoxLevelWar>().textTitle.text = Modules.listTextFont[15].textValue.Replace("[?]", lv.ToString());
                boxLevelWar.GetComponent<BoxLevelWar>().textContent.font = Modules.listFont[Modules.listTextFont[16].indexFont];
                boxLevelWar.GetComponent<BoxLevelWar>().textContent.text = Modules.listTextFont[16].textValue.Replace("[?]", (lv - 1).ToString());
            }
            boxLevelWar.GetComponent<Animator>().Play("MessageBoxOpen", 0, 0f);
        }
        else
        {
            Modules.runEndless = false;
            PanelMask.SetActive(true);
            PanelMask.GetComponent<Animator>().SetTrigger("Close");
            //SceneManager.LoadScene(Key_Const.Play);
            SceneManager.LoadScene(Key_Const.Loading);
        }
        SoundController.Instance.PlayFX();

    }

    #endregion

    #region Ads 
    [Header("Watch Ads")]
    [SerializeField] private UIButton btnWatch;
    private AdMobManager adMobNow;

    private void ShowAds()
    {
        if (!adMobNow.VideoAdLoaded())
        {
            adMobNow.loadRewardVideo();
            return;
        }
        if (Modules.myAdController != null)
        {
            Modules.typeRewardVideo = type_RewardAds.Goldx200;
            adMobNow.showRewardVideo();
            Modules.pauseTimeSave = true;
          
        }
        SoundController.Instance.PlayFX();
    }

    private Coroutine fakeUpdate;
    private IEnumerator RunFakeUpdate()
    {
        while (true)
        {
            float timeStep = 0.03f;
            if (btnWatch != null)
            {
                if (adMobNow.VideoAdLoaded())
                {
                    btnWatch.GetComponent<Image>().sprite = DataManager.Instance.sprAdsOn;
                }
                else
                {
                    btnWatch.GetComponent<Image>().sprite = DataManager.Instance.sprAdsOff;
                }
            }
            yield return new WaitForSecondsRealtime(timeStep);
        }
    }

    #endregion

    #region
    public List<GameObject> ListItem;
    public void SetListItem()
    {
        for(int i = 0; i < ListItem.Count; i++)
        {
            ListItem[i].transform.GetChild(1).GetComponent<Text>().text = "x"+DataManager.Instance.shopModel.shop_Items[i].having;
        }
    }
    #endregion
}
