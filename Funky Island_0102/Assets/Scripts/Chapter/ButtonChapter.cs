﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonChapter : MonoBehaviour {

    //[HideInInspector]
    public int codeLevel = 1;
    [HideInInspector]
    public GameObject levelControll;
    public Text levelName;
    public GameObject starContain;
    public Sprite bgEnable, bgDisable;
    public Image butClick;

    public UIButton btnSelectLevel;

    public void CallStart()
    {
        btnSelectLevel.SetUpEvent(ButLevelClick);
        if (Modules.dataLevel.Count <= 0) return;
        levelName.text = "Level " + codeLevel.ToString();
        if (Modules.listTextFont.Count > 0)
        {
            levelName.font = Modules.listFont[Modules.listTextFont[15].indexFont];
            levelName.text = Modules.listTextFont[15].textValue.Replace("[?]", codeLevel.ToString());
        }
        for (int i = 0; i < starContain.transform.childCount; i++)
        {
            if (Modules.dataLevel[codeLevel - 1] <= 0)
            {
                starContain.gameObject.SetActive(false);
                break;
            }
            else
            {
                starContain.gameObject.SetActive(true);
                if (i < Modules.dataLevel[codeLevel - 1])
                    starContain.transform.GetChild(i).GetChild(0).gameObject.SetActive(true);
                else starContain.transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
            }
        }
        if (Modules.dataLevel[codeLevel - 1] == -1)//disable button
            butClick.sprite = bgDisable;
        else butClick.sprite = bgEnable;
    }

    public void ButLevelClick()
    {
        PageChapter.Instance.Level_Select(codeLevel);

        //SoundController.Instance.PlayFX();
    }
}
