﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxLevelWar : MonoBehaviour {

    public Text textTitle;
    public Text textContent;

    public void ButCloseClick()
    {
        transform.GetComponent<Animator>().Play("MessageBoxClose", 0, 0f);
    }
}
