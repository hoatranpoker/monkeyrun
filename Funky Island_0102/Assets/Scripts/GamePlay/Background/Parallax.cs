﻿using UnityEngine;

public class Parallax : MonoBehaviour {
    public Transform[] backgrounds;
    float[] parallaxScales;
    public float smooth;
    Vector3 lastCamPos;
    private void Start()
    {
        lastCamPos = transform.position;
        parallaxScales = new float[backgrounds.Length];
        for (int i = 0; i < parallaxScales.Length; i++)
            parallaxScales[i] = -backgrounds[i].position.z;
    }

    private void LateUpdate()
    {
        for(int i = 0; i< backgrounds.Length; i++)
        {
            Vector3 parallax = (lastCamPos - transform.position) * (parallaxScales[i] / smooth);
            backgrounds[i].position = new Vector3(backgrounds[i].position.x + parallax.x, backgrounds[i].position.y + parallax.y, backgrounds[i].position.z);
        }
        lastCamPos = transform.position;
    }
}
