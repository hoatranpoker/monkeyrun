﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMoveScript : MonoBehaviour {

	public float scrollSpeed = 1f;

    float scrollOffset;
	Vector2 startPos;
	float newPos;
	void Start () {
        NewPos();
	}

    public void NewPos()
    {
        startPos = transform.position;
        scrollOffset = -
            GetComponent<SpriteRenderer>().size.x;
    }
	
	void Update () {
		newPos = Mathf.Repeat (Time.time * - scrollSpeed, scrollOffset);
		transform.position = startPos + Vector2.right * newPos;
	}
}
