﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGPrallax : MonoBehaviour {
    Material material;
    public bool X, Y;
    [Range(-1f, 1f)]
    public float speed;
    public int OrderInLayer = 0;

    private void Start()
    {
        var m = GetComponent<MeshRenderer>();
        if (!m)
        {
            material = GetComponent<SpriteRenderer>().sharedMaterial;
            GetComponent<SpriteRenderer>().sortingOrder = OrderInLayer;
        }
        else
        {
            material = m.sharedMaterial;
            m.sortingOrder = OrderInLayer;
        }
        StartCoroutine(ChangeOffset());
        
    }

    IEnumerator ChangeOffset()
    {
        if (material == null) yield break;
        while (true)
        {
            Vector2 os = material.mainTextureOffset;
            if (X)
            {
                os.x += speed;
                if (speed > 0 && os.x > 1) os.x = 0;
                if (speed < 0 && os.x < -1) os.x = 0;
            }

            if (Y)
            {
                os.y += speed;
                if (speed > 0 && os.y > 1) os.y = 0;
                if (speed < 0 && os.y < -1) os.y = 0;
            }
            material.mainTextureOffset = os;
            yield return new WaitForSeconds(0.01f);
        }
    }

}
