﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour {
    public Sprite[] sprites;

	void Start () {
        CreateNew();
	}
	
	void LateUpdate () {
        if (PlayerController.Instance.transform.position.x == lastPos.x)
            CreateNew();

    }

    Vector2 lastPos;
    float lastSize;
    void CreateNew()
    {
        int r = Random.Range(0, sprites.Length);
        GameObject temp = new GameObject();
        temp.name = transform.parent.name;
        temp.transform.SetParent(transform);
        temp.AddComponent<SpriteRenderer>().sprite = sprites[r];
        
        Vector2 groundOffSet = new Vector2(lastSize * 2f, 0);
        temp.transform.position = (Vector2)transform.position + groundOffSet;
        lastSize = sprites[r].rect.width / 100;
    }
}
