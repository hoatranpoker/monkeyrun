﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSellect : MonoBehaviour {

	// Use this for initialization
	private void Awake () {
        SellectBackground();
    }

    #region BackGround
    [Header("Sellect Background")]
    public GameObject[] Background;
    public void SellectBackground()
    {
        if (Modules.runEndless)
        {
            int tmp = Random.RandomRange(0, Background.Length);
            Debug.LogError("BG : " + tmp);
            Background[tmp].SetActive(true);
        }
        else
        {
            int tmp = Modules.levelRun % 10;
            Debug.LogError("levlel : " + tmp);
            if (0 < tmp && tmp < 5)
            {
                Background[0].SetActive(true);
            }
            else if (5 < tmp && tmp <= 9)
            {
                Background[1].SetActive(true);
            }
            else if (tmp == 0 || tmp == 5)
            {
                Background[2].SetActive(true);
            }
        }

    }

    #endregion
}
