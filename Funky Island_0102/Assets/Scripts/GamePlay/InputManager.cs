﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : EventTrigger
{
    PlayerController player;
    private void Start()
    {
        player = GetComponent<PlayerController>();
        dragSensitivity = 0.15f * Screen.height;
    }

    private void Update()
    {
        if (player.playerStates == PlayerStates.Dead) return;
        if (isPointerClick)
            player.Parachute();
    }
    public float dragSensitivity = 70f;
    Vector2 firstDrag;
    public override void OnBeginDrag(PointerEventData data)
    {
        if (player.playerStates == PlayerStates.Dead) return;
        firstDrag = data.position;
        //isPointerClick = false;
    }

    public override void OnDrag(PointerEventData data)
    {
        if (player.playerStates == PlayerStates.Dead || player.playerStates == PlayerStates.Surf) return;
        if (player.usingFlyingCarpet)
        {
            player.Draging(Camera.main.ScreenToWorldPoint(data.position));
            isPointerClick = false;
            firstDrag = data.position;
            return;
        }

        Vector2 vector = data.position - firstDrag;

        if (Mathf.Abs(vector.x) <= Mathf.Abs(vector.y) && vector.y < 0 && Mathf.Abs(vector.y) >= dragSensitivity)
        {
            if (!PlayerController.Instance.usingBird)
            {
                if (waitJump != null) StopCoroutine(waitJump);
                player.InputDrag(Direction.DOWN);
                isPointerClick = false;
                firstDrag = data.position;
            }
        }
        else if (vector.x > 0 && Mathf.Abs(vector.x) >= dragSensitivity)
        {
            if (waitJump != null) StopCoroutine(waitJump);
            player.InputDrag(Direction.RIGHT);
            isPointerClick = false;
            firstDrag = data.position;
        }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        isPointerClick = false;
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        isPointerClick = false;
    }

    public bool isPointerClick = false;
    public override void OnPointerDown(PointerEventData data)
    {
        if (player.playerStates == PlayerStates.Dead || player.playerStates == PlayerStates.Surf) return;
        isPointerClick = true;
        
        if (waitJump != null) StopCoroutine(waitJump);
        float t = 0.025f;
        if(PlayerController.Instance.usingBird) t = 0f;
        waitJump = StartCoroutine(DelayAction(t, () => {
            if (player.playerStates == PlayerStates.Surf) return;
            player.Jump();
        })); 
    }
    Coroutine waitJump;

    public override void OnPointerUp(PointerEventData data)
    {
        isPointerClick = false;
        if (player.playerStates == PlayerStates.Dead 
            || player.playerStates == PlayerStates.Surf 
            || player.playerStates == PlayerStates.Down 
            || player.isRolling) return;

        if (player.playerStates == PlayerStates.Idle)
        {
            player.Run();
            return;
        }
        player.InputRelease();
    }

    IEnumerator DelayAction(float timeDelay, System.Action action)
    {
        yield return new WaitForSeconds(timeDelay);
        if (action != null) action();
        yield break;
    }
}