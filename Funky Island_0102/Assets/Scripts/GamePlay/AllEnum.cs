﻿public enum PlayerStates
{
    Idle,               //Mặc định idle
    Run,                //chạy  
    Jump,               //nhảy lên
    Parachute,          //bung dù
    Fall,               //rơi xuống
    Down,               //xuống dưới
    Surf,               //lướt
    Dead,               //chết
}

public enum Direction
{
    NONE,
    UP,
    DOWN,
    LEFT,
    RIGHT
}

public enum ItemType
{
    Skateboard,         //VÁN TRƯỢT
    FlyingCarpet,       //THẢM BAY
    Rhino,              //TÊ GIÁC
    Bird,               //CHIM  
    Apple,              //TÁO
    Magnet,             //NAM CHÂM
    Revival,            //HỒI SIM
    Protect,            //BẢO VỆ
    Banana,             //CHUỐI
    BananaBunch,        //NẢI CHUỐI
    Key,                //CHÌA KHÓA
    BOX,                //HỘP ĐẠO CỤ
    X2Banana,           //X2 CHUỐI
    None
}

public enum ObstacleType
{
    Forceground,        //NỀN LƠ LỬNG
    Springs,            //LÒ XO
    Vines,              //DÂY LEO
    WoodBox,            //HỘP GỖ
    Bush,               //BỤI GAI
    Rock,               //CỤC ĐÁ
    StoneColumn,        //CỘT ĐÁ
    River,              //SÔNG - NƯỚC
    Cliff,              //VÁCH NÚI
    BridgeCollapsed,    //CẦU SẬP
    Fire,               //ĐỐNG LỬA
    Spiderweb,          //MẠNG NHỆN
    Switch,             //CÔNG TẮC
    Door                //CỬA
}

public enum EnemyType
{
    Spider,             //NHỆN
    Native,             //THỔ DÂN
    Wave,               //SÓNG
    Mole,               //CHUỘT CHŨI
    Gorilla,               //SÓI
    RollingStone,        //ĐÁ LĂN
    GorillaBack
}