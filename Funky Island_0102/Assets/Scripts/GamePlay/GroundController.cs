﻿using UnityEngine;

public class GroundController : MonoBehaviour
{
    public static GroundController Instance;
    public GameObject firstGround, endGround, parentGround;
    public string[] blockLevel;
    public float[] groundLength;
    private float lastDistance = 0, lastLenght = 28;
    private float lastX = 10;
    private Vector2 lastPos;

    public string[] maps;
    [SerializeField] private string mapString = "";
    private int current = -1;
    [SerializeField] private string[] map;

    int endLessLevel = 0;
    private void Awake()
    {
        if(Instance==null)
            Instance = this;
        if (!Modules.runEndless)
            mapString = MapInitialization(Modules.levelRun - 1);
        else
            mapString = MapInitialization(endLessLevel);
        mapLenght = 0;
        if (mapString != "")
        {
            map = mapString.Split(';');
            current = 0;
            foreach (var m in map)
            {
                int r = -1;
                if (int.TryParse(m, out r))
                {
                    r--;
                    if (r >= 0 && r < groundLength.Length)
                        mapLenght += groundLength[r];
                }
            }
            mapLenght += 28;
        }
        parentGround.SetActive(false);
    }

    private string MapInitialization(int level = -1)
    {
        string res = "";
        if (level >= 0 && level < maps.Length)
        {
            string[] lv = maps[level].Split(';');
            bool haveBox = false;
            if (lv != null && lv.Length > 0)
                for (int i = 0; i < lv.Length; i++)
                {
                    int count = 0;
                    if (lv[i] != "" && int.TryParse(lv[i], out count))
                    {
                        string[] blocks = blockLevel[i].Split(';');
                        string lblock = "";            
                        for (int c = 0; c < count; c++)
                        {
                            string sblock = blocks[Random.Range(0, blocks.Length)];
                            while ((sblock.Contains("b") && (haveBox || res == "")) || lblock == sblock|| res.Contains(sblock))
                                sblock = blocks[Random.Range(0, blocks.Length)];
                            if (sblock.Contains("b")) haveBox = true;
                            res += sblock.Replace("i", "").Replace("b", "") + ";";
                            lblock = sblock;  
                        }
                    }
                }
        }
        return res;
    }

    [HideInInspector]
    public float mapLenght = 0;
    [HideInInspector]
    public float endPos = 0f;
    private void LateUpdate()
    {
        //float distance = Vector2.Distance(lastPos, PlayerController.Instance.transform.position);
        float distance = lastX - PlayerController.Instance.transform.position.x;
        if (distance < Camera.main.orthographicSize)
        {
            int r = Random.Range(0, groundLength.Length);
            if (current != -1)
            {
                int.TryParse(map[current], out r);
                r--;
                current++;
                if (current >= map.Length)
                {
                    if (Modules.runEndless)
                    {
                        endLessLevel++;
                        if (endLessLevel >= groundLength.Length) endLessLevel = groundLength.Length - 1;
                        mapString = MapInitialization(endLessLevel);
                        map = mapString.Split(';');
                        current = 0;
                    }
                    else
                        current = -1;
                }
            }
            if (r < 0 || r >= parentGround.transform.childCount) return;
            GameObject ngr = parentGround.transform.GetChild(r).gameObject;
            float lenght = groundLength[r];
            if (current == -1 && !Modules.runEndless)
            {
                ngr = endGround;
                lenght = 22f;
            }

            if (ngr)
            {
                GameObject temp = Instantiate(ngr, transform, false);
                lastX += (lenght + lastLenght) / 2;
                if (current == -1 && !Modules.runEndless && endPos == 0f)
                    endPos = lastX;
                lastLenght = lenght;
                temp.transform.position = new Vector3(lastX, 0f, 0f);
                lastPos = temp.transform.position;
            }
            if (transform.childCount > 2)
                Destroy(transform.GetChild(0).gameObject);
        }
    }

    public static Direction DetectSide(Collision2D collision)
    {
        var hit = collision.contacts[0].normal;
        float angle = Vector2.Angle(hit, Vector2.right);
        if (Mathf.Approximately(angle, 0))
            return Direction.LEFT;

        if (Mathf.Approximately(angle, 180))
            return Direction.RIGHT;

        if (Mathf.Approximately(angle, 90))
        {
            Vector3 cross = Vector3.Cross(Vector2.right, hit);
            if (cross.y > 0)
                return Direction.UP;
            else
                return Direction.DOWN;
        }

        return Direction.NONE;
    }
}

[System.Serializable]
public class GroundObj
{
    public string ID;
    public GameObject Obj;
    public float Length;
}
