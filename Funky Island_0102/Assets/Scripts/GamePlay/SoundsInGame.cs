﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsInGame : MonoBehaviour {
    public AudioClip[] backClips;
    public AudioClip backSpecialClip;

    public static SoundsInGame Instance;
    [HideInInspector]
    public float volFX;
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        var s = new GameObject();
        s.name = "BackFX";
        s.transform.parent = transform;
        sourceBack = s.AddComponent<AudioSource>();
        ChangeSound();
        SoundController.Instance.ChangeMusic();
    }

    private void OnDestroy()
    {
        SoundController.Instance.ChangeMusic(true);
    }
    [HideInInspector]
    public AudioSource sourceBack;
    private bool isSpecial = true;
    public void ChangeSound(bool isSpecial = false)
    {
        if (this.isSpecial == isSpecial) return;
        this.isSpecial = isSpecial;
        iTween.AudioTo(sourceBack.gameObject, iTween.Hash("audiosource", sourceBack, "volume", 0f, "time", 0.5f));
        StartCoroutine(DelayAction(0.5f, () => {
            if (isSpecial)
                sourceBack.clip = backSpecialClip;
            else
            {
                if(Modules.levelRun%5==0)
                    sourceBack.clip = backClips[3];
                else
                    sourceBack.clip = backClips[Random.RandomRange(0,2)];
            }
            sourceBack.loop = true;
            sourceBack.volume = 0f;
            sourceBack.Play();
            
            iTween.AudioTo(sourceBack.gameObject, iTween.Hash("audiosource", sourceBack, "volume", Modules.musicSetting, "time", 0.5f));
        }));
    }

    public void SoundChangeSpinShow()
    {
        sourceBack.volume = 0.3f;
    }

    private IEnumerator DelayAction(float timeDelay, System.Action action)
    {
        yield return new WaitForSeconds(timeDelay);
        if (action != null) action();
        yield break;
    }
}
