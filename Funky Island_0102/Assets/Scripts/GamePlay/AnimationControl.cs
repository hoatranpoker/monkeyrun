﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class AnimationControl : MonoBehaviour {
    public bool isPlayer = false;
    private void Start()
    {
        if (isPlayer) return;
        if (skeletonAnimation)
            skeletonAnimation.gameObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        if (isPlayer) return;
        if (skeletonAnimation)
        {
            float dis = transform.position.x - PlayerController.Instance.transform.position.x;
            if ((dis <= 15f) && !skeletonAnimation.gameObject.activeInHierarchy)
                skeletonAnimation.gameObject.SetActive(true);
            if(dis <= -5f && skeletonAnimation.gameObject.activeInHierarchy)
                skeletonAnimation.gameObject.SetActive(false);
        }
    }

    public SkeletonAnimation skeletonAnimation;
    string currentAnimation;
    public float Play(string Name, bool loop = true, float speed = 1f, System.Action action = null)
    {
        if (!skeletonAnimation.gameObject.activeInHierarchy) return 0;
        if (Name == currentAnimation)
            return 0;
        var anim = skeletonAnimation.state.SetAnimation(0, Name, loop);
        anim.timeScale = speed;
        currentAnimation = Name;
        float time = anim.animation.Duration / speed;
        if (action != null)
        {
            if (coroutineWaitEndAnimation != null) StopCoroutine(coroutineWaitEndAnimation);
            coroutineWaitEndAnimation = StartCoroutine(DelayAction(time / speed, action));
        }
        return time;
    }

    Coroutine coroutineWaitEndAnimation;
    IEnumerator DelayAction(float time, System.Action action)
    {
        yield return new WaitForSeconds(time);
        if (action != null) action();
        yield break;
    }
}