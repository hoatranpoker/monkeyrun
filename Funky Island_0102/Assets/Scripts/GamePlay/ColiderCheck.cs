﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColiderCheck : MonoBehaviour {

    public Rigidbody2D rigidbodyTarget;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            rigidbodyTarget.bodyType = RigidbodyType2D.Dynamic;
        }
    }


}
