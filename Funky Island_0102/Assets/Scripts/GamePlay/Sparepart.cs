﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sparepart : MonoBehaviour
{
    public ItemType itemType;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        var c = collision.collider.GetComponent<Obstacle>();
        var e = collision.collider.GetComponent<Enemy>();
       //if ((c && c.wasEnable) || (e && e.wasEnable)) return;

        switch (itemType)
        {
            case ItemType.Skateboard:
                if (collision.gameObject.tag == "Ground"
                    || (c && (c.obstacleType == ObstacleType.Rock
                    || c.obstacleType == ObstacleType.StoneColumn
                    || c.obstacleType == ObstacleType.Cliff
                    || c.obstacleType == ObstacleType.Fire)) || (e))
                {
                    if (GroundController.DetectSide(collision) == Direction.RIGHT || e)
                    {
                        if (/*(c && c.obstacleType != ObstacleType.River) ||*/ (e && e.enemyType == EnemyType.RollingStone && !e.wasEnable))
                        {       
                            if (PlayerController.Instance.counterWObstacle > 0f)
                            {
                                if (c) c.PlayerThowHandle();
                                if (e) e.PlayerThowHandle();
                                PlayerController.Instance.UpdateCounterCollision("obstacle");
                                if (PlayerController.Instance.counterWObstacle <= 0f)
                                    PlayerController.Instance.CancelSkateboard(true);
                            }else
                            {
                                if (c && c.obstacleType != ObstacleType.WoodBox)
                                    PlayerController.Instance.CancelSkateboard(false);
                                if (c) c.PlayerThowHandle();
                                if (e) e.PlayerThowHandle();   
                            }
                        }
                        else if (e && !e.wasEnable)
                        {
                            if (PlayerController.Instance.counterWEnemy > 0)
                            {
                                e.PlayerThowHandle();
                                PlayerController.Instance.UpdateCounterCollision("enemy");
                                if (PlayerController.Instance.counterWEnemy <= 0f)
                                    PlayerController.Instance.CancelSkateboard(true);
                            }else
                            {
                                PlayerController.Instance.CancelSkateboard(false);
                                e.PlayerThowHandle();
                            }
                        }else
                            PlayerController.Instance.CancelSkateboard(true);
                    }

                }
                if (collision.gameObject.tag == "Water")
                {
                    gameObject.GetComponent<AnimationControl>().Play("water");
                }else if (collision.gameObject.tag == "Ground")
                {
                    gameObject.GetComponent<AnimationControl>().Play("ground");
                }
                break;

            case ItemType.Rhino:
                if (collision.gameObject.tag == "Water")
                {
                    if (c) c.HandleRiver(collision);
                    if (GroundController.DetectSide(collision) == Direction.DOWN)
                        PlayerController.Instance.CancelRhino(true);                   
                }
                break;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        OnCollisionEnter2D(collision);
    }

    //private void OnCollisionStay2D(Collider2D other)
    //{
    //    switch (itemType)
    //    {
    //        case ItemType.Skateboard:
               
    //            break;
    //    }
    //}

    private void OnTriggerEnter2D(Collider2D collider)
    {
        var c = collider.GetComponent<Obstacle>();
        var e = collider.GetComponent<Enemy>();
        switch (itemType)
        {
            case ItemType.Protect:
                if (c && (c.obstacleType != ObstacleType.BridgeCollapsed
                    && c.obstacleType != ObstacleType.Vines
                    && c.obstacleType != ObstacleType.Springs))
                    c.PlayerThowHandle();

                if (e && e.enemyType == EnemyType.RollingStone)
                    e.PlayerThowHandle();
                break;

            case ItemType.Magnet:
                if (collider.tag == "Item")
                {
                    var i = collider.GetComponent<Item>();
                    if (i) i.flowPlayer = true;
                }
                break;

            case ItemType.Rhino:
                if (c && (c.obstacleType != ObstacleType.BridgeCollapsed
                    && c.obstacleType != ObstacleType.Vines
                    && c.obstacleType != ObstacleType.Springs))
                    c.PlayerThowHandle();

                if (e && !e.wasEnable)
                {
                    if (e.enemyType == EnemyType.RollingStone)
                        e.PlayerThowHandle();
                    else
                    {
                        if (PlayerController.Instance.counterWEnemy > 0)
                        {
                            e.PlayerThowHandle();
                            PlayerController.Instance.UpdateCounterCollision("enemy");
                            if (PlayerController.Instance.counterWEnemy <= 0f)
                                PlayerController.Instance.CancelRhino(true);
                        }
                        else
                        {
                            PlayerController.Instance.CancelRhino(false);
                            e.PlayerThowHandle();
                        }
                    }
                }
                break;

            case ItemType.FlyingCarpet:
                if ((c && (c.obstacleType == ObstacleType.Rock
                    || c.obstacleType == ObstacleType.StoneColumn
                    || c.obstacleType == ObstacleType.Cliff
                    || c.obstacleType == ObstacleType.Fire)) || (e))
                {
                    if (c)
                    {
                        if (PlayerController.Instance.counterWObstacle > 0f)
                        {
                            c.PlayerThowHandle();
                            PlayerController.Instance.UpdateCounterCollision("obstacle");
                            if (PlayerController.Instance.counterWObstacle <= 0f)
                                PlayerController.Instance.CancelFlyingCarpet(true);
                        }
                        else
                        {
                            c.PlayerThowHandle();
                            PlayerController.Instance.CancelFlyingCarpet(false);
                        }
                    }
                    else if (e && !e.wasEnable)
                    {
                        if (PlayerController.Instance.counterWEnemy > 0)
                        {
                            e.PlayerThowHandle();
                            PlayerController.Instance.UpdateCounterCollision("enemy");
                            if (PlayerController.Instance.counterWEnemy <= 0f)
                                PlayerController.Instance.CancelFlyingCarpet(true);
                        }
                        else
                        {
                            PlayerController.Instance.CancelFlyingCarpet(false);
                            e.PlayerThowHandle();
                        }
                    }
                }
                break;

            case ItemType.Skateboard:
                if (collider.tag == "Ground"
                    || (c && (c.obstacleType == ObstacleType.Rock
                    || c.obstacleType == ObstacleType.StoneColumn
                    || c.obstacleType == ObstacleType.Cliff
                    || c.obstacleType == ObstacleType.Fire)) || (e && !e.wasEnable))
                {
                    if (c || e)
                    {
                        if ((c && c.obstacleType != ObstacleType.River) || (e && e.enemyType == EnemyType.RollingStone))
                        {
                            if (PlayerController.Instance.counterWObstacle > 0f)
                            {
                                if (c) c.PlayerThowHandle();
                                if (e) e.PlayerThowHandle();
                                PlayerController.Instance.UpdateCounterCollision("obstacle");
                                if (PlayerController.Instance.counterWObstacle <= 0f)
                                    PlayerController.Instance.CancelSkateboard(true);
                            }
                            else
                            {
                                if ((c && c.obstacleType != ObstacleType.WoodBox) || (e && e.enemyType == EnemyType.RollingStone))
                                    PlayerController.Instance.CancelSkateboard(false);
                                if (c) c.PlayerThowHandle();
                                if (e) e.PlayerThowHandle();
                            }
                        }
                        else if (e)
                        {
                            if (PlayerController.Instance.counterWEnemy > 0)
                            {
                                e.PlayerThowHandle();
                                PlayerController.Instance.UpdateCounterCollision("enemy");
                                if (PlayerController.Instance.counterWEnemy <= 0f)
                                    PlayerController.Instance.CancelSkateboard(true);
                            }
                            else
                            {
                                PlayerController.Instance.CancelSkateboard(false);
                                e.PlayerThowHandle();
                            }
                        }
                        else
                            PlayerController.Instance.CancelSkateboard(true);
                    }

                }
                break;            
        }

    }
}
