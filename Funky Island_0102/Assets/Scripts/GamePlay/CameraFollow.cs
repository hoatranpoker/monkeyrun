﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Vector2 offset;

    private void FixedUpdate()
    {
        if (PlayerController.Instance == null) return;
        if (!PlayerController.Instance.isCompleted)
            iTween.MoveUpdate(gameObject, iTween.Hash("x", PlayerController.Instance.transform.position.x + offset.x, "time", 1f, "easetype", iTween.EaseType.linear));
        else
        {
            if (PagePlay.Instance.TutorialSence) return;
            iTween.MoveUpdate(gameObject, iTween.Hash("x", GroundController.Instance.endPos, "time", 2f, "easetype", iTween.EaseType.linear));
        }
            
    }
}
