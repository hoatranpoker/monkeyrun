﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {
    public ItemType itemType;
    public float m_timeDestroy = 0f;
    public AudioClip soundCash;

    [Header("SHATTERED SETTING")]
    public GameObject fxShattered;
    public float timeDestroy = 0f;

    [Header("ONLY FOR BANANA BUNCH")]
    public GameObject banana;
    public Vector2 Offset;

    [Header("ONLY FOR BOX( Total = 100)")]
    [Range(0, 100)]
    public float abilityBird = 20f;
    [Range(0, 100)]
    public float abilityRhino = 20f;
    [Range(0, 100)]
    public float abilityCarpet = 20f;
    [Range(0, 100)]
    public float abilitySkateboard = 20f;
    [Range(0, 100)]
    public float abilityApple = 20f;

    AnimationControl animator;
    private void Awake()
    {
        animator = GetComponent<AnimationControl>();
    }

    [HideInInspector]
    public bool flowPlayer = false;
    private void FixedUpdate()
    {
        if (flowPlayer)
            iTween.MoveUpdate(gameObject, iTween.Hash("position", PlayerController.Instance.transform.position, "time", 1f, "easetype", iTween.EaseType.easeInSine));

        if (itemType == ItemType.Banana && !wasEnable && animator) {
            if (PlayerController.Instance.usingX2Banana)
                animator.Play("idlex2");
            else
                animator.Play("idle");
        }
    }

    [HideInInspector]
    public bool wasEnable = false;
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(wasEnable) return;
        if (collider.tag == "Player")
        {
            switch (itemType)
            {
                case ItemType.Skateboard:
                    HandleSkateboard();
                    break;

                case ItemType.FlyingCarpet:
                    HandleFlyingCarpet();
                    break;

                case ItemType.Rhino:
                    HandleRhino();
                    break;

                case ItemType.Bird:
                    HandleBird();
                    break;

                case ItemType.Apple:
                    HandleApple();
                    break;

                case ItemType.Magnet:
                    HandleMagnet();
                    break;

                case ItemType.Revival:
                    HandleRevival();
                    break;

                case ItemType.Protect:
                    HandleProtect();
                    break;

                case ItemType.Banana:
                    HandleBanana();
                    break;

                case ItemType.BananaBunch:
                    HandleBananaBunch();
                    break;

                case ItemType.Key:
                    HandleKey();
                    break;

                case ItemType.BOX:
                    HandleBOX();
                    break;

                case ItemType.X2Banana:
                    HandleX2Banana();
                    break;
            }

            if (soundCash)
                AudioSource.PlayClipAtPoint(soundCash, Camera.main.transform.position, Modules.soundSetting);

            if (fxShattered)
            {
                GameObject fx = Instantiate(fxShattered);
                fx.transform.position = transform.position;
                if (timeDestroy > 0)
                    Destroy(fx, timeDestroy);
            }
            Destroy(gameObject, m_timeDestroy);
        }
    }

    void HandleSkateboard()
    {
        PlayerController.Instance.CancelAllTranform();
        PlayerController.Instance.ChangeToSkateboard();
    }

     void HandleFlyingCarpet()
    {
        PlayerController.Instance.CancelAllTranform();
        PlayerController.Instance.ChangeToFlyingCarpet();
    }

    void HandleRhino() {
        PlayerController.Instance.CancelAllTranform();
        PlayerController.Instance.ChangeToRhino();
    }

    void HandleBird()
    {
        PlayerController.Instance.CancelAllTranform();
        PlayerController.Instance.ChangeToBird();
    }

    void HandleApple()
    {
        PlayerController.Instance.CancelAllTranform();
        PlayerController.Instance.ChangeToApple();
    }

    void HandleMagnet()
    {
        PlayerController.Instance.UsingMagnet();
    }

    void HandleRevival()
    {
        PlayerController.Instance.UsingRevival();
    }

    void HandleProtect()
    {
        PlayerController.Instance.UsingProtect();
    }

    void HandleBanana()
    {
        float t = 0;
        wasEnable = true;
        //if (PlayerController.Instance.usingX2Banana)
        //{
        //    iTween.ShakeRotation(gameObject, iTween.Hash("z", 25f, "time", 1f, "space", Space.Self));
        //    iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(2f, 2f, 2f), "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
        //    StartCoroutine(DelayAction(1f, () =>
        //    {
        //        iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.one, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
        //    }));
        //    t = 0.5f;
        //}

        StartCoroutine(DelayAction(t, () =>
        {
            animator.Play("eat", true, 1.5f, () => {
                Destroy(gameObject);
            });
            StartCoroutine(FlyOutPlayer());
            PlayerController.Instance.EatBanana();
        }));
        
    }

    IEnumerator FlyOutPlayer()
    {
        Vector2 start, end;
        float time = 0;
        start = transform.position;
        end = new Vector2(start.x - 5, start.y + 3);
        while (true)
        {
            time += Time.deltaTime;
            Vector3 pos = Vector3.Lerp(start, end, time);
            transform.position = pos;
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    void HandleBananaBunch()
    {
        for(int i = 0; i < 3; i++)
        {
            GameObject temp = Instantiate(banana);
            temp.transform.position = new Vector3(transform.position.x + Offset.x, transform.position.y - Offset.y, transform.position.z);
            iTween.MoveTo(temp, iTween.Hash("x", transform.position.x + Offset.x * (i + 1), "time", 1f));
            Destroy(temp, 5f);
        }
        //PlayerController.Instance.EatBananaBunch();
    }

    void HandleKey()
    {
        PlayerController.Instance.haveKey = true;
    }

    void HandleBOX()
    {
        //PlayerController.Instance.haveBox = true;
        float r = Random.value * 100;
        if (r < abilityBird)
            HandleBird();
        else if (r < abilityBird + abilityRhino)
            HandleRhino();
        else if (r < abilityBird + abilityRhino + abilityCarpet)
            HandleFlyingCarpet();
        else if (r < abilityBird + abilityRhino + abilityCarpet + abilitySkateboard)
            HandleSkateboard();
        else if (r <= abilityBird + abilityRhino + abilityCarpet + abilitySkateboard + abilityApple)
            HandleApple();
    }

    void HandleX2Banana()
    {
        if (wasEnable) return;
        wasEnable = true;
        PlayerController.Instance.UsingX2Banana();
        iTween.ShakeRotation(gameObject, iTween.Hash("z", 25f, "time", 1f, "space", Space.Self));
        iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(2f, 2f, 2f), "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
        StartCoroutine(DelayAction(1f, () => {
            iTween.ScaleTo(gameObject, iTween.Hash("scale", Vector3.zero, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
        }));
    }

    IEnumerator DelayAction(float timeDelay, System.Action action)
    {
        yield return new WaitForSeconds(timeDelay);
        if (action != null) action();
        yield break;
    }
}
