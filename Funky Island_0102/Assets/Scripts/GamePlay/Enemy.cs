﻿///Scrip này xử lý tất cả đối tượng Enemy xuất hiện trong game.
///Mỗi đối tượng cần cài đặt riêng, các đối tượng có khả năng di chuyển cần cài đặt tốc độ riêng.
///
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public EnemyType enemyType;
    public static Enemy Wave;

    [Tooltip("Tốc độ tiến đến")]
    public float speedRun;

    /// <summary>
    /// Khoảng cách kích hoạt chức năng 
    /// Mặc định Gorilla, Wave được ẩn
    /// Khi player tiến đến khoảng cách cài đặt sẽ kích hoạt khả năng truy đuổi
    /// </summary>
    [Tooltip("Khoảng cách kích hoạt chức năng so với Player")]
    public float activeDistance;
    [Tooltip("Khoảng cách hủy bỏ chức năng so với Player sau khi được kích hoạt")]
    public float deactiveDistance;

    public AudioClip idleClip, soundCash;

    public AnimationControl animator;
    Rigidbody2D rigid;
    GameObject fbx;
    AudioSource audioSource;
    private void OnEnable()
    {
        if (idleClip)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.spatialBlend = 1f;
            audioSource.loop = true;
            audioSource.clip = idleClip;
            audioSource.minDistance = 1f;
            audioSource.Play();
            audioSource.volume = Modules.soundSetting;
        }

        switch (enemyType)
        {
            case EnemyType.Gorilla:
                rigid = GetComponent<Rigidbody2D>();
                fbx = transform.GetChild(0).gameObject;
                fbx.SetActive(false);
                break;

            case EnemyType.Wave:
                rigid = GetComponent<Rigidbody2D>();
                fbx = transform.GetChild(0).gameObject;
                
                break;

            case EnemyType.Spider:
                fbx = transform.GetChild(0).gameObject;
                break;

            case EnemyType.Native:
            case EnemyType.RollingStone:
                rigid = GetComponent<Rigidbody2D>();
                break;

        }
        ResetState();

       
        //if (enemyType == EnemyType.Gorilla)
        //    transform.parent = null;
    }

    private void Awake()
    {
        if (enemyType == EnemyType.Wave)
            Wave = this;
    }

    public void ResetState()
    {
        isEnabled = false;
        if (rigid)
        {
            rigid.bodyType = RigidbodyType2D.Kinematic;
            GetComponent<Collider2D>().isTrigger = true;
        }
        switch (enemyType)
        {
            case EnemyType.Native:
            case EnemyType.RollingStone:
            case EnemyType.Spider:
            case EnemyType.Mole:
                isChecking = true;
                break;

            case EnemyType.Gorilla:
            case EnemyType.Wave:
                isChecking = true;
                break;

            default:
                isChecking = false;
                break;
        }
    }

    private void Update()
    {
        float dis = PlayerController.Instance.transform.position.x - transform.position.x;
        switch (enemyType)
        {
            case EnemyType.Spider:
            case EnemyType.Mole:
            case EnemyType.Native:
            case EnemyType.RollingStone:
                if (isChecking && !isEnabled)
                {
                    if (Mathf.Abs(dis) <= activeDistance)
                    {
                        switch (enemyType)
                        {
                            case EnemyType.Spider:
                                SpiderMove();
                                break;

                            case EnemyType.Mole:
                                ///Thực hiện 5 animation liên tiếp
                                ///Xuất hiện=> Nhảy lên => Lơ lửng => Nhảy xuống => Chui xuống đất
                                GameObject collider = transform.GetChild(1).gameObject;
                                collider.transform.localPosition = Vector2.zero;
                                float time = 0;
                                animator.Play("appear", false, 1.5f, () => {
                                    time = animator.Play("jump", false, 2f, () => {
                                        time = animator.Play("upper");
                                        //StartCoroutine(DelayAction(2 * time, () =>
                                        //{
                                        //    time = animator.Play("down", false, 2f, () =>
                                        //    {
                                        //        animator.Play("prepare");
                                        //    });
                                        //    iTween.MoveTo(collider, iTween.Hash("y", 0f, "time", time, "islocal", true));
                                        //}));
                                    });
                                    iTween.MoveTo(collider, iTween.Hash("y", 1.5f, "time", time - 0.25f, "delay", 0.25f, "islocal", true));
                                });
                                break;

                            case EnemyType.RollingStone:
                                GetComponent<Collider2D>().isTrigger = false;
                                rigid.bodyType = RigidbodyType2D.Dynamic;
                                break;

                            case EnemyType.Native:
                                GetComponent<Collider2D>().isTrigger = false;
                                rigid.bodyType = RigidbodyType2D.Dynamic;
                                animator.Play("di chuyen");
                                break;
                        }
                        isEnabled = true;
                        isChecking = false;
                        if (audioSource)
                            audioSource.Play();
                    }
                }
                else if (isEnabled)
                {
                    if (rigid && rigid.bodyType != RigidbodyType2D.Static)
                        rigid.velocity = new Vector2(speedRun, rigid.velocity.y);
                    if (dis > deactiveDistance)
                        DestroyObj();
                }
                break;

            case EnemyType.Wave:

                if (Mathf.Abs(dis) <= activeDistance)
                {
                    if (audioSource)
                        audioSource.Play();
                }

                if (PlayerController.Instance.isCompleted || PlayerController.Instance.playerStates == PlayerStates.Dead)
                {
                    rigid.velocity = Vector2.zero;
                    return;
                }
                if (isChecking && !isEnabled)
                {
                    rigid.velocity = Vector2.zero;
                    if (PlayerController.Instance.playerStates != PlayerStates.Idle && dis >= activeDistance)
                    {
                        isChecking = false;
                        isEnabled = true;
                        animator.Play("run");
                       
                    }
                }
                else if (isEnabled && !isChecking)
                {
                    if (PlayerController.Instance.playerStates != PlayerStates.Dead)
                    {
                        rigid.velocity = new Vector2(PlayerController.Instance.runSpeed + speedAdd, 0f);
                        if (dis <= deactiveDistance) speedAdd = 0;
                    }
                    else
                    {
                        rigid.velocity = Vector2.zero;
                        isChecking = true;
                        isEnabled = false;
                        animator.Play("idle");
                    }
                }

                break;

            case EnemyType.Gorilla:
                if (PlayerController.Instance.isCompleted || PlayerController.Instance.playerStates == PlayerStates.Dead)
                {
                    rigid.velocity = Vector2.zero;
                    wasEnable = true;
                    gameObject.SetActive(false);
                    return;
                }

                if (isChecking && !isEnabled)
                {
                    if (dis >= activeDistance)
                    {
                        GetComponent<Collider2D>().isTrigger = false;
                        rigid.bodyType = RigidbodyType2D.Dynamic;
                        fbx.SetActive(true);
                        isChecking = false;
                        isEnabled = true;
                        transform.parent = null;
                    }
                }
                else if (isEnabled && !isChecking)
                {
                    int spa = 0;
                    if (dis < 0) spa = 3;
                    if (rigid.bodyType != RigidbodyType2D.Static)
                        rigid.velocity = new Vector2(PlayerController.Instance.runSpeed + 1f + spa, rigid.velocity.y);
                    if (dis <= deactiveDistance || dis > activeDistance + 1f)
                        DestroyObj();
                }

                break;
        }
    }

    float speedAdd = 0;
    Coroutine waitTeleport;
    public void WaveTeleport(int type = 0)
    {
        if (waitTeleport != null) StopCoroutine(waitTeleport);
        float dis = Camera.main.transform.position.x - transform.position.x;
        float time = Random.Range(2f, 3f);
        speedAdd = 0;
        switch (type)
        {
            case 0:
                speedAdd = 0;
                break;

            case 1:
                if (dis > 5f)
                    speedAdd = (Mathf.Abs(dis) - 5f) / time;
                waitTeleport = StartCoroutine(DelayAction(time, () =>
                {
                    speedAdd = 0.1f;
                }));
                break;

            case 2:
                if (dis >= 5f)
                {
                    speedAdd = (Mathf.Abs(dis) - 5f) / time;
                    waitTeleport = StartCoroutine(DelayAction(time, () =>
                    {
                        speedAdd = 0.25f;
                    }));
                }
                else speedAdd = 0.25f;
                break;

            case 3:
                if (dis >= 5f)
                {
                    speedAdd = (Mathf.Abs(dis) - 5f) / time;
                    waitTeleport = StartCoroutine(DelayAction(time, () =>
                    {
                        speedAdd = 0.4f;
                    }));
                }
                else speedAdd = 0.4f;
                break;

            default:
                speedAdd = 0f;
                break;
        }
    }

    void DestroyObj()
    {
        Destroy(gameObject);
    }

    void SpiderMove()
    {
        float yTo = -speedRun;
        if (fbx.transform.localPosition.y <= -speedRun) yTo = 0;
        float t = Random.Range(0.75f, 1.25f);
        iTween.MoveTo(fbx, iTween.Hash("y", yTo, "time", t, "delay", 0.5f, "islocal", true, "easetype", iTween.EaseType.easeInOutSine));
        SpriteRenderer silk = transform.GetChild(1).GetComponent<SpriteRenderer>();
        StartCoroutine(RepeatAction(0.5f, 0.01f, (int)(t / 0.01f), () => {
            silk.size = new Vector2(silk.size.x, -fbx.transform.localPosition.y);
        }));
        StartCoroutine(DelayAction(t + 1f, () => { SpiderMove(); }));
    }
    [HideInInspector]
    public bool isEnabled = false;
    [HideInInspector]
    bool isChecking = false;
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (PlayerController.Instance.playerStates == PlayerStates.Dead || wasEnable) return;
        if (collider.tag == "Player")
        {
            switch (enemyType)
            {
                case EnemyType.Spider:
                    HandleSpider();
                    break;

                case EnemyType.Wave:
                    HandleWave();
                    break;

                case EnemyType.Mole:
                    HandleMole();
                    break;

                case EnemyType.RollingStone:
                    PlayerThowHandle();
                    break;
            }
        }
    }

    [HideInInspector]
    public bool wasEnable = false;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (PlayerController.Instance.playerStates == PlayerStates.Dead || wasEnable) return;
        if (collision.gameObject.tag == "Player")
        {
            switch (enemyType)
            {
                case EnemyType.Spider:
                    HandleSpider();
                    break;

                case EnemyType.Native:
                    HandleNative();
                    break;

                case EnemyType.RollingStone:
                    HandleRollingStone(collision);
                    break;

                case EnemyType.Wave:
                    HandleWave();
                    break;

                case EnemyType.Mole:
                    HandleMole();
                    break;

                case EnemyType.Gorilla:
                    HandleGorilla();
                    break;
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (wasEnable) return;
        OnCollisionEnter2D(collision);
    }

    /// <summary>
    /// Xử lý va chạm với NHỆN
    /// </summary>
    void HandleSpider()
    {
        print("dead with SPIDER");
        PlayerThowHandle();
    }

    /// <summary>
    /// Xử lý va chạm với THỔ DÂN
    /// </summary>
    void HandleNative()
    {
        if (wasEnable) return;
        if (!isEnabled && !isChecking) return;
        animator.Play("tan cong", true, 1f, () =>
        {
            isEnabled = false;
            isChecking = false;
            //rigid.velocity = Vector2.zero;
            rigid.bodyType = RigidbodyType2D.Static;
            rigid.GetComponent<Collider2D>().isTrigger = true;
        });
        PlayerThowHandle();
    }


    void HandleRollingStone(Collision2D collision)
    {
        if (!isEnabled && !isChecking) return;
        if (GroundController.DetectSide(collision) != Direction.LEFT) return;

        animator.Play("explode", false, 1f, () =>
        {
            isEnabled = false;
            isChecking = false;
            if (rigid.bodyType != RigidbodyType2D.Static)
                rigid.velocity = Vector2.zero;
            rigid.bodyType = RigidbodyType2D.Kinematic;
            rigid.GetComponent<Collider2D>().isTrigger = true;
        });
        PlayerThowHandle();
    }

    /// <summary>
    /// Xử lý va chạm với SÓNG
    /// </summary>
    void HandleWave()
    {
        if (PlayerController.Instance.playerStates == PlayerStates.Dead) return;
        PlayerThowHandle();
    }

    /// <summary>
    /// Xử lý va chạm với CHUỘT CHŨI
    /// </summary>
    void HandleMole()
    {
        if (PlayerController.Instance.playerStates == PlayerStates.Dead) return;
        PlayerThowHandle();
    }

    /// <summary>
    /// Xử lý va chạm với GORILLA
    /// </summary>
    void HandleGorilla()
    {
        if (PlayerController.Instance.playerStates == PlayerStates.Dead) return;
        PlayerThowHandle();
    }

    public void PlayerThowHandle()
    {
        if (enemyType != EnemyType.Wave)
            wasEnable = true;

        if (PlayerController.Instance.playerStates == PlayerStates.Surf)
        {
            Explode();
        }
        else if (PlayerController.Instance.usingSkateboard)
        {
            Explode();
        }
        else if (PlayerController.Instance.usingRhino)
        {
            Explode();

        }
        else if (PlayerController.Instance.usingProtect)
        {
            Explode();
        }
        else if (PlayerController.Instance.usingFlyingCarpet)
        {
            Explode();
        }
        else
        {
            PlayerController.Instance.Dead(enemyType.ToString());
        }
        if (enemyType != EnemyType.Wave)
        {
            if (rigid) rigid.bodyType = RigidbodyType2D.Static;

            foreach (Collider2D c in GetComponents<Collider2D>())
                c.isTrigger = true;
        }
        if (soundCash)
            AudioSource.PlayClipAtPoint(soundCash, Camera.main.transform.position, Modules.soundSetting);
    }

    void Explode()
    {
        switch (enemyType)
        {
            case EnemyType.Spider:
                break;

            case EnemyType.Wave:

                break;

            case EnemyType.Native:
                PlayerController.Instance.pagePlay.CheckKillNative(1);
                rigid.bodyType = RigidbodyType2D.Static;
                animator.Play("chet", false, 1f);
                break;

            case EnemyType.Mole:
                transform.GetChild(1).gameObject.SetActive(false);
                animator.Play("down", false, 1f, () => {
                    animator.Play("prepare");
                });
                break;

            case EnemyType.RollingStone:
                rigid.bodyType = RigidbodyType2D.Static;
                animator.Play("explode", false, 1f);
                break;

            case EnemyType.Gorilla:
                break;
        }
    }

    IEnumerator DelayAction(float timeDelay, System.Action action)
    {
        yield return new WaitForSeconds(timeDelay);
        if (action != null) action();
        yield break;
    }

    IEnumerator RepeatAction(float timeDelay, float timeRepeat, int repeatCount, System.Action action)
    {
        yield return new WaitForSeconds(timeDelay);
        if (action != null)
            while (repeatCount > 0)
            {
                action();
                yield return new WaitForSeconds(timeRepeat);
                repeatCount--;
            }
        yield break;
    }
}
