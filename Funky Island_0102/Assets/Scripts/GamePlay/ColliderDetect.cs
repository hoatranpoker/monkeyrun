﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderDetect : MonoBehaviour {
    private void OnTriggerEnter2D(Collider2D collider)
    {
        transform.parent.gameObject.SendMessage("OnTriggerEnter2D", collider, SendMessageOptions.DontRequireReceiver);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        transform.parent.gameObject.SendMessage("OnCollisionEnter2D", collision, SendMessageOptions.DontRequireReceiver);
    }
}
