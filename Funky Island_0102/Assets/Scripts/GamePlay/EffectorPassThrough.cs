﻿using UnityEngine;

public class EffectorPassThrough : MonoBehaviour
{
    PlatformEffector2D PlatformEffector2D;
    private void Awake()
    {
        PlatformEffector2D = GetComponent<PlatformEffector2D>();
        PlatformEffector2D.colliderMask &= ~LayerMask.GetMask("Vines");
        wasEnable = false;
    }
    [HideInInspector]
    public bool wasEnable = false;
    private void LateUpdate()
    {
        if (!resetMask) return;
        if (PlayerController.Instance.usingFlyingCarpet 
            || PlayerController.Instance.usingBird 
            || PlayerController.Instance.playerStates == PlayerStates.Surf 
            || PlayerController.Instance.usingVines)
        {
            if (wasEnable) return;
            PlatformEffector2D.colliderMask &= ~LayerMask.GetMask("Player");    //remove layermask
            PlatformEffector2D.colliderMask &= ~LayerMask.GetMask("Skateboard");
            PlatformEffector2D.colliderMask &= ~LayerMask.GetMask("Transform");
            wasEnable = true;
        }
        else
        {
            if (!wasEnable) return;
            PlatformEffector2D.colliderMask |= LayerMask.GetMask("Player");     //add layermask
            PlatformEffector2D.colliderMask |= LayerMask.GetMask("Skateboard");
            PlatformEffector2D.colliderMask |= LayerMask.GetMask("Transform");
            wasEnable = false;
        }
    }

    [HideInInspector]
    public bool resetMask = true;
}