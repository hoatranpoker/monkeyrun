﻿///Scrip này xử lý tất cả những va chạm của player với vật cản
///Vật cản là trigger hoặc non trigger
///Cần phải cài đặt cho từng loại vật cản

using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public ObstacleType obstacleType;

    [Tooltip("Vật cản bị xóa sau khi va chạm. Đây là độ trễ tính từ lúc va chạm.")]
    public float m_timeDestroy = 0f;
    [Tooltip("Âm thanh khi va chạm")]
    public AudioClip soundCash;

    [Header("SHATTERED SETTING")]
    [Tooltip("Hiệu ứng phá vỡ (Không yêu cầu)")]
    public GameObject fxShattered;
    [Tooltip("Độ trễ chờ xóa hiệu ứng phá vỡ.")]
    public float timeDestroy = 0f;

    [Header("IMPACT SPEED")]
    [Tooltip("Áp dụng với vật cản làm tăng hoặc giảm tốc độ chạy hoặc là lực đẩy tác động khi chạm vào lò xo")]
    public float cutRate = -1f;
    [Tooltip("Thời gian tác động giảm hoặc tăng tốc độ chạy.")]
    public float inTime = 3f;

    AnimationControl animator;
    private void Awake()
    {
        animator = GetComponent<AnimationControl>();
    }

    private void Update()
    {
        if (obstacleType == ObstacleType.Door && !wasEnable && PlayerController.Instance.haveKey)
        {
            Collision();
            wasEnable = true;
            StartCoroutine(DelayAction(0.5f, () => {
                PlayerController.Instance.haveKey = false;
            }));
        }
    }

    [HideInInspector]
    public bool wasEnable = false;
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (PlayerController.Instance.playerStates == PlayerStates.Dead || wasEnable) return;
        //Chỉ xử lý với Player
        if (collider.tag == "Player")
        {
            switch (obstacleType)
            {
                case ObstacleType.WoodBox:
                    HandleWoodBox();
                    break;

                case ObstacleType.Bush:
                    HandleBush();
                    break;

                case ObstacleType.Fire:
                    HandleFire();
                    break;

                case ObstacleType.Spiderweb:
                    HandleSpiderweb();
                    break;

                case ObstacleType.Switch:
                    HandleSwitch();
                    break;

                case ObstacleType.Door:
                    HandleDoor();
                    break;

                case ObstacleType.StoneColumn:
                case ObstacleType.Rock:
                    PlayerThowHandle();
                    break;
            }
        }
        if (collider.gameObject.tag == "Protect")
        {
            switch (obstacleType)
            {
                case ObstacleType.Bush:
                    HandleBush();
                    break;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        //Chỉ xử lý với Player
        if (PlayerController.Instance.playerStates == PlayerStates.Dead || wasEnable) return;
        if (collision.gameObject.tag == "Player")
        {
            switch (obstacleType)
            {
                case ObstacleType.Bush:
                    HandleBush();
                    break;
            }
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Chỉ xử lý với Player
        if (PlayerController.Instance.playerStates == PlayerStates.Dead || wasEnable) return;
        if (collision.gameObject.tag == "Player")
        {
            switch (obstacleType)
            {
                //case ObstacleType.WoodBox:
                //    HandleWoodBox(collision);
                //    break;

                case ObstacleType.Forceground:
                    HandleForceground(collision);
                    break;

                case ObstacleType.Springs:
                    HandleSprings(collision);
                    break;

                case ObstacleType.Vines:
                    HandleVines(collision);
                    break;

                case ObstacleType.Rock:
                    HandleRock(collision);
                    break;

                case ObstacleType.StoneColumn:
                    HandleStoneColumn(collision);
                    break;

                case ObstacleType.River:
                    HandleRiver(collision);
                    break;

                case ObstacleType.Cliff:
                    HandleCliff(collision);
                    break;

                case ObstacleType.BridgeCollapsed:
                    HandleBridgeCollapsed(collision);
                    break;
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (wasEnable) return;
        switch (obstacleType)
        {
            case ObstacleType.Rock:
            case ObstacleType.StoneColumn:
            case ObstacleType.Cliff:
            case ObstacleType.BridgeCollapsed:
            case ObstacleType.Springs:
                if (collision.gameObject.tag != "Water")
                    OnCollisionEnter2D(collision);
                break;
            case ObstacleType.River:
                OnCollisionEnter2D(collision);
                break;
        }
    }

    /// <summary>
    /// FORCRGROUND
    /// </summary>
    void HandleForceground(Collision2D collision)
    {

    }

    /// <summary>
    /// LÒ XO
    /// </summary>
    private void HandleSprings(Collision2D collision)
    {
        if (wasEnable) return;
        float t = 0.5f;
        if (PlayerController.Instance.usingRhino || PlayerController.Instance.usingSkateboard)
            t = 1f;
        if ((t < 1f && transform.position.y < PlayerController.Instance.transform.position.y - t) || (t >= 1f && transform.position.y < PlayerController.Instance.transform.position.y - t))
        {
            if (soundCash)
                AudioSource.PlayClipAtPoint(soundCash, Camera.main.transform.position, Modules.soundSetting);
            animator.Play("animation", false);

            if (PlayerController.Instance.playerStates == PlayerStates.Down)
            {
                PlayerController.Instance.Jump(cutRate * 1.3f);
            }
            else PlayerController.Instance.Jump(cutRate);
            wasEnable = true;
            PlayerController.Instance.pagePlay.CheckJumpMushrooms(1);
        }
    }

    /// <summary>
    /// DÂY LEO
    /// </summary>
    private void HandleVines(Collision2D collision)
    {
        if (wasEnable) return;
        if (PlayerController.Instance.usingBird
            || PlayerController.Instance.playerStates == PlayerStates.Surf
            || PlayerController.Instance.usingSkateboard
            || PlayerController.Instance.usingRhino
            || PlayerController.Instance.usingFlyingCarpet) return;
        if (soundCash)
            AudioSource.PlayClipAtPoint(soundCash, Camera.main.transform.position, Modules.soundSetting);
        PlayerController.Instance.ChangeToVines(GetComponent<Rigidbody2D>());
        AnimationControl animator = GetComponent<AnimationControl>();
        if (animator)
            animator.Play("rotate", false);
        wasEnable = true;
        //GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
    }

    /// <summary>
    /// THÙNG GỖ
    /// </summary>
    private void HandleWoodBox()//Collision2D collision)
    {
        if (wasEnable) return;
        //if (GroundController.DetectSide(collision) == Direction.LEFT)
        //{
        Explode();
        CutRate();
        PlayerController.Instance.pagePlay.CheckBreakBarrel(1);
        wasEnable = true;
        //}    
    }

    /// <summary>
    /// BỤI GAI
    /// </summary>
    private void HandleBush()
    {
        if (wasEnable) return;
        GetComponent<AnimationControl>().Play("vacham", false, 1f);
        CutRate();
        wasEnable = true;
        PlayerController.Instance.pagePlay.CheckPassQuagmire(1);
    }

    /// <summary>
    /// TẢNG ĐÁ
    /// </summary>
    private void HandleRock(Collision2D collision)
    {
        if (GroundController.DetectSide(collision) == Direction.LEFT)
        {
            PlayerThowHandle();
        }
    }

    /// <summary>
    /// CỘT ĐÁ
    /// </summary>
    void HandleStoneColumn(Collision2D collision)
    {
        if (GroundController.DetectSide(collision) == Direction.LEFT)
        {
            PlayerThowHandle();
        }
    }

    /// <summary>
    /// SÔNG
    /// </summary>
    public void HandleRiver(Collision2D collision)
    {
        if (!PlayerController.Instance.usingSkateboard && !PlayerController.Instance.usingRhino && !PlayerController.Instance.CheckWaterJump())
        {
            if (soundCash)
                AudioSource.PlayClipAtPoint(soundCash, Camera.main.transform.position, Modules.soundSetting);

            Vector2 hit = collision.contacts[0].point;
            hit.y = -3.3f;
            if (fxShattered)
            {
                GameObject fx = Instantiate(fxShattered);
                fx.transform.position = hit;
                if (timeDestroy > 0)
                    Destroy(fx, timeDestroy);
            }

            GetComponent<Collider2D>().isTrigger = true;
            PlayerController.Instance.Dead(obstacleType.ToString());
            StartCoroutine(DelayAction(1f, () =>
            {
                GetComponent<Collider2D>().isTrigger = false;
            }));
        }
        else
        {
            if (PlayerController.Instance.usingSkateboard) return;
            if (PlayerController.Instance.CheckWaterJump())
            {
                PlayerController.Instance.Jump(PlayerController.Instance.jumpSpeed + 5f);
                PlayerController.Instance.WaitWaterJump();
                //return;
            }

            if (soundCash)
                AudioSource.PlayClipAtPoint(soundCash, Camera.main.transform.position, Modules.soundSetting);

            Vector2 hit = collision.contacts[0].point;
            hit.y = -3.3f;
            if (fxShattered)
            {
                GameObject fx = Instantiate(fxShattered);
                fx.transform.position = hit;
                if (timeDestroy > 0)
                    Destroy(fx, timeDestroy);
            }
        }
    }

    /// <summary>
    /// VÁCH NÚI
    /// </summary>
    void HandleCliff(Collision2D collision)
    {
        if (GroundController.DetectSide(collision) == Direction.LEFT)
        {
            PlayerThowHandle();
        }
    }

    /// <summary>
    /// CẦU SẬP
    /// </summary>
    void HandleBridgeCollapsed(Collision2D collision)
    {
        if (wasEnable) return;
        if (GroundController.DetectSide(collision) == Direction.DOWN)
        {
            if (soundCash)
                AudioSource.PlayClipAtPoint(soundCash, Camera.main.transform.position, Modules.soundSetting);
            iTween.MoveTo(gameObject, iTween.Hash("y", transform.position.y - 10, "time", 0.75f, "delay", 0.35f, "easetype", iTween.EaseType.easeInSine));
            iTween.RotateTo(gameObject, iTween.Hash("z", Random.Range(0f, 175f), "time", Random.Range(0.15f, 0.35f), "delay", 0.35f, "easetype", iTween.EaseType.easeInOutSine));
            StartCoroutine(DelayAction(1.5f, () =>
            {
                Destroy(gameObject);
            }));
            wasEnable = true;
        }
    }

    /// <summary>
    /// ĐỐNG LỬA
    /// </summary>
    void HandleFire()
    {
        if (PlayerController.Instance.usingProtect || PlayerController.Instance.playerStates == PlayerStates.Surf) return;

        if (soundCash)
            AudioSource.PlayClipAtPoint(soundCash, Camera.main.transform.position, Modules.soundSetting);
        PlayerController.Instance.Dead(obstacleType.ToString());
        GetComponent<Collider2D>().isTrigger = true;
    }

    /// <summary>
    /// MẠNG NHỆN
    /// </summary>
    void HandleSpiderweb()
    {
        if (soundCash)
            AudioSource.PlayClipAtPoint(soundCash, Camera.main.transform.position, Modules.soundSetting);
        CutRate();
        animator.Play("animation", false, 2f);
        PlayerController.Instance.pagePlay.CheckBreakCobweb(1);
    }

    /// <summary>
    /// CÔNG TẮC
    /// </summary>
    void HandleSwitch()
    {
        Collision();
        //PlayerController.Instance.haveKey = true;
    }

    /// <summary>
    /// CÁNH CỬA
    /// </summary>
    void HandleDoor()
    {
        //if (PlayerController.Instance.haveKey)
        //    Collision();
        //else
        PlayerController.Instance.Dead(obstacleType.ToString());
    }

    /// <summary>
    /// Hàm xử lý chung nhưng đối tượng làm tăng hoặc giảm tốc độ
    /// </summary>
    void CutRate()
    {
        if (cutRate != 0)
            PlayerController.Instance.CutRate(cutRate, inTime);

        if (soundCash)
            AudioSource.PlayClipAtPoint(soundCash, Camera.main.transform.position, Modules.soundSetting);
    }

    IEnumerator DelayAction(float timeDelay, System.Action action)
    {
        yield return new WaitForSeconds(timeDelay);
        if (action != null) action();
        yield break;
    }

    void Collision()
    {
        if (soundCash)
            AudioSource.PlayClipAtPoint(soundCash, Camera.main.transform.position, Modules.soundSetting);

        if (fxShattered)
        {
            GameObject fx = Instantiate(fxShattered);
            fx.transform.position = transform.position;
            if (timeDestroy > 0)
                Destroy(fx, timeDestroy);
        }
        Destroy(gameObject, m_timeDestroy);
    }

    public void PlayerThowHandle()
    {
        if (PlayerController.Instance.playerStates == PlayerStates.Surf)
        {
            Explode();
        }
        else if (PlayerController.Instance.usingSkateboard)
        {
            Explode();
            //PlayerController.Instance.CancelSkateboard(false);
        }
        else if (PlayerController.Instance.usingFlyingCarpet)
        {
            Explode();
        }
        else if (PlayerController.Instance.usingProtect)
        {
            Explode();
        }
        else if (PlayerController.Instance.usingRhino)
        {
            Explode();
        }
        else
        {
            switch (obstacleType)
            {
                case ObstacleType.Door:
                case ObstacleType.Rock:
                case ObstacleType.Fire:
                case ObstacleType.StoneColumn:
                    PlayerController.Instance.Dead(obstacleType.ToString());
                    break;
            }
        }
    }

    void Explode()
    {
        switch (obstacleType)
        {
            case ObstacleType.Rock:
            case ObstacleType.StoneColumn:
            case ObstacleType.Cliff:
            case ObstacleType.WoodBox:
                if (animator)
                    animator.Play("explode", false, 1f);
                break;

            case ObstacleType.Spiderweb:
                if (animator)
                    animator.Play("animation", false, 1f);
                break;
        }

        if (obstacleType == ObstacleType.River)
        {
            if (soundCash)
                AudioSource.PlayClipAtPoint(soundCash, Camera.main.transform.position, Modules.soundSetting);

            Vector2 hit = PlayerController.Instance.transform.position;
            hit.y = -3.3f;
            if (fxShattered)
            {
                GameObject fx = Instantiate(fxShattered);
                fx.transform.position = hit;
                if (timeDestroy > 0)
                    Destroy(fx, timeDestroy);
            }

            GetComponent<Collider2D>().isTrigger = true;
            PlayerController.Instance.Dead(obstacleType.ToString());
            StartCoroutine(DelayAction(1f, () =>
            {
                GetComponent<Collider2D>().isTrigger = false;
            }));
        }
        else
        {
            foreach (Collider2D c in GetComponents<Collider2D>())
                c.enabled = false;
            if (soundCash)
                AudioSource.PlayClipAtPoint(soundCash, Camera.main.transform.position, Modules.soundSetting);
        }
    }
}

