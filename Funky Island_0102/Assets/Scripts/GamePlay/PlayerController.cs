﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Config
    public static PlayerController Instance;
    [HideInInspector]
    public Rigidbody2D rigid;
    public Vector2 gravity = new Vector2(0f, -20f);
    public PlayerStates playerStates = PlayerStates.Idle;
    public float runSpeed, jumpSpeed;
    public float distanceSteps = 500;
    public float[] runSpeeds;
    public AnimationControl animator;

    [HideInInspector]
    public bool haveKey = false;
    [HideInInspector]
    public bool haveBox = false;

    [Header("SETTIING FOR SUFT STATE")]
    public bool canbeSuft = false;
    [Tooltip("Suft")]
    public GameObject fxSurt; 
    public float timeSuft = 1f;
    public float speedSuft = 5f;
    public int sBananaRequire = 5;
    int sBananaCount = 0;

    [Header("SETTING GROUND CHECK")]
    public Transform groundCheck;
    public float groundedRadius;
    public LayerMask whatIsGround;
    public Collider2D bodyCollider;

    [Header("SETTING PARACHUTE")]
    public float parachuteScale = 0.05f;
    public GameObject parachute;
    public Sprite[] parachutes;

    [Header("WHEN USING RHINO")]
    public bool usingRhino;
    public GameObject Rhino;
    public float rhinoTime = 60f;
    public float rhinoRadius = 1f;
    public float rAddSpeed = 1f;

    [Header("WHEN USING APPLE")]
    public bool usingApple;
    public float appleTime = 10f;

    [Header("WHEN USING X2 BANANA")]
    public bool usingX2Banana;
    float x2BananaTime = 10f;

    [Header("WHEN USING SKATEBOARD")]
    public bool usingSkateboard;
    public GameObject skateboard;
    float skaterboardTime = 60f;
    public float sAddSpeed = 1f;

    [Header("WHEN USING BIRD")]
    public bool usingBird;
    public GameObject Bird;
    float birdTime = 10f;
    public float jumpSpeedBird = 5f;

    [Header("WHEN USING FLYING CARPET")]
    public bool usingFlyingCarpet;
    public GameObject flyingCarpet;
    public float flyingSensitivity = 2f;
    float flyingTime = 5f;
    public float fAddSpeed = 1f;

    [Header("WHEN USING PROTECT")]
    public bool usingProtect;
    public GameObject protect;
    float protectTime = 5f;

    [Header("WHEN USING MAGNET")]
    public bool usingMagnet;
    public GameObject magnet;
    float magnetTime = 5f;

    [Header("WHEN USING VINES")]
    public bool usingVines;
    [Tooltip("Tốc độ cộng thêm với tốc độ x hiện tại.")]
    public float speedXVines;
    [Tooltip("Tốc độ cộng thêm với y khi buông dây")]
    public float speedYVines;

    [Header("SOUND FX")]
    public AudioClip soundJump;
    public AudioClip soundSuft, soundDown, soundReborn;
    public AudioClip[] deadClips;
    public AudioClip soundFail, soundComplete;

    private float updateSpeed = 0;

    public bool isGrounded {
        get
        {
            if (usingRhino)
                return Physics2D.OverlapCircle(groundCheck.position, groundedRadius + rhinoRadius, whatIsGround);
            else
                return Physics2D.OverlapCircle(groundCheck.position, groundedRadius, whatIsGround);
        }
        set {
            this.isGrounded = value;
        }
    }

    private bool isGroundedToRun
    {
        get
        {
            return Physics2D.Raycast(groundCheck.position, Vector3.down, groundedRadius+3f, whatIsGround);
        }
        set
        {
            this.isGroundedToRun = value;
        }
    }
    public PagePlay pagePlay;
    private void Awake()
    {
        Instance = this;
        rigid = GetComponent<Rigidbody2D>();
        rigid.drag = 0;
        if (fxSurt) fxSurt.transform.localScale = Vector3.zero;
      
    }

    #endregion

    #region Set Inits and Start
    private void Start()
    {
        Physics2D.gravity = gravity;
        ResetState();
        string startP = SaveLoadData.LoadData("EndlessMetter", true);
        int.TryParse(startP, out startPoint);
        SaveLoadData.SaveData("EndlessMetter", "0", true);
        if (!Modules.runEndless) updateSpeed = 0.04f * Modules.levelRun ;
        if (updateSpeed > 3f) updateSpeed = 3f;
    }
    private int startPoint = 0;

    private float lastTime = 0f;
    public void ResetState()
    {
        //playerStates = PlayerStates.Idle;
        CancelAllTranform(resetBanana: true);
        CancelMagnet();
        CancelProtect();
        animator.skeletonAnimation.gameObject.GetComponent<MeshRenderer>().sortingOrder = 2;
        lastTime = Time.time;
        magnetTime = Modules.listLevelMagnet[Modules.levelMagnet].effectItem;
        protectTime = Modules.listLevelProtect[Modules.levelProtect].effectItem;
        x2BananaTime = Modules.listLevel2XBanana[Modules.level2XBanana].effectItem;
        rhinoTime = Modules.listLevelRhino[Modules.levelRhino].effectItem;
        flyingTime = Modules.listLevelCarpet[Modules.levelCarpet].effectItem;
        skaterboardTime = Modules.listLevelSkateboard[Modules.levelSkateboard].effectItem;
        pagePlay.UpdateTimer(0f);
        isCompleted = false;

        if (Modules.waterBounce && jumpWaterCount == 0)
            whatIsGround |= LayerMask.GetMask("Water");

        if (Modules.luckyCharm)
            totalLuckyCharm = 1f;
    }
    float totalLuckyCharm = 0;
    [HideInInspector]
    public float counterWObstacle, counterWEnemy;
    private void SetCounterCollision(ItemType itemType = ItemType.None)
    {
        counterWEnemy = 0; counterWEnemy = 0;
        switch (itemType)
        {
            case ItemType.Bird:

                break;

            case ItemType.Rhino:
                if(Modules.levelRhino >= 2)
                {
                    counterWEnemy = 1;
                    counterWObstacle = -1;
                }else if(Modules.levelRhino >= 5)
                {
                    counterWEnemy = 2;
                    counterWObstacle = -1;
                }
                break;

            case ItemType.FlyingCarpet:
                if (Modules.levelCarpet >= 2)
                {
                    counterWEnemy = 0;
                    counterWObstacle = 1;
                }
                else if (Modules.levelCarpet >= 5)
                {
                    counterWEnemy = 0.5f;
                    counterWObstacle = 0.5f;
                }
                break;

            case ItemType.Skateboard:
                if (Modules.levelSkateboard >= 2)
                {
                    counterWEnemy = 0;
                    counterWObstacle = 1;
                }
                else if (Modules.levelSkateboard >= 5)
                {
                    counterWEnemy = 0.5f;
                    counterWObstacle = 0.5f;
                }
                break;
        }
    }

    public void UpdateCounterCollision(string type = "obstacle")
    {
        switch (type)
        {
            case "obstacle":
                if (counterWObstacle >= 1f)
                    counterWObstacle--;
                else
                {
                    counterWObstacle = 0;
                    counterWEnemy--;
                    if (counterWEnemy < 0f)
                        counterWEnemy = 0f;
                }
                break;

            case "enemy":
                if (counterWEnemy >= 1f)
                    counterWEnemy--;
                else
                {
                    counterWEnemy = 0;
                    counterWObstacle--;
                    if (counterWObstacle < 0f)
                        counterWObstacle = 0f;
                }
                break;
        }
    }

    private int lastd = -1;

    #endregion

    #region Update
    private void Update() {

        if (pagePlay.TutorialSence)
        {
            switch (playerStates)
            {
                case PlayerStates.Idle:
                    rigid.velocity = new Vector2(0, rigid.velocity.y);
                    break;

                case PlayerStates.Dead:
                    if (isCompleted)
                    {
                        animator.Play("run", true, 1.5f);
                        rigid.velocity = new Vector2(runSpeed, rigid.velocity.y);
                    }
                    else
                        rigid.velocity = new Vector2(0, rigid.velocity.y);
                    break;

                case PlayerStates.Parachute:
                    rigid.velocity = new Vector2(runSpeed + cutRate + addRate, rigid.velocity.y);
                    rigid.gravityScale = parachuteScale;
                    if (rigid.velocity.y <= 0)
                        Run();
                    break;

                case PlayerStates.Jump:
                    rigid.velocity = new Vector2(runSpeed + cutRate + addRate, rigid.velocity.y);
                    if (rigid.velocity.y <= 0 && !usingVines && !isWaitingBird)
                    {
                        if (usingBird)
                            Run();
                        else
                            CheckJumpDown();
                    }
                    break;

                case PlayerStates.Surf:
                    rigid.velocity = new Vector2(speedSuft, 0f);
                    animator.Play("Surfing");

                    break;

                case PlayerStates.Down:
                    rigid.velocity = new Vector2(runSpeed + cutRate + addRate, rigid.velocity.y);
                    if (rigid.velocity.y <= 0 && !isRolling)
                        Roll();
                    break;

                default:
                    rigid.velocity = new Vector2(runSpeed + cutRate + addRate, rigid.velocity.y);
                    //if (rigid.velocity.y <= 0)
                    Run();
                    break;
            }
        }
        else
        {
            if (Modules.runEndless)
            {
                int d = (int)(transform.position.x + startPoint);
                if (lastd == -1) lastd = d;
                pagePlay.UpdateMetter(d);
                if (usingBird)
                    Achievements.CheckTaskCollectStupidBird(d - lastd);
                else if (usingFlyingCarpet)
                    Achievements.CheckTaskRunFlying(d - lastd);
                else if (usingRhino)
                    Achievements.CheckTaskRunRhino(d - lastd);
                else if (usingSkateboard)
                    Achievements.CheckTaskRunSkateboard(d - lastd);
                else
                    Achievements.CheckTaskRunMarathon(d - lastd);
                lastd = d;
            }
            else
            {
                float p = (transform.position.x / GroundController.Instance.mapLenght) * 100;
                pagePlay.UpdateProcess(p);

            }

            //update speed
            if (Modules.runEndless)
            {
                int i = (int)((startPoint + transform.position.x - (transform.position.x + startPoint) % distanceSteps) / distanceSteps);
                if (i >= runSpeeds.Length) i = runSpeeds.Length - 1;
                runSpeed = runSpeeds[i];
            }
            else
                runSpeed = 5 + updateSpeed;

            if (usingFlyingCarpet)
            {
                rigid.gravityScale = 0f;
                animator.Play("flying_carpet");
                return;
            }

            switch (playerStates)
            {
                case PlayerStates.Idle:
                    rigid.velocity = new Vector2(0, rigid.velocity.y);
                    break;

                case PlayerStates.Dead:
                    if (isCompleted)
                    {
                        animator.Play("run", true, 1.5f);
                        rigid.velocity = new Vector2(runSpeed, rigid.velocity.y);
                    }
                    else
                        rigid.velocity = new Vector2(0, rigid.velocity.y);
                    break;

                case PlayerStates.Parachute:
                    rigid.velocity = new Vector2(runSpeed + cutRate + addRate, rigid.velocity.y);
                    rigid.gravityScale = parachuteScale;
                    if (rigid.velocity.y <= 0)
                        Run();
                    break;

                case PlayerStates.Jump:
                    rigid.velocity = new Vector2(runSpeed + cutRate + addRate, rigid.velocity.y);
                    if (rigid.velocity.y <= 0 && !usingVines && !isWaitingBird)
                    {
                        if (usingBird)
                            Run();
                        else
                            CheckJumpDown();
                    }
                    break;

                case PlayerStates.Surf:
                    rigid.velocity = new Vector2(speedSuft, 0f);
                    animator.Play("Surfing");

                    break;

                case PlayerStates.Down:
                    rigid.velocity = new Vector2(runSpeed + cutRate + addRate, rigid.velocity.y);
                    if (rigid.velocity.y <= 0 && !isRolling)
                        Roll();
                    break;

                default:
                    rigid.velocity = new Vector2(runSpeed + cutRate + addRate, rigid.velocity.y);
                    //if (rigid.velocity.y <= 0)
                    Run();
                    break;
            }

            if (parachute) parachute.SetActive(playerStates == PlayerStates.Parachute);

            if (usingBird)
                animator.Play("monkey_riding_bird");

            if (usingSkateboard)
                animator.Play("windsurfing");

            if (!usingFlyingCarpet && flyingCarpet.activeSelf)
                rigid.gravityScale = 1f;

            if (usingRhino)
                animator.Play("monkey_riding");

            if (playerStates == PlayerStates.Surf && !fxSurt.activeSelf)
                fxSurt.SetActive(true);
            if (playerStates != PlayerStates.Surf && fxSurt.activeSelf)
                fxSurt.SetActive(false);

        } 
    }

    #endregion

    #region State

    float cutRate = 0f, addRate = 0f;
    /// <summary>
    /// Change run speed
    /// </summary>
    /// <param name="cut"></param>
    public void CutRate(float cut, float inTime)
    {
        cutRate = cut;
        if (coroutineCutRate != null) StopCoroutine(coroutineCutRate);
        coroutineCutRate = StartCoroutine(DelayAction(inTime, ()=> {
            cutRate = 0;
        }));
    }
    private Coroutine coroutineCutRate;


    /// <summary>
    /// Run
    /// </summary>
    public void Run()
    {
        if (playerStates == PlayerStates.Dead || usingFlyingCarpet || playerStates == PlayerStates.Down || isRolling || usingVines || isWaitingBird) return;

        if (isGrounded)
        {
            if (!usingSkateboard && !usingRhino)
            {
                animator.Play("run", true, 1.5f+updateSpeed*0.15f);
            }
            playerStates = PlayerStates.Run;
            rigid.gravityScale = 1f;
            //jumpCount = 0;
            if (usingBird) CancelBird();

            if (usingSkateboard)
            {
                AnimationControl s = skateboard.GetComponent<AnimationControl>();
                RaycastHit2D hit = Physics2D.Raycast(groundCheck.position, Vector3.down, 0.5f, whatIsGround);
                if (hit && hit.collider.tag == "Water")
                    s.Play("water");
                else
                    s.Play("ground");
            }
        }
        else
            FloatingAnimator();
    }

    /// <summary>
    /// Check Roll
    /// </summary>
    Coroutine coroutineRoll;
    public void Roll()
    {
        if (isGroundedToRun)
        {
            if (!usingSkateboard && !usingRhino)
            {
                animator.Play("jump_down", true, 1.25f);
                isRolling = true;
                if (coroutineRoll != null) StopCoroutine(coroutineRoll);
                coroutineRoll = StartCoroutine(DelayAction(0.75f, () =>
                {
                    if(playerStates == PlayerStates.Surf)
                    {
                        isRolling = false;
                        return;
                    }
                    if (isRolling && playerStates != PlayerStates.Dead)
                    {     
                        isRolling = false;
                        playerStates = PlayerStates.Run;
                        FloatingAnimator();
                    }     
                }));
                //playerStates = PlayerStates.Run;
            }
            else
                playerStates = PlayerStates.Run;
            rigid.gravityScale = 1f;
            //jumpCount = 0;
            if (usingBird) CancelBird();
        }
        else
            FloatingAnimator();
    }
    [HideInInspector]
    public bool isRolling = false;

    private void CheckJumpDown()
    {
        if (usingVines && playerStates == PlayerStates.Dead) return;
           
        if (isGroundedToRun && playerStates == PlayerStates.Jump)
        {
            animator.Play("jump_ down_2", false, 1f, () =>
            {
                isRolling = false;
                Run();
                //FloatingAnimator();
            });
        }
    
    }

    [HideInInspector]
    public int jumpCount = 0;
    [HideInInspector]
    public int jumpWaterCount = 0;
    public bool CheckWaterJump()
    {
        if ((Modules.waterBounce && jumpWaterCount == 0) || isWaitingWaterJump)
            return true;
        return false;
    }

    private bool isWaitingWaterJump = false;
    public void WaitWaterJump()
    {
        isWaitingWaterJump = true;
        StartCoroutine(DelayAction(1f, () => {
            jumpWaterCount++;
            isWaitingWaterJump = false;
        }));
    }

    /// <summary>
    /// Hàm gọi nhảy mặc định. Chỉ nhảy khi đang đứng trên GROUND, hoặc khi sử dụng CHIM
    /// </summary>
    public void Jump()
    {
        if (playerStates == PlayerStates.Dead) return;
        isRolling = false;
        if (usingBird)
        {
            if (!isGrounded)
                Jump(jumpSpeedBird);
            //else
            //    Run();
        }

        if (!usingFlyingCarpet)
        {
            if (isGrounded || (usingApple && jumpCount <= 1) || (Modules.airBounce && jumpCount <= 1))
            {
                rigid.velocity = new Vector2(rigid.velocity.x, 0);
                rigid.gravityScale = 1f;
                float jumpAdd = 0;
                if (usingSkateboard || usingRhino) jumpAdd = 3f;
                if (isGrounded) jumpCount = 0;
                if (jumpCount == 0)
                    Jump(jumpSpeed + jumpAdd);
                else
                {
                    Jump((jumpSpeed + jumpAdd) * 1f);

                }
            }
        }
    }

    /// <summary>
    /// Gọi nhảy cưỡng bức. Nhảy bất kể vị trí
    /// </summary>
    /// <param name="v"></param>
    public void Jump(float v = 0f)
    {
        if (playerStates == PlayerStates.Dead || playerStates == PlayerStates.Surf || usingFlyingCarpet) return;

        if (v == 0f) v = jumpSpeed;
        rigid.velocity = new Vector2(rigid.velocity.x, v);
        playerStates = PlayerStates.Jump;
        jumpCount++;

        if (!usingSkateboard && !usingRhino && !usingBird)
        {
            if (jumpCount == 1)
            {
                animator.Play("jump_up", false, 1f, () =>
                {
                    if (!isGrounded && playerStates == PlayerStates.Jump && !isRolling)
                        animator.Play("jump");
                });
            }
            else
            {
                if (usingApple || Modules.airBounce)
                {
                    if (!isGrounded)
                    {
                        animator.Play("jump_down", true, 1.45f, () =>
                        {
                            if (!isGrounded && playerStates == PlayerStates.Jump && !isRolling)
                            {
                                animator.Play("jump_down", true, 1.45f);
                            }

                        });
                    }
                    return;
                }
               
                animator.Play("jump_down", false, 1.25f, () =>
                {
                    if (!isGrounded && playerStates == PlayerStates.Jump && !isRolling)
                        animator.Play("jump", false, 1f);
                });
            }
        }

        if (usingSkateboard)
        {
            AnimationControl s = skateboard.GetComponent<AnimationControl>();
            float t = 0;
            if (Random.value <= 0.5f)
                t = s.Play("xoay1", false, 1f);
            else
                t = s.Play("xoay2", false, 1f);

            if (t > 0f)
            {
                iTween.MoveTo(skateboard.transform.GetChild(0).gameObject, iTween.Hash("y", -0.3f, "isLocal", true, "time", 0.5f));
                StartCoroutine(DelayAction(t, () =>
                {
                    iTween.MoveTo(skateboard.transform.GetChild(0).gameObject, iTween.Hash("y", 0f, "isLocal", true, "time", 0.5f));
                    if (!usingSkateboard) return;
                    if (isGrounded)
                    {
                        RaycastHit2D hit = Physics2D.Raycast(groundCheck.position, Vector3.down, 0.5f, whatIsGround);
                        if (hit && hit.collider.tag == "Water")
                            s.Play("water");
                        else
                            s.Play("ground");
                    }
                    else
                        s.Play("jump");
                }));
            }
        }
        AudioSource.PlayClipAtPoint(soundJump, Camera.main.transform.position, Modules.soundSetting);
    }


    /// <summary>
    /// Điều khiển nhân vật khi sử dụng THẢM BAY
    /// </summary>
    /// <param name="vector"></param>
    public void Draging(Vector2 vector)
    {
        rigid.velocity = new Vector2(runSpeed + addRate, (vector.y - rigid.position.y) * flyingSensitivity);
    }

    /// <summary>
    /// Điều khiển nhân vật khi sử dụng THẢM BAY with button
    /// </summary>
    /// <param name="vector"></param>
    public void DragingWithButton(Vector2 vector)
    {
        rigid.velocity = new Vector2(runSpeed + addRate, vector.y * flyingSensitivity * 20 * Time.deltaTime);
    }

    /// <summary>
    /// Đổi trạng thái nhảy dù
    /// </summary>
    public void Parachute()
    {
        if (usingBird || usingVines || playerStates == PlayerStates.Dead || usingFlyingCarpet || usingRhino || usingSkateboard || playerStates == PlayerStates.Surf) return;
        if (playerStates == PlayerStates.Jump && !isGrounded && rigid.velocity.y <= 0)
        {
            animator.Play("fly", true, 1.5f);
            playerStates = PlayerStates.Parachute;
            rigid.velocity = new Vector2(runSpeed + cutRate + addRate, 0f);
            
        }
    }

    /// <summary>
    /// Đổi trạng thái lướt
    /// </summary>
    public void Surf(bool isForced = false)
    {
        if (playerStates == PlayerStates.Dead || usingFlyingCarpet) return;
        if (!isForced && !canbeSuft) return;
        playerStates = PlayerStates.Surf;
        sBananaCount = 0;
        UpdateSuftProcess();
        pagePlay.CheckSurfing(1);
        animator.Play("Surfing");    
        if (fxSurt)
        {
            fxSurt.SetActive(true);
            fxSurt.transform.localScale = Vector3.one;
            AudioSource.PlayClipAtPoint(soundSuft, Camera.main.transform.position, Modules.soundSetting);

            //fxSurt.transform.GetChild(0).gameObject.SetActive(true);
            //fxSurt.transform.GetChild(0).GetComponent<Animation>().Play("suftLight");
            //iTween.ScaleTo(fxSurt.gameObject, Vector3.one, 0.25f);
        }
        Vector2 oldV = rigid.velocity;
        rigid.velocity = new Vector2(speedSuft, 0f);
        rigid.gravityScale = 0f;
       

        StartCoroutine(DelayAction(timeSuft, ()=> {
            if (playerStates != PlayerStates.Surf || playerStates == PlayerStates.Dead) return;
            CancelSuft(oldV);
            UsingProtect(false, false);
        }));
    }

    /// <summary>
    /// Hủy trạng thái SUFT
    /// </summary>
    public void CancelSuft(Vector2 oldV)
    {
        rigid.gravityScale = 1f;
        if (isGrounded)
            Run();
        else
        {
            if (oldV.y < 0)
                rigid.velocity = oldV;
            playerStates = PlayerStates.Jump;
        }

        if (fxSurt)
        {
            //iTween.ScaleTo(fxSurt.gameObject, iTween.Hash("scale", Vector3.zero, "time", 0.25f));
            StartCoroutine(DelayAction(0.3f, () => { fxSurt.SetActive(false);
                //fxSurt.transform.GetChild(0).gameObject.SetActive(false);
            }));
        }
    }

    /// <summary>
    /// Đổi trạng thái rơi khi đang đứng trên ground
    /// </summary>
    public void Down()
    {
        if (usingFlyingCarpet 
            || usingBird 
            || playerStates == PlayerStates.Dead 
            || playerStates == PlayerStates.Down
            || playerStates == PlayerStates.Surf
            ) return;
       
        RaycastHit2D hit = Physics2D.Raycast(groundCheck.position, Vector3.down, 25f, whatIsGround);
        if (hit)
        {
            PlatformEffector2D pl = hit.transform.GetComponent<PlatformEffector2D>();
            if (pl != null)
            {
                EffectorPassThrough p = hit.transform.GetComponent<EffectorPassThrough>();
                if (p)
                {
                    p.resetMask = false;
                    p.wasEnable = true;
                }
                pl.colliderMask &= ~LayerMask.GetMask("Player");
                pl.colliderMask &= ~LayerMask.GetMask("Skateboard");
                pl.colliderMask &= ~LayerMask.GetMask("Transform");

                AudioSource.PlayClipAtPoint(soundDown, Camera.main.transform.position, Modules.soundSetting);
                StartCoroutine(DelayAction(1f, () =>
                {
                    if (p)
                        p.resetMask = true;
                }));
            }
        }
        playerStates = PlayerStates.Down;
        rigid.gravityScale = 5f;
    }

    Rigidbody2D lastVines;
    /// <summary>
    /// Chuyển trạng thái lướt khi vớ được DÂY LEO
    /// </summary>
    public void ChangeToVines(Rigidbody2D rigidbody2D) {
        if (playerStates == PlayerStates.Dead) return;

        if (lastVines == rigidbody2D) return;
        lastVines = rigidbody2D;
        rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
        GetComponent<HingeJoint2D>().connectedBody = rigidbody2D;
        GetComponent<HingeJoint2D>().enabled = true;
        addRate = speedXVines;
        usingVines = true;
        pagePlay.CheckTeeter(1);
        animator.Play("swing");
        StartCoroutine(DelayAction(0.5f, () =>
        {
            if (playerStates == PlayerStates.Dead) return;
            CancelVines();
        }));
    }

    /// <summary>
    /// Hủy bỏ trạng thái lướt với DÂY LEO
    /// </summary>
    public void CancelVines()
    {
        GetComponent<HingeJoint2D>().enabled = false;
        GetComponent<HingeJoint2D>().connectedBody = null;
        animator.Play("jump");
        addRate = 0f;
        usingVines = false;
        if (rigid.velocity.y < 0f)
            Jump(speedYVines);
    }

    /// <summary>
    /// Chuyển trạng thái đã chết
    /// </summary>
    /// <param name="type"></param>
    public void Dead(string type = "")
    {
        //bool rs = usingBird || usingFlyingCarpet || usingRhino || usingSkateboard;
        if(Modules.luckyCharm && totalLuckyCharm > 0f)
        {
            UsingProtect(false, false, 0.5f);
            totalLuckyCharm -= 1f;
            if (type == EnemyType.Wave.ToString())
                Surf(true);
            return;
        }

        bool rs = usingRhino || usingSkateboard;
        CancelAllTranform(false);
        if (rs) return;

        if (playerStates == PlayerStates.Dead) return;

        CancelAllTranform(resetBanana: true);
        CancelMagnet();
        CancelProtect();

        string anim = "die1";
        if (type == ObstacleType.Fire.ToString())
            anim = "die4";
        else if (type == ObstacleType.StoneColumn.ToString() || type == ObstacleType.Cliff.ToString())
        {
            if(Random.value <=0.5f)
                anim = "die2";
            else
                anim = "die3";
            AudioSource.PlayClipAtPoint(deadClips[0], Camera.main.transform.position, Modules.soundSetting);
        }

        print("call dead by " + type);
        animator.Play(anim, false, action: ()=> {
            print("recall");
            if (pagePlay.TutorialSence)
            {
                pagePlay.ShowPopupFinishTutorial();
                return;
            }
            FirebaseManager.Instance.LogEventDieByGround(type);
            pagePlay.ShowSaveLevel();
            pagePlay.UpdateTimePlay(Time.time - lastTime);
        });
        playerStates = PlayerStates.Dead;
        rigid.velocity = new Vector2(0f, -5f);
        if(type == ObstacleType.River.ToString())
            animator.skeletonAnimation.gameObject.GetComponent<MeshRenderer>().sortingOrder = -7;
    }

    /// <summary>
    /// Đổi sang control VÁN TRƯỢT
    /// </summary>
    public void ChangeToSkateboard()
    {
        usingSkateboard = true;
        whatIsGround |=  LayerMask.GetMask("Water");
        skateboard.SetActive(usingSkateboard);
        Achievements.CheckTaskCollectSkateboard(1);
        bodyCollider.gameObject.SetActive(false);
        if (coroutineSkateboard != null) StopCoroutine(coroutineSkateboard);
        coroutineSkateboard = StartCoroutine(DelayAction(skaterboardTime, ()=> {     
            CancelSkateboard();
            UsingProtect(false, false);
        }));
        animator.Play("windsurfing");
        pagePlay.UpdateTimer(skaterboardTime, ItemType.Skateboard);
        addRate = sAddSpeed;
        SoundsInGame.Instance.ChangeSound(true);
        SetCounterCollision(ItemType.Skateboard);
    }
    Coroutine coroutineSkateboard;

    /// <summary>
    /// Hủy bỏ VÁN TRƯỢT. Nếu đang ở dưới nước, thực hiện 1 bước nhảy
    /// </summary>
    /// <param name="isForced"></param>
    public void CancelSkateboard(bool isForced = false,bool isOverride = false)
    {
        if (!(Modules.waterBounce && jumpWaterCount == 0))
            whatIsGround &= ~LayerMask.GetMask("Water");
        usingSkateboard = false;
        skateboard.SetActive(usingSkateboard);
        addRate = 0;
        bodyCollider.gameObject.SetActive(true);
        if (coroutineSkateboard != null) StopCoroutine(coroutineSkateboard);
        if (!isOverride)
        {
            if (isForced) Jump(jumpSpeed);
            SoundsInGame.Instance.ChangeSound();
        }
        pagePlay.UpdateTimer(0f);
        SetCounterCollision();
    }

    /// <summary>
    /// Đổi sang control THẢM BAY
    /// </summary>
    public void ChangeToFlyingCarpet()
    {
        usingFlyingCarpet = true;
        flyingCarpet.SetActive(usingFlyingCarpet);
        Achievements.CheckTaskCollectFlying(1);
        animator.Play("flying_carpet");
        pagePlay.UpdateTimer(flyingTime, ItemType.FlyingCarpet);
        if (coroutineFlyingCarpet != null) StopCoroutine(coroutineFlyingCarpet);
        coroutineFlyingCarpet = StartCoroutine(DelayAction(flyingTime, () => {
            CancelFlyingCarpet(true);
            UsingProtect(false, false);
        }));
        addRate = fAddSpeed;
        SoundsInGame.Instance.ChangeSound(true);
        SetCounterCollision(ItemType.FlyingCarpet);
    }

    Coroutine coroutineFlyingCarpet;
    /// <summary>
    /// Hủy bỏ THẢM BAY
    /// </summary>
    /// <param name="isForced"></param>
    public void CancelFlyingCarpet(bool isForced = false, bool isOverride = false)
    {
        rigid.gravityScale = 1f;
        usingFlyingCarpet = false;
        flyingCarpet.SetActive(usingFlyingCarpet);
        addRate = 0f;
        if (coroutineFlyingCarpet != null) StopCoroutine(coroutineFlyingCarpet);
        if (!isOverride)
        {
            Jump(jumpSpeed);
            SoundsInGame.Instance.ChangeSound();
        }
        pagePlay.UpdateTimer(0f);
        SetCounterCollision();
    }

    /// <summary>
    /// Đổi sang control TÊ GIÁC
    /// </summary>
    public void ChangeToRhino()
    {
        print("tê giác 1 sừng");
        usingRhino = true;
        Rhino.SetActive(usingRhino);
        Achievements.CheckTaskCollectRhino(1);
        SoundsInGame.Instance.ChangeSound(true);
        animator.Play("monkey_riding",true);
        pagePlay.UpdateTimer(rhinoTime, ItemType.Rhino);
        bodyCollider.gameObject.SetActive(false);
        if (coroutineRhino != null) StopCoroutine(coroutineRhino);
        coroutineRhino = StartCoroutine(DelayAction(rhinoTime, () => {   
            CancelRhino(true);
            UsingProtect(false, false);
        }));
        addRate = rAddSpeed;
        SetCounterCollision(ItemType.Rhino);
    }

    Coroutine coroutineRhino;
    /// <summary>
    /// Hủy bỏ TÊ TÊ, nếu đang ở dưới nước thực hiện 1 bước nhảy
    /// </summary>
    /// <param name="isForced"></param>
    public void CancelRhino(bool isForced = false, bool isOverride = false)
    { 
        usingRhino = false;
        Rhino.SetActive(usingRhino);
        addRate = 0f;
        bodyCollider.gameObject.SetActive(true);
        if (coroutineRhino != null) StopCoroutine(coroutineRhino);
        if (!isOverride)
        {
            if (isForced) Jump(jumpSpeed);
            SoundsInGame.Instance.ChangeSound();
        }
        pagePlay.UpdateTimer(0f);
        SetCounterCollision();
    }

    bool isWaitingBird = false;
    /// <summary>
    /// Đổi sang control CHIM CHIM
    /// </summary>
    public void ChangeToBird()
    {
        //Jump(jumpSpeed + 5f);
        //rigid.velocity = new Vector2(rigid.velocity.x, jumpSpeed + 5f);
        usingBird = true;
        isWaitingBird = true;
        animator.Play("monkey_riding_bird");
        Bird.SetActive(usingBird);
        SoundsInGame.Instance.ChangeSound(true);
        StartCoroutine(DelayAction(1f, () =>
        {
            isWaitingBird = false;
            if (!usingBird) return;
            whatIsGround &= ~LayerMask.GetMask("Ground-Force");
            pagePlay.UpdateTimer(birdTime, ItemType.Bird);
            Achievements.CheckTaskCollectStupidBird(1);
            SetCounterCollision(ItemType.Bird);

            if (coroutineBird != null) StopCoroutine(coroutineBird);
            coroutineBird = StartCoroutine(DelayAction(birdTime, () =>
            {
                CancelBird(true);
                UsingProtect(false, false);
            }));
          
        }));   
    }

    /// <summary>
    /// Hủy bỏ Bird
    /// </summary>
    private Coroutine coroutineBird;
    public void CancelBird(bool isForced = false, bool isOverride = false)
    {
        if (isWaitingBird && !isOverride) return;
        whatIsGround |= LayerMask.GetMask("Ground-Force");
        usingBird = false;
        pagePlay.UpdateTimer(0f);
        Bird.SetActive(usingBird);
        if (coroutineBird != null)
            StopCoroutine(coroutineBird);
        if (!isOverride)
            SoundsInGame.Instance.ChangeSound();
        SetCounterCollision();     
        rigid.gravityScale = 1f;
        isWaitingBird = false;

    }

    /// <summary>
    /// Sử dụng TÁO
    /// </summary>
    public void ChangeToApple()
    {
        print("táo cắn dở");
        usingApple = true;
        SoundsInGame.Instance.ChangeSound(true);
        
        pagePlay.UpdateTimer(appleTime, ItemType.Apple);
        if (appleTime > 0)
        {
            if (coroutineApple != null) StopCoroutine(coroutineApple);
            coroutineApple = StartCoroutine(DelayAction(appleTime, () =>
            {
                CancelApple();
            }));
        }
    }

    Coroutine coroutineApple;
    /// <summary>
    /// Huy TÁO
    /// </summary>
    public void CancelApple(bool isForced = false, bool isOverride = false)
    {
        usingApple = false;
        if (!isOverride)
            SoundsInGame.Instance.ChangeSound();
        pagePlay.UpdateTimer(0f);
    }

    #endregion

    #region Using Item and Achivement
    /// <summary>
    /// Thêm hoặc sử dụng NAM CHÂM
    /// </summary>
    public void UsingMagnet()
    {
        print("nam châm");
        usingMagnet = true;
        magnet.SetActive(usingMagnet);
        Achievements.CheckTaskCollectMagnet(1);
        pagePlay.CheckGetMagnet(1);
        if (coroutineMagnet != null) StopCoroutine(coroutineMagnet);
        coroutineMagnet = StartCoroutine(DelayAction(magnetTime, () => {
            CancelMagnet();
        }));
    }

    Coroutine coroutineMagnet;
    /// <summary>
    /// Huỷ bỏ NAM CHÂM
    /// </summary>
    public void CancelMagnet()
    {
        usingMagnet = false;
        magnet.SetActive(usingMagnet);
    }

    /// <summary>
    /// Thêm hoặc sử dụng HỒI SINH
    /// </summary>
    public void UsingRevival()
    {
        StartCoroutine(DelayAction(0f, () => {
            print("hồi sinh ở đây");
            playerStates = PlayerStates.Run;
            animator.Play("run");
            animator.skeletonAnimation.gameObject.GetComponent<MeshRenderer>().sortingOrder = 2;
            rigid.gravityScale = 1f;
            rigid.velocity = Vector2.zero;
            transform.localPosition = new Vector2(transform.localPosition.x, 1.5f);
            //GroundController.Instance.ResetGround();
            RaycastHit2D hit = Physics2D.Raycast(groundCheck.position, Vector3.down, 25f, 1 << LayerMask.NameToLayer("Water"));
            if (hit)
            {
                Surf(true);
                print("call suf");
            }

            Run();
            UsingProtect(true);
            isRolling = false;
            Enemy.Wave.transform.position = new Vector3(transform.position.x - Camera.main.orthographicSize, Enemy.Wave.transform.position.y, Enemy.Wave.transform.position.z);
            Enemy.Wave.WaveTeleport(-1);

            if (soundReborn)
                AudioSource.PlayClipAtPoint(soundReborn, Camera.main.transform.position, Modules.soundSetting);
        }));  
    }

    /// <summary>
    /// Thêm hoặc sử dụng BẢO VỆ
    /// </summary>
    public void UsingProtect(bool isReborn = false, bool useGraphic = true, float inTime = -1)
    {
        if (usingProtect && !useGraphic) return;
        print("được bảo vệ");
        usingProtect = true;
        protect.SetActive(usingProtect);
        protect.transform.GetChild(0).gameObject.SetActive(true);

        float t = protectTime;
        if (isReborn)
            t = 2.5f;
        else if (!useGraphic)
        {
            t = 1.5f;
            if (inTime != -1f)
            {
                t = inTime;
                protect.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
        else
        {
            Achievements.CheckTaskCollectProtect(1);
            pagePlay.CheckGetProtect(1);
        }

        if (coroutineProtect != null) StopCoroutine(coroutineProtect);
        coroutineProtect = StartCoroutine(DelayAction(t, () => {
            CancelProtect();
        }));
    }

    private Coroutine coroutineProtect;
    /// <summary>
    /// Hủy bỏ BẢO VỆ
    /// </summary>
    public void CancelProtect()
    {
        usingProtect = false;
        protect.SetActive(usingProtect);
    }

    /// <summary>
    /// Sử dụng X2 CHUỐI
    /// </summary>
    public void UsingX2Banana()
    {
        Achievements.CheckTaskCollect2XBanana(1);
        usingX2Banana = true;
        //pagePlay.UpdateTimer(x2BananaTime);
        if (coroutineX2Banana != null) StopCoroutine(coroutineX2Banana);
        coroutineX2Banana = StartCoroutine(DelayAction(x2BananaTime, () => {
            usingX2Banana = false;
            //pagePlay.UpdateTimer(0f);
        }));
    }
    Coroutine coroutineX2Banana;

    /// <summary>
    /// Ăn chuối => THÊM CHUỐI
    /// </summary>
    public void EatBanana()
    {
        int count = 1;
        if (usingX2Banana && Modules.goldenBanana) count *= 3;
        else if (usingX2Banana || Modules.goldenBanana) count *= 2;
        pagePlay.UpdateCoin(count);
        pagePlay.CheckCollectBanana(count);
        sBananaCount += count;
        UpdateSuftProcess();
    }

    private void UpdateSuftProcess()
    {
        if (sBananaCount > sBananaRequire) sBananaCount = sBananaRequire;
        float p = (float)sBananaCount / (float)sBananaRequire;
        pagePlay.UpdateSuft(p);

        canbeSuft = sBananaCount >= sBananaRequire;
        if (pagePlay.TutorialSence)
            return;

        if (p >= 1f)
            Enemy.Wave.WaveTeleport(3);
        else if (p >= 0.75f)
            Enemy.Wave.WaveTeleport(2);
        else if (p >= 0.4f)
            Enemy.Wave.WaveTeleport(1);
        //else if (sBananaCount > 0)
        //    Enemy.Wave.WaveTeleport(0);
        else
            Enemy.Wave.WaveTeleport(-1);
       
    }

    /// <summary>
    /// Ăn nguyên nải => THÊM NGUYÊN NẢI ~~
    /// </summary>
    public void EatBananaBunch()
    {
        int count = 3;
        if (usingX2Banana && Modules.goldenBanana) count *= 3;
        else if (usingX2Banana || Modules.goldenBanana) count *= 2;
        pagePlay.UpdateCoin(count);
        pagePlay.CheckCollectBanana(count);
        sBananaCount += count;
        UpdateSuftProcess();
    }

    /// <summary>
    /// Ăn DƯA
    /// </summary>
    public void EatWatermelon()
    {
    }

    public void CancelAllTranform(bool isOverride = true, bool resetBanana = false)
    {
        CancelBird(isOverride: isOverride);
        CancelFlyingCarpet(isOverride: isOverride);
        CancelRhino(isOverride: isOverride);
        CancelSkateboard(isOverride: isOverride);
        CancelApple(isOverride: isOverride);
        //CancelMagnet();
        //CancelProtect();

        GetComponent<HingeJoint2D>().connectedBody = null;
        GetComponent<HingeJoint2D>().enabled = false;

        if (resetBanana)
            sBananaCount = 0;
        UpdateSuftProcess();
    }

    #endregion

    #region Collision 
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (playerStates == PlayerStates.Dead) return;
        switch (collider.tag)
        {
            case "Dead":
                Dead();
                break;
            case "EndGame":
                isCompleted = true;
                CancelAllTranform();
                CancelSuft(Vector2.zero);
                playerStates = PlayerStates.Dead;
                rigid.gravityScale = 1f;
                pagePlay.UpdateTimePlay(Time.time - lastTime);
                StartCoroutine(DelayAction(3f, () =>
                {
                    if (pagePlay.TutorialSence)
                    {
                        pagePlay.ShowPopupFinishTutorial();
                        return;
                    }
                    if (Modules.levelRun == 72) pagePlay.ShowCongrateLevel72();
                    DataManager.Instance.game_Finish = GAME_FINISH.Victory;
                    int tmp = Random.Range(0,2);
                    Debug.LogError("spin: " + tmp);
                    if(tmp==0)
                        pagePlay.ShowPopupLuckySpin();
                    else
                        pagePlay.ShowCompleteLevel();
                    isCompleted = false;
                    playerStates = PlayerStates.Dead;

                }));
                break;
        }
    }

    [HideInInspector]
    public bool isCompleted = false;
    private void OnTriggerStay2D(Collider2D collider)
    {
        if (playerStates == PlayerStates.Dead) return;
        OnTriggerEnter2D(collider);
    }

   
    #endregion

    #region Input Handle
    public void InputDrag(Direction dragDirection)
    {
        switch (dragDirection)
        {
            case Direction.DOWN:
                Down();
                if (pagePlay.TutorialSence) MapTutorial.Instance.tutDown = true;
                break;

            case Direction.UP:
                Jump();
                break;

            case Direction.LEFT:

                break;

            case Direction.RIGHT:
                if (pagePlay.TutorialSence) MapTutorial.Instance.tutSuft = true;
                Surf();        
                break;
        }
    }

    public void InputRelease()
    {
        if (usingFlyingCarpet)
        {
            rigid.velocity = new Vector2(rigid.velocity.x, 0f);
            return;
        }

        if(playerStates == PlayerStates.Parachute)
            rigid.velocity = new Vector2(rigid.velocity.x, -1f);

        if (isGrounded)
        {
            Run();
            return;
        }
        else
        {
            playerStates = PlayerStates.Jump;
            FloatingAnimator();
        }

        if (!usingBird)
            rigid.gravityScale = 1f;
        else
            rigid.gravityScale = 0.5f;
    }

    private void FloatingAnimator()
    {
        if (usingBird 
            || usingFlyingCarpet 
            || usingSkateboard 
            || usingVines 
            || usingRhino       
            || isRolling 
            || usingVines 
            || playerStates == PlayerStates.Dead
            || playerStates == PlayerStates.Surf
            ) return;
        if ( playerStates == PlayerStates.Jump && !isGrounded)
        {
            if ((usingApple || Modules.airBounce)&&jumpCount!=1) animator.Play("jump_down", true, 1.45f);
            else
                animator.Play("jump");
        }
        else
        if (isGrounded) Run();

    }
    #endregion

    #region GENERAL
    IEnumerator DelayAction(float timeDelay, System.Action action)
    {
        yield return new WaitForSeconds(timeDelay);
        if (action != null) action();
        yield break;
    }

    #endregion

}

public enum MODE_GAMEPLAY
{
    Base, //sử dụng touch and cơ chế bung dù như ban đầu
    Version1,// sử dụng button để controll game
    Version2 //sử dụng button để controll game và cơ chế double jump - bỏ bung dù
}