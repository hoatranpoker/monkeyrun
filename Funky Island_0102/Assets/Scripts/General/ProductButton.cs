﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductButton : MonoBehaviour {
    [SerializeField]private string Name; //Ten trung voi ten co trong Name list product IAP
    [SerializeField]private Text price;
    [SerializeField]string productID;

    private void OnEnable()
    {
        ReloadCost();
    }

    public bool UpdateData(string productName)
    {
        bool sucsses = false;
        if (Purchaser.Instance == null) return false;
        foreach (var item in Purchaser.Instance.productIAP)
        {
            if (item.Name == productName)
            {
                Name = productName;
                if (Purchaser.Instance.IsInitialized())
                {
                    if (price != null)
                    {
                        price.text =  "????";
                        string priceStr = Purchaser.m_StoreController.products.WithID(item.ID).metadata.localizedPriceString.ToString();
                        if (priceStr != "")
                        {
                            price.text = priceStr;
                            sucsses = true;
                        }  
                    }
                }
                productID = item.ID;
                break;
            }
        }
        return sucsses;
    }

    private void ReloadCost()
    {
        if (!UpdateData(Name))
            Invoke("ReloadCost", 1);
    }

    public void OnClick()
    {
        if (Purchaser.Instance != null)
        {
            if (productID != "")
            {
                Purchaser.Instance.BuyProductID(productID);
            }
        }
        SoundController.Instance.PlayFX();
    }
}
