﻿
using UnityEngine;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;
using System;
using GooglePlayGames.BasicApi.SavedGame;

public class GoogleSeviceManager : MonoBehaviour {

    #region Singleton
    public static GoogleSeviceManager Instance;

    private void Awake()
    {
        if (Instance == null) Instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        //SettingPopupGravity();
        DG.Tweening.DOVirtual.DelayedCall(1800, () => { UnlockAchivement(GoogleID.Achivement_Gamer_PlayGame30m); });
        ConfigGoogleSevice();
    }

    private void ConfigGoogleSevice()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();
        
    }

    public void SignIn()
    {
        // authenticate user:
        Social.localUser.Authenticate((bool success) => {
            // handle success or failure
            Debug.Log("Login GoogleSevice");
            
        });
    }

    public void SettingPopupGravity()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                Debug.Log("Login GoogleSevice");
                ((GooglePlayGames.PlayGamesPlatform)Social.Active).SetGravityForPopups(Gravity.BOTTOM);
            }
        });
    }

    #endregion

    #region Achivement

    public bool Authenticated
    {
        get
        {
            return Social.Active.localUser.authenticated;
        }
    }

    public void ShowAchivementUI()
    {
        if (Authenticated)
        {
            Debug.Log("ShowAchivementUI");
            Social.ShowAchievementsUI();
        }
        //Social.ShowAchievementsUI();
    }

    ///unlock achievement (achievement ID "_nameAchive")
    public void UnlockAchivement(string _nameAchive)
    {
        Social.ReportProgress(_nameAchive, 100.0f, (bool success) => {
            // handle success or failure
        });
    }

    // increment achievement (achievement ID "_nameAchive") by "_number" steps
    public void IncrementAchivemnet(string _nameAchive,int _number)
    {
        PlayGamesPlatform.Instance.IncrementAchievement(
        _nameAchive, _number, (bool success) => {
            // handle success or failure
        });
    }

    #endregion

    #region LeaderBroad

    public void ShowLeaderbroadUI()
    {
        if (Authenticated)
        {
            Social.ShowLeaderboardUI();
        }
    }

    public void ShowLeaderbroadUIWithID(string Id)
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI(Id);
    }

    // post score "_Score" to leaderboard ID "_IdLeadbroad")
    public void PostScoreLeaderbroad(int _score)
    {
        Social.ReportScore(_score, GPGSIds.leaderboard_distance, (bool success) => {
            // handle success or failure
        });
    }

    //// post score 12345 to leaderboard ID "Cfji293fjsie_QA" and tag "FirstDaily")
    //Social.ReportScore(12345, "Cfji293fjsie_QA", "FirstDaily", (bool success) => {
    //    // handle success or failure
    //});
    
    #endregion
  

    public void SignOut()
    {
        ((PlayGamesPlatform)Social.Active).SignOut();
    }

}
