﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoad : MonoBehaviour {

    #region Save & Load LevelData

    public static void SaveLevelData(List<int> listData)
    {
        string data = "";
        for (int i = 0; i < listData.Count; i++)
        {
            data += listData[i];
            if (i < listData.Count - 1) data += ";";
        }
        SaveLoadData.SaveData("LevelData", data, true);
    }

    public static List<int> LoadLevelData()
    {
        List<int> result = null;
        string load = SaveLoadData.LoadData("LevelData", true);
        if (load == "")//neu chua co gi thi set gia tri mac dinh
        {
            for (int i = 0; i < Modules.totalLevel; i++)
            {
                if (i == 0) load += "0";
                else load += "-1";
                if (i < Modules.totalLevel - 1) load += ";";
            }
        }
        string[] loadData = load.Split(';');
        result = new List<int>();
        for (int i = 0; i < loadData.Length; i++)
            result.Add(int.Parse(loadData[i]));
        //kiem tra so luong level co trung khop
        if (result.Count > Modules.totalLevel)//neu save cu co so luong level lon hon phien ban hien tai thi cat bo
        {
            int numCount = result.Count - Modules.totalLevel;
            for (int i = 0; i < numCount; i++) result.RemoveAt(result.Count - 1);
        }
        else if (result.Count < Modules.totalLevel)//neu co so luong map nho hon phien ban hien tai thi them
        {
            int numCount = Modules.totalLevel - result.Count;
            for (int i = 0; i < numCount; i++) result.Add(-1);
        }
        return result;
    }

    #endregion

    #region Item & Object

    public static void SaveTotalCoin()
    {
        SaveLoadData.SaveData("TotalCoin", Modules.totalCoin.ToString(), true);
    }

    public static int LoadTotalCoin()
    {
        string load = SaveLoadData.LoadData("TotalCoin", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

    public static void SaveTotalMagnet()
    {
        SaveLoadData.SaveData("TotalMagnet", Modules.totalMagnet.ToString(), true);
    }

    public static int LoadTotalMagnet()
    {
        string load = SaveLoadData.LoadData("TotalMagnet", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

    public static void SaveTotal2XBanana()
    {
        SaveLoadData.SaveData("Total2XBanana", Modules.total2XBanana.ToString(), true);
    }

    public static int LoadTotal2XBanana()
    {
        string load = SaveLoadData.LoadData("Total2XBanana", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

    public static void SaveTotalProtect()
    {
        SaveLoadData.SaveData("TotalProtect", Modules.totalProtect.ToString(), true);
    }

    public static int LoadTotalProtect()
    {
        string load = SaveLoadData.LoadData("TotalProtect", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

    public static void SaveTotalReborn()
    {
        SaveLoadData.SaveData("TotalReborn", Modules.totalReborn.ToString(), true);
    }

    public static int LoadTotalReborn()
    {
        string load = SaveLoadData.LoadData("TotalReborn", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

   
    public static void SaveExpPlayer()
    {
        SaveLoadData.SaveData("ExpPlayer", Modules.expPlayer.ToString(), true);
    }

    public static int LoadExpPlayer()
    {
        string load = SaveLoadData.LoadData("ExpPlayer", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

    public static void SaveHighScore()
    {
        SaveLoadData.SaveData("HighScore", Modules.highScore.ToString(), true);
    }

    public static int LoadHighScore()
    {
        string load = SaveLoadData.LoadData("HighScore", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

    public static bool LoadFirstTutorial()
    {
        bool result = false;
        string load = SaveLoadData.LoadData("Tutorial", true);
        if (load == "1") result = true;
        return result;
    }

    public static void SaveFirstTutorial() //Tutorial lan dau
    {
        string data = "0";
        if (Modules.firstTutorial) data = "1";
        SaveLoadData.SaveData("Tutorial", data, true);
    }

    public static bool LoadRateApp()
    {
        bool result = false;
        string load = SaveLoadData.LoadData("RateApp", true);
        if (load == "1") result = true;
        return result;
    }

    public static void SaveRateApp() //Rateapp
    {
        string data = "0";
        if (Modules.firstTutorial) data = "1";
        SaveLoadData.SaveData("RateApp", data, true);
    }

    public static void SaveFirstRecharge() //nap tien lan dau
    {
        string data = "0";
        if (Modules.firstRecharge) data = "1";
        SaveLoadData.SaveData("FirstRecharge", data, true);
    }

    public static bool LoadFirstRecharge()
    {
        bool result = false;
        string load = SaveLoadData.LoadData("FirstRecharge", true);
        if (load == "1") result = true;
        return result;
    }

    public static void SaveGetFirstRecharge()
    {
        string data = "0";
        if (Modules.getFirstRecharge) data = "1";
        SaveLoadData.SaveData("GetFirstRecharge", data, true);
    }

    public static bool LoadGetFirstRecharge()
    {
        bool result = false;
        string load = SaveLoadData.LoadData("GetFirstRecharge", true);
        if (load == "1") result = true;
        return result;
    }

    public static void SaveLuckyCharm()
    {
        string data = "0";
        if (Modules.luckyCharm) data = "1";
        SaveLoadData.SaveData("LuckyCharm", data, true);
    }

    public static bool LoadLuckyCharm()
    {
        bool result = false;
        string load = SaveLoadData.LoadData("LuckyCharm", true);
        if (load == "1") result = true;
        return result;
    }

    public static void SaveGoldenBanana()
    {
        string data = "0";
        if (Modules.goldenBanana) data = "1";
        SaveLoadData.SaveData("GoldenBanana", data, true);
    }

    public static bool LoadGoldenBanana()
    {
        bool result = false;
        string load = SaveLoadData.LoadData("GoldenBanana", true);
        if (load == "1") result = true;
        return result;
    }

    public static void SaveRemoveAds()
    {
        string data = "0";
        if (Modules.removeAds) data = "1";
        SaveLoadData.SaveData("RemoveAds", data, true);
    }

    public static bool LoadRemoveAds()
    {
        bool result = false;
        string load = SaveLoadData.LoadData("RemoveAds", true);
        if (load == "1") result = true;
        return result;
    }

    public static void SaveMegaSale()
    {
        string data = "0";
        if (Modules.megaSale) data = "1";
        SaveLoadData.SaveData("MegaSale", data, true);
    }

    public static bool LoadMegaSale()
    {
        bool result = false;
        string load = SaveLoadData.LoadData("MegaSale", true);
        if (load == "1") result = true;
        return result;
    }

    public static void SaveWaterBounce()
    {
        string data = "0";
        if (Modules.waterBounce) data = "1";
        SaveLoadData.SaveData("WaterBounce", data, true);
    }

    public static bool LoadWaterBounce()
    {
        bool result = false;
        string load = SaveLoadData.LoadData("WaterBounce", true);
        if (load == "1") result = true;
        return result;
    }

    public static void SaveAirBounce()
    {
        string data = "0";
        if (Modules.airBounce) data = "1";
        SaveLoadData.SaveData("AirBounce", data, true);
    }

    public static bool LoadAirBounce()
    {
        bool result = false;
        string load = SaveLoadData.LoadData("AirBounce", true);
        if (load == "1") result = true;
        return result;
    }

    public static void SaveLevelMagnet()
    {
        SaveLoadData.SaveData("LevelMagnet", Modules.levelMagnet.ToString(), true);
    }

    public static int LoadLevelMagnet()
    {
        string load = SaveLoadData.LoadData("LevelMagnet", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

    public static void SaveLevel2XBanana()
    {
        SaveLoadData.SaveData("Level2XBanana", Modules.level2XBanana.ToString(), true);
    }

    public static int LoadLevel2XBanana()
    {
        string load = SaveLoadData.LoadData("Level2XBanana", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

    public static void SaveLevelProtect()
    {
        SaveLoadData.SaveData("LevelProtect", Modules.levelProtect.ToString(), true);
    }

    public static int LoadLevelProtect()
    {
        string load = SaveLoadData.LoadData("LevelProtect", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

    //public static void SaveLevelTransform()
    //{
    //    SaveLoadData.SaveData("LevelTransform", Modules.levelTransform.ToString(), true);
    //}

    //public static int LoadLevelTransform()
    //{
    //    string load = SaveLoadData.LoadData("LevelTransform", true);
    //    if (load == "") load = "0";
    //    return int.Parse(load);
    //}

    public static void SaveLevelBird()
    {
        SaveLoadData.SaveData("LevelBird", Modules.levelBird.ToString(), true);
    }

    public static int LoadLevelBird()
    {
        string load = SaveLoadData.LoadData("LevelBird", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

    public static void SaveLevelRhino()
    {
        SaveLoadData.SaveData("LevelRhino", Modules.levelRhino.ToString(), true);
    }

    public static int LoadLevelRhino()
    {
        string load = SaveLoadData.LoadData("LevelRhino", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

    public static void SaveLevelCarpet()
    {
        SaveLoadData.SaveData("LevelCarpet", Modules.levelCarpet.ToString(), true);
    }

    public static int LoadLevelCarpet()
    {
        string load = SaveLoadData.LoadData("LevelCarpet", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

    public static void SaveLevelSkateboard()
    {
        SaveLoadData.SaveData("LevelSkateboard", Modules.levelSkateboard.ToString(), true);
    }

    public static int LoadLevelSkateboard()
    {
        string load = SaveLoadData.LoadData("LevelSkateboard", true);
        if (load == "") load = "0";
        return int.Parse(load);
    }

    #endregion

    #region xu ly language
    private static readonly Dictionary<SystemLanguage, string> COUTRY_CODES = new Dictionary<SystemLanguage, string>()
        {
            { SystemLanguage.Afrikaans, "ZA" },
            { SystemLanguage.Arabic    , "SA" },
            { SystemLanguage.Basque    , "US" },
            { SystemLanguage.Belarusian    , "BY" },
            { SystemLanguage.Bulgarian    , "BJ" },
            { SystemLanguage.Catalan    , "ES" },
            { SystemLanguage.Chinese    , "CN" },
            { SystemLanguage.Czech    , "HK" },
            { SystemLanguage.Danish    , "DK" },
            { SystemLanguage.Dutch    , "BE" },
            { SystemLanguage.English    , "US" },
            { SystemLanguage.Estonian    , "EE" },
            { SystemLanguage.Faroese    , "FU" },
            { SystemLanguage.Finnish    , "FI" },
            { SystemLanguage.French    , "FR" },
            { SystemLanguage.German    , "DE" },
            { SystemLanguage.Greek    , "JR" },
            { SystemLanguage.Hebrew    , "IL" },
            { SystemLanguage.Icelandic    , "IS" },
            { SystemLanguage.Indonesian    , "ID" },
            { SystemLanguage.Italian    , "IT" },
            { SystemLanguage.Japanese    , "JP" },
            { SystemLanguage.Korean    , "KR" },
            { SystemLanguage.Latvian    , "LV" },
            { SystemLanguage.Lithuanian    , "LT" },
            { SystemLanguage.Norwegian    , "NO" },
            { SystemLanguage.Polish    , "PL" },
            { SystemLanguage.Portuguese    , "PT" },
            { SystemLanguage.Romanian    , "RO" },
            { SystemLanguage.Russian    , "RU" },
            { SystemLanguage.SerboCroatian    , "SP" },
            { SystemLanguage.Slovak    , "SK" },
            { SystemLanguage.Slovenian    , "SI" },
            { SystemLanguage.Spanish    , "ES" },
            { SystemLanguage.Swedish    , "SE" },
            { SystemLanguage.Thai    , "TH" },
            { SystemLanguage.Turkish    , "TR" },
            { SystemLanguage.Ukrainian    , "UA" },
            { SystemLanguage.Vietnamese    , "VN" },
            { SystemLanguage.ChineseSimplified    , "CN" },
            { SystemLanguage.ChineseTraditional    , "CN" },
            { SystemLanguage.Hungarian    , "HU" },
            { SystemLanguage.Unknown    , "EN" },
        };

    private static string ToCountryCode(SystemLanguage language)
    {
        string result;
        if (COUTRY_CODES.TryGetValue(language, out result))
        {
            return result;
        }
        else
        {
            return COUTRY_CODES[SystemLanguage.Unknown];
        }
    }

    public static string LoadCodeLanguage()
    {
        string result = SaveLoadData.LoadData("CodeLanguage", false);
        if (result == "")
        {
            result = ToCountryCode(Application.systemLanguage);
            Modules.codeLanguage = result;
            SaveCodeLanguage();
        }
        return result;
    }

    public static void SaveCodeLanguage()
    {
        SaveLoadData.SaveData("CodeLanguage", Modules.codeLanguage, false);
    }

    #endregion

    #region Audio
    public static void LoadAudioSetting()
    {
        string sound = SaveLoadData.LoadData("SoundSetting", true);
        string music = SaveLoadData.LoadData("MusicSetting", true);
        if (sound == "") sound = "1";
        if (music == "") music = "1";
        Modules.soundSetting = int.Parse(sound);
        Modules.musicSetting = int.Parse(music);
    }

    public static void SaveAudioSetting()
    {
        SaveLoadData.SaveData("SoundSetting", Modules.soundSetting.ToString(), true);
        SaveLoadData.SaveData("MusicSetting", Modules.musicSetting.ToString(), true);
    }

    #endregion

    #region Save Mission
    //Xu ly luu tru du lieu nhiem vu
    public static void LoadTaskState()
    {
        string taskData = SaveLoadData.LoadData("TaskData", true);
        if (taskData == "")
        {
            List<int> listTaskBegin = new List<int>() { 0, 9, 17, 24,31,38,48,53,58,63,68,73,78,83 };
            for (int i = 0; i < Achievements.totalTask; i++)
            {
                string state = "0";//chua hien
                if (listTaskBegin.Contains(i)) state = "1";//da hien
                taskData += state;

                if (i < Achievements.totalTask - 1) taskData += ";";
            }
        }
        string[] taskSave = taskData.Split(';');
        int minTask = Achievements.totalTask;
        if (minTask > taskSave.Length) minTask = taskSave.Length;
        for (int i = 0; i < minTask; i++)
            Achievements.listTask[i].stateTask = int.Parse(taskSave[i]);
    }

    public static void SaveTaskState()
    {
        string dataSave = "";
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            dataSave += Achievements.listTask[i].stateTask.ToString();
            if (i < Achievements.listTask.Count - 1) dataSave += ";";
        }
        SaveLoadData.SaveData("TaskData", dataSave, true);
    }

    #endregion

    #region Collect Achivements

    public static void LoadCollectStar()
    {
        string result = SaveLoadData.LoadData("CollectStar", true);
        if (result == "") result = "0";
        Achievements.numCollectStar = int.Parse(result);
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].typeTask == TypeTask.CollectStar && Achievements.listTask[i].stateTask == 1)
            {
                Achievements.maxCollectStar = Achievements.listTask[i].numberNeed;
                break;
            }
        }
    }

    public static void SaveCollectStar()
    {
        SaveLoadData.SaveData("CollectStar", Achievements.numCollectStar.ToString(), true);
    }

    public static void LoadPassLevel()
    {
        string result = SaveLoadData.LoadData("PassLevel", true);
        if (result == "") result = "0";
        Achievements.numPassLevel = int.Parse(result);
    }

    public static void SavePassLevel()
    {
        SaveLoadData.SaveData("PassLevel", Achievements.numPassLevel.ToString(), true);
    }

    public static void LoadCollectMagnet()
    {
        string result = SaveLoadData.LoadData("CollectMagnet", true);
        if (result == "") result = "0";
        Achievements.numCollectMagnet = int.Parse(result);
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].typeTask == TypeTask.CollectMagnet && Achievements.listTask[i].stateTask == 1)
            {
                Achievements.maxCollectMagnet = Achievements.listTask[i].numberNeed;
                break;
            }
        }
    }

    public static void SaveCollectMagnet()
    {
        SaveLoadData.SaveData("CollectMagnet", Achievements.numCollectMagnet.ToString(), true);
    }

    public static void LoadCollectProtect()
    {
        string result = SaveLoadData.LoadData("CollectProtect", true);
        if (result == "") result = "0";
        Achievements.numCollectProtect = int.Parse(result);
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].typeTask == TypeTask.CollectProtect && Achievements.listTask[i].stateTask == 1)
            {
                Achievements.maxCollectProtect = Achievements.listTask[i].numberNeed;
                break;
            }
        }
    }

    public static void SaveCollectProtect()
    {
        SaveLoadData.SaveData("CollectProtect", Achievements.numCollectProtect.ToString(), true);
    }

    public static void LoadCollect2XBanana()
    {
        string result = SaveLoadData.LoadData("Collect2XBanana", true);
        if (result == "") result = "0";
        Achievements.numCollect2XBanana = int.Parse(result);
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].typeTask == TypeTask.Collect2XBanana && Achievements.listTask[i].stateTask == 1)
            {
                Achievements.maxCollect2XBanana = Achievements.listTask[i].numberNeed;
                break;
            }
        }
    }

    public static void SaveCollect2XBanana()
    {
        SaveLoadData.SaveData("Collect2XBanana", Achievements.numCollect2XBanana.ToString(), true);
    }

    public static void LoadRunMarathon()
    {
        //string result = SaveLoadData.LoadData("RunMarathon", true);
        //if (result == "") result = "0";
        //Achievements.numRunMarathon = int.Parse(result);
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].typeTask == TypeTask.RunMarathon && Achievements.listTask[i].stateTask == 1)
            {
                Achievements.maxRunMarathon = Achievements.listTask[i].numberNeed;
                break;
            }
        }
    }

    public static void SaveRunMarathon()
    {
        SaveLoadData.SaveData("RunMarathon", Achievements.numRunMarathon.ToString(), true);
    }

    public static void LoadCollectFlying()
    {
        string result = SaveLoadData.LoadData("CollectFlying", true);
        if (result == "") result = "0";
        Achievements.numCollectFlying = int.Parse(result);
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].typeTask == TypeTask.CollectFlying && Achievements.listTask[i].stateTask == 1)
            {
                Achievements.maxCollectFlying = Achievements.listTask[i].numberNeed;
                break;
            }
        }
    }

    public static void SaveCollectFlying()
    {
        SaveLoadData.SaveData("CollectFlying", Achievements.numCollectFlying.ToString(), true);
    }

    public static void LoadRunFlying()
    {
        //string result = SaveLoadData.LoadData("RunFlying", true);
        //if (result == "") result = "0";
        //Achievements.numRunFlying = int.Parse(result);
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].typeTask == TypeTask.RunFlying && Achievements.listTask[i].stateTask == 1)
            {
                Achievements.maxRunFlying = Achievements.listTask[i].numberNeed;
                break;
            }
        }
    }

    public static void SaveRunFlying()
    {
        SaveLoadData.SaveData("RunFlying", Achievements.numRunFlying.ToString(), true);
    }

    public static void LoadCollectSkateboard()
    {
        string result = SaveLoadData.LoadData("CollectSkateboard", true);
        if (result == "") result = "0";
        Achievements.numCollectSkateboard = int.Parse(result);
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].typeTask == TypeTask.CollectSkateboard && Achievements.listTask[i].stateTask == 1)
            {
                Achievements.maxCollectSkateboard = Achievements.listTask[i].numberNeed;
                break;
            }
        }
    }

    public static void SaveCollectSkateboard()
    {
        SaveLoadData.SaveData("CollectSkateboard", Achievements.numCollectSkateboard.ToString(), true);
    }

    public static void LoadRunSkateboard()
    {
        //string result = SaveLoadData.LoadData("RunSkateboard", true);
        //if (result == "") result = "0";
        //Achievements.numRunSkateboard = int.Parse(result);
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].typeTask == TypeTask.RunSkateboard && Achievements.listTask[i].stateTask == 1)
            {
                Achievements.maxRunSkateboard = Achievements.listTask[i].numberNeed;
                break;
            }
        }
    }

    public static void SaveRunSkateboard()
    {
        SaveLoadData.SaveData("RunSkateboard", Achievements.numRunSkateboard.ToString(), true);
    }

    public static void LoadCollectRhino()
    {
        string result = SaveLoadData.LoadData("CollectRhino", true);
        if (result == "") result = "0";
        Achievements.numCollectRhino = int.Parse(result);
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].typeTask == TypeTask.CollectRhino && Achievements.listTask[i].stateTask == 1)
            {
                Achievements.maxCollectRhino = Achievements.listTask[i].numberNeed;
                break;
            }
        }
    }

    public static void SaveCollectRhino()
    {
        SaveLoadData.SaveData("CollectRhino", Achievements.numCollectRhino.ToString(), true);
    }

    public static void LoadRunRhino()
    {
        //string result = SaveLoadData.LoadData("RunRhino", true);
        //if (result == "") result = "0";
        //Achievements.numRunRhino = int.Parse(result);
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].typeTask == TypeTask.RunRhino && Achievements.listTask[i].stateTask == 1)
            {
                Achievements.maxRunRhino = Achievements.listTask[i].numberNeed;
                break;
            }
        }
    }

    public static void SaveRunRhino()
    {
        SaveLoadData.SaveData("RunRhino", Achievements.numRunRhino.ToString(), true);
    }

    public static void LoadCollectStupidBird()
    {
        string result = SaveLoadData.LoadData("CollectStupidBird", true);
        if (result == "") result = "0";
        Achievements.numCollectStupidBird = int.Parse(result);
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].typeTask == TypeTask.CollectStupidBird && Achievements.listTask[i].stateTask == 1)
            {
                Achievements.maxCollectStupidBird = Achievements.listTask[i].numberNeed;
                break;
            }
        }
    }

    public static void SaveCollectStupidBird()
    {
        SaveLoadData.SaveData("CollectStupidBird", Achievements.numCollectStupidBird.ToString(), true);
    }

    public static void LoadRunStupidBird()
    {
        for (int i = 0; i < Achievements.listTask.Count; i++)
        {
            if (Achievements.listTask[i].typeTask == TypeTask.RunStupidBird && Achievements.listTask[i].stateTask == 1)
            {
                Achievements.maxRunStupidBird = Achievements.listTask[i].numberNeed;
                break;
            }
        }
    }

    public static void SaveRunStupidBird()
    {
        SaveLoadData.SaveData("RunStupidBird", Achievements.numRunStupidBird.ToString(), true);
    }

    #endregion
}
