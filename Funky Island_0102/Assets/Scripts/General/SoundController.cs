﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour {

    #region singleton
    public static SoundController Instance;
    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    #endregion

    #region Inits
    AudioSource audioSource, clickSource;
    public AudioClip[] audioClips;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        UpdateSetting();
        clickSource = gameObject.AddComponent<AudioSource>();
        clickSource.clip = audioClips[0];
    }

    #endregion

    #region Action
    public void UpdateSetting()
    {
        if (audioSource)
        {
            if (isEnable)
                audioSource.volume = Modules.musicSetting;
            if (!audioSource.isPlaying)
                audioSource.Play();
        }

        if (SoundsInGame.Instance && SoundsInGame.Instance.gameObject)
            SoundsInGame.Instance.sourceBack.volume = Modules.musicSetting;
    }

    public void PlayFX(string type = "click")
    {
        switch (type)
        {
            case "click":
                clickSource.volume = Modules.soundSetting;
                clickSource.Play();
                break;
        }
    }

    bool isEnable = true;
    public void ChangeMusic(bool isEnable = false)
    {
        if (!audioSource && !audioSource.gameObject) return;
        if (isEnable)
            iTween.AudioTo(audioSource.gameObject, iTween.Hash("audiosource", audioSource, "volume", Modules.musicSetting, "time", 0.5f));
        else
            iTween.AudioTo(audioSource.gameObject, iTween.Hash("audiosource", audioSource, "volume", 0f, "time", 0.5f));
        this.isEnable = isEnable;
    }

    #endregion
}
