﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class Purchaser : MonoBehaviour, IStoreListener
{
    public static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;
    public static Purchaser Instance;
    public string AndroidKey;
    public List<ProductIAP> productIAP;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        DontDestroyOnLoad(gameObject);
        //InitializePurchasing();
    }

    void Start()
    {
        if (m_StoreController == null)
        {
            InitializePurchasing();
        }
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 0;
        
    }

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        builder.Configure<IGooglePlayConfiguration>().SetPublicKey(AndroidKey);

        //KHOI TAO CAC TEN MAT HANG TREN CAC STORE
        foreach (var item in productIAP)
        {
            builder.AddProduct(item.ID, item.productType, new IDs(){
                {item.ID, AppleAppStore.Name.ToString()},//dinh nghia them cho ro rang cac store, cung khong can thiet + IOS
                {item.ID, GooglePlay.Name.ToString()},
                {item.ID, FacebookStore.Name.ToString()},
                {item.ID, WindowsStore.Name.ToString()},
                {item.ID, AmazonApps.Name.ToString()},
                {item.ID, TizenStore.Name.ToString()},
                {item.ID, SamsungApps.Name.ToString()},
                {item.ID, XiaomiMiPay.Name.ToString()},
                {item.ID, MoolahAppStore.Name.ToString()},
                {item.ID, MacAppStore.Name.ToString()}
            });
        }
        UnityPurchasing.Initialize(this, builder);
    }

    public bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void BuyProductID(string productId)
    {
        Debug.LogError(1);
        DataManager.Instance.IAPBuying = true;
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) =>
            {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                //RestoreIAP();
                allowRetore = true;
            });
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            RestoreIAP();
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        //Modules.textDebug.text += "\ninit done";
        Debug.Log("OnInitialized: PASS");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
        //RestorePurchases();
    }

    bool allowRetore = false;
    public void RestoreIAP()
    {
        if (!allowRetore) return;
        AddLuckyCharm();
        AddGoldenBanana();
        AddRemoveAds();
        AddMegaSale();
        AddWaterBounce();
        AddAirBounce();
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        //Modules.textDebug.text += "\ninit failed";
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        foreach (var item in productIAP) {
            Debug.LogError(2);
            if (!DataManager.Instance.IAPBuying) break;
            Debug.LogError(3);
            if (String.Equals(args.purchasedProduct.definition.id, item.ID, StringComparison.Ordinal)) {
                switch (item.Type) { 
                    case ModificationType.Normal:
                        AddCoin(item.Count);
                        FacebookController.Instance.LogFinishBuyPurchased(ModificationType.Normal.ToString(), item.Count);
                        break;
                    case ModificationType.LuckyCharm:
                        AddLuckyCharm();
                        FacebookController.Instance.LogFinishBuyPurchased(ModificationType.LuckyCharm.ToString(), item.Count);
                        break;
                    case ModificationType.GoldenBanana:
                        AddGoldenBanana();
                        FacebookController.Instance.LogFinishBuyPurchased(ModificationType.GoldenBanana.ToString(), item.Count);
                        break;
                    case ModificationType.RemoveAds:
                        AddRemoveAds();
                        FacebookController.Instance.LogFinishBuyPurchased(ModificationType.RemoveAds.ToString(), item.Count);
                        break;
                    case ModificationType.MegaSale:
                        AddMegaSale();
                        FacebookController.Instance.LogFinishBuyPurchased(ModificationType.MegaSale.ToString(), item.Count);
                        break;
                    case ModificationType.WaterBounce:
                        AddWaterBounce();
                        FacebookController.Instance.LogFinishBuyPurchased(ModificationType.WaterBounce.ToString(), item.Count);
                        break;
                    case ModificationType.AirBounce:
                        AddAirBounce();
                        FacebookController.Instance.LogFinishBuyPurchased(ModificationType.AirBounce.ToString(), item.Count);
                        break;
                }
                GoogleSeviceManager.Instance.UnlockAchivement(GoogleID.Achivement_MonkeyRich_Buy1PackInIAP);
                FacebookController.Instance.LogPurchasedEvent(1, "", "", "USD", double.Parse(m_StoreController.products.WithID(item.ID).metadata.localizedPriceString));
            }
        }
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        FacebookController.Instance.LogCancerPurchaseEvent(product.definition.storeSpecificId, 1);
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }

    public void AutoBuy(ProductType type, int quantity) {
        foreach (var item in productIAP) {
            if (item.productType == type && item.Count >= quantity) {
                BuyProductID(item.ID);
                break;
            }
        }
    }

    #region Result Buy

    public void AddCoin(int number)
    {
        Modules.firstRecharge = true;
        SaveLoad.SaveFirstRecharge();
        if (number == 1000)
        {
            Modules.totalCoin += 1000;
            SaveLoad.SaveTotalCoin();
            FirebaseManager.Instance.LogEventEarnBanana("IAP_Mini");
        }
        else if (number == 2000)
        {
            Modules.totalCoin += 2000;
            SaveLoad.SaveTotalCoin();
            Modules.totalMagnet += 1;
            SaveLoad.SaveTotalMagnet();
            FirebaseManager.Instance.LogEventEarnBanana("IAP_Small");
        }
        else if (number == 5000)
        {
            Modules.totalCoin += 5000;
            SaveLoad.SaveTotalCoin();
            Modules.totalMagnet += 5;
            SaveLoad.SaveTotalMagnet();
            Modules.total2XBanana += 5;
            SaveLoad.SaveTotal2XBanana();
            if (DataManager.Instance.Special) AddLuckyCharm();
            FirebaseManager.Instance.LogEventEarnBanana("IAP_Medium");
        }
        else if (number == 10000)
        {
            Modules.totalCoin += 10000;
            SaveLoad.SaveTotalCoin();
            Modules.totalMagnet += 5;
            SaveLoad.SaveTotalMagnet();
            Modules.total2XBanana += 5;
            SaveLoad.SaveTotal2XBanana();
            Modules.totalProtect += 5;
            SaveLoad.SaveTotalProtect();
            FirebaseManager.Instance.LogEventEarnBanana("IAP_Large");
        }
        else if (number == 20000)
        {
            Modules.totalCoin += 20000;
            SaveLoad.SaveTotalCoin();
            Modules.totalMagnet += 10;
            SaveLoad.SaveTotalMagnet();
            Modules.total2XBanana += 10;
            SaveLoad.SaveTotal2XBanana();
            Modules.totalProtect += 10;
            SaveLoad.SaveTotalProtect();
            Modules.totalReborn += 4;
            SaveLoad.SaveTotalReborn();
            FirebaseManager.Instance.LogEventEarnBanana("IAP_ExtraLarge");
        }
        else if (number == 50000)
        {
            Modules.totalCoin += 57000;
            SaveLoad.SaveTotalCoin();
            Modules.totalMagnet += 20;
            SaveLoad.SaveTotalMagnet();
            Modules.total2XBanana += 20;
            SaveLoad.SaveTotal2XBanana();
            Modules.totalProtect += 20;
            SaveLoad.SaveTotalProtect();
            Modules.totalReborn += 8;
            SaveLoad.SaveTotalReborn();
            FirebaseManager.Instance.LogEventEarnBanana("IAP_Huge");          
        }
        else if (number == 100000)
        {
            Modules.totalCoin += 110000;
            SaveLoad.SaveTotalCoin();
            Modules.totalMagnet += 40;
            SaveLoad.SaveTotalMagnet();
            Modules.total2XBanana += 40;
            SaveLoad.SaveTotal2XBanana();
            Modules.totalProtect += 40;
            SaveLoad.SaveTotalProtect();
            Modules.totalReborn += 16;
            SaveLoad.SaveTotalReborn();
            FirebaseManager.Instance.LogEventEarnBanana("IAP_ExtraHuge");
        }
    }

    public void AddLuckyCharm()
    {
        Modules.firstRecharge = true;
        SaveLoad.SaveFirstRecharge();
        Modules.luckyCharm = true;
        SaveLoad.SaveLuckyCharm();
    }

    public void AddGoldenBanana()
    {
        Modules.firstRecharge = true;
        SaveLoad.SaveFirstRecharge();
        Modules.goldenBanana = true;
        SaveLoad.SaveGoldenBanana();
    }

    public void AddRemoveAds()
    {
        Modules.firstRecharge = true;
        SaveLoad.SaveFirstRecharge();
        Modules.removeAds = true;
        SaveLoad.SaveRemoveAds();
    }

    public void AddMegaSale()
    {
        Modules.firstRecharge = true;
        SaveLoad.SaveFirstRecharge();
        Modules.megaSale = true;
        SaveLoad.SaveMegaSale();
    }

    public void AddWaterBounce()
    {
        Modules.firstRecharge = true;
        SaveLoad.SaveFirstRecharge();
        Modules.waterBounce = true;
        SaveLoad.SaveWaterBounce();
    }

    public void AddAirBounce()
    {
        Modules.firstRecharge = true;
        SaveLoad.SaveFirstRecharge();
        Modules.airBounce = true;
        SaveLoad.SaveAirBounce();
    }

    public bool Purchased(string ID)
    {
        if (m_StoreController != null)
            return m_StoreController.products.WithID(ID).hasReceipt;
        else
            return false;
    }

    #endregion
}

#region More
[System.Serializable]
public class ProductIAP
{
    public string Name;
    public string ID;
    public ProductType productType;
    public ModificationType Type;
    public int Count;
}

public enum ModificationType
{ 
    Normal,
    LuckyCharm,
    GoldenBanana,
    RemoveAds,
    MegaSale,
    WaterBounce,
    AirBounce
}

#endregion