﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableGameobject : MonoBehaviour {

    public GameObject disableGameObject;

    public void DisableGameObjectAction()
    {
        disableGameObject.SetActive(false);
        
    }
}
