﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Modules : MonoBehaviour {

    public static string gameName = "Monkey Run";
    public static string gameVersion = "1.0";
    public static string codeLanguage = "EN";

    public static List<Font> listFont = new List<Font>();//so luong font cho ngon ngu dang dung
    public static List<ListTextFont> listTextFont = new List<ListTextFont>();//text va vi tri font cho tung text cua ngon ngu dang dung
    public static List<ListCodeFlag> listFlag = new List<ListCodeFlag>();//danh sprite va code flags cho cac ngon ngu
    public static List<int> dataLevel = new List<int>();//-1: chua dat, 0: da dat, 1: 1 sao, 2: 2 sao, 3: 3 sao.
    public static List<TaskLevel> listTask = new List<TaskLevel>();

    public static int totalLevel = 72;//tong so level ma tro choi co
    public static int indexTaskLevel = -1;//la index hien tai trong danh sach listTask, -1 la khong co
    public static int numLevelPerPage = 12;//moi page chua 20 level, tu dong phan page
    public static int soundSetting = 1;//1 on, 0 off
    public static int musicSetting = 1;//1 on, 0 off 
    public static bool haveNative = false;
    public static int NumberPageLoading = 0;

    //xu ly cac du lieu cua player
    public static int highScore = 0;              //highScore
    public static int expPlayer = 0;             //Exp
    public static int totalCoin = 0;            //Coins == Banana

    #region Upgrade

    public static int totalMagnet {
        get { return DataManager.Instance.LoadData_TotalShopItem(TypeItemBuy.Magnet); }
        set { DataManager.Instance.Setdata_TotalShopItem(TypeItemBuy.Magnet, value); }
    }
    public static int total2XBanana {
        get { return DataManager.Instance.LoadData_TotalShopItem(TypeItemBuy.X2Banana); }
        set { DataManager.Instance.Setdata_TotalShopItem(TypeItemBuy.X2Banana, value); }
    }
    public static int totalProtect {
        get { return DataManager.Instance.LoadData_TotalShopItem(TypeItemBuy.Protect); }
        set { DataManager.Instance.Setdata_TotalShopItem(TypeItemBuy.Protect, value); }
    }
    public static int totalReborn {
        get { return DataManager.Instance.LoadData_TotalShopItem(TypeItemBuy.Reborn); }
        set { DataManager.Instance.Setdata_TotalShopItem(TypeItemBuy.Reborn, value); }
    }

    //Level upgrade
    public static int levelMagnet {
        get { return DataManager.Instance.LoadData_LevelItemUpgrade(TypeItemUpgrade.Magnet); }
        set { DataManager.Instance.Setdata_LevelItemUpgrade(TypeItemUpgrade.Magnet,value); }
    } 

    public static int level2XBanana{
        get { return DataManager.Instance.LoadData_LevelItemUpgrade(TypeItemUpgrade.X2Banana); }
        set { DataManager.Instance.Setdata_LevelItemUpgrade(TypeItemUpgrade.X2Banana, value); }
    }

    public static int levelProtect {
        get { return DataManager.Instance.LoadData_LevelItemUpgrade(TypeItemUpgrade.Protect); }
        set { DataManager.Instance.Setdata_LevelItemUpgrade(TypeItemUpgrade.Protect, value); }
    }

    public static int levelBird{
        get;
        set;
    }

    public static int levelRhino{
        get { return DataManager.Instance.LoadData_LevelItemUpgrade(TypeItemUpgrade.Rhino); }
        set { DataManager.Instance.Setdata_LevelItemUpgrade(TypeItemUpgrade.Rhino, value); }
    }

    public static int levelCarpet{
        get { return DataManager.Instance.LoadData_LevelItemUpgrade(TypeItemUpgrade.Carpet); }
        set { DataManager.Instance.Setdata_LevelItemUpgrade(TypeItemUpgrade.Carpet, value); }
    }

    public static int levelSkateboard {
        get { return DataManager.Instance.LoadData_LevelItemUpgrade(TypeItemUpgrade.Skateboard); }
        set { DataManager.Instance.Setdata_LevelItemUpgrade(TypeItemUpgrade.Skateboard, value); }
    }

    #endregion

    //cost item buy
    public static int costMagnet = 200;
    public static int cost2XBanana = 200;
    public static int costProtect = 200;
    public static int costReborn = 500;

    //effect item upgrade
    public static List<UpgradeLevelItem> listLevelMagnet;
    public static List<UpgradeLevelItem> listLevel2XBanana;
    public static List<UpgradeLevelItem> listLevelProtect;
    public static List<UpgradeLevelItem> listLevelTransform;
    public static List<UpgradeLevelItem> listLevelBird;
    public static List<UpgradeLevelItem> listLevelRhino;
    public static List<UpgradeLevelItem> listLevelCarpet;
    public static List<UpgradeLevelItem> listLevelSkateboard;

    //xu ly chuyen dong trong man choi
    public static bool runEndless = false;
    public static bool tutorial()
    {
        if (PlayerPrefs.GetString(SaveID.Tutorial) != SaveID.True) return true;
        else return false;
    }
    public static int levelRun = 1;

    //xu ly time load du lieu
    public static float requestTime = 0.1f;
    public static float maxTime = 3;//cho phep thuc hien toi da 10 giay

    //xu ly facebook function
    public static GameObject myFBController, myAdController;
    public static string myFBId = "";
    public static string myFBName = "";
    public static Sprite myFBAvatar = null;
    public static string myFBAvatarLink = "";
    public static string linkPostFBScore = "http://35.204.237.69/MonkeyRun/PostFBScore.php";
    public static string linkGetFBScore = "http://35.204.237.69/MonkeyRun/GetFBScore.php";
    public static string linkGetFBScoreFriend = "http://35.204.237.69/MonkeyRun/GetFBScoreFriend.php";
    public static string linkGetMoreGame = "http://35.204.237.69/MonkeyRun/GetMoreGame.php";

    //xy ly hien thi quang cao
    public static int timeShowInterAd = 3;
    public static int countTimeShow = 0;
    //public static int typeRewardVideo = 0;//0 reborn, 1 reward banana
    public static type_RewardAds typeRewardVideo = 0;
    public static int coinRewardVideo = 200;//so chuoi thuong sau khi xem video

    //xu ly purchase
    public static string oldScene = Key_Const.Menu;
    public static string oldSceneShop = Key_Const.Menu;
    public static bool tabIAPBuyCoin = false;
    public static bool tabShopItem = true;
    public static bool additiveIAP = false;
    public static bool pauseTimeSave = false;
    public static bool firstRecharge = false;
    public static bool firstTutorial = false;
    public static bool rateApp = false;
    public static bool getFirstRecharge = false;
    public static bool luckyCharm = false;
    public static bool goldenBanana = false;
    public static bool removeAds = false;
    public static bool megaSale = false;
    public static bool waterBounce = false;
    public static bool airBounce = false;
    public static bool special = false;
    
    public static void GetSaveData()
    {
        //khoi tao list level item upgrade
        listLevelMagnet = new List<UpgradeLevelItem>();
        listLevelMagnet.Add(new UpgradeLevelItem(0, 10));
        listLevelMagnet.Add(new UpgradeLevelItem(500, 12));
        listLevelMagnet.Add(new UpgradeLevelItem(1000, 14));
        listLevelMagnet.Add(new UpgradeLevelItem(2000, 16));
        listLevelMagnet.Add(new UpgradeLevelItem(5000, 18));
        listLevelMagnet.Add(new UpgradeLevelItem(10000, 20));

        listLevel2XBanana = new List<UpgradeLevelItem>();
        listLevel2XBanana.Add(new UpgradeLevelItem(0, 10));
        listLevel2XBanana.Add(new UpgradeLevelItem(500, 12));
        listLevel2XBanana.Add(new UpgradeLevelItem(1000, 14));
        listLevel2XBanana.Add(new UpgradeLevelItem(2000, 16));
        listLevel2XBanana.Add(new UpgradeLevelItem(5000, 18));
        listLevel2XBanana.Add(new UpgradeLevelItem(10000, 20));

        listLevelProtect = new List<UpgradeLevelItem>();
        listLevelProtect.Add(new UpgradeLevelItem(0, 10));
        listLevelProtect.Add(new UpgradeLevelItem(500, 12));
        listLevelProtect.Add(new UpgradeLevelItem(1000, 14));
        listLevelProtect.Add(new UpgradeLevelItem(2000, 16));
        listLevelProtect.Add(new UpgradeLevelItem(5000, 18));
        listLevelProtect.Add(new UpgradeLevelItem(10000, 20));

        listLevelTransform = new List<UpgradeLevelItem>();
        listLevelTransform.Add(new UpgradeLevelItem(0, 500));
        listLevelTransform.Add(new UpgradeLevelItem(500, 450));
        listLevelTransform.Add(new UpgradeLevelItem(1000, 400));
        listLevelTransform.Add(new UpgradeLevelItem(2000, 350));
        listLevelTransform.Add(new UpgradeLevelItem(5000, 300));
        listLevelTransform.Add(new UpgradeLevelItem(10000, 250));

        listLevelBird = new List<UpgradeLevelItem>();
        listLevelBird.Add(new UpgradeLevelItem(0, 500));
        listLevelBird.Add(new UpgradeLevelItem(1000, 450));
        listLevelBird.Add(new UpgradeLevelItem(3000, 400));
        listLevelBird.Add(new UpgradeLevelItem(7000, 350));
        listLevelBird.Add(new UpgradeLevelItem(12000, 300));
        listLevelBird.Add(new UpgradeLevelItem(18000, 250));

        listLevelRhino = new List<UpgradeLevelItem>();
        listLevelRhino.Add(new UpgradeLevelItem(0, 10));
        listLevelRhino.Add(new UpgradeLevelItem(1000, 12));
        listLevelRhino.Add(new UpgradeLevelItem(3000, 14));
        listLevelRhino.Add(new UpgradeLevelItem(7000, 16));
        listLevelRhino.Add(new UpgradeLevelItem(12000, 18));
        listLevelRhino.Add(new UpgradeLevelItem(18000, 20));

        listLevelCarpet = new List<UpgradeLevelItem>();
        listLevelCarpet.Add(new UpgradeLevelItem(0, 10));
        listLevelCarpet.Add(new UpgradeLevelItem(1000, 12));
        listLevelCarpet.Add(new UpgradeLevelItem(3000, 14));
        listLevelCarpet.Add(new UpgradeLevelItem(7000, 16));
        listLevelCarpet.Add(new UpgradeLevelItem(12000, 18));
        listLevelCarpet.Add(new UpgradeLevelItem(18000, 20));

        listLevelSkateboard = new List<UpgradeLevelItem>();
        listLevelSkateboard.Add(new UpgradeLevelItem(0, 10));
        listLevelSkateboard.Add(new UpgradeLevelItem(1000, 12));
        listLevelSkateboard.Add(new UpgradeLevelItem(3000, 14));
        listLevelSkateboard.Add(new UpgradeLevelItem(7000, 16));
        listLevelSkateboard.Add(new UpgradeLevelItem(12000, 18));
        listLevelSkateboard.Add(new UpgradeLevelItem(18000, 20));
       
        //load du lieu luu tru
        codeLanguage = SaveLoad.LoadCodeLanguage();
        totalCoin = SaveLoad.LoadTotalCoin();
        totalMagnet = SaveLoad.LoadTotalMagnet();
        total2XBanana = SaveLoad.LoadTotal2XBanana();
        totalProtect = SaveLoad.LoadTotalProtect();
        totalReborn = SaveLoad.LoadTotalReborn();

        //level
        levelMagnet = SaveLoad.LoadLevelMagnet();
        level2XBanana = SaveLoad.LoadLevel2XBanana();
        levelProtect = SaveLoad.LoadLevelProtect();
        //levelTransform = SaveLoad.LoadLevelTransform();
        levelBird = SaveLoad.LoadLevelBird();
        levelRhino = SaveLoad.LoadLevelRhino();
        levelCarpet = SaveLoad.LoadLevelCarpet();
        levelSkateboard = SaveLoad.LoadLevelSkateboard();

        expPlayer = SaveLoad.LoadExpPlayer();
        highScore = SaveLoad.LoadHighScore();
        dataLevel = SaveLoad.LoadLevelData();
        firstRecharge = SaveLoad.LoadFirstRecharge();
        firstTutorial = SaveLoad.LoadFirstTutorial();
        rateApp = SaveLoad.LoadRateApp();
        getFirstRecharge = SaveLoad.LoadGetFirstRecharge();
        luckyCharm = SaveLoad.LoadLuckyCharm();
        goldenBanana = SaveLoad.LoadGoldenBanana();
        removeAds = SaveLoad.LoadRemoveAds();
        megaSale = SaveLoad.LoadMegaSale();
        waterBounce = SaveLoad.LoadWaterBounce();
        airBounce = SaveLoad.LoadAirBounce();

        SetListTextFont();
        GetFlagLanguage();

        SaveLoad.LoadAudioSetting();
        Achievements.CreateListTask();
        Achievements.CreateListLevel();

        SaveLoad.LoadTaskState();
        SaveLoad.LoadCollectStar();
        SaveLoad.LoadPassLevel();
        SaveLoad.LoadCollectMagnet();
        SaveLoad.LoadCollectProtect();
        SaveLoad.LoadCollect2XBanana();
        SaveLoad.LoadRunMarathon();
        SaveLoad.LoadCollectFlying();
        SaveLoad.LoadRunFlying();
        SaveLoad.LoadCollectSkateboard();
        SaveLoad.LoadRunSkateboard();
        SaveLoad.LoadCollectRhino();
        SaveLoad.LoadRunRhino();
        SaveLoad.LoadCollectStupidBird();
        SaveLoad.LoadRunStupidBird();
    }

    public static void CheckShowInterAd()
    {
        AdMobManager.Instance.showInterstitial();

    }

    #region Language
    private static int GetIndexLanguage()
    {
        int result = 0;
        TextAsset bindata = Resources.Load("Languages/Info") as TextAsset;
        string[] str = bindata.text.TrimEnd('\n').Split('\n');
        for (int i = 0; i < str.Length; i++)
        {
            if (codeLanguage == str[i])
            {
                result = i;
                break;
            }
        }
        codeLanguage = str[result];
        SaveLoad.SaveCodeLanguage();
        return result;
    }

    public static void GetFlagLanguage()
    {
        TextAsset bindata = Resources.Load("Languages/Info") as TextAsset;
        string[] str = bindata.text.TrimEnd('\n').Split('\n');
        listFlag = new List<ListCodeFlag>();
        for (int i = 0; i < str.Length; i++)
        {
            Sprite icon = Resources.Load<Sprite>("Languages/" + i.ToString() + "/Flag");
            listFlag.Add(new ListCodeFlag(str[i], icon));
        }
    }

    public static void SetListTextFont()
    {
        string folderName = "Languages/" + GetIndexLanguage().ToString() + "/";
        TextAsset textFont = Resources.Load(folderName + "Text") as TextAsset;
        string[] str = textFont.text.TrimEnd('\n').Split('\n');
        listTextFont = new List<ListTextFont>();
        for (int i = 0; i < str.Length; i++)
        {
            string[] sub = str[i].Split('|');
            listTextFont.Add(new ListTextFont(sub[0], int.Parse(sub[1])));
        }
        //get list font now
        List<int> allFont = new List<int>();
        for (int i = 0; i < listTextFont.Count; i++)
        {
            if (!allFont.Contains(listTextFont[i].indexFont))
                allFont.Add(listTextFont[i].indexFont);
        }
        allFont.Sort();
        listFont = new List<Font>();
        for (int i = 0; i < allFont.Count; i++)
        {
            listFont.Add(Resources.Load(folderName + "Fonts/" + allFont[i].ToString()) as Font);
        }
    }

    public static void SetTextTaskLevel(TypeTaskLevel typeTask, Text textInput, int number)
    {
        if (listTextFont.Count <= 0) return;
        int index = 93;
        if (typeTask == TypeTaskLevel.CollectBanana)
        {
            index = 93;
        }
        else if (typeTask == TypeTaskLevel.Teeter)
        {
            index = 95;
        }
        else if (typeTask == TypeTaskLevel.KillNative)
        {
            index = 96;
        }
        else if (typeTask == TypeTaskLevel.BreakCobweb)
        {
            index = 97;
        }
        else if (typeTask == TypeTaskLevel.BreakBarrel)
        {
            index = 98;
        }
        else if (typeTask == TypeTaskLevel.JumpMushrooms)
        {
            index = 99;
        }
        else if (typeTask == TypeTaskLevel.Surfing)
        {
            index = 100;
        }
        else if (typeTask == TypeTaskLevel.PassQuagmire)
        {
            index = 101;
        }
        else if (typeTask == TypeTaskLevel.GetProtect)
        {
            index = 102;
        }
        else if (typeTask == TypeTaskLevel.GetMagnet)
        {
            index = 103;
        }
        textInput.font = listFont[listTextFont[index].indexFont];
        textInput.text = listTextFont[index].textValue.Replace("[?]", number.ToString());
    }
    #endregion

    public static bool HaveInternet()
    {
        bool result = true;
        if (Application.internetReachability == NetworkReachability.NotReachable)
            result = false;
        return result;
    }

}

#region Class More

[System.Serializable]//de show ra phan input cua unity editor
public class ListTextFont
{
    public string textValue;
    public int indexFont;
    public ListTextFont(string textValueInput, int indexFontInput)
    {
        this.textValue = textValueInput;
        this.indexFont = indexFontInput;
    }
}

public class ListCodeFlag
{
    public string codeFlag;
    public Sprite iconFlag;
    public ListCodeFlag(string codeFlagInput, Sprite iconFlagInput)
    {
        this.codeFlag = codeFlagInput;
        this.iconFlag = iconFlagInput;
    }
}

public class UpgradeLevelItem
{
    public int costItem;
    public int effectItem;
    public UpgradeLevelItem(int costInput, int effectInput)
    {
        this.costItem = costInput;
        this.effectItem = effectInput;
    }
}

#endregion