﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Unity.Editor;
using Firebase.Analytics;
using Firebase.Invites;
using Firebase.Messaging;
using System.Threading.Tasks;
using System;
using Facebook.Unity;
using Facebook.MiniJSON;

public class FirebaseManager : MonoBehaviour {

    #region singleton
    public static FirebaseManager Instance;
    private void Awake()
    {
        if (Instance == null) Instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;
    public bool firebaseInitialized = false;
    
    // When the app starts, check to make sure that we have
    // the required dependencies to use Firebase, and if not,
    // add them if possible.
    private void Start()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                InitializeFirebase();
               
            }
            else
            {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });

    }

    #endregion

    #region CallStart
    // Handle initialization of the necessary firebase modules:
    private void InitializeFirebase()
    {
        Debug.Log("Enabling data collection.");
        FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
        Debug.Log("Set user properties.");
        // Set the user's sign up method.
        FirebaseAnalytics.SetUserProperty(FirebaseAnalytics.UserPropertySignUpMethod, "Google");
        // Set default session duration values.
        FirebaseAnalytics.SetMinimumSessionDuration(new TimeSpan(0, 0, 10));
        FirebaseAnalytics.SetSessionTimeoutDuration(new TimeSpan(0, 30, 0));
        firebaseInitialized = true;
        DisplayAnalyticsInstanceId();
        Init_Invite();
        AnalyticsLogin();
        MessengeCallStart();
        Debug.Log("Done Init Firebase");
    }

    // Get the current app instance ID.
    public Task<string> DisplayAnalyticsInstanceId()
    {
        return FirebaseAnalytics.GetAnalyticsInstanceIdAsync().ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.Log("App instance ID fetch was canceled.");
            }
            else if (task.IsFaulted)
            {
                Debug.Log(String.Format("Encounted an error fetching app instance ID {0}",
                                        task.Exception.ToString()));
            }
            else if (task.IsCompleted)
            {
                Debug.Log(String.Format("App instance ID: {0}", task.Result));
            }
            return task;
        }).Unwrap();
    }

   
    public void AnalyticsLogin()
    {
        
        // Log an event with no parameters.
        Debug.Log("Logging a login event.");
        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLogin);
    }

    #endregion

    #region Event Game Analytic

    //Log Event when start Tutorial
    public void LogEventStartTutorial()
    {
        if (firebaseInitialized)
        {
            string str = "Start_Tutorial";
            FirebaseAnalytics.LogEvent(str);
            FB.LogAppEvent(str, 1);
        }
    }

    //Log Event when start Tutorial
    public void LogEventFinishTutorial()
    {
        if (firebaseInitialized)
        {
            string str = "Finish_Tutorial";
            FirebaseAnalytics.LogEvent(str);
            FB.LogAppEvent(str, 1);
        }
    }

    //Log Event when start game level
    public void LevelStart(int lv=0,string nameEvent=Log_Name_Event.Level_Start)
    {
        if (firebaseInitialized)
        {
            string str = nameEvent + lv;
            Debug.LogError(str);
            FirebaseAnalytics.LogEvent(str);
            FB.LogAppEvent(str, 1);
        }
    }

    //Log Event when monkey die
    public void LevelDie(int lv = 0, string nameEvent = Log_Name_Event.Monkey_Die_)
    {
        if (firebaseInitialized)
        {
            string str = nameEvent + lv;
            Debug.LogError(str);
            FirebaseAnalytics.LogEvent(str);
            FB.LogAppEvent(str, 1);
        }
    }

    //Event Log Continue
    public void LogEventContinue(string namePara)
    {
        if (firebaseInitialized)
        {
            PagePlay.Instance.numReborn++;
            string str = Log_Name_Event.Continue + Modules.levelRun;
            Debug.LogError(str);
            FirebaseAnalytics.LogEvent(str, PagePlay.Instance.numReborn.ToString(), namePara);
            FB.LogAppEvent(str, 1);
        }
    }

    //Log Event when game end
    public void LevelEnd(int lv = 0, string nameEvent = Log_Name_Event.Level_Victory)
    {
        if (firebaseInitialized)
        {
            string str = nameEvent + lv;
            Debug.LogError(str);
            FirebaseAnalytics.LogEvent(str);
            FB.LogAppEvent(str, 1);
        }
    }

    //Log User Level Unlock
    public void LogUserLevel()
    {
        if (firebaseInitialized)
        {
            int tmp = 0;
            for (int i = 0; i < Modules.dataLevel.Count; i++)
            {
                if (Modules.dataLevel[i] != -1) tmp++;
            }
            Debug.LogError("User Level :" + tmp);
            string paraName = Log_Name_Event.Level_User + tmp;
            FirebaseAnalytics.LogEvent(paraName);
            FB.LogAppEvent(paraName, 1);
        }
    }

    //Log Event Die from Kilometer
    public void LogEventDieBelowMeter(int _meter)
    {
        if (firebaseInitialized)
        {
            string paraName = "";
            if (_meter < 100)
            {
                paraName = "Under 100m";
            }
            else if (100 <= _meter && _meter < 250)
            {
                paraName = "Form 100m to 250m";
            }
            else if (250 <= _meter && _meter < 500)
            {
                paraName = "Form 250m to 500m";
            }
            else if (500 <= _meter && _meter < 1000)
            {
                paraName = "Form 500m to 1000m";
            }
            else if (1000 <= _meter && _meter < 2000)
            {
                paraName = "On 1000m";
            }
            else if (2000 <= _meter)
            {
                paraName = "On 2000m";
            }

            //
            Debug.LogError("Distance_" + paraName + "  -- " + _meter);
            FirebaseAnalytics.LogEvent("Distance_" + paraName, paraName,1);   
        }
    }

    //Monkey die by Ground
    public void LogEventDieByGround(string namePara)
    {
        if (firebaseInitialized)
        {
            string name = GroundController.Instance.gameObject.transform.GetChild(0).name.Replace("(Clone)","");
            string str = "Die_By_Ground_" +name ;
            Debug.LogError(str);
            FirebaseAnalytics.LogEvent(str,namePara,namePara);
            FB.LogAppEvent(str, 1);
        }
    }

    //Event Log Earn/Spent
    public void LogEventEarnBanana(string paraName)
    {
        if (firebaseInitialized)
        {
            string str = Log_Name_Event.EarnBanana + paraName;
            Debug.LogError(str);
            FirebaseAnalytics.LogEvent(str);
            FB.LogAppEvent(str,1);
        }
    }

    public void LogEventSpentBanana(string paraName)
    {
        if (firebaseInitialized)
        {
            string str = Log_Name_Event.SpentBanana + paraName;
            Debug.LogError(str);
            FirebaseAnalytics.LogEvent(str);
            FB.LogAppEvent(str);
        }
    }

    //Session time
    public void LogEventSessionTime_On_day(string paraName)
    {
        if (firebaseInitialized)
        {
            string str = Log_Name_Event.SessionTimeOnDay + paraName;
            Debug.LogError(str);
            FirebaseAnalytics.LogEvent(str);
            FB.LogAppEvent(str, 1);
        }  
    }

    public void LogEventSessionTime_One_Game(string paraName)
    {
        if (firebaseInitialized)
        {
            string str = Log_Name_Event.SessionTimeOneGame + paraName;
            Debug.LogError(str);
            FirebaseAnalytics.LogEvent(str);
            FB.LogAppEvent(str, 1);
        } 
    }

    public void LogEventSession_Open_Game(string paraName)
    {
        if (firebaseInitialized)
        {
            string str = Log_Name_Event.SessionTimeOpenGame + paraName;
            Debug.LogError(str);
            FirebaseAnalytics.LogEvent(str);
            FB.LogAppEvent(str, 1);
        }
    }


    // Reset analytics data for this app instance.
    public void ResetAnalyticsData()
    {
        Debug.Log("Reset analytics data.");
        FirebaseAnalytics.ResetAnalyticsData();
    }

    #endregion

    #region Firebase Invite

    private void Init_Invite()
    {
        FirebaseInvites.InviteReceived += OnInviteReceived;
        FirebaseInvites.InviteNotReceived += OnInviteNotReceived;
        FirebaseInvites.ErrorReceived += OnErrorReceived;
    }

    private void OnInviteReceived(object sender,InviteReceivedEventArgs e)
    {
        if (e.InvitationId != "")
        {
            Debug.Log("Invite Received: Invitation ID: " + e.InvitationId);
            FirebaseInvites.ConvertInvitationAsync(e.InvitationId).ContinueWith(HandleConversionResult);
        }
        if (e.DeepLink.ToString() != "")
        {
            Debug.Log("Invite Received: Deep Link: " + e.DeepLink);
        }
    }

    private void OnInviteNotReceived(object sender,EventArgs e)
    {
        Debug.Log("No Invite or Deep Link received on start up");
    }

    private void OnErrorReceived(object sender,InviteErrorReceivedEventArgs e)
    {
        Debug.Log("Error occurred received the invite: " + e.ErrorMessage);
    }

    private void HandleConversionResult(Task convertTask)
    {
        if (convertTask.IsCanceled)
        {
            Debug.Log("Conversion canceled.");
        }
        else if (convertTask.IsFaulted)
        {
            Debug.Log("Conversion encountered an error:");
            Debug.Log(convertTask.Exception.ToString());
        }
        else if (convertTask.IsCompleted)
        {
            Debug.Log("Conversion completed successfully!");
            Debug.Log("ConvertInvitation: Successfully converted invitation");
        }
    }

    private string LinkApp = "http://play.google.com/store/apps/details?id=monkey.run.banana.jungle";
    private string strEmailContent = "";

    //Sent
    public Task<SendInviteResult> SendInviteAsync()
    {
        ReadString();
        Invite invite = new Invite()
        {
            TitleText = "Invites Test Funky Island",
            MessageText = "Please try my app! It's awesome.",
            DeepLinkUrl = new Uri(LinkApp),
            CallToActionText = "Download it for FREE",
            EmailSubjectText = "Please try Funky Island! It's awesome.",
            EmailContentHtml = strEmailContent

        };
        return FirebaseInvites.SendInviteAsync(invite).ContinueWith<SendInviteResult>(HandleSentInvite);
    }


    SendInviteResult HandleSentInvite(Task<SendInviteResult> sendTask)
    {
        if (sendTask.IsCanceled)
        {
            Debug.Log("Invitation canceled.");
        }
        else if (sendTask.IsFaulted)
        {
            Debug.Log("Invitation encountered an error:");
            Debug.Log(sendTask.Exception.ToString());
        }
        else if (sendTask.IsCompleted)
        {
            Debug.Log("SendInvite: " +(new List<string>(sendTask.Result.InvitationIds)).Count +" invites sent successfully.");
            foreach (string id in sendTask.Result.InvitationIds)
            {
                Debug.Log("SendInvite: Invite code: " + id);
            }
            //Reward Invite
            int i = (new List<string>(sendTask.Result.InvitationIds)).Count;
            Modules.totalCoin += (300 * i);
            SaveLoad.SaveTotalCoin();
            FB.LogAppEvent("Invited User", i);
            LogEventEarnBanana("Invited_User_" + i);
            FirebaseAnalytics.LogEvent("Invite_User_"+ i);
        }
        return sendTask.Result;
    }

    public string path = "Test - Funky Island content for Email-invite";
    private void ReadString()
    {
        //Read the text from directly from the test.txt file
        TextAsset reader = Resources.Load(path) as TextAsset;
        strEmailContent = reader.text;
        Debug.LogError(strEmailContent);
       
    }

    #endregion

    #region Messaging
    public void MessengeCallStart()
    {
       FirebaseMessaging.TokenReceived += OnTokenReceived;
       FirebaseMessaging.MessageReceived += OnMessageReceived;
    }

    public void OnTokenReceived(object sender, TokenReceivedEventArgs token)
    {
        Debug.Log("Received Registration Token: " + token.Token);
    }

    public void OnMessageReceived(object sender, MessageReceivedEventArgs e)
    {
        Debug.Log("Received a new message from: " + e.Message.From);
    }
    #endregion
}
