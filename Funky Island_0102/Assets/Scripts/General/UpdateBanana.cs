﻿
using UnityEngine;
using UnityEngine.UI;

public class UpdateBanana : MonoBehaviour {

    public Text textValue;
    private void FixedUpdate()
    {
        if (Modules.totalCoin <= 0)
        {
            Modules.totalCoin = 0;
            SaveLoad.SaveTotalCoin();
        }
        textValue.text = Modules.totalCoin.ToString("#,##0");
    }
}
