﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using Facebook.MiniJSON;

public class FacebookController : MonoBehaviour {

    #region SetInits
    public static FacebookController Instance;
    private void Awake()
    {
#if !(UNITY_WEBPLAYER || UNITY_WEBGL || UNITY_STANDALONE_WIN || UNITY_IOS || UNITY_ANDROID || UNITY_EDITOR)
        return;
#endif
        if (Instance == null) Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
            SetInit();
        }
        else
        {
            //Handle FB.Init
            FB.Init(() =>
            {
                FB.ActivateApp();
                SetInit();
            });
        }
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        // Check the pauseStatus to see if we are in the foreground
        // or background
        if (!pauseStatus)
        {
            //app resume
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
            else
            {
                //Handle FB.Init
                FB.Init(() =>
                {
                    FB.ActivateApp();
                });
            }
        }
    }

    private void SetInit()
    {
        DealWithFBMenus(FB.IsLoggedIn);
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    #endregion

    #region Login

    public void FBlogin()
    {
        List<string> perms = new List<string>() { "user_friends", "public_profile", "email" };
        FB.LogInWithReadPermissions(perms, LoginReadCallback);
    }

    private void LoginReadCallback(IResult result)
    {
        if (result.Error != null)
        {
            Debug.Log(result.Error);
        }
        else
        {
            if (FB.IsLoggedIn)
            {
                Debug.Log("FB is logged in");
            }
            else
            {
                Debug.Log("FB is not logged in");
            }
            DealWithFBMenus(FB.IsLoggedIn);
        }
    }

    private void LoginPublishCallback(ILoginResult result)
    {
        //You also granted the asked publish_actions permission.
    }

    private void DealWithFBMenus(bool isLoggedIn)
    {
        if (isLoggedIn)//dang nhap thanh cong
        {
            FB.API("/me?fields=first_name", HttpMethod.GET, DisplayUsername);
            FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilePic);
            Modules.myFBId = AccessToken.CurrentAccessToken.UserId;
        }
        else
        {
            //dang nhap that bai
        }
    }

    #endregion

    #region Action Option

    private void DisplayUsername(IResult result)
    {
        if (result.Error == null)
        {
           Modules.myFBName = result.ResultDictionary["first_name"].ToString();
           //print(Modules.fbName);
           //Modules.textDebug.text += "\nName[Okay]: " + Modules.fbName;
        }
        else
        {
            Debug.Log(result.Error);
            //Modules.textDebug.text += "\nName[Error]: " + Modules.fbName;
        }
    }

    private void DisplayProfilePic(IGraphResult result)
    {
        if (result.Texture != null)
        {
            Modules.myFBAvatar = Sprite.Create(result.Texture, new Rect(0, 0, 128, 128), new Vector2());
            Modules.myFBAvatarLink = "https" + "://graph.facebook.com/" + AccessToken.CurrentAccessToken.UserId.ToString() + "/picture?g&width=128&height=128";
            print(Modules.myFBAvatarLink);
        }
    }

    public void ShareWithFriends()
    {
        FB.FeedShare("", null, "Name", "CAP", "DES", null, "", null);
    }

    public void InviteFriends()
    {
        FB.AppRequest(
            message: "This game is awesome, join me. now.",
            title: "Invite your friends to join you"
            );
    }

    private IEnumerator TakeScreenshot()
    {
        yield return new WaitForEndOfFrame();

        var width = Screen.width;
        var height = Screen.height;
        var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        // Read screen contents into the texture
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();
        byte[] screenshot = tex.EncodeToPNG();

        var wwwForm = new WWWForm();
        wwwForm.AddBinaryData("image", screenshot, "Screenshot.png");

        FB.API("me/photos", HttpMethod.POST, OnPostScreenShot, wwwForm);
    }

    private void OnPostScreenShot(IGraphResult result)
    {
        print("Post image OK");
    }

    public IEnumerator PostFBScore(int score, int level, float time, System.Action<bool> CallBack)
    {
        if (Modules.myFBName == "")
        {
            CallBack(true);
            yield break;
        }
        WWWForm form = new WWWForm();
        form.AddField("table", "Player");
        form.AddField("idFace", AccessToken.CurrentAccessToken.UserId.ToString());
        form.AddField("nameFace", Modules.myFBName);
        form.AddField("avatarFace", Modules.myFBAvatarLink);
        form.AddField("score", score);
        form.AddField("level", level);
        form.AddField("time", Mathf.RoundToInt(time * 100));
        WWW _resuilt = new WWW(Modules.linkPostFBScore, form);
        float runTime = 0f;
        while (!_resuilt.isDone && runTime < Modules.maxTime)
        {
            runTime += Modules.requestTime;
            yield return new WaitForSecondsRealtime(Modules.requestTime);
        }
        yield return _resuilt;
        bool status = false;
        if (_resuilt.text == "Done")
        { //hoan thanh
            status = true;
        }
        else
        { //qua lau, khong mang, cau lenh loi
            status = false;
        }
        CallBack(status);
        yield break;
    }

    IEnumerator GetFBScoreFriend(string idFace, int index, System.Action<bool> CallBack)
    {
        WWWForm form = new WWWForm();
        form.AddField("table", "Player");
        form.AddField("idFace", idFace);
        WWW _resuilt = new WWW(Modules.linkGetFBScoreFriend, form);
        float runTime = 0f;
        while (!_resuilt.isDone && runTime < Modules.maxTime)
        {
            runTime += Modules.requestTime;
            yield return new WaitForSecondsRealtime(Modules.requestTime);
        }
        yield return _resuilt;
        bool status = false;
        if (_resuilt.text != "null" && _resuilt.text != "")
        { //hoan thanh
            //print(_resuilt.text);
            string[] dataLine = _resuilt.text.TrimEnd('\n').Split('\n');
            if (dataLine.Length > 0 && dataLine[0].Length > 1)
            {
                string[] data = dataLine[0].Split(';');
                listScoreFriend[index] = int.Parse(data[1]);
            }
            if (listScoreFriend.Count == totalFriend + 1)
                UpdateList();
            status = true;
        }
        else
        { //qua lau, khong mang, cau lenh loi
            status = false;
        }
        CallBack(status);
        yield break;
    }

    private List<Texture2D> avatarTopEndless = new List<Texture2D>();
    private List<Texture2D> avatarTopLevel = new List<Texture2D>();
    public IEnumerator GetTopEndless(List<Image> listImage, List<Text> listScore, System.Action<bool> CallBack)
    {
        //if (Modules.myFBName == "") yield break;
        WWWForm form = new WWWForm();
        form.AddField("table", "Player");
        form.AddField("limit", "3");
        WWW _resuilt = new WWW(Modules.linkGetFBScore, form);
        float runTime = 0f;
        while (!_resuilt.isDone && runTime < Modules.maxTime)
        {
            runTime += Modules.requestTime;
            yield return new WaitForSecondsRealtime(Modules.requestTime);
        }
        yield return _resuilt;
        bool status = false;
        if (_resuilt.text != "null" && _resuilt.text != "")
        { //hoan thanh
            //print(_resuilt.text);
            avatarTopEndless = new List<Texture2D>();
            string[] dataLine = _resuilt.text.TrimEnd('\n').Split('\n');
            for (int i = 0; i < listImage.Count; i++)
            {
                if (i < dataLine.Length)
                {
                    if (dataLine[i] == "") continue;
                    string[] finalData = dataLine[i].Split(';');
                    //listName[i].text = finalData[2];
                    avatarTopEndless.Add(null);
                    StartCoroutine(LoadAvatarEndless(finalData[3], i, listImage[i]));
                    listScore[i].text = finalData[4] + "m";
                }
                else
                {
                    listImage[i].transform.parent.gameObject.SetActive(false);
                }
            }
            status = true;
        }
        else
        { //qua lau, khong mang, cau lenh loi
            for (int i = 0; i < listImage.Count; i++)
                listImage[i].transform.parent.gameObject.SetActive(false);
            status = false;
        }
        CallBack(status);
        yield break;
    }

    public IEnumerator GetTopLevel(int level, List<Image> listImage, List<Text> listScore, System.Action<bool> CallBack)
    {
        //if (Modules.myFBName == "") yield break;
        WWWForm form = new WWWForm();
        form.AddField("table", "Player");
        form.AddField("limit", "3");
        form.AddField("level", level);
        WWW _resuilt = new WWW(Modules.linkGetFBScore, form);
        float runTime = 0f;
        while (!_resuilt.isDone && runTime < Modules.maxTime)
        {
            runTime += Modules.requestTime;
            yield return new WaitForSecondsRealtime(Modules.requestTime);
        }
        yield return _resuilt;
        bool status = false;
        if (_resuilt.text != "null" && _resuilt.text != "")
        { //hoan thanh
            //print(_resuilt.text);
            avatarTopLevel = new List<Texture2D>();
            string[] dataLine = _resuilt.text.TrimEnd('\n').Split('\n');
            for (int i = 0; i < listImage.Count; i++)
            {
                if (i < dataLine.Length)
                {
                    if (dataLine[i] == "") continue;
                    string[] finalData = dataLine[i].Split(';');
                    //listName[i].text = finalData[2];
                    avatarTopLevel.Add(null);
                    StartCoroutine(LoadAvatarLevel(finalData[3], i, listImage[i]));
                    listScore[i].text = ((float)int.Parse(finalData[5]) / 100f).ToString("F2") + "s";
                }
                else
                {
                    listImage[i].transform.parent.gameObject.SetActive(false);
                }
            }
            status = true;
        }
        else
        { //qua lau, khong mang, cau lenh loi
            for (int i = 0; i < listImage.Count; i++)
                listImage[i].transform.parent.gameObject.SetActive(false);
            status = false;
        }
        CallBack(status);
        yield break;
    }

    IEnumerator LoadAvatarEndless(string url, int index, Image avatar)
    {
        WWW www = new WWW(url);
        while (!www.isDone && string.IsNullOrEmpty(www.error))
            yield return new WaitForSecondsRealtime(0.1f);
        if (string.IsNullOrEmpty(www.error) && url != "Null" && www.texture != null && avatar != null)
        {
            avatarTopEndless[index] = www.texture;
            int width = avatarTopEndless[index].width;
            int height = avatarTopEndless[index].height;
            if (width > 128) width = 128;
            if (height > 128) height = 128;
            avatar.sprite = Sprite.Create(avatarTopEndless[index], new Rect(0, 0, width, height), new Vector2(0, 0));
        }
        www.Dispose();
        //yield return Resources.UnloadUnusedAssets();
        yield break;
    }

    IEnumerator LoadAvatarLevel(string url, int index, Image avatar)
    {
        WWW www = new WWW(url);
        while (!www.isDone && string.IsNullOrEmpty(www.error))
            yield return new WaitForSecondsRealtime(0.1f);
        if (string.IsNullOrEmpty(www.error) && url != "Null" && www.texture != null && avatar != null)
        {
            avatarTopLevel[index] = www.texture;
            int width = avatarTopLevel[index].width;
            int height = avatarTopLevel[index].height;
            if (width > 128) width = 128;
            if (height > 128) height = 128;
            avatar.sprite = Sprite.Create(avatarTopLevel[index], new Rect(0, 0, width, height), new Vector2(0, 0));
        }
        www.Dispose();
        //yield return Resources.UnloadUnusedAssets();
        yield break;
    }

    public void GetScoresFriend()
    {
        string strGetScore = "/me/friends?limit=30";
        FB.API(strGetScore, HttpMethod.GET, OnGetScore);
    }

    private List<object> scoresList = null;
    private List<string> listNameFriend;
    private List<string> listIDFriend;
    private List<int> listScoreFriend;
    private int totalFriend = 0;
    private void OnGetScore(IGraphResult result)
    {
        print("Get score " + result.RawResult);
        //thuc hien khac
        scoresList = DeserializeScores(result.RawResult);
        totalFriend = scoresList.Count;
        //thuc hien lay danh sach thong tin ban be (ten, avatar, diem so)
        listNameFriend = new List<string>();
        listIDFriend = new List<string>();
        listScoreFriend = new List<int>();
        int indexData = 0;
        foreach (object score in scoresList)
        {
            var entry = (Dictionary<string, object>)score;
            listNameFriend.Add(entry["name"].ToString().Split(' ')[0]);
            string idF = entry["id"].ToString();
            listIDFriend.Add(idF);
            listScoreFriend.Add(0);
            StartCoroutine(GetFBScoreFriend(idF, indexData, null));
            indexData++;
        }
        //add them chinh minh vao danh sach
        listNameFriend.Add(Modules.myFBName);
        string myID = AccessToken.CurrentAccessToken.UserId.ToString();
        listIDFriend.Add(myID);
        listScoreFriend.Add(0);
        StartCoroutine(GetFBScoreFriend(myID, indexData, null));
        indexData++;
    }

    private void UpdateList()
    {
        //sap xep cac danh sach theo diem so thap dan
        for (int i = 0; i < listScoreFriend.Count - 1; i++)
        {
            for (int j = i; j < listScoreFriend.Count; j++)
            {
                if (listScoreFriend[j] > listScoreFriend[i])
                {
                    int scoreTemp = listScoreFriend[i];
                    listScoreFriend[i] = listScoreFriend[j];
                    listScoreFriend[j] = scoreTemp;

                    string nameTemp = listNameFriend[i];
                    listNameFriend[i] = listNameFriend[j];
                    listNameFriend[j] = nameTemp;

                    string idTemp = listIDFriend[i];
                    listIDFriend[i] = listIDFriend[j];
                    listIDFriend[j] = idTemp;
                }
            }
        }
        //thuc hien export bang xep hang
        for (int i = 0; i < listScoreFriend.Count; i++)
        {
            string name = listNameFriend[i];
            int scoreNow = listScoreFriend[i];
            FB.API(GetPictureURL(listIDFriend[i], 128, 128), HttpMethod.GET, delegate (IGraphResult pictureResult)
            {
                if (pictureResult.Error != null) // if there was an error
                {
                    Debug.Log(pictureResult.Error);
                }
                else // if everything was fine
                {
                    Image img = null;
                    img.sprite = Sprite.Create(pictureResult.Texture, new Rect(0, 0, 128, 128), new Vector2(0, 0));
                }
            });
        }
    }

    public static List<object> DeserializeScores(string response)
    {
        var responseObject = Json.Deserialize(response) as Dictionary<string, object>;
        object scoresh;
        var scores = new List<object>();
        if (responseObject.TryGetValue("data", out scoresh))
        {
            scores = (List<object>)scoresh;
        }
        return scores;
    }

    public static string GetPictureURL(string facebookID, int? width = null, int? height = null, string type = null)
    {
        string url = string.Format("/{0}/picture", facebookID);
        string query = width != null ? "&width=" + width.ToString() : "";
        query += height != null ? "&height=" + height.ToString() : "";
        query += type != null ? "&type=" + type : "";
        if (query != "") url += ("?g" + query);
        return url;
    }

    #endregion

    #region Event App

    public void LogCompletedRegistrationEvent(string registrationMethod)
    {
        var parameters = new Dictionary<string, object>();
        parameters[AppEventParameterName.RegistrationMethod] = registrationMethod;
        FB.LogAppEvent(
            AppEventName.CompletedRegistration,
            1,parameters
        );
        Debug.Log("LogCompletedRegistrationEvent");
    }

    public void LogCompletedTutorialEvent( bool success)
    {
        var parameters = new Dictionary<string, object>();
        parameters[AppEventParameterName.Success] = success ? 1 : 0;
        FB.LogAppEvent(
            AppEventName.CompletedTutorial,
            1,parameters
        );
        Debug.Log("LogCompletedTutorialEvent");
    }

    public void LogAchievedLevelEvent(int level)
    {
        var parameters = new Dictionary<string, object>();
        parameters[AppEventParameterName.Level] = "Level "+level;
        FB.LogAppEvent(
            AppEventName.AchievedLevel,
            level,parameters
        );
        Debug.Log("LogAchievedLevelEvent");
    }

    public void LogPurchasedEvent(int numItems, string contentType, string contentId, string currency, double price)
    {
        var parameters = new Dictionary<string, object>();
        parameters[AppEventParameterName.NumItems] = numItems;
        parameters[AppEventParameterName.ContentType] = contentType;
        parameters[AppEventParameterName.ContentID] = contentId;
        parameters[AppEventParameterName.Currency] = currency;
        FB.LogPurchase(
            (float)price,
            currency,
            parameters
        );
        Debug.Log("LogPurchasedEvent");
    }

    public void LogCancerPurchaseEvent(string contentType, float valToSum )
    {
        var parameters = new Dictionary<string, object>();
        parameters[AppEventParameterName.ContentType] = contentType;
        FB.LogAppEvent("Cancer Buy Purchase", valToSum,parameters);
    }

    public void LogFinishBuyPurchased(string name,float valToSum)
    {
        FB.LogAppEvent(name, valToSum);
    }

    public void LogAddedToCartEvent(int numItems, string contentId, string contentType, string currency, double price)
    {
        var parameters = new Dictionary<string, object>();
        parameters[AppEventParameterName.NumItems] = numItems;
        parameters[AppEventParameterName.ContentID] = contentId;
        parameters[AppEventParameterName.ContentType] = contentType;
        parameters[AppEventParameterName.Currency] = currency;
        FB.LogAppEvent(
            AppEventName.AddedToCart,
            (float)price,
            parameters
        );
        Debug.Log("LogAddedToCartEvent");
    }

    public void LogAds_VideoRewarded(string contentType,float valToSum)
    {
        var parameters = new Dictionary<string, object>();
        parameters[AppEventParameterName.ContentType] = contentType;
        FB.LogAppEvent("Ads_Video_Rewarded", valToSum, parameters);
    }

    public void LogAds_VideoFullAds(string contentType,float valToSum)
    {
        var parameters = new Dictionary<string, object>();
        parameters[AppEventParameterName.ContentType] = contentType;
        FB.LogAppEvent("Ads_Full_Video", valToSum, parameters);
    }

    public void LogEventWithName(string nameEvent,float valTosum)
    {
        FB.LogAppEvent(nameEvent, valTosum);
    }
    #endregion

}
