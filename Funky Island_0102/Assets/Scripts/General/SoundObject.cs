﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundObject : MonoBehaviour {

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip audioclip;

	private void Start () {
        if(!audioSource)
            audioSource = this.gameObject.AddComponent<AudioSource>();

        audioSource.clip = audioclip;
        audioSource.loop = true;
        audioSource.Play();
        audioSource.volume = Modules.musicSetting;

    }

}
