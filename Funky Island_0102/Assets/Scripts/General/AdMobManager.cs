﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GoogleMobileAds.Api;
using System;
using System.Collections.Generic;

public class AdMobManager : MonoBehaviour
{
    /// <summary>
    /// 
    /// fullAds: Ads ngắn : bao gồm các loại Ads dưới 5s
    /// NativeAds: dành cho banner 
    /// VideoAds: dành cho các Ads để nhận thưởng, thường lớn hơn 30s 
    /// 
    /// </summary>
    public static AdMobManager Instance;
    [Header("AppID")]
    public string AppID;

    [Header("Android")]
    public string bannerAndroid ;
    public string nativeAndroid;
    public string interAnroid;
    public string rewardAndroid;

    [Header("IOS")]
    public string bannerIOS;
    public string nativeIOS;
    public string interIOS;
    public string rewardIOS;
   
    [SerializeField] private string bannerAdId, interstitialAdId, rewardVideoAdId, nativeAdId;
    private RewardBasedVideoAd rewardBasedVideo;
    private BannerView bannerView;
    private InterstitialAd interstitial;

    //Ads UnifiedNatived
    private UnifiedNativeAd nativeAd;
    [HideInInspector]
    public bool unifiedNativeAdLoaded=false;
    public GameObject ObjectNative=null;

    private void Awake()
    {
        if (Instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        MobileAds.Initialize(AppID);

#if UNITY_ANDROID
        bannerAdId = bannerAndroid; //native
        interstitialAdId = interAnroid; //full
        rewardVideoAdId = rewardAndroid;  //video
        nativeAdId = nativeAndroid; //native
#else
        bannerAdId = bannerIOS;
        interstitialAdId = interIOS;
        rewardVideoAdId = rewardIOS;
        nativeAdId = nativeIOS;
#endif
    }

    private void Start()
    {
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;
        loadRewardVideo();
        loadInterstitial();
        RequestNativeAd();
        //reward Event
        rewardBasedVideo.OnAdRewarded += RewardBasedVideo_OnAdRewarded;
        rewardBasedVideo.OnAdOpening += RewardBasedVideo_OnAdOpening;
        rewardBasedVideo.OnAdClosed += RewardBasedVideo_OnAdClosed;
        rewardBasedVideo.OnAdFailedToLoad += RewardBasedVideo_OnAdFailedToLoad;
        rewardBasedVideo.OnAdLoaded += RewardBasedVideo_OnAdLoaded;
        //
    }

    private IEnumerator RunFakeUpdate()
    {
        while (true)
        {
            float timeStep = 5f;
            loadRewardVideo();
            loadInterstitial();
            yield return new WaitForSecondsRealtime(timeStep);
        }
    }

    #region Baner 
    public void showBannerAd()
    {
        if (Modules.removeAds || !Modules.HaveInternet()) return;
        //AdSize adSize = new AdSize(250, 250);
        bannerView = new BannerView(bannerAdId, AdSize.MediumRectangle, AdPosition.Top);

        // Called when an ad request has successfully loaded.
        bannerView.OnAdLoaded -= HandleOnAdLoaded;
        bannerView.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        bannerView.OnAdFailedToLoad -= HandleOnAdFailedToLoad;
        bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is clicked.
        bannerView.OnAdOpening -= HandleOnAdOpened;
        bannerView.OnAdOpening += HandleOnAdOpened;
        // Called when the user returned from the app after an ad click.
        bannerView.OnAdClosed -= HandleOnAdClosed;
        bannerView.OnAdClosed += HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        bannerView.OnAdLeavingApplication -= HandleOnAdLeavingApplication;
        bannerView.OnAdLeavingApplication += HandleOnAdLeavingApplication;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    public void hideBannerAd()
    {
        bannerView.Hide();
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        showBannerAd();
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        FacebookController.Instance.LogEventWithName(Log_Name_Event.Event_Ads_ClickBaner, 1);
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }

    #endregion

    #region Interstitial
    public void loadInterstitial()
    {
        if (!Modules.HaveInternet()) return;
        this.interstitial = new InterstitialAd(interstitialAdId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        this.interstitial.LoadAd(request);
    }

    public void showInterstitial()
    {
        if (Modules.removeAds) return;
        if (!interstitial.IsLoaded())
            loadInterstitial();
        else
        {
            this.interstitial.Show();
            // Called when an ad request has successfully loaded.
            this.interstitial.OnAdLoaded -= Interstitial_OnAdLoaded;
            this.interstitial.OnAdLoaded += Interstitial_OnAdLoaded;
            // Called when an ad request failed to load.
            this.interstitial.OnAdFailedToLoad -= Interstitial_OnAdFailedToLoad;
            this.interstitial.OnAdFailedToLoad += Interstitial_OnAdFailedToLoad;
            // Called when an ad is shown.
            this.interstitial.OnAdOpening -= Interstitial_OnAdOpening;
            this.interstitial.OnAdOpening += Interstitial_OnAdOpening;
            // Called when the ad is closed.
            this.interstitial.OnAdClosed -= Interstitial_OnAdClosed;
            this.interstitial.OnAdClosed += Interstitial_OnAdClosed;
            // Called when the ad click caused the user to leave the application.
            this.interstitial.OnAdLeavingApplication -= Interstitial_OnAdLeavingApplication;
            this.interstitial.OnAdLeavingApplication += Interstitial_OnAdLeavingApplication;
        }
        
    }

    private void Interstitial_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
    {
        loadInterstitial();
    }

    private void Interstitial_OnAdClosed(object sender, EventArgs e)
    {
        //print("On Interstitial Close Event");
        if (Modules.typeRewardVideo == type_RewardAds.Reborn&&PlayerController.Instance.playerStates==PlayerStates.Dead)
        {
            Time.timeScale = 1;
            PagePlay.Instance.boxSaveMe.SetActive(false);
            PagePlay.Instance.RebornPlayer();
            FirebaseManager.Instance.LogEventContinue("Use Reborn with Watch Ads");
        }

        loadInterstitial();
        Modules.pauseTimeSave = false;
        Camera.main.GetComponent<AudioListener>().enabled = true;
    }

    private void Interstitial_OnAdOpening(object sender, EventArgs e)
    {
        Modules.pauseTimeSave = true;
        Camera.main.GetComponent<AudioListener>().enabled = false;
        FacebookController.Instance.LogAds_VideoFullAds("Ads full with long time < 5s ", 1);
    }
   
    private void Interstitial_OnAdLoaded(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    private void Interstitial_OnAdLeavingApplication(object sender, EventArgs e)
    {
        FacebookController.Instance.LogEventWithName(Log_Name_Event.Event_Ads_LeaveTheApplication, 1);
    }

    //Clear 
    public void ClearInterstitial()
    {
        this.interstitial.Destroy();
    }

    #endregion

    #region Reward Video
    public void loadRewardVideo()
    {
        if (/*rewardLoading ||*/ !Modules.HaveInternet()) return;
        AdRequest request = new AdRequest.Builder().Build();
        rewardBasedVideo.LoadAd(request, rewardVideoAdId);
        rewardLoading = true;
        
    }

    public bool rewardLoading = false;
    public void showRewardVideo()
    {
        if (rewardBasedVideo.IsLoaded())
        {
            Debug.Log("Show reward");
            rewardBasedVideo.Show();
        }
        else
        {
            Debug.Log("Loadreward");
            //showInterstitial();
            loadRewardVideo();
        }
    }

    private void RewardBasedVideo_OnAdLoaded(object sender, EventArgs e)
    {
        rewardLoading = true;
        print("load reward video xong");
    }

    private void RewardBasedVideo_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
    {
        rewardLoading = false;
        loadRewardVideo();
    }

    private void RewardBasedVideo_OnAdClosed(object sender, EventArgs e)
    {
        rewardLoading = false;
        Modules.pauseTimeSave = false;
        Camera.main.GetComponent<AudioListener>().enabled = true;
        loadRewardVideo();
    }

    private void RewardBasedVideo_OnAdOpening(object sender, EventArgs e)
    {
        rewardLoading = false;
        Modules.pauseTimeSave = true;
        Camera.main.GetComponent<AudioListener>().enabled = false;
        FacebookController.Instance.LogAds_VideoRewarded("Video Rewarded", 1);
        loadRewardVideo();
    }

    private void RewardBasedVideo_OnAdRewarded(object sender, Reward e)
    {
        string type = e.Type;
        double amount = e.Amount;
        print("User rewarded with: " + amount.ToString() + " " + type);
        rewardLoading = false;
        switch (Modules.typeRewardVideo)
        {
            case type_RewardAds.Reborn:
                Time.timeScale = 1;
                PagePlay.Instance.boxSaveMe.SetActive(false);
                PagePlay.Instance.RebornPlayer();
                FirebaseManager.Instance.LogEventContinue("Use Reborn with Watch Ads");
                break;
            case type_RewardAds.Goldx200:
                Modules.totalCoin += 200;
                SaveLoad.SaveTotalCoin();
                FirebaseManager.Instance.LogEventEarnBanana("Reward_Ads");
                break;
            case type_RewardAds.spinMenu:
                Time.timeScale = 1;
                PageMenu.Instance.Popup_LuckySpin.GetComponent<Spin>().GoSpin();
                
                break;
            case type_RewardAds.spinMainGame:
                Time.timeScale = 0.0001f;
                PagePlay.Instance.PopupSpin.GetComponent<Spin>().SetButtonAdsReward();
                break;
        }
        loadRewardVideo();
    }

    public bool VideoAdLoaded()
    {
        return rewardBasedVideo.IsLoaded();
    }

    #endregion

    #region Native
    public void RequestNativeAd()
    {
        if (Modules.removeAds || !Modules.HaveInternet()) return;
        AdLoader adLoader = new AdLoader.Builder(nativeAdId)
            .ForUnifiedNativeAd()
            .Build();
        adLoader.OnUnifiedNativeAdLoaded -= this.HandleUnifiedNativeAdLoaded;
        adLoader.OnUnifiedNativeAdLoaded += this.HandleUnifiedNativeAdLoaded;
        adLoader.OnAdFailedToLoad -= this.HandleNativeAdFailedToLoad;
        adLoader.OnAdFailedToLoad += this.HandleNativeAdFailedToLoad;
        adLoader.LoadAd(new AdRequest.Builder().Build());
    }

    private void HandleNativeAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        print("Native ad failed to load: " + args.Message);
        unifiedNativeAdLoaded = false;
        RequestNativeAd();
    }

    private void HandleUnifiedNativeAdLoaded(object sender, UnifiedNativeAdEventArgs args)
    {
        Debug.LogError("App install ad loaded.");
        this.nativeAd = args.nativeAd;
        this.unifiedNativeAdLoaded = true;
        
    }

    public bool _unifiedNativeAdLoaded()
    {
        return nativeAd.RegisterIconImageGameObject(ObjectNative);
    }

    private List<GameObject> textures; 
    public void SetBanerNative(AdModBanner native)
    {
#if UNITY_EDITOR
        return;
#else
        native.IconTexture.texture = nativeAd.GetIconTexture();
        native.HeadlineText.text = nativeAd.GetHeadlineText();
        native.ActionText.text = nativeAd.GetCallToActionText();
        native.ImageTextures.texture = nativeAd.GetImageTextures()[0];
        native.LogoTexture.texture = nativeAd.GetAdChoicesLogoTexture();
        
        if (!this.nativeAd.RegisterHeadlineTextGameObject(native.HeadlineText.gameObject))
        {
            // Handle failure to register ad asset.
        }
        if (!this.nativeAd.RegisterCallToActionGameObject(native.ActionText.gameObject))
        {
            // Handle failure to register ad asset.
        }
        if (!this.nativeAd.RegisterAdChoicesLogoGameObject(native.LogoTexture.gameObject))
        {
            // Handle failure to register ad asset.
        }

        if (!this.nativeAd.RegisterIconImageGameObject(native.gameObject))
        {
            // Handle failure to register ad asset.
        }

#endif
    }

#endregion

    private void LateUpdate()
    {
        //unifiedNativeAdLoaded
        if (this.unifiedNativeAdLoaded && ObjectNative != null)
        {
            this.unifiedNativeAdLoaded = false;
            SetBanerNative(ObjectNative.GetComponent<AdModBanner>());

        }

        if (!rewardLoading && Modules.haveNative)
        {
            loadRewardVideo();
        }
    }
}

public enum type_RewardAds { None,Reborn,Goldx200,spinMenu,spinMainGame}