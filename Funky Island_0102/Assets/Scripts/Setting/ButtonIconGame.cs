﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonIconGame : MonoBehaviour {

    public string linkClick = "";

    public void ButIconClick()
    {
        Application.OpenURL(linkClick);
        SoundController.Instance.PlayFX();
    }
}
