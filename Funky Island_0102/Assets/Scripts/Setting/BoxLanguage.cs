﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxLanguage : MonoBehaviour {

    public GameObject containFlag;
    private string codeConfirm = "EN";

    public void CallStart()
    {
        codeConfirm = Modules.codeLanguage;
        GameObject temp = containFlag.transform.GetChild(0).gameObject;
        for (int i = 1; i < containFlag.transform.childCount; i++)
            Destroy(containFlag.transform.GetChild(i).gameObject);
        for (int i = 0; i < Modules.listFlag.Count; i++)
        {
            GameObject butFlag = temp.gameObject;
            if (i > 0) butFlag = Instantiate(temp, temp.transform.parent) as GameObject;
            butFlag.GetComponent<ButtonFlag>().indexBut = i;
            butFlag.GetComponent<ButtonFlag>().iconFlag.sprite = Modules.listFlag[i].iconFlag;
            butFlag.GetComponent<ButtonFlag>().codeLang = Modules.listFlag[i].codeFlag;
            butFlag.GetComponent<ButtonFlag>().flagControll = transform.gameObject;
            if (Modules.listFlag[i].codeFlag == Modules.codeLanguage)
                butFlag.GetComponent<ButtonFlag>().selectFlag.gameObject.SetActive(true);
            else butFlag.GetComponent<ButtonFlag>().selectFlag.gameObject.SetActive(false);
        }
    }

    void ButFlagClick(string data)
    {
        string[] dataGet = data.Split(';');
        int indexBut = int.Parse(dataGet[0]);
        string codeLang = dataGet[1];
        codeConfirm = codeLang;
        for (int i = 0; i < containFlag.transform.childCount; i++)
        {
            if (i == indexBut)
                containFlag.transform.GetChild(i).GetComponent<ButtonFlag>().selectFlag.gameObject.SetActive(true);
            else
                containFlag.transform.GetChild(i).GetComponent<ButtonFlag>().selectFlag.gameObject.SetActive(false);
        }
        SoundController.Instance.PlayFX();
    }

    public void ButConfirmClick()
    {
        Modules.codeLanguage = codeConfirm;
        Modules.SetListTextFont();
        //transform.GetComponent<Animator>().Play("MessageBoxClose", 0, 0f);
        PageSetting.Instance.boxLanguage.SetActive(false);
        SoundController.Instance.PlayFX();
    }
}
