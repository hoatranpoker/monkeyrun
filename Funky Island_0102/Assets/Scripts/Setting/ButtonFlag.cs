﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonFlag : MonoBehaviour {

    public Image selectFlag;
    public Image iconFlag;
    [HideInInspector]
    public int indexBut = 0;
    [HideInInspector]
    public string codeLang = "EN";
    [HideInInspector]
    public GameObject flagControll;

    public void ButFlagClick()
    {
        if (flagControll != null)
            flagControll.SendMessage("ButFlagClick", indexBut.ToString() + ";" + codeLang);
    }
}
