﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxMoreGame : MonoBehaviour {

    public GameObject containIcon, loading;
    public void CallStart()
    {
        containIcon.transform.parent.gameObject.SetActive(false);
        loading.SetActive(true);
        StartCoroutine(GetIconMoreGame(CallBackGet));
    }

    void CallBackGet(bool result)
    {
        if (result)
        {
            containIcon.transform.parent.gameObject.SetActive(true);
            loading.SetActive(false);
        }
    }

    IEnumerator GetIconMoreGame(System.Action<bool> CallBack)
    {
        WWWForm form = new WWWForm();
        form.AddField("table", "MoreGame");
        WWW _resuilt = new WWW(Modules.linkGetMoreGame, form);
        float runTime = 0f;
        while (!_resuilt.isDone && runTime < Modules.maxTime)
        {
            runTime += Modules.requestTime;
            yield return new WaitForSeconds(Modules.requestTime);
        }
        yield return _resuilt;
        bool status = false;
        if (_resuilt.text != "null" && _resuilt.text != "")
        { //hoan thanh
            //print(_resuilt.text);
            iconMoreGame = new List<Texture2D>();
            string[] dataLine = _resuilt.text.TrimEnd('\n').Split('\n');
            GameObject temp = containIcon.transform.GetChild(0).gameObject;
            for (int i = 1; i < containIcon.transform.childCount; i++)
                Destroy(containIcon.transform.GetChild(i).gameObject);
            for (int i = 0; i < dataLine.Length; i++)
            {
                GameObject item = temp;
                if (i > 0) item = Instantiate(temp, temp.transform.parent);
                string[] dataElement = dataLine[i].Split(';');
                string[] url = dataElement[2].Split(',');
                string urlFinal = url[0];
#if (UNITY_IOS)
                urlFinal = url[1];
#endif
                iconMoreGame.Add(null);
                item.GetComponent<ButtonIconGame>().linkClick = "https://play.google.com/store/apps/details?id=com.colorgame.CookingStarChef";
                StartCoroutine(LoadIconMoreGame(dataElement[1], i, item.GetComponent<Image>()));
            }
            status = true;
        }
        else
        { //qua lau, khong mang, cau lenh loi
            status = false;
        }
        CallBack(status);
        yield break;
    }

    private List<Texture2D> iconMoreGame = new List<Texture2D>();
    IEnumerator LoadIconMoreGame(string url, int index, Image avatar)
    {
        WWW www = new WWW(url);
        while (!www.isDone && string.IsNullOrEmpty(www.error))
            yield return new WaitForSeconds(0.1f);
        if (string.IsNullOrEmpty(www.error) && url != "Null" && www.texture != null && avatar != null)
        {
            iconMoreGame[index] = www.texture;
            int width = iconMoreGame[index].width;
            int height = iconMoreGame[index].height;
            if (width > 128) width = 128;
            if (height > 128) height = 128;
            avatar.sprite = Sprite.Create(iconMoreGame[index], new Rect(0, 0, width, height), new Vector2(0, 0));
        }
        www.Dispose();
        //yield return Resources.UnloadUnusedAssets();
        yield break;
    }

    public void ButConfirmClick()
    {
        //transform.GetComponent<Animator>().Play("MessageBoxClose", 0, 0f);
        PageSetting.Instance.boxMoreGame.SetActive(false);
        SoundController.Instance.PlayFX();
    }
}
