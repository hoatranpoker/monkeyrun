﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PageSetting : MonoBehaviour {

    #region singleton
    public static PageSetting Instance;
    private void Awake()
    {
        if (Instance == null) Instance = this;
    }

    #endregion

    public GameObject boxLanguage, boxCredit, boxMoreGame;
    [Header("Button Setting")]
    public Button btnLanguage;
    public Button btnPurechase;
    public Button btnCredit;
    public Button btnMoreGame;
    public Button btnExit;
    public Button btnSound;
    public Button btnMusic;

    private void Start () {
        //UpdateLanguage();
        SetEventButton();
        boxLanguage.SetActive(false);
        boxCredit.SetActive(false);
        boxMoreGame.SetActive(false);
        //load audio state
        UpdateStateAudio();
    }

    private void SetEventButton()
    {
        btnLanguage.onClick.AddListener(ButLanguageClick);
        btnPurechase.onClick.AddListener(ButRetorePurchaseClick);
        btnCredit.onClick.AddListener(ButCreditClick);
        btnMoreGame.onClick.AddListener(ButMoreGameClick);
        btnExit.onClick.AddListener(ButHomeClick);
        btnSound.onClick.AddListener(ButSoundClick);
        btnMusic.onClick.AddListener(ButMusicClick);
    }

    public void UpdateStateAudio()
    {
        if (Modules.soundSetting == 0) btnSound.transform.GetChild(0).gameObject.SetActive(false);
        else btnSound.transform.GetChild(0).gameObject.SetActive(true);
        if (Modules.musicSetting == 0) btnMusic.transform.GetChild(0).gameObject.SetActive(false);
        else btnMusic.transform.GetChild(0).gameObject.SetActive(true);
        SoundController.Instance.UpdateSetting();
    }


    public void ButHomeClick()
    {
        //SceneManager.LoadScene("Menu");
        gameObject.SetActive(false);
        SoundController.Instance.PlayFX();
    }

    public void ButSoundClick()
    {
        Modules.soundSetting = 1 - Modules.soundSetting;
        UpdateStateAudio();
        SaveLoad.SaveAudioSetting();
        SoundController.Instance.PlayFX();
    }

    public void ButMusicClick()
    {
        Modules.musicSetting = 1 - Modules.musicSetting;
        UpdateStateAudio();
        SaveLoad.SaveAudioSetting();
        SoundController.Instance.PlayFX();
    }

    public void ButLanguageClick()
    {
        boxLanguage.SetActive(true);
        boxLanguage.GetComponent<BoxLanguage>().CallStart();
        SoundController.Instance.PlayFX();
    }

    public void ButRetorePurchaseClick()
    {
        GoogleSeviceManager.Instance.SettingPopupGravity();
        SoundController.Instance.PlayFX();
    }

    public void ButCreditClick()
    {
        boxCredit.SetActive(true);
        SoundController.Instance.PlayFX();
    }

    public void ButMoreGameClick()
    {
        boxMoreGame.SetActive(true);
        boxMoreGame.GetComponent<BoxMoreGame>().CallStart();
        SoundController.Instance.PlayFX();
    }
}
