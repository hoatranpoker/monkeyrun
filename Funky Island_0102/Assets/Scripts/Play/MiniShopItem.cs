﻿
using UnityEngine;
using UnityEngine.UI;

public class MiniShopItem : MonoBehaviour {

    [Header("Text")]
    [SerializeField] private LabelLanguages txtName;
    [SerializeField] private LabelLanguages txtDesc;
    [SerializeField] private Text txtNumberHaving;
    [SerializeField] private Text txtTotalPrice;

    [Header("Button")]
    [SerializeField] private UIButton btnExit;
    [SerializeField] private UIButton btnBuy;
    [SerializeField] private Button btnIncre;
    [SerializeField] private Button btnDes;

    [SerializeField] private Image imgItem;
    [SerializeField] private GameObject handTutorial;

    private int number=0;
    private int megaSale = 1;
    private TypeItemBuy type;
    private Shop_Item shop_Item;

    private void Start()
    {
        if (PagePlay.Instance.TutorialSence) handTutorial.SetActive(true);
        else handTutorial.SetActive(false);
        btnExit.SetUpEvent(ActionExit);
        btnBuy.SetUpEvent(ActionBuy);
        btnIncre.onClick.AddListener(ActionInce);
        btnDes.onClick.AddListener(ActionDes);
        if (Modules.megaSale) megaSale = 2;
    }

    public void CallStart(TypeItemBuy _type)
    {
        type = _type;
        shop_Item= DataManager.Instance.shopModel.shop_Items[(int)type];
        imgItem.sprite = shop_Item.image;
        imgItem.SetNativeSize();
        txtName.KEY = shop_Item.name;
        txtName.SetText();
        txtDesc.KEY = shop_Item.Desc;
        txtDesc.SetText();
        number = 1;
        if (PagePlay.Instance.TutorialSence) {
            if(MapTutorial.Instance.tutType==TUTORIAL_TYPE.UseProtect)
                PagePlay.Instance.Tut_Protect_ShowHand();
            if (MapTutorial.Instance.tutType == TUTORIAL_TYPE.UseMagnet)
                PagePlay.Instance.Tut_Magnet_ShowHand();
        }
        SetText();
    }

    #region Action
    private void ActionExit()
    {
        gameObject.SetActive(false);
        Time.timeScale = 1;
        SoundController.Instance.PlayFX();
    }

    private void ActionInce()
    {
        number++;
        SetText();
        SoundController.Instance.PlayFX();
    }

    private void ActionDes()
    {
        if (number == 0) return;
        number--;
        SetText();
        SoundController.Instance.PlayFX();
    }

    private void SetText()
    {
        CheckCoins();
        txtNumberHaving.text = number.ToString();
        txtTotalPrice.text = (number * 200).ToString();
    }

    private void CheckCoins()
    {
        if(Modules.totalCoin < (number * 200/megaSale))
        {
            btnBuy.GetComponent<Image>().color = new Color32(116, 116, 166, 255);
        }
        else
        {
            btnBuy.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
    }

    private void ActionBuy()
    {
        SoundController.Instance.PlayFX();
        if (Modules.totalCoin < (number * 200)) return;
        if (Modules.totalCoin >= ((number * 200) / megaSale))
        {
            Modules.totalCoin -= ((number * 200) / megaSale);
            SaveLoad.SaveTotalCoin();
            DataManager.Instance.shopModel.shop_Items[(int)type].having += number;
            gameObject.SetActive(false);
            PlayerController.Instance.pagePlay.UpdateItem();
            Time.timeScale = 1;
            if (PagePlay.Instance.TutorialSence)
            {
                if (MapTutorial.Instance.tutType == TUTORIAL_TYPE.UseProtect)
                    PagePlay.Instance.Tut_Protect_ShowHand();
                if (MapTutorial.Instance.tutType == TUTORIAL_TYPE.UseMagnet)
                    PagePlay.Instance.Tut_Magnet_ShowHand();
                return;
            }
            FacebookController.Instance.LogAddedToCartEvent(number, shop_Item.name, "Buy in Game", "Coins", shop_Item.price);
            FirebaseManager.Instance.LogEventSpentBanana("InGame_Item_"+shop_Item.type);
        }
        
    }

    #endregion
}
