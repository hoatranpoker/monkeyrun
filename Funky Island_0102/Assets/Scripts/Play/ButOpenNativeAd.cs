﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButOpenNativeAd : MonoBehaviour {

    [HideInInspector]
    public string urlLink;
    public void ButLinkClick()
    {
        if (urlLink == "") return;
        Application.OpenURL(urlLink);
    }
}
