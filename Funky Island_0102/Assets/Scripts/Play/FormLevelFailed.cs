﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FormLevelFailed : MonoBehaviour {

    public GameObject containAd, bannerNative;

    [Header("Ui Button")]
    [SerializeField] private UIButton btnReplay;
    [SerializeField] private UIButton btnHome;
    [SerializeField] private Text txtColectBanana;
    [SerializeField] private Text txtTotalBanana;

    public void CallStart()
    {
        SetText();
        SetEventButton();
        PagePlay.Instance.SetBanerNative();
    }

    private void Start()
    {
        SetEventButton();
    }

    private void SetEventButton()
    {
        btnReplay.SetUpEvent(ButReplayClick);
        btnHome.SetUpEvent(ButHomeClick);
    }

    private void SetText()
    {
        int totalCoinNow = PagePlay.Instance.numCoin;
        Modules.totalCoin += totalCoinNow;
        SaveLoad.SaveTotalCoin();
        FirebaseManager.Instance.LogEventEarnBanana("FailedGame");
        txtColectBanana.text ="x" +PagePlay.Instance.numCoin.ToString();
        txtTotalBanana.text = "x"+PagePlay.Instance.numCoin.ToString();
        GoogleSeviceManager.Instance.IncrementAchivemnet(GoogleID.Achivement_iCollectBanana_1000Ingame, PagePlay.Instance.numCoin);
    }


    private void ButReplayClick()
    {
        LoadingSceneManager.LoadingScene(SCENE_TYPE.Loading);
        SoundController.Instance.PlayFX();
        DataManager.Instance.ShowAdsInter();
    }

    public void ButChapterClick()
    {
        LoadingSceneManager.LoadingScene(SCENE_TYPE.Chapter);
        SoundController.Instance.PlayFX();
        DataManager.Instance.ShowAdsInter();
    }

    private void ButHomeClick()
    {
        LoadingSceneManager.LoadingScene(SCENE_TYPE.Menu);
        SoundController.Instance.PlayFX();
        DataManager.Instance.ShowAdsInter();
    }
}
