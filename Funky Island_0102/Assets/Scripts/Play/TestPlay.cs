﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPlay : MonoBehaviour {

    public float MapLength = 20000;
    public float speedRun = 0.1f;
    private bool startRun = false;
    private float distaneRun = 0;
    private PagePlay pagePlay;

    void Start()
    {
        pagePlay = PagePlay.Instance;
    }

    public void ButRunNormal()
    {
        startRun = true;
    }

    public void ButSpeedUp()
    {
        speedRun += 0.1f;
    }

    public void ButCollectCoin()
    {
        pagePlay.UpdateCoin(1);
    }

    public void ButAddStar()
    {
        if (Modules.runEndless) return;
        pagePlay.UpdateStar();
    }

    public void ButFailed()
    {
        startRun = false;
        if (Modules.runEndless)
            pagePlay.ShowCompleteEndless();
        else
            pagePlay.ShowSaveLevel();
    }

    public void ButComplete()
    {
        if (Modules.runEndless) return;
        startRun = false;
        pagePlay.ShowCompleteLevel();
    }

    public void RebornGame()
    {
        startRun = true;
    }

    private void Update()
    {
        if (startRun)
        {
            distaneRun += speedRun;
            pagePlay.UpdateTimePlay(Time.deltaTime);
            if (Modules.runEndless)
            {
                pagePlay.UpdateMetter(Mathf.RoundToInt(distaneRun));
            }
            else
            {
                pagePlay.UpdateProcess(speedRun);
                if (pagePlay.numProcess >= 100) ButComplete();
            }
        }

      
    }
}
