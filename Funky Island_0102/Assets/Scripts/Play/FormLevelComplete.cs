﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FormLevelComplete : MonoBehaviour {
    public GameObject containStar, containGet;
    public Text textTime;
    public GameObject containAd, bannerNative;
   
    //run star effect
    private int maxStar = 3;
    private int countStar = 0;

    public void CallStart()
    {
        if ((Modules.levelRun == 3 || Modules.levelRun == 10|| Modules.levelRun == 15) &&!Modules.rateApp) DataManager.Instance.Rate = true;
        SetEventButton();
       
        PagePlay.Instance.SetBanerNative();
 
        foreach (Transform tran in containStar.transform)
            tran.GetChild(0).gameObject.SetActive(false);
        foreach (Transform tran in containGet.transform)
            tran.GetChild(2).GetComponent<Text>().text = "x0";
        countStar = 0;
        maxStar = PagePlay.Instance.numStarNow;
        if (maxStar < 3) maxStar += 1;

        Achievements.CheckTaskCollectStar(maxStar);
        StartCoroutine(PlayStarEffect(0.5f));

        //save list level
        if (Modules.dataLevel[Modules.levelRun - 1] < maxStar)
            Modules.dataLevel[Modules.levelRun - 1] = maxStar;
        if (Modules.levelRun < Modules.dataLevel.Count)
        {
            if (Modules.dataLevel[Modules.levelRun] == -1)
            {
                Modules.dataLevel[Modules.levelRun] = 0;
                Achievements.CheckTaskPassLevel(1);
            }
        }
        SaveLoad.SaveLevelData(Modules.dataLevel);
        //Google Achivement
        if (Modules.levelRun == 1)
            GoogleSeviceManager.Instance.UnlockAchivement(GoogleID.Achivement_Newbie_CompleteLv1);
        if (Modules.levelRun == 20)
            GoogleSeviceManager.Instance.UnlockAchivement(GoogleID.Achivement_Adventure_PassLv20);
        GoogleSeviceManager.Instance.IncrementAchivemnet(GoogleID.Achivement_iCollectBanana_1000Ingame, PagePlay.Instance.numCoin);
        CheckStart();

        
    }

    private void CheckStart()
    {
        int tmp=0;
        for(int i = 0; i < Modules.dataLevel.Count; i++)
        {
            if(Modules.dataLevel[i]>0)
                tmp += Modules.dataLevel[i];
        }
        if (tmp > PlayerPrefs.GetInt(SaveID.TotalStarAchiment))
        {
            int i = tmp - PlayerPrefs.GetInt(SaveID.TotalStarAchiment);
            GoogleSeviceManager.Instance.IncrementAchivemnet(GoogleID.Achivement_iCollecStarInGame_30, i);
            PlayerPrefs.SetInt(SaveID.TotalStarAchiment, tmp);
        }
    }

    private IEnumerator ShowInterAd(float timeStep)
    {
        yield return new WaitForSecondsRealtime(timeStep);
        Modules.CheckShowInterAd();
    }

    private IEnumerator PlayStarEffect(float timeStep)
    {
        yield return new WaitForSecondsRealtime(timeStep);
        if (countStar < maxStar)
        {
            containStar.transform.GetChild(countStar).GetChild(0).gameObject.SetActive(true);
            containStar.transform.GetChild(countStar).GetChild(0).GetComponent<Animator>().Play("StarPlay");
            countStar++;
            StartCoroutine(PlayStarEffect(0.5f));
        }
        else
        {
            if (Modules.listTextFont.Count > 0)
            {
                textTime.font = Modules.listFont[Modules.listTextFont[32].indexFont];
                textTime.text = Modules.listTextFont[32].textValue.Replace("[?]", PagePlay.Instance.timePlay.ToString("F2"));
            }
            //thuc hien tinh toan cac so luong get
            int bonus = maxStar * 20;
            int totalCoinNow = PagePlay.Instance.numCoin + bonus;
            containGet.transform.GetChild(0).GetChild(2).GetComponent<Text>().text = "x" + PagePlay.Instance.numCoin.ToString();
            containGet.transform.GetChild(1).GetChild(2).GetComponent<Text>().text = "x" + bonus.ToString();
            containGet.transform.GetChild(2).GetChild(2).GetComponent<Text>().text = "x" + totalCoinNow.ToString();

            //thuc hien save
            Modules.totalCoin += totalCoinNow;
            SaveLoad.SaveTotalCoin();
            FirebaseManager.Instance.LogEventEarnBanana("CompleteGame");
        }
        
    }

    [Header("Button")]
    public UIButton btnReplay;
    public UIButton btnChapter;
    public UIButton btnHome;
    public void SetEventButton()
    {
        btnReplay.SetUpEvent(ButReplayClick);
        btnChapter.SetUpEvent(ButChapterClick);
        btnHome.SetUpEvent(ButHomeClick);

    }

    private void ButReplayClick()
    {
        SoundController.Instance.PlayFX();
        LoadingSceneManager.LoadingScene(SCENE_TYPE.Loading);
        DataManager.Instance.ShowAdsInter();

    }

    public void ButChapterClick()
    {
        SoundController.Instance.PlayFX();
        LoadingSceneManager.LoadingScene(SCENE_TYPE.Chapter);
        DataManager.Instance.ShowAdsInter();

    }

    private void ButHomeClick()
    {
        SoundController.Instance.PlayFX();
        LoadingSceneManager.LoadingScene(SCENE_TYPE.Menu);
        DataManager.Instance.ShowAdsInter();
    }
}
