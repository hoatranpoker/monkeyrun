﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PagePlay : MonoBehaviour {

    #region Config
    public static PagePlay Instance;
    public bool TutorialSence = false;
    public GameObject boxPause, boxSaveMe, boxBuyCoin;
    public GameObject formCompleteLevel, formFailedLevel, formCompleteEndless, bannerNative;
    public GameObject containCoin, containStar, containProcess, containMetter, containSuft, containTimer;
    public GameObject testControll;

    [Header("UI Button")]
    public UIButton btnPause;
   
    //xu ly cac bien chuyen dong trong tro choi
    [HideInInspector]
    public int numCoin = 0;
    [HideInInspector]
    public int numMetter = 0;
    [HideInInspector]
    public int numStarNow = 0;
    [HideInInspector]
    public float numProcess = 0;
    [HideInInspector]
    public float timePlay = 0;
    [HideInInspector]
    public int numReborn = 0;
    [HideInInspector]
    public float speedBonusAni = 0;
    public Text textPause, textSound, textMusic, textQuit, textResume, textReplaySave;
    public Text textSaveMe, textFree;

    #endregion

    #region Main

    private void Awake()
    {
        if (Instance == null) Instance = this;  
        containTimer.transform.localScale = Vector3.zero;
        if (TutorialSence) return;
        AdMobManager.Instance.ObjectNative = bannerNative;
        AdMobManager.Instance.RequestNativeAd();
        //if (!AdMobManager.Instance.VideoAdLoaded() && Modules.HaveInternet())
        //    AdMobManager.Instance.loadRewardVideo();
    }

    private void Start()
    {
        Mask.SetActive(true);
        //button Set event
        btnPause.SetUpEvent(ButPauseClick);
        btnSkipTutorial.SetUpEvent(ActionSkipTutorial);
        btnNextImg.SetUpEvent(ActionNextTutorial);
        BtnStartGame.onClick.AddListener(ButStartGame);
      
        CallStart(); //reset data
        SetModePlay(); //level or EndLess
        
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (formCompleteEndless.activeSelf) formCompleteEndless.GetComponent<FormEndlessComplete>().ButHomeClick();
            else if (formCompleteLevel.activeSelf) formCompleteLevel.GetComponent<FormLevelComplete>().ButChapterClick();
            else if (formFailedLevel.activeSelf) formFailedLevel.GetComponent<FormLevelFailed>().ButChapterClick();
            else if (boxSaveMe.activeSelf) boxSaveMe.GetComponent<BoxSaveMe>().ButCloseClick();
            else if (boxPause.activeSelf) boxPause.GetComponent<BoxPause>().ButResumeClick();
            else ButPauseClick();
        }
    }

    private void OnApplicationPause(bool pauseStatus)
    {

        if (formCompleteEndless.activeSelf) return;
        else if (formCompleteLevel.activeSelf) return;
        else if (formFailedLevel.activeSelf) return;
        else if (boxSaveMe.activeSelf) return;
        else if (boxPause.activeSelf) return;
        ButPauseClick();
    }

    #endregion

    #region Action CallStart

    private void CallStart()
    {
        PlayerController.Instance.playerStates = PlayerStates.Idle;
        DataManager.Instance.numberTutorial = 0;
        DataManager.Instance.OnShowTutorial = true;
        Modules.pauseTimeSave = false;
        boxPause.SetActive(false);
        formCompleteLevel.SetActive(false);
        formFailedLevel.SetActive(false);
        formCompleteEndless.SetActive(false);
        UpdateTimePlay(0);
        UpdateCoin(0);
        UpdateItem();
        numReborn = 0;
    }

    private void SetModePlay()
    {
        if (TutorialSence)
        {
            //Time.timeScale = 0;
            PanelTutorial.SetActive(true);
            containCoin.SetActive(true);
            containMetter.SetActive(false);
            
            UpdateProcess(0);
            foreach (Transform tran in containStar.transform)
            {
                tran.GetChild(0).gameObject.SetActive(false);
            }
        }
        else
        {
            if (Modules.runEndless)
            {
                FirebaseManager.Instance.LevelStart(0, Log_Name_Event.Endless_Start);
                StartPlay();
                containStar.SetActive(false);
                containProcess.SetActive(false);
                containMetter.SetActive(true);
                UpdateMetter(0);
                speedBonusAni = 0;
            }
            else
            {
                ShowInforMap();
                containStar.SetActive(true);
                containProcess.SetActive(true);
                containMetter.SetActive(false);
                UpdateProcess(0);
                foreach (Transform tran in containStar.transform)
                {
                    tran.GetChild(0).gameObject.SetActive(false);
                }
                speedBonusAni = Modules.levelRun * 0.05f;
                FirebaseManager.Instance.LevelStart(Modules.levelRun);
            }
        }
    }

    public BGPrallax bGPrallax;
    private void StartPlay()
    {
        InfoMap.SetActive(false);
        PlayerController.Instance.playerStates = PlayerStates.Run;
        if (bGPrallax) bGPrallax.speed = 0;
    }

    #endregion
    
    #region Item Use & Minishop

    public Button[] useItems;
    public void UpdateItem()
    {
        foreach(var item in useItems)
        {
            TypeItemBuy type = item.GetComponent<ButtonUseItem>().itemType;
            switch (type)
            {
                case TypeItemBuy.Magnet:
                    item.GetComponent<ButtonUseItem>().btnAdd.SetActive(Modules.totalMagnet== 0);
                    item.transform.GetChild(1).GetComponent<Text>().text = "x" + Modules.totalMagnet.ToString("#,##0");
                    SaveLoad.SaveTotalMagnet();
                    break;

                case TypeItemBuy.Protect:
                    //item.gameObject.SetActive(Modules.totalProtect > 0);
                    item.GetComponent<ButtonUseItem>().btnAdd.SetActive(Modules.totalProtect == 0);
                    item.transform.GetChild(1).GetComponent<Text>().text = "x" + Modules.totalProtect.ToString("#,##0");
                    SaveLoad.SaveTotalProtect();
                    break;

                case TypeItemBuy.X2Banana:
                    //item.gameObject.SetActive(Modules.total2XBanana > 0);
                    item.GetComponent<ButtonUseItem>().btnAdd.SetActive(Modules.total2XBanana == 0);
                    item.transform.GetChild(1).GetComponent<Text>().text = "x" + Modules.total2XBanana.ToString("#,##0");
                    SaveLoad.SaveTotal2XBanana();
                    break;

                case TypeItemBuy.Reborn:
                    //item.gameObject.SetActive(Modules.totalReborn > 0);
                    item.GetComponent<ButtonUseItem>().btnAdd.SetActive(Modules.totalReborn == 0);
                    item.transform.GetChild(1).GetComponent<Text>().text = "x" + Modules.totalReborn.ToString("#,##0");
                    SaveLoad.SaveTotalReborn();
                    break;
            }
        }
    }

    public MiniShopItem Minishop;
    public void ShowMiniShopItem(TypeItemBuy type)
    {
        Time.timeScale = 0;
        Minishop.gameObject.SetActive(true);
        Minishop.CallStart(type);
    }

    #endregion

    #region Timer Item
    private Coroutine coroutineTimer;
    public Sprite[] transformSprites;
    public void UpdateTimer(float totalTime, ItemType itemType = ItemType.None)
    {
        if (coroutineTimer != null)
            StopCoroutine(coroutineTimer);
        else
        {
            if (totalTime > 0f)
            {
                iTween.Stop(containTimer);
                iTween.ScaleTo(containTimer, iTween.Hash("scale", Vector3.one, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
            }
        }
        Image icon = containTimer.transform.GetChild(0).GetChild(1).GetComponent<Image>();
        switch (itemType)
        {
            case ItemType.Rhino:
                icon.sprite = transformSprites[0];
                break;

            case ItemType.Skateboard:
                icon.sprite = transformSprites[1];
                break;

            case ItemType.FlyingCarpet:
                icon.sprite = transformSprites[2];
                break;

            case ItemType.Apple:
                icon.sprite = transformSprites[3];
                break;

            case ItemType.Bird:
                icon.sprite = transformSprites[4];
                break;
        }

        coroutineTimer = StartCoroutine(WaitTimer(totalTime, totalTime));
        containTimer.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);
        //if (itemType == ItemType.Bird)
        //{
        //    Image process = containTimer.transform.GetChild(0).GetChild(0).GetComponent<Image>();
        //    if (process) process.fillAmount = 0f;
        //    containTimer.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
        //}
        //else
        //{
        //    coroutineTimer = StartCoroutine(WaitTimer(totalTime, totalTime));
        //    containTimer.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);
        //}
    }

    private IEnumerator WaitTimer(float totalTime, float totalDuration)
    {
        //Text timer = containTimer.transform.GetChild(0).GetChild(0).GetComponent<Text>();
        Image process = containTimer.transform.GetChild(0).GetChild(0).GetComponent<Image>();
        if (process) process.fillAmount = 1f;
        while (totalTime > 0)
        {
            //timer.text = totalTime.ToString("f1");
            process.fillAmount = totalTime / totalDuration;
            totalTime -= 0.1f;
            yield return new WaitForSeconds(0.1f);
        }
        iTween.Stop(containTimer);
        iTween.ScaleTo(containTimer, iTween.Hash("scale", Vector3.zero, "time", 0.5f, "easetype", iTween.EaseType.easeInOutSine));
    }

    #endregion

    #region Update parameter

    public void UpdateCoin(int coinAdd)
    {
        numCoin += coinAdd;
        containCoin.transform.GetChild(1).GetComponent<Text>().text = "x" + numCoin.ToString();
    }

    public void UpdateStar()
    {
        if (numStarNow < 3)
        {
            containStar.transform.GetChild(numStarNow).GetChild(0).gameObject.SetActive(true);
            containStar.transform.GetChild(numStarNow).GetChild(0).GetComponent<Animator>().Play("StarPlay");
            numStarNow++;
        }
    }

    public void UpdateMetter(int metAdd)
    {
        //numMetter += metAdd;
        numMetter = metAdd;
        containMetter.transform.GetChild(0).GetComponent<Text>().text = numMetter.ToString("#,##0") + "m";
    }

    public void UpdateProcess(float proAdd)
    {
        //numProcess += proAdd;
        numProcess = proAdd;
        if (numProcess > 100) numProcess = 100;
        int intNum = Mathf.FloorToInt(numProcess);
        string head = intNum.ToString();
        string tail = (numProcess - intNum).ToString();
        if (tail.Length > 2) tail = tail.Remove(0, 2);
        if (tail.Length > 2) tail = tail.Substring(0, 2);
        if (tail.Length < 2) tail += "0";
        containProcess.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = head;
        containProcess.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = "." + tail;
    }

    public void UpdateTimePlay(float timeAdd)
    {
        //timePlay += timeAdd;
        timePlay = timeAdd;
    }

    public void UpdateSuft(float process)
    {
        containSuft.transform.GetChild(1).GetComponent<Image>().fillAmount = process;
        containSuft.transform.GetChild(3).GetChild(0).GetComponent<Image>().enabled = process >= 1f;
    }

    #endregion

    #region Action Button
    private void ButStartGame()
    {
        SoundController.Instance.PlayFX();
        Time.timeScale = 1;
        StartPlay();
    }

    public void ButPauseClick()
    {
        Time.timeScale = 0;
        boxPause.SetActive(true);
        boxPause.GetComponent<BoxPause>().CallStart();
        SoundController.Instance.PlayFX();
    }

    public void RebornPlayer()
    {
        PlayerController.Instance.UsingRevival();
    }

    //Show When Monkey Die
    public void ShowSaveLevel()
    {
        Time.timeScale = 0;
        FirebaseManager.Instance.LogEventDieBelowMeter((int)PlayerController.Instance.transform.position.x);
        boxSaveMe.SetActive(true);
        boxSaveMe.GetComponent<BoxSaveMe>().CallStart();
        
    }

    [Header("Lucky spin Popup")]
    public GameObject PopupSpin;

    public void ShowPopupLuckySpin()
    {
        Time.timeScale = 0.00001f;
        SoundsInGame.Instance.SoundChangeSpinShow();
        PopupSpin.SetActive(true);
        PopupSpin.GetComponent<Animator>().Play("ShowSpinlucky");
        if (PlayerController.Instance.soundComplete)
            AudioSource.PlayClipAtPoint(PlayerController.Instance.soundComplete, Camera.main.transform.position, Modules.soundSetting);
    }

    public void ShowCompleteEndless()
    {
        if (PlayerController.Instance.soundComplete)
            AudioSource.PlayClipAtPoint(PlayerController.Instance.soundComplete, Camera.main.transform.position, Modules.soundSetting);
        Time.timeScale = 0;
        formCompleteEndless.SetActive(true);
        //AdMobManager.Instance.RequestNativeAd(bannerNative);
        formCompleteEndless.GetComponent<FormEndlessComplete>().CallStart();
        FirebaseManager.Instance.LevelEnd(0,Log_Name_Event.Endless_Complete);
    }

    public void ShowCompleteLevel()
    {
        if (PlayerController.Instance.soundComplete)
            AudioSource.PlayClipAtPoint(PlayerController.Instance.soundComplete, Camera.main.transform.position, Modules.soundSetting);
        Time.timeScale = 0;
        formCompleteLevel.SetActive(true);
        //AdMobManager.Instance.RequestNativeAd(bannerNative);
        formCompleteLevel.GetComponent<FormLevelComplete>().CallStart();
        FirebaseManager.Instance.LevelEnd(Modules.levelRun);
    }

    public void ShowFailedLevel()
    {
        if (PlayerController.Instance.soundFail)
            AudioSource.PlayClipAtPoint(PlayerController.Instance.soundFail, Camera.main.transform.position, Modules.soundSetting);
        Time.timeScale = 0;
        formFailedLevel.SetActive(true);
        //AdMobManager.Instance.RequestNativeAd(bannerNative);
        formFailedLevel.GetComponent<FormLevelFailed>().CallStart();
        FirebaseManager.Instance.LevelEnd(Modules.levelRun,Log_Name_Event.Level_Failed);
    }

    #endregion

    #region Tutorial
    [Header("Tutorial Image")]
    public GameObject PanelTutorial;
    public List<Sprite> listTutorial;
    public Image imgTutorial;
    public UIButton btnSkipTutorial;
    public UIButton btnNextImg;
    private int numberImg = 0;

    [Header("Tutorial Info")]
    public GameObject UITutInfo;
    public Text txtTutInfo;
    public GameObject maskTutorial;
    public GameObject Tut_DragDown;
    public GameObject Tut_DragSuft;
    public GameObject Tut_PressScreen;  //pos bottomright
    public GameObject Tut_PressScreen2; //pos topleft

    public void SetTutInfo(string _text)
    {
        txtTutInfo.text = _text;
    }

    public void ShowTutorial(TUTORIAL_TYPE _type)
    {
        Time.timeScale = 0;
        UITutInfo.SetActive(true);

        switch (_type)
        {
            case TUTORIAL_TYPE.Jump: //jump
                Tut_PressScreen.SetActive(true);
                Tut_DragDown.SetActive(false);
                Tut_DragSuft.SetActive(false);

                break;
            case TUTORIAL_TYPE.Down: //down
                Tut_DragDown.SetActive(true);
                Tut_PressScreen.SetActive(false);
                Tut_DragSuft.SetActive(false);
                break;
            case TUTORIAL_TYPE.Suft: //Suft
                Tut_DragSuft.SetActive(true);
                Tut_PressScreen2.SetActive(true);
                Tut_PressScreen.SetActive(false);
                Tut_DragDown.SetActive(false);
                break;
            case TUTORIAL_TYPE.JumpAndHold:
                Tut_PressScreen.SetActive(true);
                Tut_DragDown.SetActive(false);
                Tut_DragSuft.SetActive(false);
                break;
            case TUTORIAL_TYPE.UseProtect:
                Modules.totalCoin += 200;
                SaveLoad.SaveTotalCoin();
                Tut_DragSuft.SetActive(false);
                Tut_PressScreen2.SetActive(false);
                Tut_PressScreen.SetActive(false);
                Tut_Protect_ShowHand();
                break;
            case TUTORIAL_TYPE.UseMagnet:
                for (int i = 0; i < HandClickProtect.Length; i++)
                {
                    HandClickProtect[i].SetActive(false);
                }
                Modules.totalCoin += 200;
                SaveLoad.SaveTotalCoin();
                Tut_Magnet_ShowHand();
                break;
        }
    }

    public GameObject[] HandClickProtect;
    [HideInInspector]
    public int Number_ProtectStep = 0;
    public void Tut_Protect_ShowHand()
    {
        for (int i = 0; i < HandClickProtect.Length; i++)
        {
            HandClickProtect[i].SetActive(false);
        }
        HandClickProtect[Number_ProtectStep].SetActive(true);
        Number_ProtectStep++;
    }

    public GameObject[] HandClickMagnet;
    [HideInInspector]
    public int Number_Magnet = 0;
    public void Tut_Magnet_ShowHand()
    {
        for (int i = 0; i < HandClickMagnet.Length; i++)
        {
            HandClickMagnet[i].SetActive(false);
        }
        HandClickMagnet[Number_Magnet].SetActive(true);
        Number_Magnet++;
    }

    public void CLoseTutorial()
    {
        Time.timeScale = 1;
        UITutInfo.SetActive(false);
    }

    [Header("Popup Finish Tutorial")]
    public GameObject PopupFinishTutorial;
    public UIButton btnTusNext;
    public Text txtColectBanana, txtTotalBanana;

    private void SetText()
    {
        Modules.totalCoin += numCoin;
        SaveLoad.SaveTotalCoin();
        FirebaseManager.Instance.LogEventEarnBanana("FailedGame");
        txtColectBanana.text = "x" + Instance.numCoin.ToString();
        txtTotalBanana.text = "x" + Instance.numCoin.ToString();
        
    }

    public void ShowPopupFinishTutorial()
    {
        SetText();
        if (PlayerController.Instance.soundComplete)
            AudioSource.PlayClipAtPoint(PlayerController.Instance.soundComplete, Camera.main.transform.position, Modules.soundSetting);
        PopupFinishTutorial.SetActive(true);
        FirebaseManager.Instance.LogEventFinishTutorial();
        btnTusNext.SetUpEvent(() => { SoundController.Instance.PlayFX();
            DataManager.Instance.chapterTutorial = true;
            LoadingSceneManager.LoadingScene(SCENE_TYPE.Chapter); 
        });
    }

    private void ActionNextTutorial()
    {
        numberImg++;
        if (numberImg >= listTutorial.Count) {
            numberImg = 0;
            ActionSkipTutorial();
            return;
        }
        imgTutorial.sprite = listTutorial[numberImg];
        SoundController.Instance.PlayFX();
    }

    private void ActionSkipTutorial()
    {
        SoundController.Instance.PlayFX();
        PanelTutorial.SetActive(false);
        StartPlay();
       
    }

    #endregion

    #region Action Singleton 

    [Header("Admod baner")]
    public AdModBanner NativeAdmod;
    public void SetBanerNative()
    {
        if (!Modules.HaveInternet()) return;
        NativeAdmod.gameObject.SetActive(true);
        AdMobManager.Instance.SetBanerNative(NativeAdmod);
    }

    [Header("Show mask")]
    [SerializeField] private GameObject Mask;
    private void CloseMask()
    {
        Mask.GetComponent<Animator>().Play("Open", 0, 0f);
    }

    [Header("Infor Map Start Game")]
    public GameObject InfoMap;
    public Button BtnStartGame;

    private void ShowInforMap()
    {
        //Time.timeScale = 0;
        InfoMap.SetActive(true);
        if (Modules.listTextFont.Count > 0)
        {
            Modules.indexTaskLevel = -1;
            for (int i = 0; i < Modules.listTask.Count; i++)
            {
                if (Modules.listTask[i].level == Modules.levelRun)
                {
                    Modules.indexTaskLevel = i;
                    break;
                }
            }
            GameObject contain = InfoMap.GetComponent<BoxLevelInfo>().containText;
            GameObject temp = contain.transform.GetChild(0).gameObject;
            temp.SetActive(false);
            for (int i = 1; i < contain.transform.childCount; i++) Destroy(contain.transform.GetChild(i).gameObject);
            if (Modules.indexTaskLevel != -1)
            {
                temp.SetActive(true);
                for (int i = 0; i < Modules.listTask[Modules.indexTaskLevel].taskDetail.Count; i++)
                {
                    GameObject newTask = Instantiate(temp, contain.transform);
                    TaskLevelDetail typeLevel = Modules.listTask[Modules.indexTaskLevel].taskDetail[i];
                    Modules.SetTextTaskLevel(typeLevel.typeTask, newTask.transform.GetChild(1).GetComponent<Text>(), typeLevel.nunberNeed);
                }
                temp.SetActive(false);
            }
        }
    }

    #region  Popup Level 60 FinishGame
    [Header("Popup Level 60 FinishGame")]
    public GameObject PopupLevel60;
    public UIButton btnClosePopupLevel60;
    public void ShowCongrateLevel72()
    {
        PopupLevel60.SetActive(true);
        btnClosePopupLevel60.SetUpEvent(() =>
        {
            SoundController.Instance.PlayFX();
            LoadingSceneManager.LoadingScene(SCENE_TYPE.Menu);
        });
    }
    #endregion

    #endregion

    #region Misson On game

    //UPDATE CAC NHIEM VU TRONG LEVEL
    private int numCollectBanana = 0;
    private bool doneCollectBanana = false;
    private int startIndexCollectBanana = 0;
    public void CheckCollectBanana(int number)
    {
        if (Modules.runEndless || Modules.indexTaskLevel == -1 || doneCollectBanana) return;
        TaskLevel taskNow = Modules.listTask[Modules.indexTaskLevel];
        for (int i = startIndexCollectBanana; i < taskNow.taskDetail.Count; i++)
        {
            if (taskNow.taskDetail[i].typeTask == TypeTaskLevel.CollectBanana)
            {
                numCollectBanana += number;
                if (numCollectBanana >= taskNow.taskDetail[i].nunberNeed)
                {
                    doneCollectBanana = true;
                    UpdateStar();
                    //kiem tra xem o task detail sau co giong task detail nay hay khong, neu co thi reset lai
                    if (i < taskNow.taskDetail.Count - 1)
                    {
                        for (int j = i + 1; j < taskNow.taskDetail.Count; j++)
                        {
                            if (taskNow.taskDetail[j].typeTask == TypeTaskLevel.CollectBanana)
                            {
                                numCollectBanana = 0;
                                doneCollectBanana = false;
                                startIndexCollectBanana++;
                                break;
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    private int numTeeter = 0;
    private bool doneTeeter = false;
    private int startIndexTeeter = 0;
    public void CheckTeeter(int number)
    {
        if (Modules.runEndless || Modules.indexTaskLevel == -1 || doneTeeter) return;
        TaskLevel taskNow = Modules.listTask[Modules.indexTaskLevel];
        for (int i = startIndexTeeter; i < taskNow.taskDetail.Count; i++)
        {
            if (taskNow.taskDetail[i].typeTask == TypeTaskLevel.Teeter)
            {
                numTeeter += number;
                if (numTeeter >= taskNow.taskDetail[i].nunberNeed)
                {
                    doneTeeter = true;
                    UpdateStar();
                    //kiem tra xem o task detail sau co giong task detail nay hay khong, neu co thi reset lai
                    if (i < taskNow.taskDetail.Count - 1)
                    {
                        for (int j = i + 1; j < taskNow.taskDetail.Count; j++)
                        {
                            if (taskNow.taskDetail[j].typeTask == TypeTaskLevel.CollectBanana)
                            {
                                numTeeter = 0;
                                doneTeeter = false;
                                startIndexTeeter++;
                                break;
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    private int numKillNative = 0;
    private bool doneKillNative = false;
    private int startIndexKillNative = 0;
    public void CheckKillNative(int number)
    {
        if (Modules.runEndless || Modules.indexTaskLevel == -1 || doneKillNative) return;
        TaskLevel taskNow = Modules.listTask[Modules.indexTaskLevel];
        for (int i = startIndexKillNative; i < taskNow.taskDetail.Count; i++)
        {
            if (taskNow.taskDetail[i].typeTask == TypeTaskLevel.KillNative)
            {
                numKillNative += number;
                if (numKillNative >= taskNow.taskDetail[i].nunberNeed)
                {
                    doneKillNative = true;
                    UpdateStar();
                    //kiem tra xem o task detail sau co giong task detail nay hay khong, neu co thi reset lai
                    if (i < taskNow.taskDetail.Count - 1)
                    {
                        for (int j = i + 1; j < taskNow.taskDetail.Count; j++)
                        {
                            if (taskNow.taskDetail[j].typeTask == TypeTaskLevel.CollectBanana)
                            {
                                numKillNative = 0;
                                doneKillNative = false;
                                startIndexKillNative++;
                                break;
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    private int numBreakCobweb = 0;
    private bool doneBreakCobweb = false;
    private int startIndexBreakCobweb = 0;
    public void CheckBreakCobweb(int number)
    {
        if (Modules.runEndless || Modules.indexTaskLevel == -1 || doneBreakCobweb) return;
        TaskLevel taskNow = Modules.listTask[Modules.indexTaskLevel];
        for (int i = startIndexBreakCobweb; i < taskNow.taskDetail.Count; i++)
        {
            if (taskNow.taskDetail[i].typeTask == TypeTaskLevel.BreakCobweb)
            {
                numBreakCobweb += number;
                if (numBreakCobweb >= taskNow.taskDetail[i].nunberNeed)
                {
                    doneBreakCobweb = true;
                    UpdateStar();
                    //kiem tra xem o task detail sau co giong task detail nay hay khong, neu co thi reset lai
                    if (i < taskNow.taskDetail.Count - 1)
                    {
                        for (int j = i + 1; j < taskNow.taskDetail.Count; j++)
                        {
                            if (taskNow.taskDetail[j].typeTask == TypeTaskLevel.CollectBanana)
                            {
                                numBreakCobweb = 0;
                                doneBreakCobweb = false;
                                startIndexBreakCobweb++;
                                break;
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    private int numBreakBarrel = 0;
    private bool doneBreakBarrel = false;
    private int startIndexBreakBarrel = 0;
    public void CheckBreakBarrel(int number)
    {
        if (Modules.runEndless || Modules.indexTaskLevel == -1 || doneBreakBarrel) return;
        TaskLevel taskNow = Modules.listTask[Modules.indexTaskLevel];
        for (int i = startIndexBreakBarrel; i < taskNow.taskDetail.Count; i++)
        {
            if (taskNow.taskDetail[i].typeTask == TypeTaskLevel.BreakBarrel)
            {
                numBreakBarrel += number;
                if (numBreakBarrel >= taskNow.taskDetail[i].nunberNeed)
                {
                    doneBreakBarrel = true;
                    UpdateStar();
                    //kiem tra xem o task detail sau co giong task detail nay hay khong, neu co thi reset lai
                    if (i < taskNow.taskDetail.Count - 1)
                    {
                        for (int j = i + 1; j < taskNow.taskDetail.Count; j++)
                        {
                            if (taskNow.taskDetail[j].typeTask == TypeTaskLevel.CollectBanana)
                            {
                                numBreakBarrel = 0;
                                doneBreakBarrel = false;
                                startIndexBreakBarrel++;
                                break;
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    private int numJumpMushrooms = 0;
    private bool doneJumpMushrooms = false;
    private int startIndexJumpMushrooms = 0;
    public void CheckJumpMushrooms(int number)
    {
        if (Modules.runEndless || Modules.indexTaskLevel == -1 || doneJumpMushrooms) return;
        TaskLevel taskNow = Modules.listTask[Modules.indexTaskLevel];
        for (int i = startIndexJumpMushrooms; i < taskNow.taskDetail.Count; i++)
        {
            if (taskNow.taskDetail[i].typeTask == TypeTaskLevel.JumpMushrooms)
            {
                numJumpMushrooms += number;
                if (numJumpMushrooms >= taskNow.taskDetail[i].nunberNeed)
                {
                    doneJumpMushrooms = true;
                    UpdateStar();
                    //kiem tra xem o task detail sau co giong task detail nay hay khong, neu co thi reset lai
                    if (i < taskNow.taskDetail.Count - 1)
                    {
                        for (int j = i + 1; j < taskNow.taskDetail.Count; j++)
                        {
                            if (taskNow.taskDetail[j].typeTask == TypeTaskLevel.CollectBanana)
                            {
                                numJumpMushrooms = 0;
                                doneJumpMushrooms = false;
                                startIndexJumpMushrooms++;
                                break;
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    private int numSurfing = 0;
    private bool doneSurfing = false;
    private int startIndexSurfing = 0;
    public void CheckSurfing(int number)
    {
        if (Modules.runEndless || Modules.indexTaskLevel == -1 || doneSurfing) return;
        TaskLevel taskNow = Modules.listTask[Modules.indexTaskLevel];
        for (int i = startIndexSurfing; i < taskNow.taskDetail.Count; i++)
        {
            if (taskNow.taskDetail[i].typeTask == TypeTaskLevel.Surfing)
            {
                numSurfing += number;
                if (numSurfing >= taskNow.taskDetail[i].nunberNeed)
                {
                    doneSurfing = true;
                    UpdateStar();
                    //kiem tra xem o task detail sau co giong task detail nay hay khong, neu co thi reset lai
                    if (i < taskNow.taskDetail.Count - 1)
                    {
                        for (int j = i + 1; j < taskNow.taskDetail.Count; j++)
                        {
                            if (taskNow.taskDetail[j].typeTask == TypeTaskLevel.CollectBanana)
                            {
                                numSurfing = 0;
                                doneSurfing = false;
                                startIndexSurfing++;
                                break;
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    private int numPassQuagmire = 0;
    private bool donePassQuagmire = false;
    private int startIndexPassQuagmire = 0;
    public void CheckPassQuagmire(int number)
    {
        if (Modules.runEndless || Modules.indexTaskLevel == -1 || donePassQuagmire) return;
        TaskLevel taskNow = Modules.listTask[Modules.indexTaskLevel];
        for (int i = startIndexPassQuagmire; i < taskNow.taskDetail.Count; i++)
        {
            if (taskNow.taskDetail[i].typeTask == TypeTaskLevel.PassQuagmire)
            {
                numPassQuagmire += number;
                if (numPassQuagmire >= taskNow.taskDetail[i].nunberNeed)
                {
                    donePassQuagmire = true;
                    UpdateStar();
                    //kiem tra xem o task detail sau co giong task detail nay hay khong, neu co thi reset lai
                    if (i < taskNow.taskDetail.Count - 1)
                    {
                        for (int j = i + 1; j < taskNow.taskDetail.Count; j++)
                        {
                            if (taskNow.taskDetail[j].typeTask == TypeTaskLevel.CollectBanana)
                            {
                                numPassQuagmire = 0;
                                donePassQuagmire = false;
                                startIndexPassQuagmire++;
                                break;
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    private int numGetProtect = 0;
    private bool doneGetProtect = false;
    private int startIndexGetProtect = 0;
    public void CheckGetProtect(int number)
    {
        if (Modules.runEndless || Modules.indexTaskLevel == -1 || doneGetProtect) return;
        TaskLevel taskNow = Modules.listTask[Modules.indexTaskLevel];
        for (int i = startIndexGetProtect; i < taskNow.taskDetail.Count; i++)
        {
            if (taskNow.taskDetail[i].typeTask == TypeTaskLevel.GetProtect)
            {
                numGetProtect += number;
                if (numGetProtect >= taskNow.taskDetail[i].nunberNeed)
                {
                    doneGetProtect = true;
                    UpdateStar();
                    //kiem tra xem o task detail sau co giong task detail nay hay khong, neu co thi reset lai
                    if (i < taskNow.taskDetail.Count - 1)
                    {
                        for (int j = i + 1; j < taskNow.taskDetail.Count; j++)
                        {
                            if (taskNow.taskDetail[j].typeTask == TypeTaskLevel.CollectBanana)
                            {
                                numGetProtect = 0;
                                doneGetProtect = false;
                                startIndexGetProtect++;
                                break;
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    private int numGetMagnet = 0;
    private bool doneGetMagnet = false;
    private int startIndexGetMagnet = 0;
    public void CheckGetMagnet(int number)
    {
        if (Modules.runEndless || Modules.indexTaskLevel == -1 || doneGetMagnet) return;
        TaskLevel taskNow = Modules.listTask[Modules.indexTaskLevel];
        for (int i = startIndexGetMagnet; i < taskNow.taskDetail.Count; i++)
        {
            if (taskNow.taskDetail[i].typeTask == TypeTaskLevel.GetMagnet)
            {
                numGetMagnet += number;
                if (numGetMagnet >= taskNow.taskDetail[i].nunberNeed)
                {
                    doneGetMagnet = true;
                    UpdateStar();
                    //kiem tra xem o task detail sau co giong task detail nay hay khong, neu co thi reset lai
                    if (i < taskNow.taskDetail.Count - 1)
                    {
                        for (int j = i + 1; j < taskNow.taskDetail.Count; j++)
                        {
                            if (taskNow.taskDetail[j].typeTask == TypeTaskLevel.CollectBanana)
                            {
                                numGetMagnet = 0;
                                doneGetMagnet = false;
                                startIndexGetMagnet++;
                                break;
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    #endregion

}
