﻿
using UnityEngine;
using UnityEngine.UI;

public class AdModBanner : MonoBehaviour {

    public RawImage ImageTextures;
    public RawImage LogoTexture;
    public RawImage IconTexture;
    public Text ActionText;
    public Text HeadlineText;
}
