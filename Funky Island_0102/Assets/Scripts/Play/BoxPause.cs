﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BoxPause : MonoBehaviour {

    //xu ly audio state
    public GameObject butSound, butMusic;
    public Text textSound, textMusic;
    public Image imgSave;
    public Sprite sprSave, sprRestart;

    public void CallStart()
    {
        UpdateStateAudio();
        SetButton();
    }

    public void SetButton()
    {
        if (Modules.runEndless)
        {
            imgSave.sprite = sprSave;
        }
        else
        {
            imgSave.sprite = sprRestart;
        }
    }

    public void ButResumeClick()
    {
        Time.timeScale = 1;
        Camera.main.GetComponent<AudioListener>().enabled = true;
        gameObject.SetActive(false);
        SoundController.Instance.PlayFX();
    }

    public void ButQuitClick()
    {
        Time.timeScale = 1;
        if (Modules.runEndless)
            LoadingSceneManager.LoadingScene(SCENE_TYPE.Menu);
        else
            LoadingSceneManager.LoadingScene(SCENE_TYPE.Chapter);
        SoundController.Instance.PlayFX();
    }

    public void ButReplaySaveClick()
    {
        SoundController.Instance.PlayFX();
        Time.timeScale = 1;
        if (PagePlay.Instance.TutorialSence)
        {
            LoadingSceneManager.LoadingScene(SCENE_TYPE.Tutorial);
            return;
        }

        if (Modules.runEndless)
        {
            //save roi ra menu
            //xu ly save o day
            SaveLoadData.SaveData("EndlessMetter", PlayerController.Instance.pagePlay.numMetter.ToString(), true);
            ButQuitClick();
        }
        else
        {
            //SceneManager.LoadScene("Loading");
            LoadingSceneManager.LoadingScene(SCENE_TYPE.Loading);
        }
        
    }

    public void UpdateStateAudio()
    {
        if (Modules.soundSetting == 0)
        {
            butSound.transform.GetChild(0).gameObject.SetActive(false);
            if (Modules.listTextFont.Count > 0)
            {
                textSound.font = Modules.listFont[Modules.listTextFont[21].indexFont];
                textSound.text = Modules.listTextFont[21].textValue;
            }
        }
        else
        {
            butSound.transform.GetChild(0).gameObject.SetActive(true);
            if (Modules.listTextFont.Count > 0)
            {
                textSound.font = Modules.listFont[Modules.listTextFont[20].indexFont];
                textSound.text = Modules.listTextFont[20].textValue;
            }
        }
        if (Modules.musicSetting == 0)
        {
            butMusic.transform.GetChild(0).gameObject.SetActive(false);
            if (Modules.listTextFont.Count > 0)
            {
                textMusic.font = Modules.listFont[Modules.listTextFont[21].indexFont];
                textMusic.text = Modules.listTextFont[21].textValue;
            }
        }
        else
        {
            butMusic.transform.GetChild(0).gameObject.SetActive(true);
            if (Modules.listTextFont.Count > 0)
            {
                textMusic.font = Modules.listFont[Modules.listTextFont[20].indexFont];
                textMusic.text = Modules.listTextFont[20].textValue;
            }
        }
        SoundController.Instance.UpdateSetting();
    }

    public void ButSoundClick()
    {
        Modules.soundSetting = 1 - Modules.soundSetting;
        UpdateStateAudio();
        SaveLoad.SaveAudioSetting();
        SoundController.Instance.PlayFX();
    }

    public void ButMusicClick()
    {
        Modules.musicSetting = 1 - Modules.musicSetting;
        UpdateStateAudio();
        SaveLoad.SaveAudioSetting();
        SoundController.Instance.PlayFX();
    }
}
