﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FormEndlessComplete : MonoBehaviour {

    public GameObject containHigh, containGet;
    public Text textScore, textTime,txtHighScore,txtCoinColecct;
    public GameObject containAd, bannerNative;

    public void CallStart()
    {
        //bannerNative.SetActive(Modules.haveNative);
        //bannerNative.transform.SetParent(containAd.transform);
        PagePlay.Instance.SetBanerNative();
        //StartCoroutine(ShowInterAd(0.5f));

        //xu ly du lieu hien thi
        txtHighScore.text = "HighScore : " + Modules.highScore + "m";
        int scoreNow = PagePlay.Instance.numMetter;
        if (scoreNow > Modules.highScore)
        {
            Modules.highScore = scoreNow;
            SaveLoad.SaveHighScore();
            containHigh.SetActive(true);
        }
        else {
            containHigh.SetActive(false); }
        int totalCoinNow = PagePlay.Instance.numCoin;
        Modules.totalCoin += totalCoinNow;
        SaveLoad.SaveTotalCoin();
        FirebaseManager.Instance.LogEventEarnBanana("EnlessGame");
        if (Modules.listTextFont.Count > 0)
        {
            textScore.font = Modules.listFont[Modules.listTextFont[36].indexFont];
            textScore.text = Modules.listTextFont[36].textValue.Replace("[?]", scoreNow.ToString());
            textTime.font = Modules.listFont[Modules.listTextFont[32].indexFont];
            textTime.text = Modules.listTextFont[32].textValue.Replace("[?]", PagePlay.Instance.timePlay.ToString("F2"));
            txtCoinColecct.text = "x" + totalCoinNow.ToString();
        }

        GoogleSeviceManager.Instance.PostScoreLeaderbroad(Modules.highScore);
        if(scoreNow>2000)
            GoogleSeviceManager.Instance.UnlockAchivement(GoogleID.Achivement_Marathon_Run2000m);
        GoogleSeviceManager.Instance.IncrementAchivemnet(GoogleID.Achivement_iCollectBanana_1000Ingame, PagePlay.Instance.numCoin);

    }

    private IEnumerator ShowInterAd(float timeStep)
    {
        yield return new WaitForSecondsRealtime(timeStep);
        Modules.CheckShowInterAd();
    }

    public void ButReplayClick()
    {
        SoundController.Instance.PlayFX();
        LoadingSceneManager.LoadingScene(SCENE_TYPE.Loading);
        DataManager.Instance.ShowAdsInter();
    }

    public void ButHomeClick()
    {
        SoundController.Instance.PlayFX();
        LoadingSceneManager.LoadingScene(SCENE_TYPE.Menu);
        DataManager.Instance.ShowAdsInter();
    }
}
