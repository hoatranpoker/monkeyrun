﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonUseItem : MonoBehaviour {
    public TypeItemBuy itemType;
    public GameObject btnAdd;

    public void OnClick()
    {
        switch (itemType)
        {
            case TypeItemBuy.X2Banana:
                if (Modules.total2XBanana > 0)
                {
                    Modules.total2XBanana--;
                    SaveLoad.SaveTotal2XBanana();
                    PlayerController.Instance.UsingX2Banana();
                }
                else PagePlay.Instance.ShowMiniShopItem(itemType);
              
                break;

            case TypeItemBuy.Magnet:
                if (Modules.totalMagnet > 0)
                {
                    Modules.totalMagnet--;
                    SaveLoad.SaveTotalMagnet();
                    PlayerController.Instance.UsingMagnet();
                    if (PagePlay.Instance.TutorialSence) MapTutorial.Instance.tutMagnet = true;
                }
                else PagePlay.Instance.ShowMiniShopItem(itemType);

                break;

            case TypeItemBuy.Protect:
                if (Modules.totalProtect > 0)
                {
                    Modules.totalProtect--;
                    SaveLoad.SaveTotalProtect();
                    PlayerController.Instance.UsingProtect();
                    if (PagePlay.Instance.TutorialSence) MapTutorial.Instance.tutProtect = true;
                }
                else PagePlay.Instance.ShowMiniShopItem(itemType);

                break;

            case TypeItemBuy.Reborn:
                Modules.totalReborn--;
                SaveLoad.SaveTotalReborn();
                PlayerController.Instance.UsingRevival();
                break;
        }
        PlayerController.Instance.pagePlay.UpdateItem();
        SoundController.Instance.PlayFX();
    }
}
