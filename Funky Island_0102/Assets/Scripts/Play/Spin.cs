﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class Spin : MonoBehaviour {

    public GameObject SpinObject; //object xoay vong
    public UIButton btnSpin;
    public UIButton btnSpinByBanana;
    public UIButton btnWatchAds;
    public UIButton btnExit;

    public AudioClip audioSpin;
    public AudioClip audioShowItem;

    public List<float> listSpin;

    [Header("PopupShowItem")]
    public GameObject PopupShowItem;
    public GameObject objectCountDown;
    public Image imgItemBonus;
    public Text txtDescItemBonus;

    public List<Sprite> listSprite;
    private string txtDesc="x1";

    public bool mainMenu=false;
    private bool bSpining=false;
    private int NumberSpin
    {
        get { return PlayerPrefs.GetInt(SaveID.NumberSpin); }
        set { PlayerPrefs.SetInt(SaveID.NumberSpin, value); }
    }

    private void Start()
    {
        btnSpin.SetUpEvent(SpinFree);
        btnWatchAds.SetUpEvent(ActionWatchAds);
        btnExit.SetUpEvent(ActionExit);
        if (mainMenu)
        {
            CheckCountDown();
            if (btnSpinByBanana != null) btnSpinByBanana.SetUpEvent(SpinByBanana);
        }
    }

    #region button Action
    public void SetButtonAdsReward()
    {
        btnSpin.gameObject.SetActive(true);
        btnWatchAds.gameObject.SetActive(false);
    }

    public void ActionWatchAds()
    {
        if (bSpining) return;
        if (mainMenu)
            Modules.typeRewardVideo = type_RewardAds.spinMenu;
        else
            Modules.typeRewardVideo = type_RewardAds.spinMainGame;
        AdMobManager.Instance.showRewardVideo();
        SoundController.Instance.PlayFX();
    }

    private void ActionExit()
    {
        SoundController.Instance.PlayFX();
        gameObject.SetActive(false);

        if (mainMenu) return;
        switch (DataManager.Instance.game_Finish)
        {
            case GAME_FINISH.Victory:
                PagePlay.Instance.ShowCompleteLevel();
                break;
            case GAME_FINISH.Lose:
                PagePlay.Instance.ShowFailedLevel();
                break;
            case GAME_FINISH.RunEnless:
                PagePlay.Instance.ShowCompleteEndless();
                break;
        }
    }

    private void SpinByBanana()
    {
        SoundController.Instance.PlayFX();
        if (bSpining) return;
        if (Modules.totalCoin < NumberSpin)
        {
            PageMenu.Instance.ShowPopup_buyCoin();
            return;
        }
        Modules.totalCoin -= NumberSpin;
        SaveLoad.SaveTotalCoin();
        FirebaseManager.Instance.LogEventSpentBanana("Spin_"+NumberSpin);
        GoSpin();
        SetTextButtonSpinWithBanana();
      
    }

    private void SetTextButtonSpinWithBanana()
    {

        NumberSpin += 100;
        if (NumberSpin >= 300) NumberSpin = 300;
        btnSpinByBanana.GetComponentInChildren<Text>().text = NumberSpin.ToString();

    }

    private void SpinFree()
    {
        SoundController.Instance.PlayFX();
        if (bSpining) return;
        if (mainMenu)
        {
            PlayerPrefs.SetString(SaveID.FreeSpin, SaveID.False);
            PageMenu.Instance.warningspin.SetActive(false);
            NumberSpin = 100;
            btnSpinByBanana.GetComponentInChildren<Text>().text = NumberSpin.ToString();
            if(timeSpinCountdown <= 0)
                CountDowntStart(3600);
        }
        else
            Time.timeScale = 0.0001f;
        GoSpin();
       
    }

    public void GoSpin()
    {
        bSpining = true;
        int i = UnityEngine.Random.Range(0, listSpin.Count - 1);
        if (i == 6) i = UnityEngine.Random.Range(0, i);

        ItemBonus(i);
        SpinObject.transform.DOLocalRotate(Vector3.zero, 0).
           OnComplete(() => {
               if (audioSpin)
                   AudioSource.PlayClipAtPoint(audioSpin, Camera.main.transform.position, Modules.soundSetting);
               SpinObject.transform.DOLocalRotate(new Vector3(0, 0, listSpin[i] - 360 * 4), Time.timeScale * 4.7f, RotateMode.LocalAxisAdd).
           OnComplete(() => { SetItemBonus(listSprite[i], txtDesc); });
           });
        DOVirtual.DelayedCall(Time.timeScale * 6f, () => {
            btnSpin.gameObject.SetActive(false);
            if (mainMenu)
            {
                objectCountDown.SetActive(true);
                btnSpinByBanana.gameObject.SetActive(true);
            }
            else
                btnWatchAds.gameObject.SetActive(true);
        });
    }

    #endregion

    #region Spin countdown on MenuMain

    private void CheckCountDown()
    {
        if (timespinCD() > 0)
        {
            objectCountDown.SetActive(true);
            btnSpin.gameObject.SetActive(false);
            btnSpinByBanana.gameObject.SetActive(true);
        }
        else
        {
            btnSpin.gameObject.SetActive(true);
            btnSpinByBanana.gameObject.SetActive(false);
            NumberSpin = 100;
            objectCountDown.SetActive(false);
        }

        btnSpinByBanana.GetComponentInChildren<Text>().text = NumberSpin.ToString();
       
    }

    private int timespinCD()
    {
        double second = Convert.ToDateTime(DataManager.Instance.timeNow()).Subtract(Convert.ToDateTime(PlayerPrefs.GetString(SaveID.TimeQuitSpin))).TotalSeconds;
        int tmp = PlayerPrefs.GetInt(SaveID.TimeSpinCD) - (int)second;
        if (PlayerPrefs.GetString(SaveID.FreeSpin) == SaveID.True) tmp = 0;
        return tmp;
    }

    private void OnDisable()
    {
        if (mainMenu)
        {
            PlayerPrefs.SetString(SaveID.TimeQuitSpin, DataManager.Instance.timeNow().ToString());
            PlayerPrefs.SetInt(SaveID.TimeSpinCD, (int)timeSpinCountdown);
        }
    }

    private void OnEnable()
    {
        if (mainMenu)
        {
            StopAllCoroutines();
            CountDowntStart(timespinCD());
        }

    }

    private void CountDowntStart(float _time)
    {
        if (!mainMenu) return;
        timeSpinCountdown = _time;
        fakeCd = StartCoroutine(fakeCountDownSpin());
    }

    public float timeSpinCountdown = 0;
    private Coroutine fakeCd;
    private IEnumerator fakeCountDownSpin()
    {
        while (timeSpinCountdown > 0)
        {
            objectCountDown.SetActive(true);
            timeSpinCountdown--;
            if (PageMenu.Instance.txtTimeCountdownSpin)
                UpdateLevelTimer_Ms(timeSpinCountdown);
            yield return new WaitForSeconds(Time.timeScale*1f);
            if (timeSpinCountdown <= 0)
            {
                PlayerPrefs.SetString(SaveID.FreeSpin, SaveID.True);
                btnSpin.gameObject.SetActive(true);
                NumberSpin = 100;
            }
                
        }
    }

    private void UpdateLevelTimer_Ms(float totalSeconds)
    {
        int seconds = Mathf.RoundToInt(totalSeconds % 60f);
        int minutes = Mathf.FloorToInt(totalSeconds / 60f);
        string formatedSeconds = seconds.ToString();
        if (seconds == 60)
        {
            seconds = 0;
            minutes += 1;
        }
        PageMenu.Instance.txtTimeCountdownSpin.text = minutes.ToString("00") + "m" + seconds.ToString("00") + "s";
    }

    #endregion

    #region Reward
    private void SetItemBonus(Sprite spr, string textDesc)
    {
        bSpining = false;
        imgItemBonus.sprite = spr;
        txtDescItemBonus.text = textDesc;
        PopupShowItem.SetActive(true);
        if (audioShowItem)
            AudioSource.PlayClipAtPoint(audioShowItem, Camera.main.transform.position, Modules.soundSetting);
    }

    private void ItemBonus(int i)
    {
        switch (i)
        {
            case 0:
                //x2bana
                txtDesc = "x1";
                Modules.total2XBanana++;
                SaveLoad.SaveTotal2XBanana();
                break;
            case 1:
                txtDesc = "x1";
                Modules.totalMagnet++;
                SaveLoad.SaveTotalMagnet();
                //namcham
                break;
            case 2:
                if (mainMenu)
                {
                    txtDesc = "x300";
                    DelayCallGetCoins(300);
                }
                else
                {
                    txtDesc = "x200";
                    DelayCallGetCoins(200);
                }
                //gold200
                break;
            case 3:
                txtDesc = "";
                //mat luot
                break;
            case 4:
                txtDesc = "x1";
                Modules.totalProtect++;
                SaveLoad.SaveTotalProtect();
                //khien
                break;
            case 5:
                txtDesc = "x1";
                Modules.totalReborn++;
                SaveLoad.SaveTotalReborn();
                //reborn
                break;
            case 6:
                if (!mainMenu)
                {
                    txtDesc = "x500";
                    DelayCallGetCoins(500);
                }
                else
                {
                    txtDesc = "x1000";
                    DelayCallGetCoins(1000);
                }

                //500gold
                break;
            case 7:
                txtDesc = "";
                //mat luot
                break;
        }
    }

    private void DelayCallGetCoins(int _value)
    {
        DOVirtual.DelayedCall(Time.timeScale * 5f, () =>
        {
            Modules.totalCoin += _value;
            SaveLoad.SaveTotalCoin();
            FirebaseManager.Instance.LogEventEarnBanana("Spin");
        });
    }
    #endregion
}
