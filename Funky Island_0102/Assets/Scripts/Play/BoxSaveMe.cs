﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxSaveMe : MonoBehaviour {

    public Image process;
    public Text textTime;
    public int coinSave = 500;
    public float timeCooldown = 20;
    public Image myImageBut;
    public Sprite imgEnable, imgDisable;
    private float countTime = 0;
    private bool playTimeCool = false;
    [Header("Button")]
    [SerializeField] private UIButton btnBuyCoins;
    [SerializeField] private UIButton btnReporn;
    [SerializeField] private UIButton btnWacthAds;
    [SerializeField] private UIButton btnExit;

    public void CallStart()
    {
        if (Modules.totalReborn > 0)
        {
            btnBuyCoins.gameObject.SetActive(false);
            btnReporn.gameObject.SetActive(true);
            btnReporn.transform.GetChild(0).GetComponent<Text>().text = "x" + Modules.totalReborn;
        }
        else
        {
            btnBuyCoins.gameObject.SetActive(true);
            btnReporn.gameObject.SetActive(false);
        }
        btnBuyCoins.SetUpEvent(ButSaveCoin);
        btnWacthAds.SetUpEvent(ButSaveAd);
        btnReporn.SetUpEvent(ActionReporn);
        btnExit.SetUpEvent(ButCloseClick);

        countTime = timeCooldown;
        process.fillAmount = 1;
        textTime.text = Mathf.RoundToInt(timeCooldown).ToString();
        PlayCooldown();
        if (fakeUpdate != null) StopCoroutine(fakeUpdate);
        fakeUpdate = StartCoroutine(RunFakeUpdate());
    }

    private void PlayCooldown()
    {
        playTimeCool = true;
        countTime = 0;
        if (coroutineRunEffectProcess != null) StopCoroutine(coroutineRunEffectProcess);
        coroutineRunEffectProcess = StartCoroutine(RunEffectProcess());
    }

    Coroutine fakeUpdate;
    private IEnumerator RunFakeUpdate()
    {
        while (true)
        {
            float timeStep = 0.02f;
            if (myImageBut != null)
            {
                if (AdMobManager.Instance.VideoAdLoaded())
                {
                    myImageBut.sprite = imgEnable;
                }
                else
                {
                    myImageBut.sprite = imgDisable;
                }
            }
            yield return new WaitForSecondsRealtime(timeStep);
        }
    }

    Coroutine coroutineRunEffectProcess;
    private IEnumerator RunEffectProcess()
    {
        while (playTimeCool)
        {
            float timeStep = 0.03f;
            yield return new WaitForSecondsRealtime(timeStep);
            if (!Modules.pauseTimeSave)
            {
                countTime += timeStep;
                float percent = countTime / timeCooldown;
                process.fillAmount = 1 - percent;
                textTime.text = Mathf.RoundToInt(timeCooldown - countTime).ToString();
                if (percent >= 1)
                    GameOver();
            }
        }
        yield break;
    }

    private void ActionReporn()
    {
        gameObject.SetActive(false);
        SoundController.Instance.PlayFX();
        Modules.totalReborn--;    
        SaveLoad.SaveTotalReborn();
        Time.timeScale = 1;
        PagePlay.Instance.RebornPlayer();
        FirebaseManager.Instance.LogEventContinue("Use Reborn Item");
    }

    public void ButSaveCoin()
    {
        if (coinSave > Modules.totalCoin)
        {
            PagePlay.Instance.boxBuyCoin.SetActive(true);
            Modules.pauseTimeSave = true;
            return;
        }
        Modules.totalCoin -= coinSave;
        SaveLoad.SaveTotalCoin();
        PagePlay.Instance.RebornPlayer();
        gameObject.SetActive(false);
        Time.timeScale = 1;
        SoundController.Instance.PlayFX();

        FirebaseManager.Instance.LogEventSpentBanana("Reborn_MainGame");
        FirebaseManager.Instance.LogEventContinue("Use_Reborn_with_500_coins");
    }

    public void ButSaveAd()
    {
        SoundController.Instance.PlayFX();
        Modules.typeRewardVideo = type_RewardAds.Reborn;
        AdMobManager.Instance.showRewardVideo();
    }

    private void GameOver()
    {
        PlayerController.Instance.gameObject.SetActive(false);
        Time.timeScale = 0.0001f;
        playTimeCool = false;
        int tmp = Random.Range(0,3);
        Debug.LogError("spin: " + tmp);
        gameObject.SetActive(false);
        if (Modules.runEndless)
        {
            DataManager.Instance.game_Finish = GAME_FINISH.RunEnless;
            if (tmp == 0)
            {
                PagePlay.Instance.ShowPopupLuckySpin();
            }
            else
            {
                DataManager.Instance.ShowAdsInter();
                PagePlay.Instance.ShowCompleteEndless();
            } 
        }
        else
        {
            DataManager.Instance.game_Finish = GAME_FINISH.Lose;
            if (tmp == 0){
                PagePlay.Instance.ShowPopupLuckySpin();
            }
            else
            {
                DataManager.Instance.ShowAdsInter();
                PagePlay.Instance.ShowFailedLevel();   
            }
            
        }
        
    }

    public void ButCloseClick()
    {
        GameOver(); 
        SoundController.Instance.PlayFX();
    }
}
