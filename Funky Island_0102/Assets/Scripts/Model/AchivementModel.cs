﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "My Assets/AchivementModel")]
[System.Serializable]
public class AchivementModel : ScriptableObject
{
    public List<TaskDetail> taskDetails = new List<TaskDetail>();
    public List<LevelMonkey> listLevel = new List<LevelMonkey>();
}