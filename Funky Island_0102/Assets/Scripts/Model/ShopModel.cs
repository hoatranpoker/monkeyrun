﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "My Assets/Shop Model")]
[System.Serializable]
public class ShopModel : ScriptableObject
{
    public List<Shop_Item> shop_Items;
    public List<Shop_Upgrade> shop_Upgrades;
    public List<Shop_Pack> shop_Packs;
    public List<Shop_Special> shop_Specials;
}


[System.Serializable]
public class Shop_Item
{
    public string name;
    public Sprite image;
    public TypeItemBuy type;  //loai item
    public string Desc;  // mo ta 
    public int price;   //gia item
    public int having; // so luong dang co
}

public enum TypeItemBuy //4 loai
{
    Magnet,
    X2Banana,
    Protect,
    Reborn,
    Banana
}

[System.Serializable]
public class Shop_Upgrade
{
    public string name;
    public Sprite sprItem;
    public TypeItemUpgrade type;
    public string Desc;
    public int levelCurent;
    public Level_Upgrade[] level_upgrade;

}

[System.Serializable]
public class Level_Upgrade
{
    public int costItem; //gia upgrade item
    public int effectItem; //thoi gian item su dung
 
}

public enum TypeItemUpgrade // 6 loai
{
    Magnet,
    X2Banana,
    Protect,
    Rhino,
    Carpet,
    Skateboard,
    //Bird
}

[System.Serializable]
public class Shop_Pack
{
    public string name;
    public Sprite sprPack;
    public TypeNormalPack type;
    public float pricePack;   //gia goi pack
    public float ValueClaim; //gia tri banana
    public List<BonusItem> List_bonusItem;
}

[System.Serializable]
public class BonusItem
{
    public TypeItemBuy type;
    public int number;
}

public enum TypeNormalPack  //7 loai
{
    Mini,
    Small,
    Medium,
    Large,
    ExtraLarge,
    Huge,
    ExtraHuge
}

[System.Serializable]
public class Shop_Special
{
    public string name;
    public TypeSpecialPack type;
    public string Desc;
    public float priceSpecial;
}

public enum TypeSpecialPack  //7 loai
{
    LuckyCharm,
    GoldenBanana,
    RemoveAds,
    MegaSale,
    WaterBounce,
    AirBounce
}