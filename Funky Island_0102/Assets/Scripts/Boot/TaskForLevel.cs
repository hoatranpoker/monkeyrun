﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskForLevel : MonoBehaviour {

    public List<TaskLevel> listTask = new List<TaskLevel>();
	void Start () {
        Modules.listTask = listTask;
    }
}
[System.Serializable]
public class TaskLevel
{
    public int level;
    public List<TaskLevelDetail> taskDetail;
    public TaskLevel(int levelInput, List<TaskLevelDetail> taskDetail)
    {
        this.level = levelInput;
        this.taskDetail = taskDetail;
    }
}
[System.Serializable]
public class TaskLevelDetail
{
    public TypeTaskLevel typeTask;
    public int nunberNeed;
    public TaskLevelDetail(TypeTaskLevel typeInput, int numberInput)
    {
        this.typeTask = typeInput;
        this.nunberNeed = numberInput;
    }
}
public enum TypeTaskLevel
{
    CollectBanana,
    Teeter,
    KillNative,
    BreakCobweb,
    BreakBarrel,
    JumpMushrooms,
    Surfing,
    PassQuagmire,
    GetProtect,
    GetMagnet
}