
gift_icon.png
size: 1024,256
format: RGBA8888
filter: Linear,Linear
repeat: none
gift_body1
  rotate: true
  xy: 487, 17
  size: 112, 68
  orig: 112, 68
  offset: 0, 0
  index: -1
gift_body2
  rotate: false
  xy: 487, 131
  size: 107, 88
  orig: 107, 88
  offset: 0, 0
  index: -1
gift_e
  rotate: false
  xy: 268, 2
  size: 217, 217
  orig: 217, 217
  offset: 0, 0
  index: -1
gift_ef
  rotate: true
  xy: 2, 4
  size: 215, 264
  orig: 215, 264
  offset: 0, 0
  index: -1
